<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
  Notes
  =====

  L'usage  voudrait   que  les   ouvrages  signés  par   des  anonymes
  apparaissent avant tous les autres. Mais c'est agaçant à écrire.

  Il aurait été bien de segmenter en plus de templates XSL plutôt que de
  faire un doublon item, sub-item,  mais on perd alors les informations
  quant aux nodes suivants.
-->

<!-- Paramètres de transformation ===================================== -->
<xsl:param name="path" select="'../'" />
<!--
  Type de transformation:
  dynamic => PHP
  static => HTML (sans accessibilité)
-->
<xsl:param name="target" select="'dynamic'" />

<!-- Templates externes appelés via leur nom ========================== -->

<!-- Template du pied de page -->
<xsl:include href="text-footer.xsl" />

<!-- Template de la page intégrale ==================================== -->

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="cit">«&#160;<xsl:apply-templates />&#160;»</xsl:template>
<xsl:template match="cit/cit">“<xsl:apply-templates />”</xsl:template>

<xsl:template name="author">
  <xsl:param name="anon" />
  <xsl:param name="index" />
  <xsl:param name="fname" />
  <xsl:param name="lname" />

  <xsl:choose>
    <xsl:when test="$anon = 'yes'">
      <xsl:text>Anonyme, </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:choose>
	<xsl:when test="$index and $index != ''">
	  <xsl:element name="a">
	    <xsl:attribute name="href">idx/names.php#<xsl:value-of select="$index" /></xsl:attribute>
	    <xsl:attribute name="class">idxref</xsl:attribute>
	    <xsl:choose>
	      <xsl:when test="$fname">
		<xsl:element name="span">
		  <xsl:attribute name="class">author</xsl:attribute>
		  <xsl:value-of select="$lname" />
		</xsl:element>
		<xsl:text>, </xsl:text>
		<xsl:value-of select="$fname" />
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:element name="span">
		  <xsl:attribute name="class">author</xsl:attribute>
		  <xsl:value-of select="$lname" />
		</xsl:element>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:element>
	  <xsl:text>, </xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:choose>
	    <xsl:when test="$fname">
	      <xsl:element name="span">
		<xsl:attribute name="class">author</xsl:attribute>
		<xsl:value-of select="$lname" />
	      </xsl:element>
	      <xsl:text>, </xsl:text>
	      <xsl:value-of select="$fname" />
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:element name="span">
		<xsl:attribute name="class">author</xsl:attribute>
		<xsl:value-of select="$lname" />
	      </xsl:element>
	    </xsl:otherwise>
	  </xsl:choose>
	  <xsl:text>, </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="editor">
  <xsl:param name="anon" />
  <xsl:param name="coll" />
  <xsl:param name="fname" />
  <xsl:param name="lname" />

  <xsl:choose>
    <xsl:when test="$anon = 'yes'"><xsl:text>Anonyme, </xsl:text></xsl:when>
    <xsl:otherwise>
      <xsl:choose>
	<xsl:when test="$fname and $fname != ''">
	  <xsl:element name="span">
	    <xsl:attribute name="class">author</xsl:attribute>
	    <xsl:value-of select="$lname" />
	  </xsl:element>
	  <xsl:text>, </xsl:text>
	  <xsl:value-of select="$fname" />
	  <xsl:text> (éd.), </xsl:text>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:choose>
	    <xsl:when test="$coll = 'yes'">
	      <xsl:value-of select="$lname" />
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:element name="span">
		<xsl:attribute name="class">author</xsl:attribute>
		<xsl:value-of select="$lname" />
	      </xsl:element>
	    </xsl:otherwise>
	  </xsl:choose>
	  <xsl:text> (éd.), </xsl:text>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="/">
  <html>
    <head>
      <meta charset="utf-8" />
      <title>Bibliographie</title>
      <xsl:element name="link">
	<xsl:attribute name="rel">alternate</xsl:attribute>
	<xsl:attribute name="type">application/rdf+xml</xsl:attribute>
	<xsl:attribute name="href">http://www.gnu.org/licenses/fdl-1.3.rdf</xsl:attribute>
      </xsl:element>
      <xsl:element name="link">
	<xsl:attribute name="rel">stylesheet</xsl:attribute>
	<xsl:attribute name="type">text/css</xsl:attribute>
	<xsl:attribute name="href"><xsl:value-of select="$path" />inc/master.css</xsl:attribute>
      </xsl:element>
      <xsl:element name="link">
	<xsl:attribute name="rel">icon</xsl:attribute>
	<xsl:attribute name="type">image/gif</xsl:attribute>
	<xsl:attribute name="href"><xsl:value-of select="$path" />inc/favicon.ico</xsl:attribute>
      </xsl:element>
      <xsl:if test="$target = 'dynamic'">
      <xsl:text disable-output-escaping="yes">
      &lt;?php require "inc/loadaccess_up.php"; ?&gt;
      </xsl:text>
      </xsl:if>
      <style>
        .bib-item {}
        .bib-sub-item {margin-left: 3%;}
        .author {text-transform: uppercase;}
      </style>
    </head>
    <body class="biblio">
      <header>
	<a id="back"></a>
        <h1 class="left">La FAQ Anarchiste (francophone)</h1>
      </header>

      <h1>Bibliographie</h1>

      <p class="first">
      Cette  bibliographie   liste  tous  les  livres   cités  dans  la
      FAQ.  Cependant,  des détails  pour  certains  livres manquent.  Ces
      informations seront  ajoutés au fil  du temps. Quelques  livres sont
      listés  en  plusieurs  éditions&#160;:  cela est  au  processus  de
      révision de  la FAQ pour  la publication, en utilisant  les versions
      les plus récentes des livres  cités. Une fois la révision achevé,
      les vieux détails seront supprimés.
      </p>

      <p class="nocount">La bibliographie est scindée en quatre parties&#160;:</p>

      <ol>
        <li>
          <a href="#anthologies">Anthologies d’auteurs anarchistes</a>
        </li>
        <li>
          <a href="#anarchist-works">Travaux anarchistes</a>
        </li>
        <li>
          <a href="#works-on-anarchism">
            Travaux sur l’anarchisme (dont son histoire) et les anarchistes
          </a>
        </li>
        <li>
          <a href="#non-anarchist-works">Travaux d’auteurs non-anarchistes</a>
        </li>
      </ol>

      <xsl:apply-templates />

    <!-- Retour en haut de page -->

    <div class="goback">
      <p><a href="#back">Retour en haut de la page</a></p>
    </div>

    <!-- Licence -->

    <xsl:call-template name="text-footer" />
    </body>
  </html>
</xsl:template>

<xsl:template match="bibliography/group">
  <xsl:element name="div">
    <xsl:element name="a"><xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute></xsl:element><xsl:text>
    </xsl:text>
    <xsl:element name="h2"><xsl:value-of select="@name" /></xsl:element>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="item">
  <xsl:element name="p">
    <xsl:attribute name="class">bib-item</xsl:attribute>

    <xsl:if test="author">
      <xsl:variable name="anonymous"><xsl:choose><xsl:when test="author/@anon = 'yes'">yes</xsl:when><xsl:otherwise>no</xsl:otherwise></xsl:choose></xsl:variable>

      <xsl:call-template name="author">
	<xsl:with-param name="anon"><xsl:value-of select="$anonymous" /></xsl:with-param>
	<xsl:with-param name="fname"><xsl:value-of select="author/fname" /></xsl:with-param>
	<xsl:with-param name="lname"><xsl:value-of select="author/lname" /></xsl:with-param>
	<xsl:with-param name="index"><xsl:value-of select="author/@index" /></xsl:with-param>
      </xsl:call-template>
    </xsl:if>

    <xsl:if test="editor">
      <xsl:variable name="anonymous"><xsl:choose><xsl:when test="editor/@anon = 'yes'">yes</xsl:when><xsl:otherwise>no</xsl:otherwise></xsl:choose></xsl:variable>
      <xsl:variable name="collective"><xsl:choose><xsl:when test="editor/@collective = 'yes'">yes</xsl:when><xsl:otherwise>no</xsl:otherwise></xsl:choose></xsl:variable>

      <xsl:call-template name="editor">
	<xsl:with-param name="anon"><xsl:value-of select="$anonymous" /></xsl:with-param>
	<xsl:with-param name="coll"><xsl:value-of select="$collective" /></xsl:with-param>
	<xsl:with-param name="fname"><xsl:value-of select="editor/fname" /></xsl:with-param>
	<xsl:with-param name="lname"><xsl:value-of select="editor/lname" /></xsl:with-param>
      </xsl:call-template>
    </xsl:if>

    <xsl:if test="authors">
      <xsl:for-each select="authors/a">
        <xsl:choose>
          <xsl:when test="./fname">
            <xsl:choose>
              <xsl:when test="./@collective = 'yes'">
                <xsl:value-of select="./lname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:element name="span">
                  <xsl:attribute name="class">author</xsl:attribute>
                  <xsl:value-of select="./lname" />
                </xsl:element>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="./fname" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="./@collective = 'yes'">
                <xsl:value-of select="./lname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:element name="span">
                  <xsl:attribute name="class">author</xsl:attribute>
                  <xsl:value-of select="./lname" />
                </xsl:element>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:choose><xsl:when test="position() = last()"><xsl:text>, </xsl:text></xsl:when><xsl:otherwise><xsl:text>; </xsl:text></xsl:otherwise></xsl:choose>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="editors">
      <xsl:for-each select="editors/e">
        <xsl:choose>
          <xsl:when test="./fname">
            <xsl:choose>
              <xsl:when test="./@collective = 'yes'">
                <xsl:value-of select="./lname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:element name="span">
                  <xsl:attribute name="class">author</xsl:attribute>
                  <xsl:value-of select="./lname" />
                </xsl:element>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="./fname" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="./@collective = 'yes'">
                <xsl:value-of select="./lname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:element name="span">
                  <xsl:attribute name="class">author</xsl:attribute>
                  <xsl:value-of select="./lname" />
                </xsl:element>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:choose>
          <xsl:when test="position() = last()">
            <xsl:text> (éds.), </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>; </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:if>

    <xsl:choose>
      <xsl:when test="@type = 'article'">
        <xsl:text>«&#160;</xsl:text><xsl:value-of select="title" /><xsl:text>&#160;», </xsl:text>
        <xsl:element name="em"><xsl:value-of select="journal" /></xsl:element>
	<xsl:if test="issue or volume or pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
      </xsl:when>
      <xsl:when test="@type = 'inbook'">
        <xsl:text>«&#160;</xsl:text><xsl:value-of select="title" /><xsl:text>&#160;», </xsl:text>
        <xsl:element name="em"><xsl:value-of select="book" /></xsl:element>
	<xsl:if test="issue or volume or pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:element name="em">
          <xsl:value-of select="title" />
        </xsl:element>
        <xsl:if test="commentary">
          <xsl:text>, </xsl:text>
          <xsl:value-of select="commentary" />
        </xsl:if>
	<xsl:if test="pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="issue">
      <xsl:text>n°</xsl:text><xsl:value-of select="issue" />
      <xsl:if test="volume or pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="volume">
      <xsl:text>vol. </xsl:text><xsl:value-of select="volume" />
      <xsl:if test="pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="pub">
      <xsl:value-of select="pub" />
      <xsl:if test="loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="publishers">
      <xsl:for-each select="publishers/p">
	<xsl:value-of select="." /><xsl:if test="position() != last()"> &amp; </xsl:if>
      </xsl:for-each>
      <xsl:if test="loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="loc">
      <xsl:value-of select="loc" />
      <xsl:if test="date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="date">
      <xsl:choose>
        <xsl:when test="date/@unknown = 'yes'">
          date inconnue
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="date" />
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="pages">
      <xsl:text>p. </xsl:text><xsl:value-of select="pages" />
      <xsl:if test="url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="url">
      <xsl:text>disponible sur </xsl:text>
      <xsl:element name="a">
        <xsl:attribute name="href"><xsl:value-of select="url/@href" /></xsl:attribute>
        <xsl:value-of select="url" />
      </xsl:element>
    </xsl:if>

    <xsl:text>.</xsl:text>
  </xsl:element>
  <xsl:if test="items">
    <xsl:apply-templates select="items/sub-item"/>
  </xsl:if>
</xsl:template>

<xsl:template match="sub-item">
  <xsl:element name="p">
    <xsl:attribute name="class">bib-sub-item</xsl:attribute>

    <xsl:if test="author">
      <xsl:variable name="anonymous"><xsl:choose><xsl:when test="author/@anon = 'yes'">yes</xsl:when><xsl:otherwise>no</xsl:otherwise></xsl:choose></xsl:variable>

      <xsl:call-template name="author">
	<xsl:with-param name="anon"><xsl:value-of select="$anonymous" /></xsl:with-param>
	<xsl:with-param name="fname"><xsl:value-of select="author/fname" /></xsl:with-param>
	<xsl:with-param name="lname"><xsl:value-of select="author/lname" /></xsl:with-param>
	<xsl:with-param name="index"><xsl:value-of select="author/@index" /></xsl:with-param>
      </xsl:call-template>
    </xsl:if>

    <xsl:if test="editor">
      <xsl:variable name="anonymous"><xsl:choose><xsl:when test="editor/@anon = 'yes'">yes</xsl:when><xsl:otherwise>no</xsl:otherwise></xsl:choose></xsl:variable>
      <xsl:variable name="collective"><xsl:choose><xsl:when test="editor/@collective = 'yes'">yes</xsl:when><xsl:otherwise>no</xsl:otherwise></xsl:choose></xsl:variable>

      <xsl:call-template name="editor">
	<xsl:with-param name="anon"><xsl:value-of select="$anonymous" /></xsl:with-param>
	<xsl:with-param name="coll"><xsl:value-of select="$collective" /></xsl:with-param>
	<xsl:with-param name="fname"><xsl:value-of select="editor/fname" /></xsl:with-param>
	<xsl:with-param name="lname"><xsl:value-of select="editor/lname" /></xsl:with-param>
      </xsl:call-template>
    </xsl:if>

    <xsl:if test="editors">
      <xsl:for-each select="editors/e">
        <xsl:choose>
          <xsl:when test="./fname">
            <xsl:choose>
              <xsl:when test="./@collective = 'yes'">
                <xsl:value-of select="./lname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:element name="span">
                  <xsl:attribute name="class">author</xsl:attribute>
                  <xsl:value-of select="./lname" />
                </xsl:element>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="./fname" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="./@collective = 'yes'">
                <xsl:value-of select="./lname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:element name="span">
                  <xsl:attribute name="class">author</xsl:attribute>
                  <xsl:value-of select="./lname" />
                </xsl:element>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:choose>
          <xsl:when test="position() = last()">
            <xsl:text> (éds.), </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>; </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:if>

    <xsl:if test="authors">
      <xsl:for-each select="authors/a">
        <xsl:choose>
          <xsl:when test="./fname">
            <xsl:choose>
              <xsl:when test="./@collective = 'yes'">
                <xsl:value-of select="./lname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:element name="span">
                  <xsl:attribute name="class">author</xsl:attribute>
                  <xsl:value-of select="./lname" />
                </xsl:element>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="./fname" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="./@collective = 'yes'">
                <xsl:value-of select="./lname" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:element name="span">
                  <xsl:attribute name="class">author</xsl:attribute>
                  <xsl:value-of select="./lname" />
                </xsl:element>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>

        <xsl:choose>
          <xsl:when test="position() = last()">
            <xsl:text>, </xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>; </xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:if>

    <xsl:choose>
      <xsl:when test="@type = 'article'">
        <xsl:text>«&#160;</xsl:text><xsl:value-of select="title" /><xsl:text>&#160;», </xsl:text>
        <xsl:element name="em"><xsl:value-of select="journal" /></xsl:element>
	<xsl:if test="issue or volume or pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
      </xsl:when>
      <xsl:when test="@type = 'inbook'">
        <xsl:text>«&#160;</xsl:text><xsl:value-of select="title" /><xsl:text>&#160;», </xsl:text>
        <xsl:element name="em"><xsl:value-of select="book" /></xsl:element>
	<xsl:if test="issue or volume or pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:element name="em">
          <xsl:apply-templates select="title" />
        </xsl:element>
        <xsl:if test="commentary">
          <xsl:text>, </xsl:text>
          <xsl:value-of select="commentary" />
        </xsl:if>
	<xsl:if test="pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="issue">
      <xsl:text>n°</xsl:text><xsl:value-of select="issue" />
      <xsl:if test="volume or pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="volume">
      <xsl:text>vol. </xsl:text><xsl:value-of select="volume" />
      <xsl:if test="pub or loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="pub">
      <xsl:value-of select="pub" />
      <xsl:if test="loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="pub">
      <xsl:for-each select="publishers/p">
	<xsl:value-of select="." /><xsl:if test="position() != last()"> &amp; </xsl:if>
      </xsl:for-each>
      <xsl:if test="loc or date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="loc">
      <xsl:value-of select="loc" />
      <xsl:if test="date or pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="date">
      <xsl:choose>
        <xsl:when test="date/@unknown = 'yes'">
          date inconnue
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="date" />
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="pages or url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="pages">
      <xsl:text>p. </xsl:text><xsl:value-of select="pages" />
      <xsl:if test="url"><xsl:text>, </xsl:text></xsl:if>
    </xsl:if>

    <xsl:if test="url">
      <xsl:text>disponible sur </xsl:text>
      <xsl:element name="a">
        <xsl:attribute name="href"><xsl:value-of select="url/@href" /></xsl:attribute>
        <xsl:value-of select="url" />
      </xsl:element>
    </xsl:if>

    <xsl:text>.</xsl:text>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
