<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="br"><xsl:element name="br" /></xsl:template>
<xsl:template match="pre"><xsl:element name="pre"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="i"><xsl:element name="i"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="b"><xsl:element name="b"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="em"><xsl:element name="em"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="sup"><xsl:element name="sup"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="ol"><xsl:element name="ol"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="ul"><xsl:element name="ul"><xsl:apply-templates /></xsl:element></xsl:template>

<xsl:template match="text/div//li"><xsl:element name="li"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="footnotes/system/fn//li"><xsl:element name="li"><xsl:apply-templates /></xsl:element></xsl:template>

<xsl:template match="table"><xsl:element name="table"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="thead"><xsl:element name="thead"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="tbody"><xsl:element name="tbody"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="caption"><xsl:element name="caption"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="tr"><xsl:element name="tr"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="td"><xsl:element name="td"><xsl:apply-templates /></xsl:element></xsl:template>
<xsl:template match="th"><xsl:element name="th"><xsl:apply-templates /></xsl:element></xsl:template>

</xsl:stylesheet>
