<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template name="text-translation-info">
  <xsl:param name="tstate" />
  <xsl:element name="aside">
    <xsl:attribute name="class">
      <xsl:choose>
	<xsl:when test="$tstate = 'not'">translation-warning zero</xsl:when>
	<xsl:when test="$tstate = 'hideous'">translation-warning hideous</xsl:when>
	<xsl:otherwise>translation-warning still</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>

    <xsl:element name="div">
      <xsl:attribute name="class">head</xsl:attribute>
      <xsl:choose>
	<xsl:when test="$tstate = 'hideous'">Attention, la traduction de cette page est entièrement à revoir.</xsl:when>
	<xsl:when test="$tstate = 'almost'">La traduction de cette section n’est pas terminée.</xsl:when>
	<xsl:when test="$tstate = 'incomplete'">La traduction de cette section n’est pas terminée.</xsl:when>
	<xsl:when test="$tstate = 'uncomplete'">La traduction de cette section n’est pas terminée.</xsl:when>
	<xsl:when test="$tstate = 'not'">La traduction de cette section n’est pas (ou très peu) entamée.</xsl:when>
      </xsl:choose>
    </xsl:element>

    <xsl:choose>
      <xsl:when test="$tstate = 'hideous'">
	<xsl:element name="div">
	  Nous tenons à signaler au lecteur  que des passages entiers du texte
	  de cette page sont traduits de façon déplorable.
	</xsl:element>
	<xsl:element name="div">
	  Afin  d’éviter  tout  contre-sens, ou  toute  exaspération,  nous
	  conseillons  au lecteur,  s’il peut  le faire,  de plutôt  lire la
	  version  en  langue originale  (miroir  numéro  1), ou  toute  autre
	  traduction dans une langue qu’il maîtrise.
	</xsl:element>
	<xsl:element name="div">Avec nos excuses.</xsl:element>
      </xsl:when>
      <xsl:when test="$tstate = 'almost'">
	<xsl:element name="div">
	  Il manque encore quelques paragraphes et quelques phrases en français,
	  mais vous pouvez tout de même lire une bonne part de cette section.
	</xsl:element>
      </xsl:when>
      <xsl:when test="$tstate = 'incomplete' or $tstate = 'uncomplete'">
	<xsl:element name="div">
	  Il manque encore quelques sous-sections en français,
	  mais vous pouvez tout de même lire celles qui sont déjà traduites.
	</xsl:element>
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
