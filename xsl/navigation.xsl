<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!-- Template de scénario ============================================= -->

<!-- La section, sous-section déclarée dans le document
crée une espèce de scénario qu’on peut alors employer 
pour remplir la barre de navigation en conséquence -->

<xsl:include href="scenario.xsl" />

<!-- Templates pour formater les éléments de la barre de navigation === -->

<xsl:template name="format-navigation-faq-toc">
  <xsl:param name="url" />
  <xsl:element name="div">
    <xsl:attribute name="class">nav-toc</xsl:attribute>
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:value-of select="$url" />
      </xsl:attribute>
      Index FAQ
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="format-navigation-next-sec">
  <xsl:param name="secUrl" />
  <xsl:param name="secName" />
  <xsl:element name="div">
    <xsl:attribute name="class">nav-next</xsl:attribute>
    Section suivante&#160;:
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:value-of select="$secUrl" />
      </xsl:attribute>
      <xsl:value-of select="$secName" />
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="format-navigation-prev-sec">
  <xsl:param name="secUrl" />
  <xsl:param name="secName" />
  <xsl:element name="div">
    <xsl:attribute name="class">nav-next</xsl:attribute>
    Section précédente&#160;:
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:value-of select="$secUrl" />
      </xsl:attribute>
      <xsl:value-of select="$secName" />
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="format-navigation-sec-toc">
  <xsl:param name="secUrl" />
  <xsl:param name="secName" />
  <xsl:param name="secItem" />
  <xsl:element name="div">
    <xsl:attribute name="class">nav-toc</xsl:attribute>
    Section <xsl:value-of select="$secItem" />&#160;:
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:value-of select="$secUrl" />
      </xsl:attribute>
      Sommaire
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="format-navigation-prev-sub">
  <xsl:param name="subUrl" />
  <xsl:param name="subName" />
  <xsl:element name="div">
    <xsl:attribute name="class">nav-next</xsl:attribute>
    Précédent&#160;:
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:value-of select="$subUrl" />
      </xsl:attribute>
      <xsl:value-of select="$subName" />
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="format-navigation-next-sub">
  <xsl:param name="subUrl" />
  <xsl:param name="subName" />
  <xsl:element name="div">
    <xsl:attribute name="class">nav-next</xsl:attribute>
    Suivant&#160;:
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:value-of select="$subUrl" />
      </xsl:attribute>
      <xsl:value-of select="$subName" />
    </xsl:element>
  </xsl:element>
</xsl:template>

<!-- Templates pour obtenir les éléments de la barre de navigation ==== -->

<xsl:template name="navigation-next-sec">
  <xsl:param name="currentSecName" />
  <xsl:param name="ext" />

  <xsl:variable name="nextSec">
    <xsl:choose>
      <xsl:when test="$currentSecName = 'lecteur'">intro</xsl:when>
      <xsl:when test="$currentSecName = 'intro'">a</xsl:when>
      <xsl:when test="$currentSecName = 'a'">b</xsl:when>
      <xsl:when test="$currentSecName = 'b'">c</xsl:when>
      <xsl:when test="$currentSecName = 'c'">d</xsl:when>
      <xsl:when test="$currentSecName = 'd'">e</xsl:when>
      <xsl:when test="$currentSecName = 'e'">f</xsl:when>
      <xsl:when test="$currentSecName = 'f'">g</xsl:when>
      <xsl:when test="$currentSecName = 'g'">h</xsl:when>
      <xsl:when test="$currentSecName = 'h'">i</xsl:when>
      <xsl:when test="$currentSecName = 'i'">j</xsl:when>
      <xsl:when test="$currentSecName = 'j'">disabled</xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:if test="$nextSec != 'disabled'">
    <xsl:choose>

        <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
      <xsl:when test="$nextSec != 'lecteur' and $nextSec != 'intro'">
	<xsl:for-each select="document('../data/neo_toc.xml')//section">
          <xsl:if test="@name = $nextSec">
            <xsl:for-each select="./subsection">
              <xsl:if test="@n = '-1'">
                <xsl:call-template name="format-navigation-next-sec">
                  <xsl:with-param name="secName"><xsl:value-of select="name" /></xsl:with-param>
		  <xsl:with-param name="secUrl">../<xsl:value-of select="$nextSec" />/<xsl:value-of select="url" /><xsl:value-of select="$ext" /></xsl:with-param>
                </xsl:call-template>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>

      <xsl:otherwise>
	<xsl:if test="$nextSec = 'intro'">
	  <!-- <xsl:for-each select="document('../neo_toc.xml')//text"> -->
	  <xsl:for-each select="document('../data/neo_toc.xml')//text">
	    <xsl:if test="@order = '1'">
	      <xsl:call-template name="format-navigation-next-sec-format">
		<xsl:with-param name="secName"><xsl:value-of select="name" /></xsl:with-param>
		<xsl:with-param name="secUrl">../<xsl:value-of select="url" /><xsl:value-of select="$ext" /></xsl:with-param>
	      </xsl:call-template>
	    </xsl:if>
	  </xsl:for-each>
	</xsl:if>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:if>
</xsl:template>

<xsl:template name="navigation-sec-toc">
  <xsl:param name="currentSecName" />
  <xsl:param name="ext" />
  <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
  <xsl:for-each select="document('../data/neo_toc.xml')//section">
    <xsl:if test="@name = $currentSecName">
      <xsl:for-each select="./subsection">
	<xsl:if test="@n = '-1'">
	  <xsl:call-template name="format-navigation-sec-toc">
	    <xsl:with-param name="secName"><xsl:value-of select="name" /></xsl:with-param>
	    <xsl:with-param name="secUrl"><xsl:value-of select="url" /><xsl:value-of select="$ext" /></xsl:with-param>
	    <xsl:with-param name="secItem"><xsl:value-of select="$currentSecName" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template name="navigation-prev-sec">
  <xsl:param name="currentSecName" />
  <xsl:param name="ext" />

  <xsl:variable name="prevSec">
    <xsl:choose>
      <xsl:when test="$currentSecName = 'lecteur'">disabled</xsl:when>
      <xsl:when test="$currentSecName = 'intro'">lecteur</xsl:when>
      <xsl:when test="$currentSecName = 'a'">intro</xsl:when>
      <xsl:when test="$currentSecName = 'b'">a</xsl:when>
      <xsl:when test="$currentSecName = 'c'">b</xsl:when>
      <xsl:when test="$currentSecName = 'd'">c</xsl:when>
      <xsl:when test="$currentSecName = 'e'">d</xsl:when>
      <xsl:when test="$currentSecName = 'f'">e</xsl:when>
      <xsl:when test="$currentSecName = 'g'">f</xsl:when>
      <xsl:when test="$currentSecName = 'h'">g</xsl:when>
      <xsl:when test="$currentSecName = 'i'">h</xsl:when>
      <xsl:when test="$currentSecName = 'j'">i</xsl:when>
    </xsl:choose>
  </xsl:variable>

  <!--<xsl:element name="p">Suivante: <xsl:value-of select="$prevSec" /></xsl:element>-->

  <xsl:if test="$prevSec != 'disabled'">
    <xsl:choose>

      <xsl:when test="$prevSec != 'lecteur' and $prevSec != 'intro'">
        <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
	<xsl:for-each select="document('../data/neo_toc.xml')//section">
          <xsl:if test="@name = $prevSec">
            <xsl:for-each select="./subsection">
              <xsl:if test="@n = '-1'">
                <xsl:call-template name="format-navigation-prev-sec">
                  <xsl:with-param name="secName"><xsl:value-of select="name" /></xsl:with-param>
		  <xsl:with-param name="secUrl">../<xsl:value-of select="$prevSec" />/<xsl:value-of select="url" /><xsl:value-of select="$ext" /></xsl:with-param>
                </xsl:call-template>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>

      <xsl:otherwise>
        <!-- <xsl:for-each select="document('../neo_toc.xml')//text"> -->
	<xsl:for-each select="document('../data/neo_toc.xml')//text">
          <xsl:choose>
            <xsl:when test="$prevSec = 'lecteur'">
              <xsl:if test="@order = '0'">
                <xsl:call-template name="format-navigation-prev-sec">
                  <xsl:with-param name="secName"><xsl:value-of select="name" /></xsl:with-param>
		  <xsl:with-param name="secUrl">../<xsl:value-of select="url" /><xsl:value-of select="$ext" /></xsl:with-param>
                </xsl:call-template>
              </xsl:if>
            </xsl:when>
            <xsl:when test="$prevSec = 'intro'">
              <xsl:if test="@order = '1'">
                <xsl:call-template name="format-navigation-prev-sec">
                  <xsl:with-param name="secName"><xsl:value-of select="name" /></xsl:with-param>
		  <xsl:with-param name="secUrl">../<xsl:value-of select="url" /><xsl:value-of select="$ext" /></xsl:with-param>
                </xsl:call-template>
              </xsl:if>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:if>
</xsl:template>

<xsl:template name="navigation-prev-sub">
  <xsl:param name="currentSecName" />
  <xsl:param name="currentSubName" />
  <xsl:param name="ext" />

  <xsl:variable name="prevSub">
    <xsl:value-of select="number($currentSubName)-1" />
  </xsl:variable>

  <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
  <xsl:for-each select="document('../data/neo_toc.xml')//section">
    <xsl:if test="@name = $currentSecName">
      <xsl:for-each select="./subsection">
	<xsl:if test="@n = string($prevSub)">
	  <xsl:call-template name="format-navigation-prev-sub">
	    <xsl:with-param name="subName"><xsl:value-of select="name" /></xsl:with-param>
	    <xsl:with-param name="subUrl"><xsl:value-of select="url" /><xsl:value-of select="$ext" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template name="navigation-next-sub">
  <xsl:param name="currentSecName" />
  <xsl:param name="currentSubName" />
  <xsl:param name="ext" />

  <xsl:variable name="nextSub">
    <xsl:value-of select="number($currentSubName)+1" />
  </xsl:variable>

  <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
  <xsl:for-each select="document('../data/neo_toc.xml')//section">
    <xsl:if test="@name = $currentSecName">
      <xsl:for-each select="./subsection">
	<xsl:if test="@n = string($nextSub)">
	  <xsl:call-template name="format-navigation-next-sub">
	    <xsl:with-param name="subName"><xsl:value-of select="name" /></xsl:with-param>
	    <xsl:with-param name="subUrl"><xsl:value-of select="url" /><xsl:value-of select="$ext" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!--
Template principal qui génère les éléments de la barre de navigation 
selon la section et la sous-section données en paramètres
-->
  
<xsl:template name="navigation">
  <xsl:param name="docSec" />
  <xsl:param name="docSub" />
  <xsl:param name="docTarget" select="'static'" />

  <xsl:variable name="scenario">
    <xsl:call-template name="scenario">
      <xsl:with-param name="docSec"><xsl:value-of select="$docSec" /></xsl:with-param>
      <xsl:with-param name="docSub"><xsl:value-of select="$docSub" /></xsl:with-param>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="fileExt">.html</xsl:variable>

  <xsl:if test="$scenario != ''">
    <xsl:element name="div">
      <xsl:attribute name="class">nav</xsl:attribute>

      <!-- Code trans-scénario : lien vers l’index de la FAQ -->

      <xsl:choose>
	<xsl:when test="$scenario = 'intro' or $scenario = 'app' or $scenario = 'root' or $scenario = 'lecteur'">
	  <xsl:call-template name="format-navigation-faq-toc">
	    <xsl:with-param name="url">index<xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:call-template name="format-navigation-faq-toc">
	    <xsl:with-param name="url">../index<xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:otherwise>
      </xsl:choose>

      <xsl:choose>

	<!-- Scénario : Lecteur -->

	<xsl:when test="$scenario = 'lecteur'">
	  <xsl:call-template name="format-navigation-next-sec">
	    <xsl:with-param name="secName">Introduction</xsl:with-param>
	    <xsl:with-param name="secUrl">intro<xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:when>

	<!-- Scénario : Introduction -->

	<xsl:when test="$scenario = 'intro'">
	  <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
	  <xsl:for-each select="document('../data/neo_toc.xml')//section">
	    <xsl:if test="@name = 'a'">
	      <xsl:for-each select="./subsection">
		<xsl:if test="@n = '-1'">
		  <xsl:call-template name="format-navigation-next-sec">
		    <xsl:with-param name="secName"><xsl:value-of select="name" /></xsl:with-param>
		    <xsl:with-param name="secUrl">a/<xsl:value-of select="url" /></xsl:with-param>
		    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
		  </xsl:call-template>
		</xsl:if>
	      </xsl:for-each>
	    </xsl:if>
	  </xsl:for-each>
	</xsl:when>

	<!-- Scénario : Table des matières de la section A -->

	<xsl:when test="$scenario = 'first_sec_toc'">
	  <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
	  <xsl:for-each select="document('../data/neo_toc.xml')//section">

	    <!-- Première sous-section -->
	    <xsl:if test="@name = 'a'">
	      <xsl:for-each select="./subsection">
		<xsl:if test="@n = '0'">
		  <xsl:call-template name="format-navigation-next-sub">
		    <xsl:with-param name="subName"><xsl:value-of select="name" /></xsl:with-param>
		    <xsl:with-param name="subUrl"><xsl:value-of select="url" /><xsl:value-of select="$fileExt" /></xsl:with-param>
		  </xsl:call-template>
		</xsl:if>
	      </xsl:for-each>
	    </xsl:if>

	    <!-- Section suivante -->
	    <xsl:if test="@name = 'b'">
	      <xsl:for-each select="./subsection">
		<xsl:if test="@n = '-1'">
		  <xsl:call-template name="format-navigation-next-sec">
		    <xsl:with-param name="secName"><xsl:value-of select="name" /></xsl:with-param>
		    <xsl:with-param name="secUrl">../b/<xsl:value-of select="url" /><xsl:value-of select="$fileExt" /></xsl:with-param>
		  </xsl:call-template>
		</xsl:if>
	      </xsl:for-each>
	    </xsl:if>

	  </xsl:for-each>
	</xsl:when>

	<!-- Scénario : Table des matières d’une section -->

	<xsl:when test="$scenario = 'sec_toc'">
	  <!-- Section précédente -->
	  <xsl:call-template name="navigation-prev-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
		    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Sous-section suivante -->
	  <xsl:call-template name="navigation-next-sub">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Section suivante -->
	  <xsl:call-template name="navigation-next-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:when>

	<!-- Scénario : Première sous-section d’une section -->

	<xsl:when test="$scenario = 'first_sub'">
	  <!-- Table des matières de la section -->
	  <xsl:call-template name="navigation-sec-toc">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Section précédente -->
	  <xsl:call-template name="navigation-prev-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Sous-section suivante -->
	  <xsl:call-template name="navigation-next-sub">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Section suivante -->
	  <xsl:call-template name="navigation-next-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:when>

	<!-- Scénario : Sous-section -->

	<xsl:when test="$scenario = 'sub'">
	  <!-- Table des matières de la section -->
	  <xsl:call-template name="navigation-sec-toc">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Section précédente -->
	  <xsl:call-template name="navigation-prev-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Sous-section précédente -->
	  <xsl:call-template name="navigation-prev-sub">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Sous-section suivante -->
	  <xsl:call-template name="navigation-next-sub">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Section suivante -->
	  <xsl:call-template name="navigation-next-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:when>

	<!-- Scénario : Dernière sous-section d’une section -->

	<xsl:when test="$scenario = 'last_sub'">
	  <!-- Table des matières de la section -->
	  <xsl:call-template name="navigation-sec-toc">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Section précédente -->
	  <xsl:call-template name="navigation-prev-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Sous-section précédente -->
	  <xsl:call-template name="navigation-prev-sub">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Section suivante -->
	  <xsl:call-template name="navigation-next-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:when>

	<!-- Scénario : Table des matières de la dernière section (J) -->

	<xsl:when test="$scenario = 'last_sec_toc'">
	  <!-- Table des matières de la section -->
	  <xsl:call-template name="navigation-sec-toc">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Section précédente -->
	  <xsl:call-template name="navigation-prev-sec">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	  <!-- Sous-section suivante -->
	  <xsl:call-template name="navigation-next-sub">
	    <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
	    <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
	    <xsl:with-param name="ext"><xsl:value-of select="$fileExt" /></xsl:with-param>
	  </xsl:call-template>
	</xsl:when>

      </xsl:choose>
    </xsl:element>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
