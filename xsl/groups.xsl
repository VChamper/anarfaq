<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" />

<!-- Paramètres de transformation ===================================== -->

<!-- Chemin des médias par rapport au fichier en cours de génération -->
<xsl:param name="path" select="'../'" />

<!--
  Type de transformation:
  dynamic => PHP
  static => HTML (sans accessibilité)
-->
<xsl:param name="target" select="'dynamic'" />

<!-- Templates externes appelés via leur nom ========================== -->

<!-- Template de navigation -->
<xsl:include href="navigation.xsl" />
<!-- Template du pied de page -->
<xsl:include href="text-footer.xsl" />

<!-- Template de la page intégrale ==================================== -->

<xsl:template match="/">
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>Index des groupes – FAQ Anarchiste (francophone)</title>
    <link rel="stylesheet" type="text/css" href="../inc/style.css" /> 
    <link rel="stylesheet" type="text/css" href="../inc/names.css" /> 
    <link rel="icon" href="../inc/favicon.ico" type="image/gif" />
  </head>
  <body>
    <a id="back"></a>

    <header>
      <h1 class="left"><a href="{$path}index.html">La FAQ Anarchiste (francophone)</a></h1>
    </header>

    <nav>
      <div class="navigation">
	<div class="alt-lang"></div>
	<xsl:call-template name="navigation">
	  <xsl:with-param name="docSec">names</xsl:with-param>
	  <xsl:with-param name="docSub">1</xsl:with-param>
	  <xsl:with-param name="docTarget"><xsl:value-of select="$target" /></xsl:with-param>
	</xsl:call-template>
      </div>
    </nav>

    <xsl:element name="main">
      <xsl:element name="section">

    <h1>Index des groupes politiques</h1>

    <xsl:element name="div">
      <xsl:attribute name="class">letters</xsl:attribute>
      <xsl:for-each select="index/letter">
	<xsl:sort select="@name" order="ascending" data-type="text" />
	<xsl:element name="div">
	  <xsl:attribute name="class">letter</xsl:attribute>
	  <xsl:element name="a">
	    <xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:value-of select="@name" /></xsl:attribute>
	    <xsl:value-of select="@name" />
	  </xsl:element>
	</xsl:element>
      </xsl:for-each>
    </xsl:element>

    <p class="centered">La sous-section numérotée 0 correspond à l’introduction d’une section.</p>

    <xsl:for-each select="index/letter">
      <xsl:sort select="@name" order="ascending" data-type="text" />

      <xsl:element name="h2">
	<xsl:attribute name="id">
	  <xsl:value-of select="@name" />
	</xsl:attribute>
	<xsl:value-of select="@name" />
      </xsl:element>

      <xsl:for-each select="entry">
	<xsl:sort select="entry/name" order="ascending" data-type="text" />

	<xsl:element name="a">
	  <xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
	</xsl:element>

	<xsl:element name="h3">
	  <xsl:value-of select="name" />
	  <xsl:if test="acronym or geo">
	    <xsl:text> (</xsl:text>

	    <xsl:if test="acronym">
	      <xsl:value-of select="acronym" />
	    </xsl:if>

	    <xsl:if test="geo">
	      <xsl:if test="acronym">
		<xsl:text>, </xsl:text>
	      </xsl:if>
	      <xsl:value-of select="geo" />
	    </xsl:if>

	    <xsl:text>)</xsl:text>
	  </xsl:if>
	</xsl:element>

	<xsl:if test="bio">
	  <xsl:element name="p">
	    <xsl:attribute name="class">bio</xsl:attribute>
	    <xsl:value-of select="bio" />
	  </xsl:element>
	</xsl:if>

	<xsl:if test="syn">
	  <xsl:element name="p">
	    <xsl:attribute name="class">bio</xsl:attribute>
	    <xsl:value-of select="syn/text" />
	    <xsl:text> Voir l’entrée </xsl:text>
	    <xsl:element name="a">
	      <xsl:choose>
		<xsl:when test="syn/link/@target">
		  <xsl:attribute name="href">names.php#<xsl:value-of select="syn/link/@id" /></xsl:attribute>
		  <xsl:value-of select="syn/link/." />
		</xsl:when>
		<xsl:otherwise>
		  <xsl:attribute name="href">#<xsl:value-of select="syn/link/@id" /></xsl:attribute>
		  <xsl:value-of select="syn/link/." />
		</xsl:otherwise>
	      </xsl:choose>
	    </xsl:element>
	    <xsl:text>.</xsl:text>
	  </xsl:element>
	</xsl:if>

	<xsl:for-each select="locations/action">
	  <xsl:element name="p">
	    <xsl:attribute name="class">action</xsl:attribute>
	    <xsl:value-of select="@name" />
	  </xsl:element>
	  <xsl:element name="p">
	    <xsl:attribute name="class">idx</xsl:attribute>
	    <xsl:for-each select="./loc">
	      <xsl:choose>
		<xsl:when test="@sub">
		  <xsl:element name="a">
		    <xsl:attribute name="class">ref</xsl:attribute>
		    <xsl:attribute name="href">
		      <xsl:text>../</xsl:text>
		      <xsl:value-of select="@sec" />
		      <xsl:text>/</xsl:text>
		      <xsl:value-of select="@sec" />
		      <xsl:text>_</xsl:text>
		      <xsl:value-of select="@sub" />.html
		    </xsl:attribute>
		    <xsl:value-of select="@sec" />
		    <xsl:text>-</xsl:text>
		    <xsl:value-of select="@sub" />
		  </xsl:element>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:element name="a">
		    <xsl:attribute name="href">../intro.html</xsl:attribute>
		    <xsl:attribute name="class">ref</xsl:attribute>
		    <xsl:attribute name="style">text-transform:none;</xsl:attribute>
		    <xsl:value-of select="@sec" />
		  </xsl:element>
		</xsl:otherwise>
	      </xsl:choose>
	      <xsl:text>; </xsl:text>
	      <xsl:if test="position() mod 10 = 1 and position() != 1">
		<xsl:element name="br" />
	      </xsl:if>
	    </xsl:for-each>
	  </xsl:element>
	</xsl:for-each>

	<xsl:if test="locations/loc">
	  <xsl:if test="count(locations/loc) &gt; 1">
	    <xsl:element name="p">
	      <xsl:attribute name="class">idx</xsl:attribute>
	      Apparaît dans <xsl:value-of select="count(locations/loc)" /> sections&#160;:
	    </xsl:element>
	  </xsl:if>
	  <xsl:element name="p">
	    <xsl:attribute name="class">idx</xsl:attribute>
	    <xsl:for-each select="locations/loc">
	      <xsl:choose>
		<xsl:when test="@sub">
		  <xsl:element name="a">
		    <xsl:attribute name="class">ref</xsl:attribute>
		    <xsl:attribute name="href">
		      <xsl:text>../</xsl:text>
		      <xsl:value-of select="@sec" />
		      <xsl:text>/</xsl:text>
		      <xsl:value-of select="@sec" />
		      <xsl:text>_</xsl:text>
		      <xsl:value-of select="@sub" />
		      <xsl:text>.php</xsl:text>
		    </xsl:attribute>
		    <xsl:value-of select="@sec" />
		    <xsl:if test="@sec != 'app2'">
		      <xsl:text>-</xsl:text>
		      <xsl:value-of select="@sub" />
		    </xsl:if>
		  </xsl:element>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:element name="a">
		    <xsl:attribute name="href">../intro.php</xsl:attribute>
		    <xsl:attribute name="class">ref</xsl:attribute>
		    <xsl:attribute name="style">text-transform:none;</xsl:attribute>
		    <xsl:value-of select="@sec" />
		  </xsl:element>
		</xsl:otherwise>
	      </xsl:choose>
	      <xsl:text>; </xsl:text>
	      <xsl:if test="position() mod 10 = 1 and position() != 1">
		<xsl:element name="br" />
	      </xsl:if>
	    </xsl:for-each>
	  </xsl:element>
	</xsl:if>

	<xsl:if test="also">
	  <xsl:element name="p">
	    <xsl:attribute name="class">bio</xsl:attribute>
	    Voir aussi&#160;:
	  </xsl:element>
	  <xsl:element name="ul">
	    <xsl:attribute name="class">also</xsl:attribute>
	    <xsl:for-each select="also/link">
	      <xsl:element name="li">
		<xsl:element name="a">
		  <xsl:choose>
		    <xsl:when test="@target = 'names'">
		      <xsl:attribute name="href">names.php#<xsl:value-of select="@id" /></xsl:attribute>
		      <xsl:value-of select="." />
		    </xsl:when>
		    <xsl:otherwise>
		      <xsl:attribute name="href">#<xsl:value-of select="@id" /></xsl:attribute>
		      <xsl:value-of select="." />
		    </xsl:otherwise>
		  </xsl:choose>
		</xsl:element>
	      </xsl:element>
	    </xsl:for-each>
	  </xsl:element>
	</xsl:if>
      </xsl:for-each>
    </xsl:for-each>

    </xsl:element>
  </xsl:element>

    <!-- Retour en haut de page -->

    <div class="goback">
      <p><a href="#back">Retour en haut de la page</a></p>
    </div>

    <!-- Licence -->

    <xsl:call-template name="text-footer" />
  </body>
</html>
</xsl:template>

</xsl:stylesheet>
