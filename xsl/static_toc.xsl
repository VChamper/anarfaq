<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!-- Template de navigation -->
<xsl:include href="navigation.xsl" />
<!-- Template du pied de page -->
<xsl:include href="text-footer.xsl" />
<!-- Template pour les miroirs du texte -->
<xsl:include href="mirrors.xsl" />

<!-- Paramètres de transformation ===================================== -->

<!--<xsl:param name="selectedSection">all</xsl:param>-->
<xsl:param name="selectedSection">a</xsl:param>
<xsl:param name="path" select="'../'" />

<xsl:template match="section">
  <xsl:variable name="secName">
    <xsl:value-of select="@name" />
  </xsl:variable>
  <xsl:variable name="secNAME">
    <xsl:value-of select="translate($secName,'abcdefghij','ABCDEFGHIJ')" />
  </xsl:variable>

  <xsl:choose>

    <xsl:when test="$selectedSection = 'all'">
      <xsl:element name="p">
	<xsl:attribute name="class">secToc0</xsl:attribute>
	Section <xsl:value-of select="subsection[@n='-1']/name" />
      </xsl:element>
      <xsl:for-each select="subsection">
	<xsl:sort select="@n" order="ascending" data-type="number" />
	<xsl:if test="@n > -1">

	  <xsl:element name="p">
	    <xsl:attribute name="class">secToc1</xsl:attribute>
	    <xsl:element name="a">
	      <xsl:attribute name="href"><xsl:value-of select="../@name" />/<xsl:value-of select="url" />.html</xsl:attribute>
	      <xsl:value-of select="name" />
	    </xsl:element>
	  </xsl:element>

	  <xsl:for-each select="subs/s">
	    <xsl:element name="p">
	      <xsl:attribute name="class">secToc2</xsl:attribute>
	      <xsl:element name="a">
		<xsl:attribute name="href"><xsl:value-of select="$secName" />/<xsl:value-of select="../../url" />.html#<xsl:value-of select="../../../@name" /><xsl:value-of select="../../@n" />-<xsl:value-of select="@n" /></xsl:attribute>
		<xsl:value-of select="$secNAME" />.<xsl:value-of select="../../@n" /><xsl:value-of select="../@n" />.<xsl:value-of select="@n" />&#160;–&#160;<xsl:value-of select="." />
	      </xsl:element>
	    </xsl:element>
	  </xsl:for-each>

	</xsl:if>
      </xsl:for-each>
    </xsl:when>

    <xsl:otherwise>
      <xsl:element name="h1">
	Section <xsl:value-of select="subsection[@n='-1']/name" />
      </xsl:element>
      <xsl:for-each select="subsection">
	<xsl:sort select="@n" order="ascending" data-type="number" />
	<xsl:if test="@n > -1">

	  <xsl:element name="p">
	    <xsl:attribute name="class">secToc1</xsl:attribute>
	    <xsl:element name="a">
	      <xsl:attribute name="href"><xsl:value-of select="url" />.html</xsl:attribute>
	      <xsl:value-of select="name" />
	    </xsl:element>
	  </xsl:element>

	  <xsl:for-each select="subs/s">
	    <xsl:element name="p">
	      <xsl:attribute name="class">secToc2</xsl:attribute>
	      <xsl:element name="a">
		<xsl:attribute name="href"><xsl:value-of select="../../url" />.html#<xsl:value-of select="../../../@name" /><xsl:value-of select="../../@n" />-<xsl:value-of select="@n" /></xsl:attribute>
		<xsl:value-of select="$secNAME" />.<xsl:value-of select="../../@n" /><xsl:value-of select="../@n" />.<xsl:value-of select="@n" />&#160;–&#160;<xsl:value-of select="." />
	      </xsl:element>
	    </xsl:element>
	  </xsl:for-each>

	</xsl:if>
      </xsl:for-each>
  </xsl:otherwise>

  </xsl:choose>
</xsl:template>

<!-- Éléments du document à ne pas afficher =========================== -->

<xsl:template match="/front/text/name"></xsl:template>
<xsl:template match="/front/text/url"></xsl:template>

<!-- Template de la page intégrale ==================================== -->

<xsl:template match="/">
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="type">text/css</xsl:attribute>
      <xsl:attribute name="href">../inc/style.css</xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">icon</xsl:attribute>
      <xsl:attribute name="type">image/gif</xsl:attribute>
      <xsl:attribute name="href">../inc/favicon.ico</xsl:attribute>
    </xsl:element>
    <link rel="alternate" type="application/rdf+xml" href="http://www.gnu.org/licenses/fdl-1.3.rdf" />
    <xsl:choose>
      <xsl:when test="$selectedSection = 'all'">
	<xsl:element name="title">FAQ Anarchiste - Table des matières</xsl:element>
      </xsl:when>
      <xsl:otherwise>
	<xsl:variable name="sectionNode">
	  <xsl:value-of select="toc/content/section[@name = $selectedSection]/@name" />
	</xsl:variable>
	<xsl:variable name="sectionNAME">
	  <xsl:value-of select="translate($sectionNode,'abcdefghij','ABCDEFGHIJ')" />
	</xsl:variable>
	<xsl:element name="title">FAQ Anarchiste - Section <xsl:value-of select="$sectionNAME" /></xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </head>
  <body>
    <header>
      <h1>La FAQ Anarchiste (francophone)</h1>
    </header>
    <nav>
        <div class='navigation'>
	  <xsl:if test="$selectedSection != 'all'">
	    <xsl:variable name="sectionNode">
	      <xsl:value-of select="toc/content/section[@name = $selectedSection]/@name" />
	    </xsl:variable>
	    <xsl:call-template name="mirrors">
	      <xsl:with-param name="docSec"><xsl:value-of select="$sectionNode" /></xsl:with-param>
	      <xsl:with-param name="docSub">toc</xsl:with-param>
	    </xsl:call-template>
	    <xsl:call-template name="navigation">
	      <xsl:with-param name="docSec"><xsl:value-of select="$sectionNode" /></xsl:with-param>
	      <xsl:with-param name="docSub">toc</xsl:with-param>
	    </xsl:call-template>
	  </xsl:if>
        </div>
    </nav>

    <xsl:element name="main">
      <xsl:element name="section">
     
    <xsl:choose>

      <!-- Génération de la table des matières générale -->

      <xsl:when test="$selectedSection = 'all'">
	<xsl:element name="h1">Table des matières générale</xsl:element>

	<xsl:element name="p">
	  <xsl:attribute name="class">secToc0</xsl:attribute>
	  Liminaires
	</xsl:element>

	<xsl:for-each select="toc/front/text">
	  <xsl:sort select="@order" order="ascending" data-type="number" />
	  <xsl:element name="p">
	    <xsl:attribute name="class">secToc1</xsl:attribute>
	    <xsl:element name="a">
	      <xsl:attribute name="href"><xsl:value-of select="url" />.html</xsl:attribute>
	      <xsl:value-of select="name" />
	    </xsl:element>
	  </xsl:element>
	</xsl:for-each>

	<xsl:for-each select="toc/content/section">
	  <xsl:apply-templates select="." />
	</xsl:for-each>
      </xsl:when>

      <!-- Génération de la table des matières correspondant à $selectedSection -->

      <xsl:otherwise>
	<xsl:apply-templates select="toc/content/section[@name = $selectedSection]" />
      </xsl:otherwise>

    </xsl:choose>

    <!-- Retour en haut de page -->

    <div class="goback">
      <p><a href="#back">Retour en haut de la page</a></p>
    </div>

      </xsl:element>
    </xsl:element>

    <!-- Licence -->

    <xsl:call-template name="text-footer" />

  </body>
</html>
</xsl:template>

</xsl:stylesheet>
