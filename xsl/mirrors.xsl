<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:param name="gb_flag">../inc/flags/gb.svg</xsl:param>
<xsl:param name="it_flag">../inc/flags/it.svg</xsl:param>
<xsl:param name="pl_flag">../inc/flags/pl.svg</xsl:param>
<xsl:param name="pt_flag">../inc/flags/pt.svg</xsl:param>

<!-- Template pour formater le lien qui va bien. ====================== -->
<xsl:template name="mirror-link">
  <xsl:param name="imgAlt" />
  <xsl:param name="imgSrc" />
  <xsl:param name="imgText" />
  <xsl:param name="linkHref" />
  <xsl:param name="linkTitle" />

  <xsl:element name="a">
    <xsl:attribute name="href"><xsl:value-of select="$linkHref" /></xsl:attribute>
    <xsl:attribute name="title"><xsl:value-of select="$linkTitle" /></xsl:attribute>
    <xsl:value-of select="$imgSrc" />
  </xsl:element>
</xsl:template>

<!-- Templates pour chaque miroir ===================================== -->

<!-- Certains miroirs forment leurs URLs de la même façon *** -->

<!-- Template pour infoshop, inventati, anarchifaq -->
<xsl:template name="common-1">
  <xsl:param name="case" />
  <xsl:param name="section" />
  <xsl:param name="subsection" />
  <xsl:param name="root" />
  <xsl:param name="imgAlt" />
  <xsl:param name="imgSrc" />
  <xsl:param name="imgText" />
  <xsl:param name="linkTitle" />

  <xsl:variable name="formatedSection">
    <xsl:value-of select="translate($section,'abcdefghij','ABCDEFGHIJ')" />
  </xsl:variable>

  <xsl:variable name="url">
    <xsl:choose>
      <xsl:when test="$case = 'toc'"><xsl:value-of select="$formatedSection" /></xsl:when>
      <xsl:when test="$case = 'sub'">
	<xsl:choose>
	  <xsl:when test="$subsection = '0'"><xsl:value-of select="$formatedSection" /></xsl:when>
	  <xsl:otherwise><xsl:value-of select="$formatedSection" /><xsl:value-of select="$subsection" /></xsl:otherwise>
	</xsl:choose>
      </xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:call-template name="mirror-link">
    <xsl:with-param name="imgAlt"><xsl:value-of select="$imgAlt" /></xsl:with-param>
    <xsl:with-param name="imgSrc"><xsl:value-of select="$imgSrc" /></xsl:with-param>
    <xsl:with-param name="imgText"><xsl:value-of select="$imgText" /></xsl:with-param>
    <xsl:with-param name="linkHref"><xsl:value-of select="$root" /><xsl:value-of select="$url" /></xsl:with-param>
    <xsl:with-param name="linkTitle"><xsl:value-of select="$linkTitle" /></xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Templates de miroirs *********************************** -->

<!-- The Anarchist Library - theanarchistlibrary.org -->
<xsl:template name="tal">
  <!--
  Pas de cas d’URL, parce que theanarchistlibrary.org parce que le texte 
  n’est pas divisé en sous-sections.
  -->
  <xsl:param name="section" />
  <xsl:param name="subsection" />

  <xsl:variable name="ref">
    <xsl:choose>
      <xsl:when test="$section = 'intro'">1</xsl:when>
      <xsl:when test="$section = 'a'">2</xsl:when>
      <xsl:when test="$section = 'b'">3</xsl:when>
      <xsl:when test="$section = 'c'">4</xsl:when>
      <xsl:when test="$section = 'd'">5</xsl:when>
      <xsl:when test="$section = 'e'">6</xsl:when>
      <xsl:when test="$section = 'f'">7</xsl:when>
      <xsl:when test="$section = 'g'">8</xsl:when>
      <xsl:when test="$section = 'h'">9</xsl:when>
      <xsl:when test="$section = 'i'">10</xsl:when>
      <xsl:when test="$section = 'j'">11</xsl:when>
      <xsl:when test="$section = 'app1'">12</xsl:when>
      <xsl:when test="$section = 'app2'">13</xsl:when>
      <xsl:when test="$section = 'app3'">14</xsl:when>
      <xsl:when test="$section = 'app4'">15</xsl:when>
      <xsl:when test="$section = 'bib'">16</xsl:when>
      <xsl:when test="$section = 'post'">17</xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:call-template name="mirror-link">
    <xsl:with-param name="imgAlt">[en 1]</xsl:with-param>
    <xsl:with-param name="imgSrc">🇬🇧</xsl:with-param>
    <xsl:with-param name="imgText"></xsl:with-param>
    <xsl:with-param name="linkHref">https://theanarchistlibrary.org/library/the-anarchist-faq-editorial-collective-an-anarchist-faq-<xsl:value-of select="$ref" />-17</xsl:with-param>
    <xsl:with-param name="linkTitle">English / Anglais</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- PageAbode - anarchism.pageabode.com -->
<xsl:template name="pageabode">
  <xsl:param name="case" />
  <xsl:param name="section" />
  <xsl:param name="subsection" />

  <xsl:variable name="url">
    <xsl:choose>
      <xsl:when test="$case = 'toc'"><xsl:value-of select="$section" />_con.html</xsl:when>
      <xsl:when test="$case = 'sub'">
	<xsl:choose>
	  <xsl:when test="$subsection = '0'">sec<xsl:value-of select="$section" />int.html</xsl:when>
	  <xsl:otherwise>sec<xsl:value-of select="$section" /><xsl:value-of select="$subsection" />.html</xsl:otherwise>
	</xsl:choose>
      </xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:call-template name="mirror-link">
    <xsl:with-param name="imgAlt">[en 2]</xsl:with-param>
    <xsl:with-param name="imgSrc">🇬🇧</xsl:with-param>
    <xsl:with-param name="imgText">2</xsl:with-param>
    <xsl:with-param name="linkHref">http://anarchism.pageabode.com/afaq/<xsl:value-of select="$url" /></xsl:with-param>
    <xsl:with-param name="linkTitle">English / Anglais</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Infoshop (en) - www.infoshop.org %common-1 -->
<xsl:template name="infoshop">
  <xsl:param name="case" />
  <xsl:param name="section" />
  <xsl:param name="subsection" />

  <xsl:call-template name="common-1">
    <xsl:with-param name="case"><xsl:value-of select="$case" /></xsl:with-param>
    <xsl:with-param name="section"><xsl:value-of select="$section" /></xsl:with-param>
    <xsl:with-param name="subsection"><xsl:value-of select="$subsection" /></xsl:with-param>
    <xsl:with-param name="root">http://www.infoshop.org/AnarchistFAQSection</xsl:with-param>
    <xsl:with-param name="imgAlt">[en 3]</xsl:with-param>
    <xsl:with-param name="imgSrc">🇬🇧</xsl:with-param>
    <xsl:with-param name="imgText">3</xsl:with-param>
    <xsl:with-param name="linkTitle">English / Anglais</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Autistici/Inventati (it) - www.inventati.org/anarchyco %common-1 -->
<xsl:template name="inventati">
  <xsl:param name="case" />
  <xsl:param name="section" />
  <xsl:param name="subsection" />

  <xsl:call-template name="common-1">
    <xsl:with-param name="case"><xsl:value-of select="$case" /></xsl:with-param>
    <xsl:with-param name="section"><xsl:value-of select="$section" /></xsl:with-param>
    <xsl:with-param name="subsection"><xsl:value-of select="$subsection" /></xsl:with-param>
    <xsl:with-param name="root">http://www.inventati.org/anarchyco/Sez</xsl:with-param>
    <xsl:with-param name="imgAlt">[it]</xsl:with-param>
    <xsl:with-param name="imgSrc">🇮🇹</xsl:with-param>
    <xsl:with-param name="imgText"></xsl:with-param>
    <xsl:with-param name="linkTitle">Italiano / Italien</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Anarchifaq (pl) - www.anarchifaq.most.org.pl %common-1 -->
<xsl:template name="anarchifaq">
  <xsl:param name="case" />
  <xsl:param name="section" />
  <xsl:param name="subsection" />

  <xsl:call-template name="common-1">
    <xsl:with-param name="case"><xsl:value-of select="$case" /></xsl:with-param>
    <xsl:with-param name="section"><xsl:value-of select="$section" /></xsl:with-param>
    <xsl:with-param name="subsection"><xsl:value-of select="$subsection" /></xsl:with-param>
    <xsl:with-param name="root">http://www.anarchifaq.most.org.pl/Sekcja</xsl:with-param>
    <xsl:with-param name="imgAlt">[pl]</xsl:with-param>
    <xsl:with-param name="imgSrc">🇵🇱</xsl:with-param>
    <xsl:with-param name="imgText"></xsl:with-param>
    <xsl:with-param name="linkTitle">Polski / Polonais</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Reocities (pt) - www.reocities.com/projetoperiferia2 -->
<xsl:template name="reocities">
  <xsl:param name="case" />
  <xsl:param name="section" />
  <xsl:param name="subsection" />

  <xsl:variable name="url">
    <xsl:choose>
      <xsl:when test="$case = 'toc'">indice.htm</xsl:when>
      <xsl:otherwise>
	<xsl:choose>
	  <xsl:when test="$subsection = '0'"><xsl:value-of select="$section" />int.html</xsl:when>
	  <xsl:otherwise>sec<xsl:value-of select="$section" /><xsl:value-of select="$subsection" />.html</xsl:otherwise>
	</xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:call-template name="mirror-link">
    <xsl:with-param name="imgAlt">[pt]</xsl:with-param>
    <xsl:with-param name="imgSrc">🇵🇹</xsl:with-param>
    <xsl:with-param name="imgText"></xsl:with-param>
    <xsl:with-param name="linkHref">http://www.reocities.com/projetoperiferia2/<xsl:value-of select="$url" /></xsl:with-param>
    <xsl:with-param name="linkTitle">Portugues / Portugais</xsl:with-param>
  </xsl:call-template>
</xsl:template>

<!-- Template principal =============================================== -->

<xsl:template name="mirrors">
  <xsl:param name="docSec" />
  <xsl:param name="docSub" />

  <xsl:variable name="case">
    <xsl:choose>
      <xsl:when test="$docSub = 'toc'">toc</xsl:when>
      <xsl:otherwise>sub</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:element name="div">
    <xsl:attribute name="class">alt-lang</xsl:attribute>

    <xsl:if test="$docSec != 'app' or $docSec != 'none' or $docSec != 'lecteur' or $docSec != '404'">
      Langues disponibles &amp; Miroirs disponibles&#160;: 

      <!-- The Anarchist Library - theanarchistlibrary.org -->
      <xsl:call-template name="tal">
	<xsl:with-param name="section"><xsl:value-of select="$docSec" /></xsl:with-param>
	<xsl:with-param name="subsection"><xsl:value-of select="$docSub" /></xsl:with-param>
      </xsl:call-template><xsl:text> </xsl:text>

      <!-- PageAbode - anarchism.pageabode.com -->
      <xsl:call-template name="pageabode">
	<xsl:with-param name="case"><xsl:value-of select="$case" /></xsl:with-param>
	<xsl:with-param name="section"><xsl:value-of select="$docSec" /></xsl:with-param>
	<xsl:with-param name="subsection"><xsl:value-of select="$docSub" /></xsl:with-param>
      </xsl:call-template><xsl:text> </xsl:text>

      <!-- Infoshop (en) - www.infoshop.org -->
      <xsl:call-template name="infoshop">
	<xsl:with-param name="case"><xsl:value-of select="$case" /></xsl:with-param>
	<xsl:with-param name="section"><xsl:value-of select="$docSec" /></xsl:with-param>
	<xsl:with-param name="subsection"><xsl:value-of select="$docSub" /></xsl:with-param>
      </xsl:call-template><xsl:text> </xsl:text>

      <!-- Autistici/Inventati (it) - www.inventati.org/anarchyco -->
      <xsl:call-template name="inventati">
	<xsl:with-param name="case"><xsl:value-of select="$case" /></xsl:with-param>
	<xsl:with-param name="section"><xsl:value-of select="$docSec" /></xsl:with-param>
	<xsl:with-param name="subsection"><xsl:value-of select="$docSub" /></xsl:with-param>
      </xsl:call-template><xsl:text> </xsl:text>

      <!-- Anarchifaq (pl) - www.anarchifaq.most.org.pl -->
      <xsl:call-template name="anarchifaq">
	<xsl:with-param name="case"><xsl:value-of select="$case" /></xsl:with-param>
	<xsl:with-param name="section"><xsl:value-of select="$docSec" /></xsl:with-param>
	<xsl:with-param name="subsection"><xsl:value-of select="$docSub" /></xsl:with-param>
      </xsl:call-template><xsl:text> </xsl:text>

      <!-- Reocities (pt) - www.reocities.com/projetoperiferia2 -->
      <xsl:call-template name="reocities">
	<xsl:with-param name="case"><xsl:value-of select="$case" /></xsl:with-param>
	<xsl:with-param name="section"><xsl:value-of select="$docSec" /></xsl:with-param>
	<xsl:with-param name="subsection"><xsl:value-of select="$docSub" /></xsl:with-param>
      </xsl:call-template>
    </xsl:if>

  </xsl:element>
</xsl:template>

</xsl:stylesheet>
