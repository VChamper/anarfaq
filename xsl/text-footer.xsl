<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template name="text-footer">
    <footer>
	<div>
	Cette FAQ est placée sous une licence libre (voir les paragraphes suivants).
	</div>

	<div>
	Si vous remarquez une faute d’orthographe, une coquille, etc, 
	vous pouvez signaler cela, voire vous impliquer plus dans la traduction 
	et l’actualisation du texte en visitant 
	<a href="https://framagit.org/VChamper/anarfaq">le dépôt GIT de la FAQ</a>.
	</div>

	<div>"An Anarchist FAQ", Version 13.1 Copyright (C) 1995–2009 
	The Anarchist FAQ Editorial Collective: Iain McKay, Gary Elkin, 
	Dave Neal, Ed Boraas. Permission is granted to copy, distribute and/or modify this
	document under the terms of the GNU Free Documentation License,
	Version 1.1 or any later version published by the Free Software
	Foundation. See the GNU Free Documentation License for more details
	at <a href="http://www.gnu.org/">http://www.gnu.org/</a></div>

	<div>Une FAQ Anarchiste (francophone) Version 1 Copyright (C) -
	2003-2017 - faqanarchiste.free.fr . Vous pouvez copier, distribuer
	ou modifier ce document selon les termes de la licence GNU de
	documentation libre, dans sa version 1.3 ou dans toute version
	ultérieure publiée par la Free Software Foundation ; sans Section
	Invariante, sans Texte De Première De Couverture, et sans Texte De
	Quatrième De Couverture. Une copie de cette licence est incluse dans
	la section intitulée "Licence GNU de documentation libre".</div>

	<div>La FAQ Anarchiste (francophone) Version 1 Copyright (C) -
	2017 - tviblindi.legtux.org. Vous pouvez copier, distribuer
	ou modifier ce document selon les termes de la licence GNU de
	documentation libre, dans sa version 1.3 ou dans toute version
	ultérieure publiée par la Free Software Foundation ; sans Section
	Invariante, sans Texte De Première De Couverture, et sans Texte De
	Quatrième De Couverture. Une copie de cette licence est incluse dans
	la section intitulée «&#160;<a href="http://tviblindi.legtux.org/afaq/fdl.php">
	GNU Free Documentation License</a>&#160;».</div>
    </footer>
</xsl:template>

</xsl:stylesheet>
