<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="meta/toc/*"></xsl:template>
<xsl:template match="meta/pagetitle"></xsl:template>
<xsl:template match="meta/translation"></xsl:template>
<xsl:template match="meta/type"></xsl:template>

</xsl:stylesheet>

