<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template name="format-goToNext-element">
  <xsl:param name="eUrl" />
  <xsl:param name="eName" />
  <xsl:param name="eType" />
  <xsl:element name="div">
    <xsl:attribute name="class">goback</xsl:attribute>
    <xsl:element name="p">
      <xsl:choose>
	<xsl:when test="$eType = 'section'">Section </xsl:when>
	<xsl:when test="$eType = 'subsection'">Sous-section </xsl:when>
	<!-- otherwise = 'name-only' -->
	<xsl:otherwise></xsl:otherwise>
      </xsl:choose>
      <xsl:if test="$eType = 'section' or $eType = 'subsection'">suivante&#160;: </xsl:if>
      <xsl:element name="a">
	<xsl:attribute name="href"><xsl:value-of select="$eUrl" /></xsl:attribute>
	<xsl:value-of select="$eName" />
      </xsl:element>
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="goToNext-sec">
  <xsl:param name="currentSecName" />

  <xsl:variable name="nextSec">
    <xsl:choose>
      <xsl:when test="$currentSecName = 'lecteur'">intro</xsl:when>
      <xsl:when test="$currentSecName = 'intro'">a</xsl:when>
      <xsl:when test="$currentSecName = 'a'">b</xsl:when>
      <xsl:when test="$currentSecName = 'b'">c</xsl:when>
      <xsl:when test="$currentSecName = 'c'">d</xsl:when>
      <xsl:when test="$currentSecName = 'd'">e</xsl:when>
      <xsl:when test="$currentSecName = 'e'">f</xsl:when>
      <xsl:when test="$currentSecName = 'f'">g</xsl:when>
      <xsl:when test="$currentSecName = 'g'">h</xsl:when>
      <xsl:when test="$currentSecName = 'h'">i</xsl:when>
      <xsl:when test="$currentSecName = 'i'">j</xsl:when>
      <xsl:when test="$currentSecName = 'j'">disabled</xsl:when>
    </xsl:choose>
  </xsl:variable>

  <xsl:if test="$nextSec != 'disabled'">
    <xsl:choose>

      <xsl:when test="$nextSec != 'lecteur' and $nextSec != 'intro'">
        <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
	<xsl:for-each select="document('../data/neo_toc.xml')//section">
          <xsl:if test="@name = $nextSec">
            <xsl:for-each select="./subsection">
              <xsl:if test="@n = '-1'">
                <xsl:call-template name="format-goToNext-element">
                  <xsl:with-param name="eName"><xsl:value-of select="name" /></xsl:with-param>
                  <xsl:with-param name="eUrl">../<xsl:value-of select="$nextSec" />/<xsl:value-of select="url" /></xsl:with-param>
                  <xsl:with-param name="eType"><xsl:value-of select="section" /></xsl:with-param>
                </xsl:call-template>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>

      <xsl:otherwise>
	<xsl:if test="$nextSec = 'intro'">
	  <!-- <xsl:for-each select="document('../neo_toc.xml')//text"> -->
	  <xsl:for-each select="document('../data/neo_toc.xml')//text">
	    <xsl:if test="@order = '1'">
	      <xsl:call-template name="format-goToNext-element">
		<xsl:with-param name="eName"><xsl:value-of select="name" /></xsl:with-param>
		<xsl:with-param name="eUrl">../<xsl:value-of select="url" /></xsl:with-param>
		<xsl:with-param name="eType"><xsl:value-of select="section" /></xsl:with-param>
	      </xsl:call-template>
	    </xsl:if>
	  </xsl:for-each>
	</xsl:if>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:if>
</xsl:template>

<xsl:template name="goToNext-sub">
  <xsl:param name="currentSecName" />
  <xsl:param name="currentSubName" />

  <xsl:variable name="nextSub">
    <xsl:value-of select="number($currentSubName)+1" />
  </xsl:variable>

  <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
  <xsl:for-each select="document('../data/neo_toc.xml')//section">
    <xsl:if test="@name = $currentSecName">
      <xsl:for-each select="./subsection">
	<xsl:if test="@n = string($nextSub)">
	  <xsl:call-template name="format-goToNext-element">
	    <xsl:with-param name="eName"><xsl:value-of select="name" /></xsl:with-param>
	    <xsl:with-param name="eUrl"><xsl:value-of select="url" /></xsl:with-param>
	    <xsl:with-param name="eType">subsection</xsl:with-param>
	  </xsl:call-template>
	</xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!--
Template principal qui génère le lien vers la section
ou la sous-section suivante.
-->
  
<xsl:template name="goToNext">
  <xsl:param name="docSec" />
  <xsl:param name="docSub" />

  <xsl:variable name="scenario">
    <xsl:call-template name="scenario">
      <xsl:with-param name="docSec"><xsl:value-of select="$docSec" /></xsl:with-param>
      <xsl:with-param name="docSub"><xsl:value-of select="$docSub" /></xsl:with-param>
    </xsl:call-template>
  </xsl:variable>

  <!--<xsl:element name="p">Scénario : <xsl:value-of select="$scenario" /></xsl:element>-->

  <xsl:if test="$scenario != ''">
    <xsl:choose>

      <!-- Scénario : Lecteur -->

      <xsl:when test="$scenario = 'lecteur'">
	<xsl:call-template name="format-goToNext-element">
	  <xsl:with-param name="eName">Introduction</xsl:with-param>
	  <xsl:with-param name="eUrl">intro.php</xsl:with-param>
	  <xsl:with-param name="eType">name-only</xsl:with-param>
	</xsl:call-template>
      </xsl:when>

      <!-- Scénario : Introduction -->

      <xsl:when test="$scenario = 'intro'">
        <!-- <xsl:for-each select="document('../neo_toc.xml')//section"> -->
	<xsl:for-each select="document('../data/neo_toc.xml')//section">
          <xsl:if test="@name = 'a'">
            <xsl:for-each select="./subsection">
              <xsl:if test="@n = '-1'">
		<xsl:call-template name="format-goToNext-element">
		  <xsl:with-param name="eName"><xsl:value-of select="name" /></xsl:with-param>
		  <xsl:with-param name="eUrl">a/<xsl:value-of select="url" /></xsl:with-param>
		  <xsl:with-param name="eType">subsection</xsl:with-param>
		</xsl:call-template>
              </xsl:if>
            </xsl:for-each>
          </xsl:if>
        </xsl:for-each>
      </xsl:when>

      <!-- Scénario : Table des matières de la section A -->

      <xsl:when test="$scenario = 'first_sec_toc'">
        <xsl:call-template name="goToNext-sub">
          <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
          <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
        </xsl:call-template>
      </xsl:when>

      <!-- Scénario : Table des matières d’une section -->

      <xsl:when test="$scenario = 'sec_toc'">
        <xsl:call-template name="goToNext-sub">
          <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
          <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
        </xsl:call-template>
      </xsl:when>

      <!-- Scénario : Première sous-section d’une section -->

      <xsl:when test="$scenario = 'first_sub'">
        <xsl:call-template name="goToNext-sub">
          <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
          <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
        </xsl:call-template>
      </xsl:when>

      <!-- Scénario : Sous-section -->

      <xsl:when test="$scenario = 'sub'">
        <xsl:call-template name="goToNext-sub">
          <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
          <xsl:with-param name="currentSubName"><xsl:value-of select="$docSub" /></xsl:with-param>
        </xsl:call-template>
      </xsl:when>

      <!-- Scénario : Dernière sous-section d’une section -->

      <xsl:when test="$scenario = 'last_sub'">
	<!-- Section suivante -->
        <xsl:call-template name="goToNext-sec">
          <xsl:with-param name="currentSecName"><xsl:value-of select="$docSec" /></xsl:with-param>
        </xsl:call-template>
      </xsl:when>

      <!-- Scénario : Table des matières de la dernière section (J) -->

      <xsl:when test="$scenario = 'last_sec_toc'">
	<xsl:call-template name="format-goToNext-element">
	  <xsl:with-param name="eName">Index</xsl:with-param>
	  <xsl:with-param name="eUrl">../index.php</xsl:with-param>
	  <xsl:with-param name="eType">name-only</xsl:with-param>
	</xsl:call-template>
      </xsl:when>

    </xsl:choose>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
