<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template name="text-toc">
  <xsl:param name="docSec" />
  <xsl:param name="docSub" />
  <xsl:element name="div">
    <xsl:attribute name="class">sec-toc</xsl:attribute>
    <xsl:element name="h6">
      <xsl:attribute name="class">title</xsl:attribute>
      Sommaire
    </xsl:element>
    <xsl:for-each select="/document/text//div/head">
      <xsl:variable name="level">
	<xsl:choose>
	  <xsl:when test="count(ancestor::div) = '1'">2</xsl:when>
	  <xsl:when test="count(ancestor::div) = '2'">3</xsl:when>
	  <xsl:when test="count(ancestor::div) = '3'">4</xsl:when>
	  <xsl:when test="count(ancestor::div) = '4'">5</xsl:when>
	  <xsl:when test="count(ancestor::div) = '5'">6</xsl:when>
	  <xsl:otherwise>2</xsl:otherwise>
	</xsl:choose>
      </xsl:variable>

      <xsl:variable name="docAnchor">
	<xsl:choose>
	  <xsl:when test="/document/meta/type = 'faqtext'">
	    <xsl:value-of select="$docSec" /><xsl:value-of select="$docSub" />
	  </xsl:when>
	  <xsl:when test="/document/meta/type = 'front'">t</xsl:when>
	  <xsl:otherwise></xsl:otherwise>
	</xsl:choose>
      </xsl:variable>

      <xsl:variable name="headName">
	<xsl:choose>
	  <xsl:when test="$level = '2'">
	    <xsl:value-of select="ancestor::div/@n" />
	  </xsl:when>
	  <xsl:when test="$level = '3'">
	    <xsl:value-of select="../ancestor::div[1]/@n" />_<xsl:value-of select="../@n" />
	  </xsl:when>
	  <xsl:when test="$level = '4'">
	    <xsl:value-of select="../ancestor::div[2]/@n" />_<xsl:value-of select="../ancestor::div[1]/@n" />_<xsl:value-of select="../@n" />
	  </xsl:when>
	  <xsl:when test="$level = '5'">
	    <xsl:value-of select="../ancestor::div[3]/@n" />_<xsl:value-of select="../ancestor::div[2]/@n" />_<xsl:value-of select="../ancestor::div[1]/@n" />_<xsl:value-of select="../@n" />
	  </xsl:when>
	  <xsl:when test="$level = '6'">
	    <xsl:value-of select="../ancestor::div[4]/@n" />_<xsl:value-of select="../ancestor::div[3]/@n" />_<xsl:value-of select="../ancestor::div[2]/@n" />_<xsl:value-of select="../ancestor::div[1]/@n" />_<xsl:value-of select="../@n" />
	  </xsl:when>
	  <xsl:otherwise>
	  </xsl:otherwise>
	</xsl:choose>
      </xsl:variable>

      <xsl:variable name="headAnchor"><xsl:value-of select="$docAnchor" />-<xsl:value-of select="$headName" /></xsl:variable>

      <xsl:element name="p">
	<xsl:element name="a">
	  <xsl:attribute name="href">#<xsl:value-of select="$headAnchor" /></xsl:attribute>
	  <xsl:apply-templates />
	</xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
