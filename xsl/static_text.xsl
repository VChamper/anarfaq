<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!-- Paramètres de transformation ===================================== -->

<xsl:param name="path" select="'../'" />

<!-- Templates externes appelés via leur nom ========================== -->

<!-- Template de navigation -->
<xsl:include href="navigation.xsl" />
<!-- Template pour les miroirs du texte -->
<xsl:include href="mirrors.xsl" />
<!-- Template du lien vers la (sous-)section suivante -->
<xsl:include href="gotonext.xsl" />
<!-- Template du message d’information sur la traduction -->
<xsl:include href="text-translation-info.xsl" />
<!-- Template du pied de page -->
<xsl:include href="text-footer.xsl" />
<!-- Template du sommaire de sous-section -->
<xsl:include href="text-toc.xsl" />

<!-- Template de la page intégrale ==================================== -->

<xsl:template match="/">
<!-- Emplacement du texte dans la FAQ générale -->
<xsl:variable name="docSec"><xsl:value-of select="/document/meta/toc/section" /></xsl:variable>
<xsl:variable name="docSub"><xsl:value-of select="/document/meta/toc/subsection" /></xsl:variable>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title><xsl:value-of select="/document/meta/pagetitle" /></title>
    <xsl:element name="link">
      <xsl:attribute name="rel">alternate</xsl:attribute>
      <xsl:attribute name="type">application/rdf+xml</xsl:attribute>
      <xsl:attribute name="href">http://www.gnu.org/licenses/fdl-1.3.rdf</xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="type">text/css</xsl:attribute>
      <xsl:attribute name="href"><xsl:value-of select="$path" />inc/master.css</xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">icon</xsl:attribute>
      <xsl:attribute name="type">image/gif</xsl:attribute>
      <xsl:attribute name="href"><xsl:value-of select="$path" />inc/favicon.ico</xsl:attribute>
    </xsl:element>
  </head>
  <body class="text">
    <a name="back"></a>
    <header>
      <h1 class="left">La FAQ Anarchiste (francophone)</h1>
      <xsl:element name="div">
	<xsl:attribute name="class">navigation</xsl:attribute>
	<xsl:call-template name="mirrors">
	  <xsl:with-param name="docSec"><xsl:value-of select="$docSec" /></xsl:with-param>
	  <xsl:with-param name="docSub"><xsl:value-of select="$docSub" /></xsl:with-param>
	</xsl:call-template>
	<xsl:call-template name="navigation">
	  <xsl:with-param name="docSec"><xsl:value-of select="$docSec" /></xsl:with-param>
	  <xsl:with-param name="docSub"><xsl:value-of select="$docSub" /></xsl:with-param>
	</xsl:call-template>
      </xsl:element>
    </header>

    <!-- Information sur la traduction -->

    <xsl:if test="/document/meta/translation != 'complete'">
      <xsl:call-template name="text-translation-info">
	<xsl:with-param name="tstate">
	  <xsl:value-of select="/document/meta/translation" />
	</xsl:with-param>
      </xsl:call-template>
    </xsl:if>

    <!-- Table des matières -->

    <xsl:if test="not(/document/meta/toc/notable)">
      <xsl:call-template name="text-toc">
	<xsl:with-param name="docSec"><xsl:value-of select="$docSec" /></xsl:with-param>
	<xsl:with-param name="docSub"><xsl:value-of select="$docSub" /></xsl:with-param>
      </xsl:call-template>
    </xsl:if>

    <!-- Contenu -->

    <xsl:apply-templates />

    <!-- Lien vers la (sous)section suivante -->

    <xsl:call-template name="goToNext">
      <xsl:with-param name="docSec"><xsl:value-of select="$docSec" /></xsl:with-param>
      <xsl:with-param name="docSub"><xsl:value-of select="$docSub" /></xsl:with-param>
    </xsl:call-template>

    <!-- Retour en haut de page -->

    <div class="goback">
      <p><a href="#back">Retour en haut de la page</a></p>
    </div>

    <!-- Licence -->

    <xsl:call-template name="text-footer" />

  </body>
</html>
</xsl:template>

<!-- Commodités ======================================================= -->

<xsl:include href="text-lazy.xsl" />

<!-- Éléments HTML restitués tels quels =============================== -->

<xsl:include href="text-html.xsl" />

<!-- Éléments du document à ne pas afficher =========================== -->

<xsl:include href="text-hide.xsl" />

<!-- Titres =========================================================== -->

<!-- Titre de la section -->
<xsl:template match="meta/head">
  <xsl:choose>
    <xsl:when test="line">
      <xsl:for-each select="line">
	<xsl:element name="h1"><xsl:apply-templates /></xsl:element>
      </xsl:for-each>
    </xsl:when>
    <xsl:otherwise>
      <xsl:element name="h1"><xsl:apply-templates /></xsl:element>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Titre d’une partie de la section -->
<xsl:template match="head">
  <xsl:variable name="level">
    <xsl:choose>
      <xsl:when test="count(ancestor::div) = '1'">2</xsl:when>
      <xsl:when test="count(ancestor::div) = '2'">3</xsl:when>
      <xsl:when test="count(ancestor::div) = '3'">4</xsl:when>
      <xsl:when test="count(ancestor::div) = '4'">5</xsl:when>
      <xsl:when test="count(ancestor::div) = '5'">6</xsl:when>
      <xsl:otherwise>2</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- Emplacement du texte dans la FAQ générale -->

  <xsl:variable name="docSec"><xsl:value-of select="/document/meta/toc/section" /></xsl:variable>
  <xsl:variable name="docSub"><xsl:value-of select="/document/meta/toc/subsection" /></xsl:variable>

  <!-- Nom d’ancre "de base" -->

  <xsl:variable name="docAnchor">
    <xsl:choose>
      <xsl:when test="/document/meta/type = 'faqtext'">
	<xsl:value-of select="$docSec" /><xsl:value-of select="$docSub" />
      </xsl:when>
      <xsl:when test="/document/meta/type = 'front'">t</xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <!-- Nom d’ancre du titre -->

  <xsl:variable name="headName">
    <xsl:choose>
      <xsl:when test="$level = '2'">
	<xsl:value-of select="ancestor::div/@n" />
      </xsl:when>
      <xsl:when test="$level = '3'">
	<xsl:value-of select="../ancestor::div[1]/@n" />_<xsl:value-of select="../@n" />
      </xsl:when>
      <xsl:when test="$level = '4'">
	<xsl:value-of select="../ancestor::div[2]/@n" />_<xsl:value-of select="../ancestor::div[1]/@n" />_<xsl:value-of select="../@n" />
      </xsl:when>
      <xsl:when test="$level = '5'">
	<xsl:value-of select="../ancestor::div[3]/@n" />_<xsl:value-of select="../ancestor::div[2]/@n" />_<xsl:value-of select="../ancestor::div[1]/@n" />_<xsl:value-of select="../@n" />
      </xsl:when>
      <xsl:when test="$level = '6'">
	<xsl:value-of select="../ancestor::div[4]/@n" />_<xsl:value-of select="../ancestor::div[3]/@n" />_<xsl:value-of select="../ancestor::div[2]/@n" />_<xsl:value-of select="../ancestor::div[1]/@n" />_<xsl:value-of select="../@n" />
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="headAnchor"><xsl:value-of select="$docAnchor" />-<xsl:value-of select="$headName" /></xsl:variable>

  <!-- Nom d’ancre du titre -->

  <xsl:element name="a">
    <xsl:attribute name="name"><xsl:value-of select="$headAnchor" /></xsl:attribute>
  </xsl:element>

  <xsl:element name="h{$level}"><xsl:apply-templates /></xsl:element>
</xsl:template>

<!-- Références (section, livre, index) =============================== -->

<!-- Référence à une section de la FAQ -->
<xsl:template match="ref">
  <!-- Emplacement du texte dans la FAQ générale -->
  <xsl:variable name="docSec"><xsl:value-of select="/document/meta/toc/section" /></xsl:variable>
  <xsl:variable name="docSub"><xsl:value-of select="/document/meta/toc/subsection" /></xsl:variable>

  <xsl:variable name="refTree">
    <xsl:choose>
      <xsl:when test="$docSec = @sec"></xsl:when>
      <xsl:otherwise>../<xsl:value-of select="@sec" />/</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="refFile">
    <xsl:choose>
      <xsl:when test="@sub">
	<xsl:choose>
	  <xsl:when test="@sec != $docSec or @sub != $docSub"><xsl:value-of select="@sec" />_<xsl:value-of select="@sub" />.php</xsl:when>
	  <xsl:otherwise></xsl:otherwise>
	</xsl:choose>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="@sec" />_toc.php</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="refAnchor">
    <xsl:choose>
      <xsl:when test="@anchor">#<xsl:value-of select="@sec" /><xsl:value-of select="@sub" />-<xsl:value-of select="@anchor" /></xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:element name="a">
    <xsl:attribute name="href">
      <xsl:value-of select="$refTree" /><xsl:value-of select="$refFile" /><xsl:value-of select="$refAnchor" />
    </xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<!-- Renvoit vers l’index de noms / de groupes politiques -->
<xsl:template match="idx">
  <xsl:element name="a">
    <xsl:attribute name="class">
      <xsl:choose>
        <xsl:when test="@s = 'name'">idxref</xsl:when>
        <xsl:when test="@s = 'group'">idxref</xsl:when>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="href">
      <xsl:value-of select="$path" />
      <xsl:choose>
	<xsl:when test="@s = 'name'">idx/names.php#<xsl:value-of select="@id" /></xsl:when>
	<xsl:when test="@s = 'group'">idx/groups.php#<xsl:value-of select="@id" /></xsl:when>
      </xsl:choose>
    </xsl:attribute>

    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<!-- Renvoit vers la lecture en ligne d’un ouvrage -->
<xsl:template match="book">
  <xsl:element name="a">
    <xsl:attribute name="class">bookref</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@author">
	  <xsl:attribute name="href"><xsl:value-of select="$path" />books.php#<xsl:value-of select="@author" />_<xsl:value-of select="@id" /></xsl:attribute>
        </xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>☰
  </xsl:element>
</xsl:template>

<!-- Paragraphes ====================================================== -->

<!-- Premier paragraphe du <div>. Pas d’alinéa. -->
<xsl:template match="text//div/p[1]">
  <xsl:variable name="fullyTranslated">
    <xsl:value-of select="/document/meta/translation = 'complete'" />
  </xsl:variable>

  <!-- Ancre de paragraphe, à moins que nocount dans @class -->

  <xsl:if test="$fullyTranslated">
    <xsl:choose>
      <xsl:when test="@class">
	<xsl:if test="not(contains(@class,'nocount'))">
	  <xsl:element name="a">
	    <xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
	  </xsl:element>
	</xsl:if>
      </xsl:when>
      <xsl:otherwise>
	  <xsl:element name="a">
	    <xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
	  </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:if>

  <!-- Element p -->

  <xsl:element name="p">
    <!-- Classe du paragraphe -->

    <xsl:choose>
      <xsl:when test="@class">
	<xsl:attribute name="class"><xsl:value-of select="@class" /> noalinea</xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
	<xsl:attribute name="class">noalinea</xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <!-- Lien de reference du paragraphe -->

    <xsl:if test="$fullyTranslated">
      <xsl:element name="a">
	<xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
      </xsl:element>
    </xsl:if>

    <!-- Contenu -->

    <xsl:apply-templates />

    <!-- Lien de reference du paragraphe -->

    <xsl:if test="$fullyTranslated">
      <xsl:element name="a">
	<xsl:attribute name="href">#<xsl:value-of select="@id" /></xsl:attribute>
	<xsl:attribute name="class">pl</xsl:attribute> 
      </xsl:element>
    </xsl:if>

  </xsl:element>
</xsl:template>

<!-- Paragraphe normal -->
<xsl:template match="text//div/p[position() > 1]">
  <xsl:variable name="fullyTranslated">
    <xsl:value-of select="/document/meta/translation = 'complete'" />
  </xsl:variable>

  <!-- Ancre de paragraphe, à moins que nocount dans @class -->

  <xsl:if test="$fullyTranslated">
    <xsl:choose>
      <xsl:when test="@class">
	<xsl:if test="not(contains(@class,'nocount'))">
	  <xsl:element name="a">
	    <xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
	  </xsl:element>
	</xsl:if>
      </xsl:when>
      <xsl:otherwise>
	<xsl:element name="a">
	  <xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
	</xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:if>

  <!-- Element p -->

  <xsl:element name="p">
    <!-- Classe du paragraphe -->

    <xsl:if test="@class">
      <xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute>
    </xsl:if>

    <!-- Lien de reference du paragraphe -->

    <xsl:if test="$fullyTranslated">
      <xsl:element name="a">
	<xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
      </xsl:element>
    </xsl:if>

    <!-- Contenu -->

    <xsl:apply-templates />

    <!-- Lien de reference du paragraphe -->

    <xsl:if test="$fullyTranslated">
      <xsl:if test="not(contains(@class,'noanchor'))">
	<xsl:element name="a">
	  <xsl:attribute name="href">#<xsl:value-of select="@id" /></xsl:attribute>
	  <xsl:attribute name="class">pl</xsl:attribute> 
	</xsl:element>
      </xsl:if>
    </xsl:if>

  </xsl:element>
</xsl:template>

<!-- Citations dans le corps du texte ================================= -->

<xsl:template match="head/cit">«&#160;<xsl:apply-templates />&#160;»</xsl:template>
<xsl:template match="text//div/p/cit">«&#160;<xsl:apply-templates />&#160;»</xsl:template>
<xsl:template match="text//div/blockquote/cit">«&#160;<xsl:apply-templates />&#160;»</xsl:template>
<xsl:template match="text//div/message/p/cit">«&#160;<xsl:apply-templates />&#160;»</xsl:template>
<xsl:template match="footnotes//system/fn/cit">«&#160;<xsl:apply-templates />&#160;»</xsl:template>

<!-- <cit> imbriqués -->
<xsl:template match="head/cit/cit">«&#160;<xsl:apply-templates />&#160;»</xsl:template>
<xsl:template match="text//div/p/cit/cit">“<xsl:apply-templates />”</xsl:template>
<xsl:template match="text//div/blockquote/cit/cit">“<xsl:apply-templates />”</xsl:template>
<xsl:template match="footnotes//system/fn/cit/cit">“<xsl:apply-templates />”</xsl:template>

<!-- Bloc de citation ================================================= -->

<xsl:template match="text/div/blockquote">
  <xsl:element name="blockquote">
    <xsl:if test="@class">
      <xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute>
    </xsl:if>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="text/div/source">
  <xsl:if test="author">
    <xsl:element name="blockquote">
      <xsl:attribute name="class">citation_author</xsl:attribute>
      <xsl:element name="a">
	<xsl:attribute name="class">idxref</xsl:attribute>
	<xsl:attribute name="href"><xsl:value-of select="$path" />idx/names.php#<xsl:value-of select="author/@id" /></xsl:attribute>
	<xsl:value-of select="author" />
      </xsl:element>
    </xsl:element>
  </xsl:if>
  <xsl:if test="book or pages or @opcit">
    <xsl:element name="blockquote">
      <xsl:attribute name="class">citation_source</xsl:attribute>
      <xsl:choose>
	<xsl:when test="@opcit">op.cit</xsl:when>
	<xsl:otherwise>
	  <xsl:if test="book">
	    <xsl:if test="book/title[@fr = 'yes']">
	      <xsl:value-of select="book/title[@fr = 'yes']" />
	    </xsl:if>
	    <xsl:if test="book/title[@other = 'yes']">
	      <xsl:if test="book/title[@fr = 'yes']">&#160;</xsl:if>
	      <i>[</i><xsl:value-of select="book/title[@other = 'yes']" /><i>]</i>
	    </xsl:if>
	  </xsl:if>
	</xsl:otherwise>
      </xsl:choose>
      <xsl:if test="pages"><i><xsl:text>, </xsl:text>p.<xsl:value-of select="pages" /></i></xsl:if>
    </xsl:element>
  </xsl:if>
</xsl:template>

<!-- Message au lecteur =============================================== -->

<xsl:template match="message">
  <xsl:element name="div">
    <xsl:attribute name="class">toReader</xsl:attribute>
    <xsl:element name="p">
      <xsl:attribute name="class">messageTitle</xsl:attribute>
      À l’attention du lecteur
    </xsl:element>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="message/p">
  <xsl:element name="p">
    <xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<!-- Appels de notes / Notes de bas de page =========================== -->

<!-- Appel de note -->
<xsl:template match="text/div//fn">
  <xsl:variable name="fnAnchorBase">
    <xsl:choose>
      <xsl:when test="@s"><xsl:value-of select="@s" />-<xsl:value-of select="@id" /></xsl:when>
      <xsl:otherwise>normal-<xsl:value-of select="@id" /></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:element name="a">
    <xsl:attribute name="class">
      <xsl:choose>
	<xsl:when test="@s = 'normal'">iref</xsl:when>
	<xsl:when test="@s = 'explain'">eref</xsl:when>
	<xsl:otherwise>iref</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="href">#<xsl:value-of select="$fnAnchorBase" /></xsl:attribute>
    <xsl:attribute name="name">ref-<xsl:value-of select="$fnAnchorBase" /></xsl:attribute>
    <xsl:choose>
      <xsl:when test=". != ''"><xsl:apply-templates /></xsl:when>
      <xsl:otherwise>[?]</xsl:otherwise>
    </xsl:choose>
  </xsl:element>
</xsl:template>

<!-- Bloc de notes de bas de page -->
<xsl:template match="footnotes">
  <xsl:element name="div">
    <xsl:attribute name="class">inline-refs</xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<!-- Notes de base de page, par système de notes -->
<xsl:template match="footnotes/system">
  <xsl:element name="p">
    <xsl:choose>
      <xsl:when test="@name = 'normal'">Notes</xsl:when>
      <xsl:when test="@name = 'explain'">Explications</xsl:when>
    </xsl:choose>
  </xsl:element>
  <xsl:element name="ol">
    <xsl:for-each select="fn">
      <xsl:element name="li">
	<xsl:apply-templates />
	<xsl:element name="a"><xsl:attribute name="name"><xsl:value-of select="../@name" />-<xsl:value-of select="@id" /></xsl:attribute><xsl:attribute name="href">#ref-<xsl:value-of select="../@name" />-<xsl:value-of select="@id" /></xsl:attribute>↩</xsl:element>
      </xsl:element>
    </xsl:for-each>
  </xsl:element>
</xsl:template>

<!-- Lien <a> ========================================================= -->

<xsl:template match="a">
  <xsl:element name="a">
    <xsl:attribute name="href"><xsl:value-of select="@href" /></xsl:attribute>
    <xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
