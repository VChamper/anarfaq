<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!-- Paramètres de transformation ===================================== -->

<!--<xsl:param name="selectedSection">all</xsl:param>-->
<xsl:param name="selectedSection">a</xsl:param>
<xsl:param name="path" select="'../'" />

<xsl:template match="section">
  <xsl:variable name="secName">
    <xsl:value-of select="@name" />
  </xsl:variable>
  <xsl:variable name="secNAME">
    <xsl:value-of select="translate($secName,'abcdefghij','ABCDEFGHIJ')" />
  </xsl:variable>

  <xsl:choose>

    <xsl:when test="$selectedSection = 'all'">
      <xsl:element name="p">
	<xsl:attribute name="class">secToc0</xsl:attribute>
	Section <xsl:value-of select="subsection[@n='-1']/name" />
      </xsl:element>
      <xsl:for-each select="subsection">
	<xsl:sort select="@n" order="ascending" data-type="number" />
	<xsl:if test="@n > -1">

	  <xsl:element name="p">
	    <xsl:attribute name="class">secToc1</xsl:attribute>
	    <xsl:element name="a">
	      <xsl:attribute name="href">
		<xsl:value-of select="../@name" />/<xsl:value-of select="url" />.php
	      </xsl:attribute>
	      <xsl:value-of select="name" />
	    </xsl:element>
	  </xsl:element>

	  <xsl:for-each select="subs/s">
	    <xsl:element name="p">
	      <xsl:attribute name="class">secToc2</xsl:attribute>
	      <xsl:element name="a">
		<xsl:attribute name="href"><xsl:value-of select="$secName" />/<xsl:value-of select="../../url" />.php#<xsl:value-of select="../../../@name" /><xsl:value-of select="../../@n" />-<xsl:value-of select="@n" /></xsl:attribute>
		<xsl:value-of select="$secNAME" />.<xsl:value-of select="../../@n" /><xsl:value-of select="../@n" />.<xsl:value-of select="@n" />&#160;–&#160;<xsl:value-of select="." />
	      </xsl:element>
	    </xsl:element>
	  </xsl:for-each>

	</xsl:if>
      </xsl:for-each>
    </xsl:when>

    <xsl:otherwise>
      <xsl:element name="h1">
	Section <xsl:value-of select="subsection[@n='-1']/name" />
      </xsl:element>
      <xsl:for-each select="subsection">
	<xsl:sort select="@n" order="ascending" data-type="number" />
	<xsl:if test="@n > -1">

	  <xsl:element name="p">
	    <xsl:attribute name="class">secToc1</xsl:attribute>
	    <xsl:element name="a">
	      <xsl:attribute name="href"><xsl:value-of select="url" />.php</xsl:attribute>
	      <xsl:value-of select="name" />
	    </xsl:element>
	  </xsl:element>

	  <xsl:for-each select="subs/s">
	    <xsl:element name="p">
	      <xsl:attribute name="class">secToc2</xsl:attribute>
	      <xsl:element name="a">
		<xsl:attribute name="href">
		  <xsl:value-of select="../../url" />.php#<xsl:value-of select="../../../@name" /><xsl:value-of select="../../@n" />-<xsl:value-of select="@n" />
		</xsl:attribute>
		<xsl:value-of select="$secNAME" />.<xsl:value-of select="../../@n" /><xsl:value-of select="../@n" />.<xsl:value-of select="@n" />&#160;–&#160;<xsl:value-of select="." />
	      </xsl:element>
	    </xsl:element>
	  </xsl:for-each>

	</xsl:if>
      </xsl:for-each>
  </xsl:otherwise>

  </xsl:choose>
</xsl:template>

<!-- Éléments du document à ne pas afficher =========================== -->

<xsl:template match="/front/text/name"></xsl:template>
<xsl:template match="/front/text/url"></xsl:template>

<!-- Template de la page intégrale ==================================== -->

<xsl:template match="/">
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <link href="inc/master.css" rel="stylesheet" />
    <link rel='stylesheet' type='text/css' href='../inc/master.css' />
    <link rel='icon' href='../inc/favicon.ico' type='image/gif' />
    <xsl:text disable-output-escaping="yes">&lt;?php </xsl:text>
    require "<xsl:value-of select="$path" />inc/loadaccess.php";
    <xsl:text disable-output-escaping="yes">?&gt;</xsl:text>
    <xsl:choose>
      <xsl:when test="$selectedSection = 'all'">
	<xsl:element name="title">FAQ Anarchiste - Table des matières</xsl:element>
      </xsl:when>
      <xsl:otherwise>
	<xsl:variable name="sectionNode">
	  <xsl:value-of select="toc/content/section[@name = $selectedSection]/@name" />
	</xsl:variable>
	<xsl:variable name="sectionNAME">
	  <xsl:value-of select="translate($sectionNode,'abcdefghij','ABCDEFGHIJ')" />
	</xsl:variable>
	<xsl:element name="title">FAQ Anarchiste - Section <xsl:value-of select="$sectionNAME" /></xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </head>
  <body>
    <header>
      <h1 class="left"><a href="{$path}index.html">La FAQ Anarchiste (francophone)</a></h1>
    </header>
    <nav>
        <div class='navigation'>
	<xsl:choose>
	  <xsl:when test="$selectedSection = 'all'">
	  </xsl:when>
	  <xsl:otherwise>
	    <xsl:variable name="sectionNode">
	      <xsl:value-of select="toc/content/section[@name = $selectedSection]/@name" />
	    </xsl:variable>
	    <xsl:text disable-output-escaping="yes">&lt;?php </xsl:text>
	    require "<xsl:value-of select="$path" />inc/mirrors.php"; 
	    altLangDiv("<xsl:value-of select='$sectionNode' />","toc"); 

	    require "<xsl:value-of select="$path" />inc/toc.php"; 
	    echo_nav("<xsl:value-of select='$sectionNode' />","toc");
	    <xsl:text disable-output-escaping="yes">?&gt; </xsl:text>
	  </xsl:otherwise>
	</xsl:choose>
        </div>
    </nav>
     
  <xsl:choose>

    <!-- Génération de la table des matières générale -->
    <xsl:when test="$selectedSection = 'all'">
      <xsl:element name="h1">Table des matières générale</xsl:element>

      <xsl:element name="p">
	<xsl:attribute name="class">secToc0</xsl:attribute>
	Liminaires
      </xsl:element>

      <xsl:for-each select="toc/front/text">
	<xsl:sort select="@order" order="ascending" data-type="number" />
	<xsl:element name="p">
	  <xsl:attribute name="class">secToc1</xsl:attribute>
	  <xsl:element name="a">
	    <xsl:attribute name="href"><xsl:value-of select="url" />.php</xsl:attribute>
	    <xsl:value-of select="name" />
	  </xsl:element>
	</xsl:element>
      </xsl:for-each>

      <xsl:for-each select="toc/content/section">
	<xsl:apply-templates select="." />
      </xsl:for-each>
    </xsl:when>

    <!-- Génération de la table des matières correspondant à $selectedSection -->
    <xsl:otherwise>
      <xsl:apply-templates select="toc/content/section[@name = $selectedSection]" />
    </xsl:otherwise>

  </xsl:choose>

    <!-- Retour en haut de page -->

    <div class="goback">
      <p><a href="#back">Retour en haut de la page</a></p>
    </div>

    <!-- Licence -->

    <footer>
	<div>"An Anarchist FAQ", Version 9.0 Copyright (C) 1995-2001 The
	Anarchist FAQ Editorial Collective: Iain McKay, Gary Elkin, Dave
	Neal, Ed Boraas </div>

	<div>Permission is granted to copy, distribute and/or modify this
	document under the terms of the GNU Free Documentation License,
	Version 1.1 or any later version published by the Free Software
	Foundation. See the GNU Free Documentation License for more details
	at <a href="http://www.gnu.org/">http://www.gnu.org/</a></div>

	<div>Une FAQ Anarchiste (francophone) Version 1 Copyright (C) -
	2003-2017 - faqanarchiste.free.fr . Vous pouvez copier, distribuer
	ou modifier ce document selon les termes de la licence GNU de
	documentation libre, dans sa version 1.3 ou dans toute version
	ultérieure publiée par la Free Software Foundation ; sans Section
	Invariante, sans Texte De Première De Couverture, et sans Texte De
	Quatrième De Couverture. Une copie de cette licence est incluse dans
	la section intitulée "Licence GNU de documentation libre".</div>

	<div>La FAQ Anarchiste (francophone) Version 1 Copyright (C) -
	2017 - tviblindi.legtux.org. Vous pouvez copier, distribuer
	ou modifier ce document selon les termes de la licence GNU de
	documentation libre, dans sa version 1.3 ou dans toute version
	ultérieure publiée par la Free Software Foundation ; sans Section
	Invariante, sans Texte De Première De Couverture, et sans Texte De
	Quatrième De Couverture. Une copie de cette licence est incluse dans
	la section intitulée "<a href="http://tviblindi.legtux.org/afaq/fdl.php">Licence 
	GNU de documentation libre</a>".</div>
    </footer>

  </body>
</html>
</xsl:template>

</xsl:stylesheet>
