<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="utf-8" indent="yes" />

<xsl:template match="/">
  <xsl:element name="toc">
    <xsl:for-each select="toc/atroot">
      <xsl:element name="file">
        <xsl:element name="name"><xsl:value-of select="file/name" /></xsl:element>
        <xsl:element name="url"><xsl:value-of select="file/url" /></xsl:element>
      </xsl:element>
    </xsl:for-each>
    <xsl:for-each select="toc/content/section">
      <xsl:for-each select="subsection">
        <xsl:element name="file">
          <xsl:attribute name="sec"><xsl:value-of select="../@name" /></xsl:attribute>
          <xsl:attribute name="sub">
            <xsl:choose>
              <xsl:when test="@position &lt; 0">toc</xsl:when>
              <xsl:otherwise><xsl:value-of select="@position" /></xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:element name="name">
            <xsl:value-of select="name" disable-output-escaping="yes" />
          </xsl:element>
          <xsl:element name="url">
            <xsl:value-of select="url" />
          </xsl:element>
        </xsl:element>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
