<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" />

<!-- Paramètres de transformation ===================================== -->

<!--
  Type de transformation:
  dynamic => PHP
  static => HTML (sans accessibilité)
-->
<xsl:param name="target" select="'dynamic'" />

<!-- Templates externes appelés via leur nom ========================== -->

<!-- Template de navigation -->
<xsl:include href="navigation.xsl" />
<!-- Template du pied de page -->
<xsl:include href="text-footer.xsl" />

<!-- Template de la page intégrale ==================================== -->

<xsl:template match="/">
<xsl:variable name="ext">
  <xsl:choose>
    <xsl:when test="$target = 'static'">.html</xsl:when>
    <xsl:otherwise>.php</xsl:otherwise>
  </xsl:choose>
</xsl:variable>
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>Table des noms propres par fréquence – FAQ Anarchiste (francophone)</title>
    <link rel="stylesheet" type="text/css" href="../inc/style.css" /> 
    <link rel="stylesheet" type="text/css" href="../inc/names_freq.css" /> 
    <link rel="icon" href="../inc/favicon.ico" type="image/gif" />
    <xsl:if test="$target = 'dynamic'">
<xsl:text disable-output-escaping="yes">
&lt;?php require "../inc/loadaccess_up.php"; ?&gt;
</xsl:text>
    </xsl:if>
  </head>
  <body>
    <a id="back"></a>

    <header>
      <h1 class="left">La FAQ Anarchiste (francophone)</h1>
    </header>

    <nav>
      <div class="navigation">
	<xsl:choose>
	  <xsl:when test="$target = 'dynamic'">
<xsl:text disable-output-escaping="yes">
&lt;?php require "../inc/mirrors.php"; altLangDiv("none", 1); ?&gt;
&lt;?php require "../inc/toc.php"; echo_nav("names", 1); ?&gt;
</xsl:text>
	  </xsl:when>
	  <xsl:when test="$target = 'static'">
	    <div class="alt-lang"></div>
	    <xsl:call-template name="navigation">
	      <xsl:with-param name="docSec">names</xsl:with-param>
	      <xsl:with-param name="docSub">1</xsl:with-param>
	      <xsl:with-param name="docTarget"><xsl:value-of select="$target" /></xsl:with-param>
	    </xsl:call-template>
	  </xsl:when>
	</xsl:choose>
      </div>
    </nav>

    <xsl:element name="main">
      <xsl:element name="section">

    <h1>Table des noms propres par fréquence d’apparition</h1>

    <p class="centered"><a href="names{$ext}">Retour à l’index des noms propres</a></p>

    <xsl:element name="table">
      <xsl:element name="tr">
	<xsl:element name="th">Score</xsl:element>
	<xsl:element name="th">Nom</xsl:element>
	<xsl:element name="th">Compte</xsl:element>
	<xsl:element name="th">Statut</xsl:element>
      </xsl:element>
      <xsl:for-each select="index/letter/entry">
	<xsl:sort select="count(locations/loc)" data-type="number" order="descending"/>
	<xsl:element name="tr">
	  <xsl:element name="td">
	    <xsl:attribute name="class">idxnumber</xsl:attribute>
	    <xsl:if test="position() &lt; 10">0</xsl:if><xsl:value-of select="position()" />
	  </xsl:element>
	  <xsl:element name="td">
	    <xsl:element name="a">
	      <xsl:attribute name="href">
		<xsl:choose>
		  <xsl:when test="$target = 'static'">names.html#<xsl:value-of select="@id"/></xsl:when>
		  <xsl:otherwise>names.php#<xsl:value-of select="@id"/></xsl:otherwise>
		</xsl:choose>
	      </xsl:attribute>
	      <xsl:value-of select="last_name" />
	    </xsl:element>
	    <xsl:if test="first_name">
	      <xsl:text>, </xsl:text>
	      <xsl:value-of select="first_name" />
	    </xsl:if>
	  </xsl:element>
	  <xsl:element name="td">
	    <xsl:attribute name="class">idxcount</xsl:attribute>
	    <xsl:value-of select="count(locations/loc)" />
	  </xsl:element>
	  <xsl:element name="td">
	    <xsl:if test="infos/tag">
	      <xsl:for-each select="infos/tag">
		<xsl:choose>
		  <xsl:when test=". = 'anarchist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_anarchist</xsl:attribute>Anarchiste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'antiracist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_anarchist</xsl:attribute>Antiraciste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'marxist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_marxist</xsl:attribute>Marxiste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'capitalist_economist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_capitalist</xsl:attribute>Capitaliste (Économiste)</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'socdem'">
		    <xsl:element name="span"><xsl:attribute name="class">c_capitalist</xsl:attribute>Socialiste d’état&#160;/&#160;Social-démocrate</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'mutualist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_mutualist</xsl:attribute>Anarcho-mutualiste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'individualist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_mutualist</xsl:attribute>Individualiste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'a_syndicalist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_red</xsl:attribute>Anarcho-syndicaliste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'pacifist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_white</xsl:attribute>Pacifiste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'a_religious'">
		    <xsl:element name="span"><xsl:attribute name="class">c_white</xsl:attribute>Anarcho-religieu·x·se</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'a_ancom'">
		    <xsl:element name="span"><xsl:attribute name="class">c_red</xsl:attribute>Anarcho-communiste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'bolchevik'">
		    <xsl:element name="span"><xsl:attribute name="class">c_capitalist</xsl:attribute>Capitaliste d’état</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'bolchevik_politician'">
		    <xsl:element name="span"><xsl:attribute name="class">c_capitalist</xsl:attribute>Capitaliste d’état (Politicien)</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'primitivist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_primitivist</xsl:attribute>Primitiviste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'municipalist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_municipalist</xsl:attribute>Municipaliste libertaire</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'synthesist'">
		    <xsl:element name="span"><xsl:attribute name="class"></xsl:attribute>Synthésiste</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'historian'">
		    <xsl:element name="span"><xsl:attribute name="class">c_historian</xsl:attribute>Scientifique (Historien·ne)</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'scientist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_scientist</xsl:attribute>Scientifique (Non-historien·ne)</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'feminist'">
		    <xsl:element name="span"><xsl:attribute name="class">c_feminist</xsl:attribute>Féministe</xsl:element>
		  </xsl:when>
		  <xsl:when test=". = 'philosopher'">Philosophe</xsl:when>
		  <xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
		</xsl:choose>
		<xsl:if test="position() != last()"> ; </xsl:if>
	      </xsl:for-each>
	    </xsl:if>
	  </xsl:element>
	</xsl:element>
      </xsl:for-each>
    </xsl:element>

    </xsl:element>
  </xsl:element>

    <!-- Licence -->

    <xsl:call-template name="text-footer" />
  </body>
</html>
</xsl:template>

</xsl:stylesheet>
