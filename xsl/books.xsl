<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!-- Paramètres de transformation ===================================== -->

<!-- Chemin des médias par rapport au fichier en cours de génération -->
<xsl:param name="path" />
<!--
  Type de transformation:
  dynamic => PHP
  static => HTML (sans accessibilité)
-->
<xsl:param name="target" select="'dynamic'" />

<!-- Templates externes appelés via leur nom ========================== -->

<!-- Template du pied de page -->
<xsl:include href="text-footer.xsl" />

<!-- Templates de la biblio =========================================== -->

<xsl:template name="letter-link">
  <xsl:param name="l" />
  <xsl:element name="div">
    <xsl:attribute name="class">letter</xsl:attribute>
    <xsl:element name="a">
      <xsl:attribute name="href"><xsl:text>#</xsl:text><xsl:value-of select="$l" /></xsl:attribute>
      <xsl:value-of select="$l" />
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="opus-link-item">
  <xsl:param name="url" />
  <xsl:param name="lang" />
  <xsl:param name="text" />

  <xsl:element name="li">
    <xsl:element name="a">
      <xsl:attribute name="href">
	<xsl:value-of select="$url" />
      </xsl:attribute>
      <xsl:if test="$lang">
	<xsl:attribute name="class">
	  <xsl:text>lang-</xsl:text><xsl:value-of select="$lang" />
	</xsl:attribute>
      </xsl:if>
      <xsl:value-of select="$text" />
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="author-title">
  <xsl:param name="first_name" />
  <xsl:param name="last_name" />
  <xsl:param name="type" />
  <xsl:param name="id" />

  <xsl:element name="a">
    <xsl:attribute name="name"><xsl:value-of select="$id" /></xsl:attribute>
  </xsl:element>

  <xsl:element name="h3">
    <xsl:choose>
      <xsl:when test="$first_name">
	<xsl:value-of select="$last_name" /><xsl:text>, </xsl:text><xsl:value-of select="$first_name" />
      </xsl:when>
      <xsl:otherwise>
	<xsl:value-of select="$last_name" />
      </xsl:otherwise>
    </xsl:choose>&#160;
    <xsl:element name="a">
      <xsl:choose>
	<xsl:when test="$type = 'group'">
	  <xsl:attribute name="href">idx/groups.html#<xsl:value-of select="$id" /></xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:attribute name="href">idx/names.html#<xsl:value-of select="$id" /></xsl:attribute>
	</xsl:otherwise>
      </xsl:choose>
      [index]
    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template name="letter-title">
  <xsl:param name="l" />
  <xsl:element name="h2">
    <xsl:attribute name="id"><xsl:value-of select="$l" /></xsl:attribute>
    <xsl:value-of select="$l" />
  </xsl:element>
</xsl:template>

<!-- Template de la page intégrale ==================================== -->

<xsl:template match="/">
<html>
<!-- 
La FAQ Anarchiste (francophone).

  Liste des ouvrages cités et lisibles en ligne.
    Feuille de transformation de données
-->
  <head>
    <meta charset="utf-8" />
    <title>FAQ Anarchiste – Ouvrages en ligne</title>
    <xsl:element name="link">
      <xsl:attribute name="rel">alternate</xsl:attribute>
      <xsl:attribute name="type">application/rdf+xml</xsl:attribute>
      <xsl:attribute name="href">http://www.gnu.org/licenses/fdl-1.3.rdf</xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="type">text/css</xsl:attribute>
      <xsl:attribute name="href"><xsl:value-of select="$path" />inc/style.css</xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="type">text/css</xsl:attribute>
      <xsl:attribute name="href"><xsl:value-of select="$path" />inc/books.css</xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">icon</xsl:attribute>
      <xsl:attribute name="type">image/gif</xsl:attribute>
      <xsl:attribute name="href"><xsl:value-of select="$path" />inc/favicon.ico</xsl:attribute>
    </xsl:element>
    <xsl:if test="$target = 'dynamic'">
    <xsl:text disable-output-escaping="yes">
    &lt;?php require "inc/loadaccess.php"; ?&gt;
    </xsl:text>
    </xsl:if>
  </head>
  <body>
    <header>
      <a name="back"></a>
      <h1 class="left"><a href="{$path}index.html">La FAQ Anarchiste (francophone)</a></h1>
    </header>

    <xsl:element name="main">
      <xsl:element name="section">

    <h1>Liste des ouvrages cités et lisibles en ligne</h1>

    <blockquote>
      Chaque  lien   est  un   miroir  de   l’œuvre  en   ligne.  Le
      mainteneur  de  ce  miroir  de  la FAQ  a  sa  préférence  pour
      fr.theanarchistlibrary.org, qui présente au mieux les textes, et
      permet de faire ses propres compilations d’ouvrages.
    </blockquote>

    <div class="letters">
      <xsl:for-each select="catalog/letter">
        <xsl:sort select="@l" order="ascending" data-type="text" />

	<xsl:call-template name="letter-link">
	  <xsl:with-param name="l"><xsl:value-of select="@l" /></xsl:with-param>
	</xsl:call-template>

      </xsl:for-each>
    </div>

    <xsl:for-each select="catalog/letter">
      <xsl:sort select="@l" order="ascending" data-type="text" />

      <xsl:call-template name="letter-title">
	<xsl:with-param name="l"><xsl:value-of select="@l" /></xsl:with-param>
      </xsl:call-template>

      <xsl:for-each select="author">
        <xsl:sort select="last_name" order="ascending" data-type="text" />

	<xsl:call-template name="author-title">
	  <xsl:with-param name="id"><xsl:value-of select="@id" /></xsl:with-param>
	  <xsl:with-param name="type"><xsl:value-of select="@type" /></xsl:with-param>
	  <xsl:with-param name="last_name"><xsl:value-of select="last_name" /></xsl:with-param>
	  <xsl:with-param name="first_name"><xsl:value-of select="first_name" /></xsl:with-param>
	</xsl:call-template>

	<!--
	<xsl:element name="a">
          <xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
	</xsl:element>
        <xsl:element name="h3">
          <xsl:choose>
            <xsl:when test="first_name">
              <xsl:value-of select="last_name" /><xsl:text>, </xsl:text><xsl:value-of select="first_name" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="last_name" />
            </xsl:otherwise>
          </xsl:choose>
	  <xsl:element name="a">
	    <xsl:choose>
	      <xsl:when test="@type = 'group'">
		<xsl:attribute name="href">idx/groups.php#<xsl:value-of select="@id" /></xsl:attribute>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:attribute name="href">idx/names.php#<xsl:value-of select="@id" /></xsl:attribute>
	      </xsl:otherwise>
	    </xsl:choose>
	    &#160;[index]
	  </xsl:element>
        </xsl:element>
	-->

        <xsl:for-each select="book">
	  <xsl:sort select="names/n[@key]/." order="ascending" data-type="text" />

	  <xsl:choose>
	    <xsl:when test="names">

	      <xsl:choose>
		<xsl:when test="names/n[@single]">
		  <xsl:element name="h4">
		    <xsl:attribute name="id">
		      <xsl:value-of select="../@id" /><xsl:text>_</xsl:text><xsl:value-of select="@id" />
		    </xsl:attribute>
		    <xsl:element name="em"><xsl:value-of select="names/n[@single]"/></xsl:element>
		    <xsl:if test="names/n[@single]/@article">
		      <xsl:text> (</xsl:text><xsl:value-of select="names/n[@single]/@article"/>)
		    </xsl:if>
		  </xsl:element>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:element name="h4">
		    <xsl:attribute name="id">
		      <xsl:value-of select="../@id" /><xsl:text>_</xsl:text><xsl:value-of select="@id" />
		    </xsl:attribute>
		    <xsl:element name="em"><xsl:value-of select="names/n[@type='original']"/></xsl:element>
		    <xsl:if test="names/n[@type='original']/@article">
		      <xsl:text> (</xsl:text><xsl:value-of select="names/n[@type='original']/@article"/>)
		    </xsl:if>
		  </xsl:element>

		  <xsl:if test="names/n[@type='main']">
		    <xsl:element name="p">
		      <xsl:attribute name="class">bib-titles</xsl:attribute>
		      <b>Titres dans d’autres langues&#160;: </b>

		      <xsl:for-each select="names/n[@type='main']">
			<xsl:sort select="@lang" order="descending" data-type="text" />
			<xsl:element name="em">
			  <xsl:if test="@article"><xsl:value-of select="@article" /><xsl:text> </xsl:text></xsl:if>
			  <xsl:value-of select="." />
			</xsl:element>
			<xsl:if test="position() != last()">&#160;; </xsl:if>
		      </xsl:for-each>
		    </xsl:element>
		  </xsl:if>

		  <xsl:if test="names/n[@type='also']">
		    <xsl:element name="p">
		      <xsl:attribute name="class">bib-titles</xsl:attribute>
		      <b>Titres alternatifs&#160;: </b>

		      <xsl:for-each select="names/n[@type='also']">
			<xsl:sort select="." order="ascending" data-type="text" />
			<xsl:sort select="@lang" order="descending" data-type="text" />
			<xsl:element name="em">
			  <xsl:if test="@article"><xsl:value-of select="@article" /><xsl:text> </xsl:text></xsl:if>
			  <xsl:value-of select="." />
			</xsl:element>
			<xsl:if test="position() != last()">&#160;; </xsl:if>
		      </xsl:for-each>
		    </xsl:element>
		  </xsl:if>
		</xsl:otherwise>
	      </xsl:choose>
	    </xsl:when>

	    <xsl:otherwise>
	      <xsl:choose>
		<xsl:when test="name-fr">
		  <xsl:element name="h4">
		    <xsl:attribute name="id">
		      <xsl:value-of select="../@id" /><xsl:text>_</xsl:text><xsl:value-of select="@id" />
		    </xsl:attribute>
		    <xsl:element name="em"><xsl:value-of select="name"/></xsl:element><xsl:text> (</xsl:text>
		    <xsl:element name="span">
		      <xsl:attribute name="class">bibtrad</xsl:attribute>
		      <xsl:value-of select="name-fr" />
		    </xsl:element>
		    <xsl:text>)</xsl:text>
		  </xsl:element>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:element name="h4">
		    <xsl:attribute name="id">
		      <xsl:value-of select="../@id" /><xsl:text>_</xsl:text><xsl:value-of select="@id" />
		    </xsl:attribute>
		    <xsl:element name="em"><xsl:value-of select="name" /></xsl:element>
		  </xsl:element>
		</xsl:otherwise>
	      </xsl:choose>
	    </xsl:otherwise>

	  </xsl:choose>

          <xsl:choose>
            <xsl:when test="opus">
              <xsl:for-each select="opus/o">
                <xsl:sort select="name" order="ascending" data-type="text" />

                <xsl:element name="h6">
                  <xsl:attribute name="id">
                    <xsl:value-of select="../../../@id" /><xsl:text>_</xsl:text><xsl:value-of select="../../@id" /><xsl:text>_</xsl:text><xsl:value-of select="@id" />
                  </xsl:attribute>
                  <xsl:choose>
                    <xsl:when test="name-fr">
                      <xsl:element name="em"><xsl:value-of select="name" /></xsl:element><xsl:text> (</xsl:text>
                      <xsl:element name="span">
                        <xsl:attribute name="class">bibtrad</xsl:attribute>
                        <xsl:value-of select="name-fr" />
                      </xsl:element>
                      <xsl:text>)</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:element name="em"><xsl:value-of select="name" /></xsl:element>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:element>

                <xsl:if test="paper">
                  <xsl:element name="p">
                    <xsl:attribute name="class">paper_opus</xsl:attribute>
                    Référence papier&#160;: <xsl:apply-templates select="paper" />.
                  </xsl:element>
                </xsl:if>

                <xsl:choose>
                  <xsl:when test="links/a">
                    <xsl:element name="ul">
                      <xsl:for-each select="links/a">
			<xsl:call-template name="opus-link-item">
			  <xsl:with-param name="text"><xsl:value-of select="." /></xsl:with-param>
			  <xsl:with-param name="url"><xsl:value-of select="@href" /></xsl:with-param>
			  <xsl:with-param name="lang"><xsl:value-of select="@lang" /></xsl:with-param>
			</xsl:call-template>
                      </xsl:for-each>
                    </xsl:element>
                  </xsl:when>
                </xsl:choose>

              </xsl:for-each>
            </xsl:when>

            <xsl:otherwise>
              <xsl:if test="paper">
                <xsl:element name="p">
                  <xsl:attribute name="class">paper_single</xsl:attribute>
                  Référence papier&#160;: <xsl:apply-templates select="paper" />.
                </xsl:element>
              </xsl:if>

              <xsl:choose>
                <xsl:when test="links/a">
                  <xsl:element name="ul">
                    <xsl:for-each select="links/a">
		      <xsl:call-template name="opus-link-item">
			<xsl:with-param name="text"><xsl:value-of select="." /></xsl:with-param>
			<xsl:with-param name="url"><xsl:value-of select="@href" /></xsl:with-param>
			<xsl:with-param name="lang"><xsl:value-of select="@lang" /></xsl:with-param>
		      </xsl:call-template>
                    </xsl:for-each>
                  </xsl:element>
                </xsl:when>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:for-each>

    <!-- Retour en haut de page -->

    <div class="goback">
      <p><a href="#back">Retour en haut de la page</a></p>
    </div>

      </xsl:element>
    </xsl:element>

    <!-- Licence -->

    <xsl:call-template name="text-footer" />

  </body>
</html>
</xsl:template>

<xsl:template match="sup">
  <xsl:element name="sup"><xsl:value-of select="." /></xsl:element>
</xsl:template>

<xsl:template match="i">
  <xsl:element name="i"><xsl:value-of select="." /></xsl:element>
</xsl:template>

</xsl:stylesheet>
