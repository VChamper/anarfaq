<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<xsl:template name="scenario">
  <xsl:param name="docSec" />
  <xsl:param name="docSub" />

  <xsl:choose>
    <xsl:when test="$docSec = 'intro'">intro</xsl:when>
    <xsl:when test="$docSec = 'lecteur'">lecteur</xsl:when>
    <xsl:when test="$docSec = 'app'">app</xsl:when>
    <xsl:when test="$docSec = 'root'">root</xsl:when>
    <xsl:when test="$docSec = 'names'">names</xsl:when>
    <xsl:otherwise>
      <xsl:choose>
	<xsl:when test="$docSub = 'toc'">
	  <xsl:choose>
	    <xsl:when test="$docSec = 'a'">first_sec_toc</xsl:when>
	    <xsl:when test="$docSec = 'j'">last_sec_toc</xsl:when>
	    <xsl:otherwise>sec_toc</xsl:otherwise>
	  </xsl:choose>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:choose>
	    <xsl:when test="$docSub = '0'">first_sub</xsl:when>
	    <xsl:otherwise>
	      <xsl:choose>
		<xsl:when test="$docSec = 'a' and $docSub = '5'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'b' and $docSub = '7'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'c' and $docSub = '12'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'd' and $docSub = '11'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'e' and $docSub = '6'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'f' and $docSub = '8'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'g' and $docSub = '7'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'h' and $docSub = '6'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'i' and $docSub = '8'">last_sub</xsl:when>
		<xsl:when test="$docSec = 'j' and $docSub = '7'">last_sub</xsl:when>
		<xsl:otherwise>sub</xsl:otherwise>
	      </xsl:choose>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
