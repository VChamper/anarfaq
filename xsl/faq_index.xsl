<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!-- Paramètres de transformation ===================================== -->

<!-- Chemin des médias par rapport au fichier en cours de génération -->
<xsl:param name="path" select="''" />

<!--
  Type de transformation:
  dynamic => PHP
  static => HTML (sans accessibilité)
-->
<xsl:param name="target" select="'dynamic'" />

<!-- Template du pied de page -->
<xsl:include href="text-footer.xsl" />

<!-- ================================================================== -->

<xsl:template match="/">
<html lang="fr">
  <head>
    <meta charset="utf-8" />
    <title>La FAQ Anarchiste (francophone)</title>
    <xsl:element name="link">
      <xsl:attribute name="rel">alternate</xsl:attribute>
      <xsl:attribute name="type">application/rdf+xml</xsl:attribute>
      <xsl:attribute name="href">http://www.gnu.org/licenses/fdl-1.3.rdf</xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">stylesheet</xsl:attribute>
      <xsl:attribute name="type">text/css</xsl:attribute>
      <xsl:attribute name="href"><xsl:value-of select="$path" />inc/landing.css</xsl:attribute>
    </xsl:element>
    <xsl:element name="link">
      <xsl:attribute name="rel">icon</xsl:attribute>
      <xsl:attribute name="type">image/gif</xsl:attribute>
      <xsl:attribute name="href"><xsl:value-of select="$path" />inc/favicon.ico</xsl:attribute>
    </xsl:element>
  </head>
  <body>
    <xsl:apply-templates />

    <!-- Licence -->

    <xsl:call-template name="text-footer" />
  </body>
</html>
</xsl:template>

<xsl:template match="document/meta"></xsl:template>

<xsl:template match="div[@id='header']">
  <xsl:element name="header">
    <h1>La FAQ Anarchiste (francophone)</h1>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="div[@id = 'links']">
  <xsl:element name="div">
    <xsl:attribute name="id">links</xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="div[@id != 'header' and @id != 'links']">
  <xsl:element name="article">
    <xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>
    <xsl:element name="a">
      <xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute>
    </xsl:element>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="div/preambule">
  <xsl:element name="div">
    <xsl:attribute name="class">pre</xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="div/preambule/head">
  <xsl:element name="h2">
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="div[@id = 'sections']/content/section">
  <xsl:element name="section">
    <xsl:element name="a"><xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute></xsl:element>

    <xsl:element name="div">
      <xsl:attribute name="class">letter</xsl:attribute>
      <xsl:value-of select="letter" />
    </xsl:element>

    <xsl:element name="div">
      <xsl:attribute name="class">title</xsl:attribute>

      <xsl:element name="a">
	<xsl:attribute name="href"><xsl:choose><xsl:when test="@id != 'intro' and @id != 'lecteur'"><xsl:value-of select="@id" />/</xsl:when></xsl:choose><xsl:value-of select="toc" />.html</xsl:attribute>
	<xsl:value-of select="title" />
      </xsl:element>

      <xsl:element name="blockquote">
	<xsl:attribute name="class">accroche</xsl:attribute>
	<xsl:value-of select="quote" /><xsl:element name="br"/>
	<xsl:element name="a">
	  <xsl:attribute name="href"><xsl:if test="@id != 'intro' and @id != 'lecteur'"><xsl:value-of select="@id" />/</xsl:if><xsl:value-of select="intro" />.html</xsl:attribute>
	  Continuer…
	</xsl:element>
      </xsl:element>

    </xsl:element>
  </xsl:element>
</xsl:template>

<xsl:template match="div[@id != 'sections']/content/section">
  <xsl:element name="section">

    <xsl:attribute name="class">
      <xsl:choose>
	<xsl:when test="../../@id = 'appendix'">annex</xsl:when>
	<xsl:when test="../../@id = 'tools'">tool</xsl:when>
      </xsl:choose>
    </xsl:attribute>

    <xsl:element name="a"><xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute></xsl:element>

    <xsl:element name="div">
      <xsl:attribute name="class">letter</xsl:attribute>
      <xsl:value-of select="letter" />
    </xsl:element>

    <xsl:element name="div">
      <xsl:attribute name="class">title</xsl:attribute>

      <xsl:element name="a">
	<xsl:attribute name="href"><xsl:value-of select="toc" />.html</xsl:attribute>
	<xsl:value-of select="title" />
      </xsl:element>
    </xsl:element>

  </xsl:element>
</xsl:template>

<xsl:template match="line">
  <xsl:element name="span">
    <xsl:attribute name="class">line</xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="blockquote">
  <xsl:element name="blockquote">
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="p">
  <xsl:element name="p">
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="a">
  <xsl:element name="a">
    <xsl:if test="@href">
      <xsl:attribute name="href">
	<xsl:value-of select="@href" />
      </xsl:attribute>
    </xsl:if>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

</xsl:stylesheet>
