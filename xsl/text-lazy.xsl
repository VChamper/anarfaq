<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" />

<!-- <e/> pour écrire &#160; -->
<xsl:template match="e">&#160;</xsl:template>

<!-- <el/> pour insérer un […] avec le bon style -->
<xsl:template match="el"><xsl:element name="span"><xsl:attribute name="class">el</xsl:attribute>[…]</xsl:element></xsl:template>

<!-- <opcit/> pour écrire op.cit. avec le bon style -->
<xsl:template match="opcit">
  <xsl:choose>
    <xsl:when test="@incit">op. cit.</xsl:when>
    <xsl:otherwise><xsl:element name="em">op. cit.</xsl:element></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- <ibid/> pour écrire op.cit. avec le bon style -->
<xsl:template match="ibid">
  <xsl:choose>
    <xsl:when test="@incit">ibid.</xsl:when>
    <xsl:otherwise><xsl:element name="em">ibid.</xsl:element></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- <rom> passe son contenu en petites capitales -->
<xsl:template match="rom">
  <xsl:element name="span">
    <xsl:attribute name="class">numrom</xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<!-- <and/> et <et/> insèrent une & avec le bon style -->

<xsl:template match="and">
  <xsl:element name="span">
    <xsl:attribute name="class">and</xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<xsl:template match="et">
  <xsl:element name="span">
    <xsl:attribute name="class">and</xsl:attribute>
    <xsl:apply-templates />
  </xsl:element>
</xsl:template>

<!-- Template <no n="15"/> pour écrire n°15 -->
<xsl:template match="no">
n<xsl:element name="sup">o</xsl:element><xsl:value-of select="@n" />
</xsl:template>

<!-- Template pour formater des ordinaux (employé par ord et siecle) -->
<xsl:template name="ordinal">
  <xsl:param name="n" />
  <xsl:variable name="insup">
    <xsl:choose>
      <xsl:when test="$n &gt; 1">e</xsl:when>
      <xsl:when test="$n = 1">er</xsl:when>
      <xsl:otherwise>e</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:value-of select="$n" /><xsl:element name="sup"><xsl:value-of select="$insup" /></xsl:element>
</xsl:template>

<!-- <ord> pour écrire XXeme -->
<xsl:template match="ord">
<xsl:call-template name="ordinal"><xsl:with-param name="n"><xsl:value-of select="@n" /></xsl:with-param></xsl:call-template>
</xsl:template>

<!-- <siecle> pour écrire "XXeme siècle" -->
<xsl:template match="siecle">
<xsl:call-template name="ordinal"><xsl:with-param name="n"><xsl:value-of select="@n" /></xsl:with-param></xsl:call-template>&#160;siècle
</xsl:template>

</xsl:stylesheet>
