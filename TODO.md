# TODO

- Supprimer toc.xml et toc.xsl quand inc/toc.php sera devenu obselète
- Style idxref (renvoi auteur dans corps du texte)
- Style idxbooks (renvoi œuvres en ligne dans l’index)
