#!/bin/bash

echo "  Table des matières de $1 => public/web/$1/$1_toc.html"

xmlstarlet tr xsl/static_toc.xsl -s selectedSection="$1" data/neo_toc.xml > "data/sections/$1/$1_toc.html"
cp "data/sections/$1/$1_toc.html" "public/web/$1/"

# Produit les textes de la section

for file in data/sections/$1/$1_*.xml
do
	if [ "$file" != "$1/$1_*.xml" ]
	then
		secname=`echo \"$file\" | cut -d '.' -f 1 | cut -d '/' -f 4`
		local_out="data/sections/$1/$secname.html"
		public_out="public/web/$1/$secname.html"

		echo "  $file => $local_out => $public_out"

		xmlstarlet tr xsl/text.xsl -s target="'static'" $file > $local_out
		cp $local_out $public_out
	fi
done
