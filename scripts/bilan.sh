#!/bin/bash

mkdir -p bilans/$1/

echo "\n  Section $1"

grep -s 'class="citation' $1/*.php | grep -v "<blockquote" > bilans/$1/citations.txt

# Éléments blockquote sans classe CSS attribuée --------------------------

grep -s '<blockquote>' data/sections/$1/*.xml >> bilans/$1/citations_noclass.txt

# FIXME en attendant que tous les fichiers soient en XML, laisser
# cette ligne de grep avec les fichiers PHP. Ça génère trop de lignes
# mais une fois corrigé, en retire autant.
grep -s '<blockquote>' $1/*.php > bilans/$1/citations_noclass.txt

noclass_cit="`wc -l bilans/$1/citations_noclass.txt | cut -d ' ' -f 1`"

if [ "$noclass_cit" != "0" ]; then
	echo "    [ATTENTION] Des citations n’ont pas de classe ($noclass_cit)."
fi

# Note Du Traducteur, à supprimer ou passer en notes ---------------------

grep -s ' ndt ' $1/$1*.php > bilans/$1/ndt.txt
grep -s ' NDT ' $1/$1*.php >> bilans/$1/ndt.txt
grep -s ' NdT ' $1/$1*.php >> bilans/$1/ndt.txt
grep -s ' ndt. ' $1/$1*.php >> bilans/$1/ndt.txt
grep -s ' NDT. ' $1/$1*.php >> bilans/$1/ndt.txt
grep -s ' NdT. ' $1/$1*.php >> bilans/$1/ndt.txt
ndt="`wc -l bilans/$1/ndt.txt | cut -d ' ' -f 1`"

if [ "$ndt" != "0" ]; then
	echo "    [ATTENTION] Des Notes Du Traducteur sont mal"
	echo "                intégrées ($ndt)."
fi

# grep 'idx' $1/$1*.php | grep -v "idxref" > bilans/$1/idxref_class.txt

# Relevé des FIXME / TODO dans les fichiers des sections -----------------

grep -s 'TODO' data/sections/$1/$1*.xml > bilans/$1/todo.txt
grep -s 'FIXME' data/sections/$1/$1*.xml > bilans/$1/fixme.txt

# FIXME en attendant que tous les fichiers soient en XML, laisser
# ces ligne de grep avec les fichiers PHP. Ça génère trop de lignes
# mais une fois corrigé, en retire autant.
grep -s 'TODO' $1/$1*.php >> bilans/$1/todo.txt
grep -s 'FIXME' $1/$1*.php >> bilans/$1/fixme.txt

todo="`wc -l bilans/$1/todo.txt | cut -d ' ' -f 1`"
fixme="`wc -l bilans/$1/fixme.txt | cut -d ' ' -f 1`"

if [ "$todo" != "0" ]; then
	echo "    [ATTENTION] Des choses sont marquées comme étant à"
	echo "                faire dans cette section ($todo)."
fi

if [ "$fixme" != "0" ]; then
	echo "    [ATTENTION] Des choses sont marquées comme étant à"
	echo "                corriger dans cette section ($fixme)."
fi
