# La FAQ anarchiste (francophone)

> Nous voulons, en un mot, l’égalité&nbsp;: l’égalité de
> fait, comme corollaire ou plutôt comme condition primordiale de
> la liberté. *De chacun selon ses facultés, à chacun selon
> ses besoins*; voilà ce que nous voulons… scélérats que nous
> sommes&nbsp;!
> 
> Louise MICHEL

[Consulter la FAQ](http://tviblindi.legtux.org/afaq/).

*The Anarchist FAQ*, par *The Anarchist FAQ Editorial Collective*
est une monumentale foire à questions sur l’anarchisme.
Malheureusement pour nous autres francophones, elle est en
anglais. 

En partant de la tentative de traduction de 
[faqanarchiste.free.fr](http://faqanarchiste.free.fr), ce dépôt
vise à créer une FAQ cohérente et uniforme pour faciliter la
lecture et la compréhension du lecteur, une FAQ dont la qualité
d’édition est quasi-professionelle.

On utilise les fichiers de [*The Anarchist Library.org*](https://theanarchistlibrary.org/category/author/the-anarchist-faq-editorial-collective?sort=title_asc&rows=100) pour référence. 
La version utilisée de la FAQ anglophone est la 1.3.

## Ce qui se fait

- Correction du texte avec Grammalecte
- Nettoyage et modernisation du code HTML
- Homepage plus accueillante
- Index de noms propres (complété au fur et à mesure)
- Index de groupes politiques (complété au fur et à mesure)
- Uniformisation des renvois vers des textes en ligne
- Citabilité des paragraphes
- Double système de notes
- Accessibilité (très relative) pour les personnes dyslexiques
- Format XML pour exporter en plein de formats
- Passage à UTF-8.

## Captures d’écran

### Page d’accueil

![Screenshot](doc/screen.png)

### Une section de la FAQ

![Screenshot](doc/screen2.png)

Les noms soulignés renvoient à l’index des noms propres.

## Contribuer

Consultez la page du [guide de contribution](contribuer/guide.md).

## Compiler le site

Utilisez `make` et copiez le contenu du dossier `out` qu’il génère.

## FAQ de la FAQ

### Est-ce une FAQ sur les libertariens ?

Non. Les libertariens sont des capitalistes. On ne saurait être
anarchiste et capitaliste.

### Pourquoi autant de XML ?

XML impose une certaine rigueur  dans la structuration des documents.
La traduction de  la FAQ va prendre  **des mois, ou des  années**. On ne
peut donc *pas*  s’en remettre à la mémoire  d’une seule personne
en ce qui concerne toutes les normes appliquées. D’ailleurs, même une 
seule personne peut faire des oublis, des erreurs en s’occupant d’une 
traduction de cette ampleur. La rigueur imposée par XML est donc 
utile : si le document XML est valide, tout se passera bien, et s’il 
ne l’est pas, ce sera immédiatement remarqué.

De plus, il est assez simple d’abstraire certaines choses avec, ce qui 
permet d’éviter des erreurs de frappe.

Par exemple, un renvoi vers l’index des auteurs :

```
<idx s="name" id="proudhon">Pierre-Joseph Proudhon</idx>
```

Produira un  lien dont  l’URL sera normalisée.  `s="name"` indique
que l’on veut renvoyer à l’index de noms (`s="group"` renvoit à
l’index des groupes politiques).

Avant le passage intégral à XML **[qui est encore en cours]**, le 
même renvoi s’écrivait en HTML&#160;:

```
<!-- Si on est dans une section -->

<a class="idxref" href="../idx/names.php#proudhon">Pierre-Joseph Proudhon</idx>

<!-- Si on est dans une partie située à la racine du site -->

<a class="idxref" href="idx/names.php#proudhon">Pierre-Joseph Proudhon</idx>
```

XML  couplé avec  XPath  permet aussi  de  repérer des  problèmes,
assurer l’homogéneité des documents…

<!-- vim:set spelllang=fr: -->
