.PHONY: web web-pre web-css web-img web-pages sections indexes bilans index

.DEFAULT_GOAL = web

clean:
	rm -r -f data/generation.xml

# ======================================================================

web: web-pre web-css web-img web-pages index sections indexes 404 bilans

web-pre:
	@echo  "\n=== WEB ===================================="
	mkdir -p public/web/inc

# === Médias à inclure (images, styles CSS) ============================

web-css: web-pre
	@echo  "\n=== CSS ===================================="
	@echo  "\n--- Styles généraux ----------------"
	scss -t expanded data/inc/css/landing.scss > data/inc/css/landing.css
	scss -t expanded data/inc/css/style.scss > data/inc/css/style.css
	@echo  "\n--- Styles par type de page --------"
	scss -t expanded data/inc/css/names_freq.scss > data/inc/css/names_freq.css
	scss -t expanded data/inc/css/names.scss > data/inc/css/names.css
	scss -t expanded data/inc/css/books.scss > data/inc/css/books.css
	# @echo  "\n--- Ancien style -------------------"
	# scss -t expanded data/inc/master.scss > data/inc/master.css
	@echo  "\n--- Copie --------------------------"
	cp data/inc/css/*.css data/inc/
	cp data/inc/css/*.css public/web/inc/
	# cp data/inc/master.css public/web/inc/

web-img:
	cp data/inc/favicon.ico public/web/inc/

# ======================================================================

# web-pages: generation intro lecteur bibliography credits fdl progressPage books
web-pages: generation intro lecteur bibliography credits fdl books

index:
	@echo  "\nPréparation de la page d’accueil (statique)-"
	xmlstarlet tr xsl/faq_index.xsl -s target="static" data/pages/index.xml > public/web/index.html

intro:
	@echo  "\nPréparation de l’introduction --------------"
	xmlstarlet tr xsl/text.xsl -s target="'static'" -s path="" data/pages/intro.xml > public/web/intro.html

lecteur:
	@echo  "\nPréparation de l’adresse au lecteur --------"
	xmlstarlet tr xsl/text.xsl -s path="" -s target="'static'" data/pages/lecteur.xml > public/web/lecteur.html

404:
	@echo  "\nPage 404 -----------------------------------"
	xmlstarlet tr xsl/text.xsl -s path="" -s target="'static'" data/pages/404.xml > public/web/404.html

bibliography:
	@echo  "\nPréparation de la bibliographie générale ---"
	xmlstarlet tr xsl/bibliography.xsl -s path="" -s target="'static'" data/bibliography.xml > public/web/bibliography.html

credits:
	@echo  "\nAjout des crédits iconographiques (statique)"
	xmlstarlet tr xsl/text.xsl -s target="'static'" data/pages/credits.xml > public/web/credits.html

fdl:
	@echo  "\nAjout de la licence GNU FDL (statique) -----"
	xmlstarlet tr xsl/text.xsl -s target="'static'" data/pages/fdl.xml > public/web/fdl.html

books:
	@echo  "\nPréparation des œuvres en ligne (statique)--"
	xmlstarlet tr xsl/books.xsl -s target="'static'" data/books.xml > public/web/books.html

# ======================================================================

toc:
	xmlstarlet tr xsl/toc.xsl data/full_toc.xml > toc.xml
	sed -f scripts/sed_tocxml toc.xml > tempfile
	rm toc.xml
	mv tempfile toc.xml

progress:
	@echo  "\nBilan de la progression de la traduction ---"
	-rm progress.txt
	-rm finished.txt
	@grep -r '<!-- TODO TRADUIRE' * --exclude tempfile --exclude contribuer/ --exclude Makefile --exclude README.md --exclude progress.txt --exclude contribuer/guide.md --exclude out-epub/* > tempfile
	@cat tempfile | sort > progress.txt
	-rm tempfile
	@grep -r '<!-- TRADUCTION FINIE' * --exclude tempfile --exclude Makefile --exclude contribuer/ --exclude finished.txt --exclude contribuer/guide.md --exclude out-epub/* > tempfile
	@cat tempfile | sort > finished.txt
	-rm tempfile

progressPage: progress
	python3 make_progress.py && cp progress.html public/web/

# ======================================================================

generation:
	@echo "\nDonnées de génération de la FAQ ------------"
	@echo -e "<?xml version='1.0' encoding='utf-8'?>" > data/generation.xml
	@echo -e "<datas>" >> data/generation.xml
	@echo -e "  <date>`date +%d/%m/%Y`</date>" >> data/generation.xml
	@echo -e "  <commit>" >> data/generation.xml
	@echo -e "<href>TODO</href>" >> data/generation.xml
	@echo -e "<name>TODO</name>" >> data/generation.xml
	# @echo -e "    <href>https://framagit.org/VChamper/anarfaq/commit/`git log | head -1 | cut -c 8-100`</href>" >> data/generation.xml
	# @echo -e "    <name>`git log | head -1 | cut -c 8-15`</name>" >> data/generation.xml
	@echo -e "  </commit>" >> data/generation.xml
	@echo -e "</datas>" >> data/generation.xml

# === Tous les index ===================================================

indexes-pre:
	@echo  "\nPréparation des indexes --------------------"
	mkdir -p public/web/idx/

indexes: indexes-pre names groups

groups-pre:
	@echo  "\nCréation de l’index des groupes -------"

groups: groups-pre
	xmlstarlet tr xsl/groups.xsl data/index/groups.xml > public/web/idx/groups.html

names-pre:
	@echo  "\nCréation de l’index -------------------"

names: names-pre
	xmlstarlet tr xsl/index-names.xsl -s target="static" data/index/names.xml > public/web/idx/names.html
	@echo  "\nCréation de la table de fréquence -----"
	xmlstarlet tr xsl/index-names-freq.xsl -s target="static" data/index/names.xml > public/web/idx/names_freq.html

# ======================================================================
#
bilans:
	@echo -e "\nBilans -------------------------------------"
	mkdir -p bilans/
	@echo -e "\nBilan de l’indexation -----------------\n"
	@-./scripts/bilan_index.sh
	@echo -e "\nBilan des sections --------------------"
	@-./scripts/bilan.sh a
	@-./scripts/bilan.sh b
	@-./scripts/bilan.sh c
	@-./scripts/bilan.sh d
	@-./scripts/bilan.sh e
	@-./scripts/bilan.sh f
	@-./scripts/bilan.sh g
	@-./scripts/bilan.sh h
	@-./scripts/bilan.sh d
	@-./scripts/bilan.sh j
	# @-./scripts/bilan.sh app1
	@-./scripts/bilan.sh app2

# ======================================================================

sections: secDirs secFiles

secDirs-pre:
	@echo  "\nPréparation des sections -------------------"

secDirs: secDirs-pre
	@mkdir -p public/web/a/
	@mkdir -p public/web/b/
	@mkdir -p public/web/c/
	@mkdir -p public/web/d/
	@mkdir -p public/web/e/
	@mkdir -p public/web/f/
	@mkdir -p public/web/g/
	@mkdir -p public/web/h/
	@mkdir -p public/web/i/
	@mkdir -p public/web/j/

secFiles:
	@echo  "\nPréparation de la section A -----------"
	@./scripts/staticsec.sh a
	@echo  "\nPréparation de la section B -----------"
	@./scripts/staticsec.sh b
	@echo  "\nPréparation de la section C -----------"
	@./scripts/staticsec.sh c
	@echo  "\nPréparation de la section D -----------"
	@./scripts/staticsec.sh d
	@echo  "\nPréparation de la section E -----------"
	@./scripts/staticsec.sh e
	@echo  "\nPréparation de la section F -----------"
	@./scripts/staticsec.sh f
	@echo  "\nPréparation de la section G -----------"
	@./scripts/staticsec.sh g
	@echo  "\nPréparation de la section H -----------"
	@./scripts/staticsec.sh h
	@echo  "\nPréparation de la section I -----------"
	@./scripts/staticsec.sh i
	@echo  "\nPréparation de la section J -----------"
	@./scripts/staticsec.sh j

# ======================================================================
