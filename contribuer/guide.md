<meta charset="utf8" />
<style>
body {
	font-family: serif;
	margin-left: 15%;
	margin-right: 15%;
	text-align: justify;
	font-size: 1.1em;
}
pre,blockquote {margin-left: 5%;}
h1,h2 {text-align: center;}
h3 {text-transform: uppercase;}
h4 {font-size: normal; font-weight: bold;}
h5 {font-size: normal; font-style: italic;}
h6 {font-size: normal;}
ol li {font-weight: bold;}
ol li ol li {text-transform: uppercase; font-weight: normal;}
ol li ol li ol li {font-weight: bolder;text-transform: none;}
ol li ol li ol li ol li {font-style: italic; font-weight: normal;}
ol li ol li ol li ol li ol li {font-style: normal; font-weight: normal;}
</style>

# Guide de contribution à la FAQ anarchiste (francophone)

## Sommaire

1. Comment contribuer&nbsp;?
	1. Comment participer au mieux
	2. Traduire
		1. Ce que l’on veut, et comment
		2. Où commencer&nbsp;?
	3. Actualiser
	4. Ajouter une section
2. Règles de traduction
	1. Choix politiques
		1. Langage antisexiste
		2. Nombres
		3. Le mot *libertarian*
	2. Choix typographiques
		1. Les renvois
			1. Renvoi vers une œuvre
			2. Renvoi vers une œuvre en ligne
			3. Renvoi vers l’index des noms propres
		2. Les citations
			1. Citations dans le corps du texte
			2. Paragraphes de citations
			3. Conventions dans les citations
				1. Sur les ouvrages cités
				2. L’ouvrage cité existe en français
				3. L’ouvrage cité n’existe pas en français
		3. Les notes (de bas de page)
			1. Les notes du traducteur
				1. Notes sur la traduction
				2. Notes d’explication
			2. Les notes du texte
	3. Détails techniques
		1. Les fichiers de section
	4. Choix de vocabulaire
3. Les indexs
	1. Éditer l’index des noms propres
	2. Éditer l’index des groupes politiques
4. Les bibliographies
	1. Éditer la liste des ouvrages lisibles en lignes
	2. Éditer la bibliographie générale de la FAQ
5. Éditer le style de la FAQ
6. Éditer les styles de transformation de la FAQ
	1. Fichiers de section
	2. Table des matière de section
	3. Index des noms
	4. Index des groupes politiques
	5. Liste des œuvres disponibles en ligne
	6. Page d’accueil
	7. Bibliographie
7. Compiler le site
8. FAQ de la FAQ
	1. Pourquoi ne pas faire un wiki&#160;?
	2. Pourquoi renvoyer vers une page de liens plutôt que d’insérer les liens directement dans le texte&#160;?
	3. Pourquoi passer en XML la bibliographie générale / la liste des textes lisibles en ligne / les indexes&#160;?

## 1. Comment contribuer&nbsp;?

Tout dépend de ce que vous savez faire (ou désirez apprendre)&nbsp;!

Puisque le but de La FAQ  anarchiste (francophone) est de fournir une
traduction française d’une œuvre imposante écrite en anglais, la
façon  la plus  évidente  de  contribuer est  de  participer à  la
traduction de l’anglais au français.

Mais sans  traduire, vous pouvez  également relire et faire  part de
vos corrections, suggérer de meilleures formulations.

Vous pouvez  également créer des  images ou proposer  de meilleures
feuilles de styles (CSS).

### 1.1 — Comment participer au mieux&nbsp;?

Pour créer  cette traduction, nous  utilisons ce qu’on  appelle un
[logiciel de gestion de versions](https://fr.wikipedia.org/wiki/Logiciel_de_gestion_de_versions).
Ce logiciel permet de faire en sorte&nbsp;:

- qu’on puisse garder un historique des modifications
- que les modifications des uns n’écrasent pas celles des autres
- qu’on puisse travailler sur certains changements sans perturber la version en cours

Il  existe  plusieurs  logiciels  de gestion  de  versions,  et  nous
utilisons GIT.

### 1.2 – Traduire

#### 1.2.1 – Ce que l’on veut, et comment

Cette  version  francophone  de  la FAQ  anarchiste  vise  à  créer
une  FAQ cohérente  et  uniforme  pour faciliter  la  lecture et  la
compréhension du lecteur, une FAQ  dont la qualité d’édition est
quasi-professionelle, qu’on  pourrait attendre de  n’importe quel
livre trouvable dans le commerce.

Mais la  volonté d’obtenir  une telle  qualité de  traduction, de
lisibilité a un inconvénient&nbsp;: il faut auparavant définir un 
certain nombre de règles, d’écriture et d’organisation.

Ces  dernières peuvent  paraître  assez strictes,  mais elles  sont
nécessaires, et  permettent d’aboutir  au plus  vite au  but. Bien
entendu, elles peuvent être discutées, modifiées, améliorées. Mais, 
quoiqu’elles soient, elles doivent <b>être appliquées uniformément 
et documentées</b>.

  L’ensemble des règles de traduction est défini dans la partie 
  « 2. Règles de traduction ».

#### 1.2.2 – Où commencer&nbsp;?

Si vous  ne savez  pas où  commencer à  traduire, regardez  la page 
[«&nbsp;État d’avancement  de la traduction&nbsp;»](http://tviblindi.legtux.org/afaq/progress.php) 
qui  liste les pages et les passages qui ne sont pas encore traduits.

Nous vous suggérons de commencer par des petits passages.

Si votre  éditeur de  texte peut  ouvrir un  fichier dont  le chemin
est  placé  sous  son  curseur (comme  Vim),  consultez  le  fichier
`progress.txt`&nbsp;;  il contient  une liste  de passage  restant à
traduire.

Le  dossier  `bilans`  contient   également  des  fichiers  pointant
vers  des   anomalies  de   traduction  /  de   formatage  repérées
automatiquement. Les corriger n’est pas très compliqué.

`bilans/LETTRESECTION/citations.txt` cherche les éléments balisés
comme des citations, mais qui ne sont pas des `<blockquote>`.

`bilans/LETTRESECTION/citations_noclass.txt` cherche les
`<blockquote>` n’ayant pas de classe: il faut leur ajouter la
classe `citation`, `citation_author`, `citation_source`.

`bilans/LETTRESECTION/ndt.txt` recherche les Notes Du Traducteur.

`bilans/LETTRESECTION/idxref_class.txt` recherche les liens vers
l’index des noms propres qui n’ont pas la classe `idxref`
appliquée.

### 1.3 – Actualiser


### 1.3 – Ajouter une section


## 2. Règles de traduction

> S’ensuit-il que je repousse toute autorité ? Loin de moi cette pensée. 
> Lorsqu’il s’agit de bottes, j’en réfère à l’autorité du cordonnier ; 
> s’il s’agit d’une maison, d’un canal ou d’un chemin de fer, je consulte 
> celle de l’architecte ou de l’ingénieur. Pour telle science spéciale, 
> je m’adresse à tel savant. Mais je ne m’en laisse imposer ni par le cordonnier, 
> ni par l’architecte, ni par le savant. Je les écoute librement et 
> avec tout le respect que méritent leur intelligence, leur caractère, 
> leur savoir, en réservant toutefois mon droit incontestable de 
> critique et de contrôle.
> 
> BAKOUNINE, *Dieu et l’État*

Éditer  un texte,  c’est faire  des  choix. Certains  sont dus  à
des  nécessités *techniques*,  d’autres  se font  par rapport  à
des *conventions  de langage* (orthographe,  grammaire, typographie),
d’autres sont des choix *politiques*, enfin certains choix sont une
combinaison des trois précédents.

Avant toute chose, si cette traduction reprend la tentative de 
traduction de [faqanarchiste.free.fr](http://faqanarchiste.free.fr), 
le texte anglais de référence est celui disponible 
sur [*The Anarchist Library.org*](https://theanarchistlibrary.org/category/author/the-anarchist-faq-editorial-collective?sort=title_asc&rows=100), 
et  **pas  un  autre**.  Le   texte  disponible  sur  *The  Anarchist
Library.org*  a   en  effet  l’avantage  d’offrir   plusieurs  de
formats de  fichiers différents.  Quant il  s’agit de  pointer les
différences entre  la traduction  et le texte  original, le  PDF est
suffisant.  Quant  il s’agit  par  contre  de coller  des  passages
entiers  non-traduits,  les formats  basés  sur  HTML doivent  être
préférés.

**Ne supprimez PAS les passages que vous ne comprenez pas&nbsp;!** Insérez
un commentaire commençant "TODO" ou "FIXME". De tels commentaires seront 
repérés et listés par un programme à chaque compilation du texte.

    `<!-- FIXME que veut dire "enserfed" ?  -->`

    `<!-- TODO TRADUIRE  -->`

**N’ajoutez   pas  directement   des  notes   du  traducteurs   (ou
NDT)&nbsp;!** Respectez les règles quant aux notes de bas de page.

### 2.1 – Choix politiques

#### 2.1.1 – Langage antisexiste

Le  langage  antisexiste est  employé.  Il  s’agit ici  de  rester
cohérent  avec  l’édition  anglophone  qui  avait  fait  de  tels
choix.  L’usage   du  langage   antisexiste  est   d’autant  plus
important que l’émancipation féministe fait partie intégrante de
l’anarchisme.

- Dans le cas d’un exemple, utiliser le masculin, le féminin 
de manière **équitable**. Par exemple, *a worker* pouvant se 
traduire par *travailleur* ou *travailleuse*, en accord avec 
le contexte, ne pas utiliser systématiquement le masculin.
- Préférer l’[accord de proximité](https://fr.wikipedia.org/wiki/Règle_de_proximité) à 
la règle du «&#160;masculin qui l’emporte sur le féminin&#160;».
- On utilise les points médians&nbsp;: « les travailleu·rs·ses »

#### 2.1.2 – Nombres

Les nombres en  chiffres romains, exceptés dans  les références à
un ouvrage (les pages peuvent  être numérotées en romains) sont à
proscrire.

On écrira donc&#160;:

```html
au 19<sup>e</sup> siècle, le mouvement social
```

et non&#160;:

```html
au XIX<sup>e</sup> siècle, le mouvement social
```

Cependant, intégrer des `<sup>` peut être fastidieux. Bien que
`19<sup>e</sup> siècle` soit valide, il est plus avisé d’écrire :

```xml
au <siecle n="19" />, le mouvement social
```

`<siecle>` assure également une meilleure lisibilité en faisant 
en sorte que "19<sup>e</sup>" ne soit pas en fin de ligne et 
"siècle" au début de la ligne suivante.

#### 2.1.3 – Le mot *libertarian*

Le mot anglais *libertarian* provient du français *libertaire*, et
a été usurpé par les «&nbsp;anarcho&nbsp;»-capitalistes très
récemment, en vue de se donner un vernis subversif. 

En  français, le  nom de  cet  extrémisme capitaliste  est, par  un
aller-retour  linguistique,  *libertarianisme*.  Ses  deux  ou  trois
militants se nomment *libertariens*.

De  ce  fait,   le  terme  anglais  *libertarian*   est  traduit  par
*libertaire* quand il s’agit  d’anarchistes, et par *libertarien*
quand il s’agit au contraire de ces capitalistes extrémistes.

La  FAQ  anarchiste  anglophone   considère  (avec  raison)  qu’il
faudrait  plutôt  nommer   les  libertariens  des  *proprietarians*,
qu’on peut traduire en *propriétarien*.

### 2.2 – Choix typographiques

#### 2.2.1 — Les renvois

##### 2.2.1.1 – Renvoi vers une œuvre

Voir 2.2.2.1 – Citations dans le corps du texte.

##### 2.2.1.2 – Renvoi vers une œuvre en ligne

Les  renvois vers  une œuvre  en ligne  se font  avec `<book>`,  en 
employant l’identifiant de l’auteur et du livre. Par exemple, un 
appel vers *Anarchie et Organisation* d’Errico Malatesta:          

```
Errico Malatesta, <em>Anarchie et Organisation</em> 
<book author="malatesta" id="AnarchieOrganisation" />
```

##### 2.2.1.3 – Renvoi vers l’index des noms propres

Les renvois vers l’index des noms propres se font avec `<idx>`, en employant 
l’identifiant de la personne. Par exemple, un appel vers Errico Malatesta:

```
<idx s="name" id="malatesta">Errico Malatesta</idx>
```

`s="name"` signifie qu’on renvoit vers l’index des noms propres.

Les identifiants valides sont ceux définis dans `idx/names.xml`.

##### 2.2.1.3 – Renvoi vers l’index des groupes politiques

Les renvois vers l’index des groupes politiques se font avec `<idx>`, en employant 
l’identifiant du groupe politique. Par exemple, un appel vers les IWW :

```
<idx s="group" id="iww">I.W.W</idx>
```

`s="group"` signifie qu’on renvoit vers l’index des groupes politiques.

Les identifiants valides sont ceux définis dans `idx/groups.xml`.

#### 2.2.2 – Les citations

##### 2.2.2.1 – Citations dans le corps du texte

Le texte d’origine étant en anglais américain, on peut trouver un
point final à l’intérieur de guillemets. En français, il se
trouve la plupart du temps à l’extérieur.

Tant que le texte d’une section n’est pas établie, il est
préférable de laisser les sources des citations dans le corps du
texte à leur place et d’appliquer les *Conventions dans les
citations*.

Pour un appel en dehors d’une citation&nbsp;:

```
Blabla<fn id="01">01</fn>.
```

> Blabla[01].

Pour un appel dans une citation&nbsp;:

```xml
<cit>Blabla<fn id="01">01</fn></cit>.
```

> « Blabla[01] ».

##### 2.2.2.2 – Paragraphes de citations

On crée  des `<blockquote>` pour  chaque paragraphe de  citation. Un
bloc de citation de plusieurs paragraphes doit donc en fait être une
suite de `<blockquote>`.

Dans le cas d’un paragraphe de  citation, on ajoute, quand ils sont
précisés, le nom  de l’auteur et la source de  citation dans leur
propre `<blockquote>`.  Pour différencier ces blocs,  on emploie des
classes  CSS&nbsp;:  La classe  de  la  citation est  `citation`,  de
l’auteur·e de  la citation `citation_author`,  de la source  de la
citation `citation_source`.

`citation_source` applique automatiquement des italiques: il faut
encadrer de `<i>` tout ce qui **n’est pas** le titre de l’œuvre.

Ainsi l’exemple suivant&nbsp;:

```html
<blockquote class="citation">
    La propriété, c’est le vol&#160;!
</blockquote>
<blockquote class="citation_author">
    Pierre-Joseph Proudhon
</blockquote>
<blockquote class="citation_source">
    Qu’est-ce que la propriété&#160?<i> [</i>What Is Property?<i>], p.42</i>
</blockquote>
```

Donnera&nbsp;:

> La propriété, c’est le vol&nbsp;!
> 
> Pierre-Joseph PROUDHON
> 
> *Qu’est-ce que la propriété*&nbsp;*?* [*What is Property?*], p.42

Il ne faut pas insérer de guillemets pour encadrer la citation. La
citation est rendue visible par l’élement `<blockquote>`.

Pour ce qui est de l’italique et du gras dans la citation: il
faut réserver l’italique aux italiques déjà employées **dans
le texte d’origine**. Si le texte de la FAQ veut mettre en valeur
une partie de la citation, utiliser du **gras**.

Le texte anglophone d’origine emploie parfois du gras pour signifier
qu’il s’agit d’un titre d’ouvrage / de journal. Il faut alors
remplacer par de l’italique. Si le texte anglophone emploie à la fois 
le gras et l’italique pour un titre, conserver cette mise en forme : 
en plus de marquer qu’il s’agit d’un titre, la FAQ anglophone veut 
indiquer que cet ouvrage est important.

##### 2.2.2.3 – Conventions dans les citations

###### 2.2.2.3.1 – Sur les ouvrages cités

Employer `<em>` pour les titres d’œuvres, et non `<i>`.

Les noms des ouvrages dans la liste des œuvres en ligne doit
reflèter les règles qui suivent, afin que le lecteur puisse
retrouver facilement l’œuvre qu’il cherche.

###### 2.2.2.3.2 – L’ouvrage cité existe en français

Employer **son titre français** et intégrer à la suite le titre
original entre crochets et en italiques. Par exemple, avec l’œuvre
de Kropotkine:

```
<em>L’entr’aide, un facteur de l’évolution</em> [<em>Mutual Aid</em>]
```

> *L’entr’aide, un facteur de l’évolution* [*Mutual Aid*]

###### 2.2.2.3.3 – L’ouvrage cité n’existe pas en français

**Garder** le nom de l’ouvrage, le mettre en italique, et intégrer à
la suite le titre traduit en français (pas en italique). Par
exemple, pour l’œuvre de John Zerzan:

```
<em>Future Primitive</em> [Futur primitif]
```

> *Future Primitive* [Futur primitif]

#### 2.2.3 — Les notes (de bas de page)

Les notes de bas de page sont  dissociées du texte de la section, et
se trouvent  toutes dans  une partie particulière  du fichier  de la
section.

Il existe deux  systèmes de notes différents.  Le système "normal"
est celui des notes ordinaires, celles où le traducteur peut également 
intervenir. Le système "explain" est le celui des notes servant à expliquer 
certaines expressions du texte, voire à détailler un point précis.

##### 2.2.3.1 – Les notes du traducteur

Les notes du traducteur sont des notes du système "normal".

###### 2.2.3.1 – Notes sur la traduction

###### 2.2.3.2 – Notes d’explication

##### 2.2.3.1 – Les notes du texte

### 2.3 – Détails techniques

#### 2.3.1 – Les fichiers de section

1. Les deux dernières lignes de chaque fichier doivent uniquement
contenir `</body>` et `</html>`. Elles sont coupées pour insérer la
licence lors de la compilation du site.
2. Le `<body>` des sections (dont le sommaire de la section) doit
avoir la classe `text`: `<body class="text">`.

### 2.4 – Choix de vocabulaire

On préférera&nbsp;:

- entraide à entr’aide
- autogestion à auto**-**gestion
- autogéré(e) à auto**-**géré(e)

## 3 – Les indexs

### 3.1 – Éditer l’index des noms propres

Pour constituer la liste dans apparitions (entry/locations/loc), on
peut utiliser Grep, mais dans ce cas-ci, la sortie d’Ack est plus
agréable (elle regroupe les résultats par fichiers, ce qui est plus
lisible).

### 3.2 – Éditer l’index des groupes politiques

## 4 – Les bibliographies

### 4.1 – Éditer la liste des ouvrages lisibles en lignes

La gestion des liens vers les ouvrages lisibles en lignes est
détaillée sur [la page dédiée](doc/books.md).

### 4.2 – Éditer la bibliographie générale de la FAQ

À l’heure actuelle, la refonte de la bibliographie générale de
la FAQ n’est pas terminée. **N’éditez pas, ne supprimez pas
 `faq_bib.php` **.

La gestion de la bibliographie générale est détaillée sur [la
page dédiée](doc/faq_bib.md).

### 4.3 – Balises utiles pour les références bibliographiques

**Cette partie ne concerne pas la bibliographie générale.
La gestion de la bibliographie générale est détaillée sur 
[la page dédiée](doc/faq_bib.md).**

Pour se simplifier la vie dans les références bibliographiques
présentes dans les sections, on peut employer :

#### `<no />`

`<no />` formate convenablement les n<sup>o</sup> de journaux,
etc.

Ainsi, au lieu de d’écrire :

```html
Dans le n<sup>o</sup> 10 du journal
```

*Note : Il ne faut pas utiliser le symbole « degré » comme 
dans "n°". Utiliser un "o" en exposant est plus correct, 
plus lisible…*

On écrira :

```xml
Dans le <no n="10" /> du journal
```

#### `<opcit />`

Faut-il écrire « Op.Cit », « Op. Cit. », « op.cit », « op. cit » ?

La balise `<opcit />` évite de se poser la question.

## 5 – Éditer le style de la FAQ

Les styles CSS se trouvent dans `inc/`.

SCSS est employé pour simplifier l’écriture des CSS.

À noter que les styles emploient Flex.

## 6 – Éditer les styles de transformation de la FAQ

### 6.1 — Fichiers de section

### 6.2 — Table des matière de section

### 6.3 — Index des noms

### 6.4 — Index des groupes politiques

### 6.5 — Liste des œuvres disponibles en ligne

### 6.6 — Page d’accueil

### 6.7 — Bibliographie

## 7 – Compiler le site

Utiliser `make`. Le site se trouve dans `out/`.

Par défaut, `make` produit également un dossier de bilans pour
repérer des petites erreurs à corriger.

## 8 – FAQ de la FAQ

(insérer ici le son d’*Inception*)

### 8.1 – Pourquoi ne pas faire un wiki&#160;?

Les wikis sont très bien pour faire de gros ouvrages (Wikipédia le
démontre chaque jour), mais&#160;:

- Ils ont des pré-requis techniques pas forcément à la portée de très petits hébergeurs.
- Ils posent quelques problèmes quant à l’établissement d’un texte
- Un wiki peut subir des modifications malveillantes.
- L’export de la FAQ vers d’autres formats ne serait pas aisé.

### 8.2 – Pourquoi renvoyer vers une page de liens plutôt que d’insérer les liens directement dans le texte&#160;?

La FAQ anarchiste est un ouvrage **très** long. La traduction va
prendre plusieurs années, et une fois le travail achevé, il
faudra éliminer les liens morts et les remplacer par des liens
fonctionnels. Il faut donc limiter au maximum les efforts nécessaire
pour maintenir les pages.

Placer des liens directement dans le texte (plutôt que de
renvoyer vers une page qui centralise liens) rend difficile la
maintenance&#160;:

- Si on se souvient d’avoir ajouté tel et tel lien dans telle ou
telle page, comment être sûr de le retrouver *deux mois après*&nbsp;?
- Si on insère des liens vers un site qui par la suite n’existe
plus, qu’est-ce qui est préférable&#160;? Éditer une page, ou en
éditer 20&nbsp;?

De plus, si le lecteur arrive directement sur une page qui n’existe
plus, il sera frustré, et la FAQ apparaîtra comme vieillotte.

Avec une page qui centralise les liens&#160;:

- Si la page est bien structurée, on retrouve tout facilement.
- Une fois le renvoi placé, il est plus facile de tester si les
liens sont encore valides, et le cas échéant, de les remplacer par
des liens corrects.

Conclusion&#160;:

> **Il vaut mieux s’embêter maintenant que s’emmerder prodigieusement dans cinq ans.**

### 8.3 – Pourquoi passer en XML la bibliographie générale / la liste des textes lisibles en ligne / les indexes&#160;?

* C’est  plus simple  à  maintenir. On  peut générer  d’autres
présentations des données (par exemple, le tableau des fréquences 
d’apparition des noms propres est créé à partir du fichier 
XML qui sert à créer l’index des noms propres).
* On peut trier les données en sortie, donc on peut éviter (en
partie seulement) de se soucier de l’ordre des données.
* Les pages générées à partir des fichiers XML sont uniformes.
* Les liens entre l’index de noms et la liste des textes lisibles
en lignes sont créés automatiquement.

### 8.4 – Pourquoi employer XML pour le texte

Mêmes réponses que pour le point précédent.
