# Éditer les ouvrages lisibles en lignes

Le fichier `books.php`  est généré à partir  de `books.xml`. Cela
permet de générer automatiquement les identifiants de chaque livre,
selon la forme `AUTEUR_IDLIVRE`, ou  `AUTEUR_IDLIVRE_IDTOME` quand il y a
un tome précis à consulter.

Par exemple:

- `guerin_nogods` renvoie à *Ni dieu ni maître* de Daniel Guérin.
- `bakounine_completeworks_t1` correspond au tome 1 des *Œuvres complètes* de Bakounine.

