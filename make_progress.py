#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Produit une page listant:

- les (sous-)sections dont la traduction (partielle ou complète) reste encore à faire.
- les (sous-)sections dont la traduction est terminée (mais nécessite une relecture).

Pour ce faire, on utilise les sorties grep contenues dans
progress.txt et finished.txt.

Le fichier toc.xml fournit quant à lui le titre complet
des (sous-)sections — plus parlant que le simple couple
section/sous-section.
"""

import xml.etree.ElementTree as ET

# --- Utilitaires -----------------------------------------

def get_section_position(line):
    """Transforme le nom des fichiers de la sortie grep en un couple section/sous-section"""

    section = line.split("/")[0].upper()

    try:
        subsection = line.split(".php")[0].split("/")[1].split("_")[1]
    except IndexError:
        if line.startswith("intro.php"):
            section = "Introduction générale de la FAQ"
            subsection = "Page unique"

    if subsection == "toc":
        subsection = "Table des matières"

    if subsection == "0":
        subsection = "Introduction"

    if "all" in subsection:
        subsection = "Page unique"

    return section, subsection

# --- Données ---------------------------------------------

def get_sections_datas():
    """Obtient le nom des sections et sous-sections à partir de toc.xml"""

    toc = []

    toc_parsed = ET.parse('toc.xml')
    toc_xml = toc_parsed.getroot()

    for t_file in toc_xml:

        if t_file.tag == "file":

            if t_file.attrib:
                t_item = {
                    "name": "",
                    "file": "",
                    "sec": t_file.attrib["sec"],
                    "sub": t_file.attrib["sub"]
                }

                for data in t_file:
                    if data.tag == "name":
                        t_item["name"] = data.text
                    if data.tag == "url":
                        t_item["file"] = data.text

                toc.append(t_item)

            else:
                intro = {"name":"","file":"","sec":"intro","sub":""}

                for data in t_file:
                    if data.tag == "name":
                        intro["name"] = data.text
                    if data.tag == "url":
                        intro["file"] = data.text

                toc.append(intro)

    return toc

def get_finished():
    """Récupère les listes des (sous-)sections terminées"""

    finished = []

    with open("finished.txt","r") as finished_file:

        for line in finished_file:
            line = line.rstrip()

            if not "contribuer/guide.md" in line:
                section, subsection = get_section_position(line)

            dts = [
                d.strip() for 
                d in line.split("<!--")[1].split("-->")[0].split("|")[1:]
            ]

            proofreading = False

            for detail in dts:

                if "RELECTURE" in detail:
                    proofreading = True

            finished.append(
                {
                    "section":section,
                    "subsection":subsection,
                    "need-proofreading":proofreading,
                    "count":0,
                    "whole":True
                }
            )

    return finished

def get_todo():
    """Récupère la liste des passages à traduire"""

    to_translate = []

    with open("progress.txt","r") as progress_file:

        for line in progress_file:
            line = line.rstrip()

            if "TRADUIRE ENTIER" in line:
                whole = True
            else:
                whole = False

            if not "contribuer/guide.md" in line:
                section, subsection = get_section_position(line)

            already = len(
                [
                    i for i in to_translate 
                    if i["section"] == section 
                    and i["subsection"] == subsection
                ]
            )

            if already:

                for i in to_translate:
                    if i["section"] == section:
                        if i["subsection"] == subsection:
                            i["count"] += 1

            else:

                to_translate.append(
                    {
                        "section":section,
                        "subsection":subsection,
                        "count":1,"whole":whole
                    }
                )

    return to_translate

# --- Fonctions pour l’écriture de la page ----------------

def write_header(fo):
    """Écrit le header de la page"""
    fo.write("<!DOCTYPE HTML>\n")
    fo.write("<html>\n")
    fo.write("    <head>\n")
    fo.write("        <meta charset='utf-8'>\n")
    fo.write("        <title>FAQ Anarchiste (francophone)</title>\n")
    fo.write("        <link rel='stylesheet' type='text/css' href='inc/master.css' />\n")
    fo.write("        <link rel='icon' href='inc/favicon.ico' type='image/gif' />\n")
    fo.write("        <?php require 'inc/loadaccess.php'; ?>\n")
    fo.write("    </head>\n")
    fo.write("    <body>\n")
    fo.write("        <a id='back'></a>\n")
    fo.write("        <h1 class='left'>La FAQ Anarchiste (francophone)</h1>\n")
    fo.write("        <header>\n")
    fo.write("            <div class='navigation'>\n")
    fo.write("            <?php require 'inc/mirrors.php'; altLangDiv('none', '1'); ?>\n")
    fo.write("            <?php require 'inc/toc.php'; echo_nav('root', '1'); ?>\n")
    fo.write("            </div>\n")
    fo.write("        </header>\n")

    return fo

def write_footer(fo):
    """Écrit le footer de la page"""

    footer = ""

    with open("src/footer/footer","r") as foot:
        footer = foot.read()

    fo.write(footer)

    return fo

def convert_to_name_file(data):
    sec,sub = i["section"],i["subsection"]

    if sec == "Introduction générale de la FAQ":
        f = {
            "sec":"IntroGenerale",
            "name":sec,
            "subsection":"",
            "count":i["count"],
            "whole":i["whole"]
        }

    elif sub == "Introduction":
        # Obtient les infos du fichier correspondant à l’introduction de la section sec
        # donc le fichier ayant pour "sec" sec, et ayant pour "sub" 0 #MortalKombat
        f = [ii for ii in toc if ii["sec"] == sec.lower() and ii["sub"] == "0"][0]

    elif sub == "Page unique":
        # TODO Complèter

        # Traitement au cas par cas, quand la section est contenue dans un seul fichier.
        # C’est le cas de certaines annexes.
        if i["section"] == "APP2":
            f = {
                "sec":"App.2",
                "name":"Appendice 2 - Les symboles de l'Anarchie",
                "subsection":"",
                "count":i["count"],
                "whole":i["whole"]
            }
    else:
        # Obtient les infos du fichier correspondant à la section sec et à la
        # sous-section sub.
        f = [ii for ii in toc if ii["sec"] == sec.lower() and ii["sub"] == sub]
        if f:
            f = f[0]

    if f:
        if f["sec"] not in ["intro","App.2","IntroGenerale"]:
            f["file"] = "{}/{}".format(f["sec"].lower(),f["file"])
        else:
            if f["sec"] == "App.2":
                f["file"] = "app2/app2_all.php"
            if f["sec"] == "IntroGenerale":
                f["file"] = "intro.php"

        return f
    else:
        return False

# --- Création de la page ---------------------------------

if __name__ == "__main__":
    # Récupère les noms et fichiers des sections de la FAQ
    toc = get_sections_datas()

    # Récupère la liste des passages à traduire
    to_translate = get_todo()

    # Récupère les listes des (sous-)sections terminées
    finished = get_finished()

    # Écrit la page de progression de la traduction

    with open("progress.php","w") as wf:
        wf = write_header(wf)

        wf.write("        <h2>Parties du texte déjà traduites intégralement</h2>\n")

        if finished:
            wf.write("        <ul>\n")

            for i in finished:
                f = convert_to_name_file(i)

                if f:

                    wf.write("            <li>")

                    wf.write("<a href='{}'>{}</a>".format(f["file"],f["name"]))

                    if i["need-proofreading"]:
                        wf.write(" <b>[Une relecture serait bienvenue]</b>")

                    wf.write("</li>\n")

            wf.write("        </ul>\n")
        else:
            wf.write("        <p>Étant donné que des parties du texte sont déjà intégralement")
            wf.write(" traduites, si vous lisez ceci, c’est que le script chargé de créer")
            wf.write(" cette partie de la page a planté.</p>\n")

        wf.write("        <h2>Parties du texte restant à traduire</h2>\n")

        if to_translate:
            wf.write("        <ul>\n")

            for i in to_translate:

                wf.write("            <li>")

                ct = i["count"]
                f = convert_to_name_file(i)

                if i["whole"]:
                    wf.write(
                        "Tout <a href='{}'>{}</a>".format(
                            f["file"],f["name"]
                        )
                    )
                else:
                    if ct > 1:
                        wf.write(
                            "{} passages dans <a href='{}'>{}</a>".format(
                                ct,f["file"],f["name"]
                            )
                        )
                    else:
                        wf.write(
                            "1 passage dans <a href='{}'>{}</a>".format(f["file"],f["name"])
                        )

                wf.write("</li>\n")

            wf.write("        </ul>\n")
        else:
            wf.write("        <p>Soit la traduction est finie, soit le script chargé de créer")
            wf.write("cette page a planté.</p>\n")

        wf = write_footer(wf)

# vim:set shiftwidth=4 softtabstop=4:
