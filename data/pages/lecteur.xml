<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet href="datas/text.xsl" type="text/xsl"?>

<document>
  <meta>
    <toc>
      <notable />
      <section>lecteur</section>
    </toc>
    <type>front</type>
    <translation>complete</translation>
    <pagetitle>FAQ Anarchiste – Pour le lecteur</pagetitle>
    <head>Pour le lecteur francophone</head>
  </meta>
  <text>
    <div n="0">
      <p class="nocount">Ce miroir constitue un essai de traduction et
      d’édition de qualité professionnelle, ou, du moins, très
      acceptable pour un lecteur français.</p>

      <p class="nocount">Si vous voulez contribuer à la traduction
      (ou un à un autre aspect du miroir), merci de visiter 
      <a href="https://framagit.org/VChamper/anarfaq">cette page</a>.</p>

      <p class="nocount">Voici quelques informations pour une lecture
      informée, ainsi que la liste des choix éditoriaux qui ont été
      faits.</p>

      <p class="nocount">Nous espérons qu’ils vous seront favorables.</p>

      <div n="1">
	<head>Lire la FAQ</head>

	<ol>
	  <li>
	    <b>Accessibilité</b> – Si vous êtes dyslexique, ou que vous nécessitez
	    un ajustement quant à l’accessibilité du site, consultez 
	    <a href="access.php">cette page</a>. <br/>Elle permet avant tout de passer
	    à la fonte Open Dyslexic. Parce qu’activer l’accessibilité a
	    des conséquences esthétiques sur le site et parce que cela
	    nécessite un <em>cookie</em>, l’accessibilité n’est pas
	    activée par défaut.
	  </li>

	  <li>
	    <b>Index</b> – Les textes de ce miroir peuvent renvoyer à un
	    <a href="idx/names.php">index des noms propres</a>, pour repérer la
	    citation (ou l’évocation) d’un auteur dans l’ensemble de la
	    FAQ. <br/>Par exemple<e/>: <idx s="name" id="bakounine">Bakounine</idx>.
	  </li>

	  <li>
	    <b>Œuvres en ligne</b> - Des œuvres sont fréquemment citées dans cette
	    FAQ. Nous avons essayé de renvoyer le lecteur le plus possible vers
	    des versions en ligne de ces œuvres. Quand une œuvre est disponible en
	    ligne, nous renvoyons vers une page qui centralise tous les miroirs
	    du texte en question, ce renvoi étant indiqué par l’icône 📖.<br/>
	    Par exemple<e/>: Errico Malatesta, <em>Anarchie et Organisation</em> 
	    <book author="malatesta" id="AnarchieOrganisation" />.
	  </li>

	  <li>
	    <b>Notes supplémentaires</b> — En plus des notes relatives à la 
	    traduction en français qui s’ajoutent à celles du texte original, nous 
	    avons intégré à part des notes pour expliquer certaines expressions. 
	    Par exemple, pour le <cit><a href="a/a_0.php#enote_01">lit de 
	    Procruste</a></cit>.
	  </li>

	  <li>
	    <b>Renvoi vers un paragraphe</b> — Dans les sections dont la
	    traduction est terminée, on trouve au début du paragraphe (ou
	    en fin, pour le tout premier paragraphe de la section),
	    l’icône 🔗 qui permet de copier l’adresse du paragraphe
	    pour renvoyer quelqu’un vers lui.<br/>Par exemple, celui-ci
	    renvoie vers le paragraphe 8 de cette page<e/>: <a href="#8"
	    class="pl">🔗</a>.
	  </li>
	</ol>
      </div>

      <div n="2">
	<head>Choix éditoriaux</head>

	<p class="nocount">Le texte a été établi à partir de la traduction incomplète de 
	  <a href="http://faqanarchiste.free.fr">faqanarchiste.free.fr</a>, 
	  et la version anglaise disponible sur 
	  <a href="https://theanarchistlibrary.org/category/author/the-anarchist-faq-editorial-collective?sort=title_asc&amp;rows=100">The Anarchist Library</a>.
	</p>

	<p class="nocount">
	Le mot anglais <i>libertarian</i> provient du français
	<i>libertaire</i>, et a été usurpé par les
	<cit>anarcho</cit>-capitalistes très récemment, en vue de
	se donner un vernis subversif. <br/>En français, le nom de cet
	extrémisme capitaliste est, par un aller-retour linguistique,
	<i>libertarianisme</i>. Ses deux ou trois militants se nomment
	<i>libertariens</i>. <br/>De ce fait, le terme anglais <i>libertarian</i>
	est traduit par <i>libertaire</i> quand il s’agit d’anarchistes,
	et par <i>libertarien</i> quand il s’agit au contraire de ces capitalistes
	extrémistes.</p>

	<p class="nocount">
	Le non-débat sur l’<cit>anarcho</cit> ayant lieu qu’aux États-Unis, l’appendice 1
	de la section anglophone originale a été supprimée. Elle est en effet redondante 
	avec plusieurs sous-sections, et avec la section F.</p>

	<p class="nocount">Le texte des sections et appendices de la FAQ a des
	paragraphes numérotés pour favoriser la citation et la prise de
	notes.</p>

	<p class="nocount">Certaines orthographes un peu datées (entr’aide, clef) sont
	remplacées par leur équivalent moderne (entraide, clé), sauf si
	elles font partie d’une citation ou d’une référence à une
	édition précise. Par exemple, une référence à
	l’<em>Entraide</em> de Kropotkine, sans mention d’édition est
	orthographiée de façon moderne. Mais si un auteur, ou
	une édition particulière de l’œuvre de Kropotkine utilise
	<em>entr’aide</em>, cela est conservé.</p>

	<p class="nocount">Les chiffres romains ne sont pas employés pour les siècles.
	D’une part, la version anglaise ne les utilise pas, et d’autre
	part, la lecture peut être pénible pour ceux qui ne sont pas à
	l’aise avec ce type de numérotation.</p>

	<p class="nocount">Les références aux œuvres citées par la FAQ anglophone 
	sont pour la plupart conservées, selon les cas présentés
	ci-dessous. <br/>Cependant, quand une version déjà traduite en français
	(ou en français à l’origine) d’une citation existe, sa
	référence anglophone est supprimée.</p>

	<ul>
	  <li style="font-size: inherit;">
	    <b>Quand le livre existe en français</b>, les titres sont
	    indiqués comme suit:<br/><cit><em>L’entraide, un facteur de
	      l’évolution</em> [<em>Mutual Aid</em>]</cit>.
	  </li>
	  <li style="font-size: inherit;">
	    <b>Quand le livre n’existe pas en français</b>, les titres sont
	    indiqués comme suit. Le titre français est alors traduit pour
	    donner une idée <b>approximative</b> du sujet du livre.
	    <br/><cit><em>Future Primitive</em> [Futur primitif]</cit>. 
	  </li>
	</ul>
      </div>
    </div>
  </text>
</document>
