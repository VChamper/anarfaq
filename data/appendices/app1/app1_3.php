<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset=utf-8 />

        <title>FAQ Anarchiste (francophone) </title>
        <link rel="stylesheet" type="text/css" href="../inc/master.css">
        <link rel="icon" href="../inc/favicon.ico" type="image/gif" />
        <?php require "../inc/loadaccess.php"; ?>

    </head>

    <body class="text">
        <a id="back"></a>
        <h1 class="left">La FAQ Anarchiste (francophone)</h1>
        <header>
            <div class="navigation">
                <?php require "../inc/mirrors.php"; altLangDiv("app", "1"); ?>
                <?php require "../inc/toc.php"; echo_nav("app", "1"); ?>
            </div>
        </header>

<h1>Appendice 1&nbsp;: Anarchisme et «&nbsp;anarcho&nbsp;»-capitalisme</h1>

<h2 id="toc15">Is “anarcho”-capitalism a type of anarchism?</h2>

<p>Anyone  who   has  followed   political  discussion  on   the  net
has  probably  come  across people  calling  themselves  libertarians
but  arguing  from  a  right-wing,  pro-capitalist  perspective.  For
most   Europeans   this   is   weird,   as   in   Europe   the   term
<em>“libertarian”</em> is almost always  used in conjunction with
<em>“socialist”</em>  or  <em>“communist.”</em>  In  the  US,
though,  the  Right has  partially  succeeded  in appropriating  this
term  for itself.  Even stranger,  however, is  that a  few of  these
right-wingers  have started  calling  themselves “anarchists”  in
what must be one of the finest examples of an oxymoron in the English
language: ‘Anarcho-capitalist’!!</p>

<p>Arguing  with  fools  is  seldom  rewarded,  but  to  allow  their
foolishness to go  unchallenged risks allowing them  to deceive those
who are new to anarchism. That’s what this section of the anarchist
FAQ  is  for,  to  show  why  the  claims  of  these  “anarchist”
capitalists are false. Anarchism  has always been anti-capitalist and
any  “anarchism” that  claims  otherwise cannot  be  part of  the
anarchist tradition. So this section of the FAQ does not reflect some
kind  of  debate  within  anarchism,  as many  of  these  types  like
to  pretend,  but a  debate  between  anarchism  and its  old  enemy,
capitalism. In  many ways this  debate mirrors the one  between Peter
Kropotkin  and Herbert  Spencer, an  English pro-capitalist,  minimal
statist, at the turn the 19<sup>th</sup>  century and, as such, it is
hardly new.</p>

<p>The   “anarcho”-capitalist  argument   hinges  on   using  the
dictionary  definition of  “anarchism”  and/or “anarchy”  —
they try to define anarchism as being “opposition to government,”
and  nothing  else.  However,  dictionaries  are  hardly  politically
sophisticated and their definitions rarely  reflect the wide range of
ideas associated with political theories  and their history. Thus the
dictionary  “definition” is  anarchism  will tend  to ignore  its
consistent views on authority,  exploitation, property and capitalism
(ideas easily discovered if actual anarchist texts are read). And, of
course,  many dictionaries  “define”  anarchy  as “chaos”  or
“disorder” but  we never  see “anarcho”-capitalists  use that
particular definition!</p>

<p>And for this strategy to work, a lot of “inconvenient” history
and  ideas from  all  branches  of anarchism  must  be ignored.  From
individualists like  Spooner and Tucker to  communists like Kropotkin
and  Malatesta,  anarchists  have always  been  anti-capitalist  (see
section G  for more  on the  anti-capitalist nature  of individualist
anarchism). Therefore “anarcho”-capitalists are not anarchists in
the same sense that rain is not dry.</p>

<p>Of course, we cannot  stop the “anarcho”-capitalists using the
words  “anarcho”, “anarchism”  and “anarchy”  to describe
their  ideas.  The  democracies  of  the  west  could  not  stop  the
Chinese  Stalinist  state  calling  itself  the  People’s  Republic
of  China.  Nor could  the  social  democrats  stop the  fascists  in
Germany calling  themselves “National Socialists”. Nor  could the
Italian anarcho-syndicalists  stop the fascists using  the expression
“National  Syndicalism”. This  does not  mean that  any of  these
movements  actual  name  reflected  their  content  —  China  is  a
dictatorship,  not  a democracy,  the  Nazi’s  were not  socialists
(capitalists made  fortunes in  Nazi Germany  because it  crushed the
labour movement), and the Italian fascist state had nothing in common
with anarcho-syndicalists ideas of  decentralised, “from the bottom
up” unions and the abolition of the state and capitalism.</p>

<p>Therefore, just because someone uses a label it does not mean that
they support  the ideas associated with  that label. And this  is the
case with “anarcho”-capitalism — its ideas are at odds with the
key ideas  associated with all  forms of traditional  anarchism (even
individualist anarchism which is often  claimed as being a forefather
of the ideology).</p>

<p>All    we     can    do    is     indicate    <strong>why</strong>
“anarcho”-capitalism is  not part of the  anarchist tradition and
so has falsely appropriated the name. This section of the FAQ aims to
do just that  — present the case  why “anarcho”-capitalists are
not  anarchists.  We do  this,  in  part,  by indicating  where  they
differ from genuine  anarchists (on such essential  issues as private
property,  equality, exploitation  and  opposition  to hierarchy)  In
addition, we  take the opportunity  to present a general  critique of
right-libertarian claims  from an anarchist perspective.  In this way
we show  up why  anarchists reject  that theory  as being  opposed to
liberty and anarchist ideals.</p>

<p>We   are   covering  this   topic   in   an  anarchist   FAQ   for
three  reasons.   Firstly,  the   number  of   “libertarian”  and
“anarcho”-capitalists on the net means that those seeking to find
out about  anarchism may conclude  that they are  “anarchists” as
well.  Secondly,  unfortunately,  some  academics  and  writers  have
taken  their  claims of  being  anarchists  at  face value  and  have
included their ideology into general accounts of anarchism. These two
reasons  are  obviously  related  and  hence the  need  to  show  the
facts of  the matter.  As we have  extensively documented  in earlier
sections, anarchist theory has  always been anti-capitalist. There is
no  relationship  between  anarchism  and capitalism,  in  any  form.
Therefore, there  is a  need for  this section  in order  to indicate
exactly  why  “anarcho”-capitalism  is  not  anarchist.  As  will
be  quickly  seen from  our  discussion,  almost all  anarchists  who
become  aware  of “anarcho”-capitalism  quickly  reject  it as  a
form  of  anarchism  (the  better  academic  accounts  do  note  that
anarchists generally reject the claim, though). The last reason is to
provide other anarchists  with arguments and evidence  to use against
“anarcho”-capitalism  and  its claims  of  being  a new  form  of
“anarchism.”</p>

<p>So  this  section  of  the  FAQ  does  not,  as  we  noted  above,
represent  some kind  of “debate”  within anarchism.  It reflects
the  attempt  by  anarchists  to  reclaim  the  history  and  meaning
of  anarchism  from  those  who  are attempting  to  steal  its  name
(just  as  right-wingers in  America  have  attempted to  appropriate
the  name  “libertarian”  for  their  pro-capitalist  views,  and
by  so  doing  ignore  over  100  years  of  anti-capitalist  usage).
However,  this  section  also  serves two  other  purposes.  Firstly,
critiquing  right-libertarian  and “anarcho”-capitalist  theories
allows  us   to  explain  anarchist   ones  at  the  same   time  and
indicate why  they are  better. Secondly,  and more  importantly, the
“ideas” and  “ideals” that  underlie “anarcho”-capitalism
are usually  identical (or, at the  very least, similar) to  those of
neo-liberalism. This was noted by Bob  Black in the early 1980s, when
a <em>“wing of the Reaganist Right has obviously appropriated, with
suspect  selectivity, such  libertarian  themes  as deregulation  and
voluntarism. Ideologues  indignant that  Reagan has  travestied their
principles. Tough  shit! I notice  that it’s their  principles, not
mine,  that  he  found  suitable  to  travesty.”</em>  [<strong>The
Libertarian  As  Conservative</strong>]  This   was  echoed  by  Noam
Chomsky two decades later  when while <em>“nobody takes [right-wing
libertarianism]  seriously”</em> as  <em>“everybody knows  that a
society that  worked by ...  [its] principles would  self-destruct in
three seconds”</em> the <em>“only  reason”</em> why some people
<em>“pretend to  take it seriously is  because you can use  it as a
weapon.”</em>  [<strong>Understanding  Power</strong>, p.  200]  As
neo-liberalism is being used as  the ideological basis of the current
attack on the working  class, critiquing “anarcho”-capitalism and
right-libertarianism also allows use  to build theoretical weapons to
use to resist this attack and aid the class struggle.</p>

<p>A   few  more   points  before   beginning.  When   debating  with
“libertarian” or “anarchist”  capitalists it’s necessary to
remember that while  they claim “real capitalism”  does not exist
(because all  existing forms  of capitalism  are statist),  they will
claim  that  all  the  good  things  we  have  —  advanced  medical
technology, consumer  choice of  products, etc. —  are nevertheless
due to “capitalism.” Yet if you  point out any problems in modern
life, these  will be blamed  on “statism.” Since there  has never
been  and  never  will  be  a capitalist  system  without  some  sort
of  state,  it’s  hard  to argue  against  this  “logic.”  Many
actually use  the example of  the Internet as  proof of the  power of
“capitalism,”  ignoring the  fact  that the  state  paid for  its
development before turning it over to companies to make a profit from
it.  Similar points  can be  made  about numerous  other products  of
“capitalism” and the  world we live in.  To artificially separate
one aspect of a complex evolution  fails to understand the nature and
history of the capitalist system.</p>

<p>In  addition to  this ability  to be  selective about  the history
and  results  of  capitalism,  their theory  has  a  great  “escape
clause.” If  wealthy employers abuse  their power or the  rights of
the  working  class  (as  they  have always  done),  then  they  have
(according to  “libertarian” ideology) ceased to  be capitalists!
This is  based upon  the misperception that  an economic  system that
relies  on force  <strong>cannot</strong>  be  capitalistic. This  is
<strong>very</strong> handy as it can absolve the ideology from blame
for any (excessive) oppression which  results from its practice. Thus
individuals are always to blame, <strong>not</strong> the system that
generated the opportunities for abuse they freely used.</p>

<p>Anarchism  has  always been  aware  of  the existence  of  “free
market” capitalism, particularly its  extreme (minimal state) wing,
and has  always rejected it. As  we discuss in section  7, anarchists
from Proudhon onwards have rejected the  idea of any similar aims and
goals  (and,  significantly, vice  versa).  As  academic Alan  Carter
notes, anarchist concern for equality as a necessary precondition for
genuine freedom  means <em>“that  is one very  good reason  for not
confusing anarchists with liberals or economic ‘libertarians’ —
in other words, for not lumping  together everyone who is in some way
or another  critical of  the state.  It is why  calling the  likes of
Nozick  ‘anarchists’ is  highly misleading.”</em>  [<em>“Some
notes  on  ‘Anarchism’”</em>,  pp.  141–5,  <strong>Anarchist
Studies</strong>, vol. 1, no. 2, p. 143] So anarchists have evaluated
“free  market” capitalism  and rejected  it as  non-anarchist for
over  150 years.  Attempts  by “anarcho”-capitalism  to say  that
their  system is  “anarchist”  flies  in the  face  of this  long
history of  anarchist analysis.  That some  academics fall  for their
attempts to  appropriate the  anarchist label  for their  ideology is
down to a false premise: it  <em>“is judged to be anarchism largely
because  some   anarcho-capitalists  <strong>say</strong>   they  are
‘anarchists’ and because they criticise the State.”</em> [Peter
Sabatini, <strong>Social Anarchism</strong>, no. 23, p. 100]</p>

<p>More generally, we  must stress that most (if  not all) anarchists
do not want to live in  a society <strong>just like this one</strong>
but without state coercion and  (the initiation of) force. Anarchists
do  not confuse  “freedom”  with the  “right”  to govern  and
exploit  others nor  with being  able to  change masters.  It is  not
enough to  say we can start  our own (co-operative) business  in such
a  society.  We  want  the  abolition of  the  capitalist  system  of
authoritarian  relationships, not  just  a change  of  bosses or  the
possibility of little  islands of liberty within a  sea of capitalism
(islands which are always in danger of being flooded and our activity
destroyed).  Thus, in  this  section  of the  FAQ,  we analysis  many
“anarcho”-capitalist claims on their  own terms (for example, the
importance  of equality  in the  market or  why capitalism  cannot be
reformed away  by exchanges on  the capitalist market) but  that does
not mean we desire a society nearly identical to the current one. Far
from it, we  want to transform this society into  one more suited for
developing and enriching individuality and freedom. But before we can
achieve  that we  must critically  evaluate the  current society  and
point out its basic limitations.</p>

<p>Finally, we  dedicate this section  of the  FAQ to those  who have
seen  the real  face of  “free  market” capitalism  at work:  the
working men  and women (anarchist or  not) murdered in the  jails and
concentration  camps or  on the  streets  by the  hired assassins  of
capitalism.</p>

<h3 id="toc16">1     Are     “anarcho”-capitalists     really anarchists?</h3>

<p>In a  word, no.  While “anarcho”-capitalists obviously  try to
associate themselves with  the anarchist tradition by  using the word
“anarcho” or by calling  themselves “anarchists”, their ideas
are distinctly  at odds  with those associated  with anarchism.  As a
result, any  claims that their ideas  are anarchist or that  they are
part of the anarchist tradition or movement are false.</p>

<p>“Anarcho”-capitalists claim to be  anarchists because they say
that they oppose  government. As such, as noted in  the last section,
they use a dictionary definition of anarchism. However, this fails to
appreciate that anarchism is a <strong>political theory</strong>, not
a  dictionary  definition.  As dictionaries  are  rarely  politically
sophisticated things,  this means  that they  fail to  recognise that
anarchism  is more  than just  opposition to  government, it  is also
marked  a opposition  to  capitalism (i.e.  exploitation and  private
property).  Thus, opposition  to government  is a  necessary but  not
sufficient  condition for  being an  anarchist —  you also  need to
be  opposed  to  exploitation  and capitalist  private  property.  As
“anarcho”-capitalists do not consider  interest, rent and profits
(i.e. capitalism)  to be exploitative nor  oppose capitalist property
rights, they are not anarchists.</p>

<p>Moreover,  “anarcho”-capitalism  is inherently  self-refuting.
This  can  be  seen   from  leading  “anarcho”-capitalist  Murray
Rothbard. he  thundered against the  evil of the state,  arguing that
it  <em>“arrogates  to itself  a  monopoly  of force,  of  ultimate
decision-making power, over a given area territorial area.”</em> In
and of itself, this definition is unremarkable. That a few people (an
elite of rulers) claim  the right to rule others must  be part of any
sensible definition of the state or government. However, the problems
begin  for  Rothbard when  he  notes  that <em>“[o]bviously,  in  a
free  society,  Smith has  the  ultimate  decision-making power  over
his  own just  property,  Jones over  his, etc.”</em>  [<strong>The
Ethics  of  Liberty</strong>,   p.  170  and  p.   173]  The  logical
contradiction  in  this  position  should  be  obvious,  but  not  to
Rothbard. It shows the power of  ideology, the ability of means words
(the  expression <em>“private  property”</em>)  to  turn the  bad
(<em>“ultimate  decision-making power  over  a given  area”</em>)
into  the good  (<em>“ultimate decision-making  power over  a given
area”</em>).</p>

<p>Now, this contradiction can be solved in only <strong>one</strong>
way  — the  owners of  the <em>“given  area”</em> are  also its
users. In  other words, a  system of possession (or  “occupancy and
use”) as favoured by anarchists.  However, Rothbard is a capitalist
and  supports  private property.  In  other  words, wage  labour  and
landlords. This means that he supports a divergence between ownership
and  use and  this  means that  this <em>“ultimate  decision-making
power”</em> extends  to those who <strong>use,</strong>  but do not
own, such property (i.e. tenants  and workers). The statist nature of
private property is  clearly indicated by Rothbard’s  words — the
property owner  in an “anarcho”-capitalist society  possesses the
<em>“ultimate  decision-making  power”</em>  over a  given  area,
which is also what the state has currently. Rothbard has, ironically,
proved  by his  own definition  that “anarcho”-capitalism  is not
anarchist.</p>

<p>Rothbard does try to solve this obvious contradiction, but utterly
fails. He simply  ignores the crux of the matter,  that capitalism is
based on hierarchy and, therefore,  cannot be anarchist. He does this
by arguing that  the hierarchy associated with capitalism  is fine as
long  as the  private property  that produced  it was  acquired in  a
“just” manner.  In so doing he  yet again draws attention  to the
identical authority structures and  social relationships of the state
and property. As he puts it:</p>

<blockquote class="citation">
“<strong>If</strong>  the State  may  be  said too  properly
<strong>own</strong>  its territory,  then  it is  proper  for it  to
make  rules for  everyone  who  presumes to  live  in  that area.  It
can  legitimately seize  or  control private  property because  there
<strong>is</strong>  no  private property  in  its  area, because  it
really owns the entire land  surface. <strong>So long</strong> as the
State permits  its subjects to leave  its territory, then, it  can be
said  to  act  as does  any  other  owner  who  sets down  rules  for
people living on his property.”</em> [<strong>Op. Cit.</strong>, p.
170]
</blockquote>

<p>Obviously Rothbard argues that the state does not “justly” own
its territory — but given that the current distribution of property
is just as much the result of violence and coercion as the state, his
argument is  seriously flawed. It amounts,  as we note in  section 4,
to  little  more  than  an  <em><strong>“immaculate  conception  of
property”</strong></em>  unrelated to  reality. Even  assuming that
private property was produced by  the means Rothbard assumes, it does
not  justify the  hierarchy associated  with  it as  the current  and
future generations of humanity have, effectively, been excommunicated
from liberty by previous ones. If,  as Rothbard argues, property is a
natural right  and the basis of  liberty then why should  the many be
excluded  from  their  birthright  by a  minority?  In  other  words,
Rothbard denies that liberty should be universal. He chooses property
over liberty while anarchists choose liberty over property.</p>

<p>Even worse,  the possibility that  private property can  result in
<strong>worse</strong> violations of individual  freedom (at least of
workers) than the  state of its citizens  was implicitly acknowledged
by  Rothbard. He  uses  as  a hypothetical  example  a country  whose
King  is  threatened  by  a rising  “libertarian”  movement.  The
King  responses by  <em>“employ[ing]  a cunning  stratagem,”</em>
namely  he  <em>“proclaims  his  government to  be  dissolved,  but
just  before doing  so he  arbitrarily  parcels out  the entire  land
area  of  his kingdom  to  the  ‘ownership’  of himself  and  his
relatives.”</em> Rather than  taxes, his subjects now  pay rent and
he can  <em>“regulate to regulate the  lives of all the  people who
presume to live on”</em> his property as he sees fit. Rothbard then
asks:</p>

<blockquote class="citation">
“Now what should  be the reply of the  libertarian rebels to
this pert challenge?  If they are consistent  utilitarians, they must
bow  to this  subterfuge, and  resign  themselves to  living under  a
regime no  less despotic than the  one they had been  battling for so
long. Perhaps,  indeed, <strong>more</strong>  despotic, for  now the
king and his  relatives can claim for  themselves the libertarians’
very  principle  of  the  absolute  right  of  private  property,  an
absoluteness which they might not have dared to claim before.”</em>
[<strong>Op. Cit.</strong>, pp. 54–5]
</blockquote>

<p>So not  only does  the property  owner have  the same  monopoly of
power over  a given  area as the  state, it  is <strong>more</strong>
despotic  as it  is based  on  the <em>“absolute  right of  private
property”</em>!  And  remember,   Rothbard  is  arguing  <strong>in
favour</strong>  of  “anarcho”-capitalismAnd  remember,  Rothbard
is  arguing  <strong>in favour</strong>  of  “anarcho”-capitalism
(<em>“if you have unbridled capitalism,  you will have all kinds of
authority: you will  have <strong>extreme</strong> authority.”</em>
[Chomksy,  <strong>Understanding  Power</strong>,  p.  200]).  So  in
practice,  private  property is  a  major  source of  oppression  and
authoritarianism within  society —  there is  little or  no freedom
within  capitalist production  (as Bakunin  noted, <em>“the  worker
sells his  person and his liberty  for a given time”</em>).  So, in
stark  contrast  to  anarchists,  “anarcho”-capitalists  have  no
problem with  factory fascism  (i.e. wage  labour), a  position which
seems highly illogical for a theory calling itself libertarian. If it
were truly libertarian, it would  oppose all forms of domination, not
just statism.  This position flows from  the “anarcho”-capitalist
definition  of  freedom  as  the  absence of  coercion  and  will  be
discussed in section 2 in more detail.</p>

<p>Of course, Rothbard  has yet another means to  escape the obvious,
namely that the market will limit  the abuses of the property owners.
If  workers do  not  like their  ruler then  they  can seek  another.
However, this  reply completely ignores  the reality of  economic and
social  power.  Thus  the  “consent” argument  fails  because  it
ignores the social circumstances of capitalism which limit the choice
of the  many. Anarchists have long  argued that, as a  class, workers
have little choice but to  “consent” to capitalist hierarchy. The
alternative is either dire poverty or starvation.</p>

<p>“Anarcho”-capitalists  dismiss  such  claims by  denying  that
there is such a thing as economic power. Rather, it is simply freedom
of contract. Anarchists consider such claims  as a joke. To show why,
we need only  quote (yet again) Rothbard on the  abolition of slavery
and  serfdom in  the 19<sup>th</sup>  century. He  argued, correctly,
that the <em>“<strong>bodies</strong> of  the oppressed were freed,
but the property which they had worked and eminently deserved to own,
remained in the hands of their former oppressors. With economic power
thus remaining in their hands, the former lords soon found themselves
virtual  masters once  more of  what were  now free  tenants or  farm
labourers.  The serfs  and slaves  had tasted  freedom, but  had been
cruelly derived of its fruits.”</em> [<strong>Op. Cit.</strong>, p.
74]</p>

<p>To  say the  least,  anarchists  fail to  see  the  logic in  this
position.  Contrast this  with the  standard “anarcho”-capitalist
claim that if market forces (“voluntary exchanges”) result in the
creation of  <em>“free tenants or farm  labourers”</em> then they
are  free.  Yet  labourers  dispossessed  by  market  forces  are  in
exactly the  same social and  economic situation as the  ex-serfs and
ex-slaves.  If  the  latter  do  not  have  the  fruits  of  freedom,
neither  do the  former.  Rothbard sees  the obvious  <em>“economic
power”</em>  in the  latter  case,  but denies  it  in the  former.
It  is  only  Rothbard’s  ideology  that  stops  him  from  drawing
the  obvious conclusion  —  identical  economic conditions  produce
identical  social  relationships  and  so  capitalism  is  marked  by
<em>“economic  power”</em>  and  <em>“virtual  masters.”</em>
The  only solution  is  for “anarcho”-capitalists  to simply  say
the  ex-serfs  and  ex-slaves  were  actually  free  to  choose  and,
consequently, Rothbard was  wrong. It might be inhuman,  but at least
it would be consistent!</p>

<p>Rothbard’s  perspective  is  alien to  anarchism.  For  example,
as  individualist anarchist  William Bailie  noted, under  capitalism
there  is a  class  system marked  by  <em>“a dependent  industrial
class  of  wage-workers”</em>  and  <em>“a  privileged  class  of
wealth-monopolisers, each  becoming more  and more distinct  from the
other  as  capitalism  advances.”</em>  This  has  turned  property
into  <em>“a  social  power,   an  economic  force  destructive  of
rights,  a  fertile  source  of   injustice,  a  means  of  enslaving
the  dispossessed.”</em>  He  concludes: <em>“Under  this  system
equal  liberty cannot  obtain.”</em> Bailie  notes that  the modern
<em>“industrial  world under  capitalistic conditions”</em>  have
<em>“arisen  under  the <strong>regime</strong>  of  status”</em>
(and  so   <em>“law-made  privileges”</em>)  however,   it  seems
unlikely that he would have concluded  that such a class system would
be  fine if  it  had developed  naturally or  the  current state  was
abolished  while  leaving the  class  structure  intact (as  we  note
in  section  G.4,  Tucker  recognised  that  even  the  <em>“freest
competition”</em>   was  powerless   against  the   <em>“enormous
concentration of  wealth”</em> associated with  modern capitalism).
[<strong>The Individualist Anarchists</strong>, p. 121]</p>

<p>Therefore  anarchists   recognise  that  “free   exchange”  or
“consent”  in  unequal  circumstances   will  reduce  freedom  as
well  as increasing  inequality between  individuals and  classes. In
other  words, as  we discuss  in section  3, inequality  will produce
social  relationships which  are based  on hierarchy  and domination,
<strong>not</strong> freedom. As Noam Chomsky put it:</p>

<blockquote class="citation">
“Anarcho-capitalism, in  my opinion,  is a  doctrinal system
which,  if ever  implemented,  would  lead to  forms  of tyranny  and
oppression that have few counterparts in human history. There isn’t
the slightest  possibility that  its (in  my view,  horrendous) ideas
would be implemented, because they  would quickly destroy any society
that  made  this colossal  error.  The  idea of  ‘free  contract’
between  the potentate  and  his  starving subject  is  a sick  joke,
perhaps  worth some  moments  in an  academic  seminar exploring  the
consequences of (in my view, absurd) ideas, but nowhere else.”</em>
[<strong>Noam Chomsky on Anarchism</strong>, interview with Tom Lane,
December 23, 1996]
</blockquote>

<p>Clearly, then,  by its  own arguments  “anarcho”-capitalism is
not  anarchist.  This  should  come as  no  surprise  to  anarchists.
Anarchism,  as  a political  theory,  was  born when  Proudhon  wrote
<strong>What is Property?</strong> specifically  to refute the notion
that workers  are free when  capitalist property forces them  to seek
employment by  landlords and capitalists.  He was well aware  that in
such circumstances property <em>“violates equality by the rights of
exclusion  and  increase, and  freedom  by  despotism ...  [and  has]
perfect identity  with robbery.”</em> He, unsurprisingly,  talks of
the <em>“proprietor, to whom [the  worker] has sold and surrendered
his liberty.”</em> For Proudhon,  anarchy was <em>“the absence of
a  master,  of  a sovereign”</em>  while  <em>“proprietor”</em>
was  <em>“synonymous”</em> with  <em>“sovereign”</em> for  he
<em>“imposes  his will  as law,  and suffers  neither contradiction
nor  control.”</em>  This   meant  that  <em>“property  engenders
despotism,”</em>  as  <em>“each   proprietor  is  sovereign  lord
within  the   sphere  of   his  property.”</em>   [<strong>What  is
Property</strong>, p.  251, p. 130, p.  264 and pp. 266–7]  It must
also be stressed that Proudhon’s classic work is a lengthy critique
of the kind of apologetics  for private property Rothbard espouses to
salvage his ideology from its obvious contradictions.</p>

<p>Ironically,  Rothbard repeats  the same  analysis as  Proudhon but
draws  the <strong>opposite</strong>  conclusions and  expects to  be
considered  an  anarchist! Moreover,  it  seems  equally ironic  that
“anarcho”-capitalism  calls itself  “anarchist” while  basing
itself  on the  arguments that  anarchism was  created in  opposition
to.  As  shown,  “anarcho”-capitalism  makes  as  much  sense  as
“anarcho-statism” — an oxymoron,  a contradiction in terms. The
idea that “anarcho”-capitalism  warrants the name “anarchist”
is simply  false. Only someone  ignorant of anarchism  could maintain
such a  thing. While you expect  anarchist theory to show  this to be
the case, the wonderful thing is that “anarcho”-capitalism itself
does the same.</p>

<p>Little  wonder Bob  Black argues  that <em>“[t]o  demonise state
authoritarianism while ignoring identical albeit contract-consecrated
subservient  arrangements  in   the  large-scale  corporations  which
control  the  world  economy   is  fetishism  at  its  worst.”</em>
[<strong>Libertarian   as  Conservative</strong>]   The  similarities
between   capitalism  and   statism  are   clear  —   and  so   why
“anarcho”-capitalism cannot be anarchist. To reject the authority
(the  <em>“ultimate  decision-making  power”</em>) of  the  state
and  embrace  that  of  the  property  owner  indicates  not  only  a
highly illogical  stance but  one at odds  with the  basic principles
of  anarchism.  This  whole-hearted   support  for  wage  labour  and
capitalist property  rights indicates  that “anarcho”-capitalists
are  not  anarchists  because  they   do  not  reject  all  forms  of
<strong>archy.</strong> They obviously  support the hierarchy between
boss and worker (wage labour)  and landlord and tenant. Anarchism, by
definition, is  against all forms  of archy, including  the hierarchy
generated  by  capitalist  property.  To  ignore  the  obvious  archy
associated with capitalist property is highly illogical.</p>

<p>In  addition,  we  must  note  that  such  inequalities  in  power
and  wealth will  need  “defending” from  those  subject to  them
(“anarcho”-capitalists  recognise  the  need for  private  police
and  courts  to  defend  property  from  theft  —  and,  anarchists
add,   to   defend   the   theft  and   despotism   associated   with
property!).  Due  to  its  support  of  private  property  (and  thus
authority),  “anarcho”-capitalism ends  up retaining  a state  in
its  “anarchy”;  namely  a <strong>private</strong>  state  whose
existence its proponents  attempt to deny simply by  refusing to call
it a state, like an ostrich hiding  its head in the sand (see section
6  for  more  on  this and  why  “anarcho”-capitalism  is  better
described as  “private state” capitalism). As  Albert Meltzer put
it:</p>

<blockquote class="citation">
“Common-sense  shows  that   any  capitalist  society  might
dispense  with a  ‘State’  ...  but it  could  not dispense  with
organised  government, or  a privatised  form  of it,  if there  were
people  amassing money  and  others  working to  amass  it for  them.
The  philosophy   of  ‘anarcho-capitalism’  dreamed  up   by  the
‘libertarian’  New Right,  has nothing  to do  with Anarchism  as
known by  the Anarchist  movement proper.  It is  a lie  ... Patently
unbridled capitalism ... needs some force at its disposal to maintain
class  privileges,  either form  the  State  itself or  from  private
armies. What they believe in is in  fact a limited State — that us,
one  in which  the  State has  one function,  to  protect the  ruling
class, does  not interfere with  exploitation, and comes as  cheap as
possible for the  ruling class. The idea also  serves another purpose
...  a  moral justification  for  bourgeois  consciences in  avoiding
taxes  without feeling  guilty about  it.”</em> [<strong>Anarchism:
Arguments For and Against</strong>, p. 50]
</blockquote>

<p>For  anarchists,  this  need  of   capitalism  for  some  kind  of
state  is unsurprising.  For <em>“Anarchy  without socialism  seems
equally  as impossible  to  us [as  socialism  without anarchy],  for
in  such  a  case it  could  not  be  other  than the  domination  of
the  strongest,  and  would  therefore   set  in  motion  right  away
the  organisation  and  consolidation  of this  domination;  that  is
to  the   constitution  of  government.”</em>   [Errico  Malatesta,
<strong>Life  and  Ideas</strong>,  p.  148]  Because  of  this,  the
“anarcho”-capitalist rejection  of anarchist ideas  on capitalist
property  economics  and  the  need  for  equality,  they  cannot  be
considered anarchists or part of the anarchist tradition.</p>

<p>Thus anarchism is  far more than the  common dictionary definition
of “no  government” — it  also entails being against  all forms
of  <strong>archy</strong>, including  those generated  by capitalist
property. This is clear from the  roots of the word “anarchy.” As
we noted  in section A.1, the  word anarchy means “no  rulers” or
“contrary to  authority.” As  Rothbard himself  acknowledges, the
property owner is  the ruler of their property  and, therefore, those
who  use  it.  For  this reason  “anarcho”-capitalism  cannot  be
considered as a form of anarchism — a real anarchist must logically
oppose  the  authority of  the  property  owner  along with  that  of
the  state.  As  “anarcho”-capitalism  does  not  explicitly  (or
implicitly, for that matter) call for economic arrangements that will
end wage labour  and usury it cannot be considered  anarchist or part
of the anarchist tradition.</p>

<p>Political theories  should be identified by  their actual features
and history rather than labels. Once  we recognise that, we soon find
out  that “anarcho”-capitalism  is  an  oxymoron. Anarchists  and
“anarcho”-capitalists  are  not  part  of the  same  movement  or
tradition. Their ideas and aims are  in direct opposition to those of
all kinds of anarchists.</p>

<p>While     anarchists    have     always    opposed     capitalism,
“anarcho”-capitalists have  embraced it. And due  to this embrace
their “anarchy” will be marked by extensive differences in wealth
and power, differences that will  show themselves up in relationships
based  upon  subordination  and  hierarchy  (such  as  wage  labour),
<strong>not</strong> freedom (little wonder that Proudhon argued that
<em>“property is despotism”</em> — it creates authoritarian and
hierarchical  relationships  between  people  in  a  similar  way  to
statism).</p>

<p>Their support for “free  market” capitalism ignores the impact
of wealth and power on the nature and outcome of individual decisions
within the market (see sections 2  and 3 for further discussion). For
example,  as  we indicate  in  sections  J.5.10, J.5.11  and  J.5.12,
wage  labour is  less  efficient than  self-management in  production
but  due to  the structure  and  dynamics of  the capitalist  market,
“market forces”  will actively discourage self-management  due to
its  empowering  nature for  workers.  In  other words,  a  developed
capitalist market will promote  hierarchy and unfreedom in production
in spite  of its effects on  individual workers and their  wants (see
also  section  10.2).  Thus  “free market”  capitalism  tends  to
re-enforce  inequalities of  wealth  and power,  <strong>not</strong>
eliminate them.</p>

<p>Furthermore,   any   such   system  of   (economic   and   social)
power  will   require  extensive  force   to  maintain  it   and  the
“anarcho”-capitalist system of competing “defence firms” will
simply be  a new state,  enforcing capitalist power,  property rights
and law.</p>

<p>Overall,  the  lack  of  concern  for  meaningful  freedom  within
production and  the effects of  vast differences in power  and wealth
within  society  as  a whole  makes  “anarcho”-capitalism  little
better  than “anarchism  for the  rich.” Emma  Goldman recognised
this when she argued that <em>“‘Rugged individualism’ has meant
all  the  ‘individualism’  for  the masters  ...  in  whose  name
political  tyranny and  social oppression  are defended  and held  up
as  virtues  while  every  aspiration  and attempt  of  man  to  gain
freedom  ... is  denounced  as ...  evil  in the  name  of that  same
individualism.”</em>  [<strong>Red  Emma Speaks</strong>,  p.  112]
And, as such, is no anarchism at all.</p>

<p>So, unlike  anarchists, “anarcho”-capitalists do not  seek the
<em>“abolition  of  the  proletariat”</em> (to  use  Proudhon’s
expression) via changing capitalist property rights and institutions.
Thus the  “anarcho”-capitalist and  the anarchist  have different
starting positions  and opposite ends in  mind and so they  cannot be
considered  part of  the same  (anarchist) tradition.  As we  discuss
further  in later  sections, the  “anarcho”-capitalist claims  to
being anarchists are bogus simply because  they reject so much of the
anarchist  tradition as  to make  what they  do accept  non-anarchist
in  theory  and practice.  Little  wonder  Peter Marshall  said  that
<em>“few  anarchists  would  accept  the  ‘anarcho-capitalists’
into  the anarchist  camp  since  they do  not  share  a concern  for
economic equality and  social justice.”</em> [<strong>Demanding the
Impossible</strong>, p. 565]</p>

<h4 id="toc17">1.1 Why  is  the failure  to  renounce hierarchy  the Achilles Heel of right-wing libertarianism</h4>

<p>Any capitalist  system will  produce vast differences  in economic
(and  social) wealth  and power.  As we  argue in  section 3.1,  such
differences will reflect themselves in  the market and any “free”
contracts  agreed  there   will  create  hierarchical  relationships.
Thus  capitalism is  marked  by hierarchy  (see  section B.1.2)  and,
unsurprisingly, right-libertarians and “anarcho”-capitalists fail
to oppose such “free market” generated hierarchy.</p>

<p>Both  groups  approve  of  it   in  the  capitalist  workplace  or
rented   accommodation  and   the  right-Libertarians   also  approve
of  it  in   a  ‘minimal’  state  to   protect  private  property
(“anarcho”-capitalists,  in  contrast,  approve  of  the  use  of
private defence firms to protect  property). But the failure of these
two  movements to  renounce  hierarchy is  their  weakest point.  For
anti-authoritarianism has sunk deep roots  into the modern psyche, as
a legacy of the sixties.</p>

<p>Many  people who  do not  even know  what anarchism  is have  been
profoundly  affected by  the personal  liberation and  counterculture
movements of the past thirty  years, epitomised by the popular bumper
sticker, <em>“Question Authority.”</em> As  a result, society now
tolerates much more  choice than ever before in  matters of religion,
sexuality, art,  music, clothing, and other  components of lifestyle.
We  need only  recall the  conservatism  that reigned  in such  areas
during  the  fifties  to  see  that the  idea  of  liberty  has  made
tremendous advances in just a few decades.</p>

<p>Although this liberatory  impulse has so far  been confined almost
entirely to the  personal and cultural realms, it may  yet be capable
of spilling  over and affecting economic  and political institutions,
provided it  continues to grow. The  Right is well aware  of this, as
seen  in  its  ongoing  campaigns  for  “family  values,”  school
prayer, suppression of women’s rights, fundamentalist Christianity,
sexual abstinence before  marriage, and other attempts  to revive the
Ozzie-and-Harriet mindset  of the  Good Old Days.  This is  where the
efforts of  “cultural anarchists” — artists,  musicians, poets,
and others —  are important in keeping alive the  ideal of personal
freedom and  resistance to  authority as  a necessary  foundation for
economic and political restructuring.</p>

<p>Indeed, the  libertarian right  (as a whole)  support restrictions
on  freedom <strong>as  long  as  its not  the  state  that is  doing
it</strong>! Their  support for  capitalism means  that they  have no
problem with  bosses dictating what  workers do during  working hours
(nor outside  working hours,  if the job  requires employees  to take
drug tests or not be gay in  order to keep it). If a private landlord
or company decrees a mandatory rule or mode of living, workers/tenets
must “love  it or leave  it!” Of  course, that the  same argument
also applies to state laws  is one hotly denied by right-Libertarians
— a definite case of not seeing the wood for the trees (see section
2.3).</p>

<p>Of course,  the “anarcho”-capitalist  will argue,  workers and
tenants  can find  a more  liberal boss  or landlord.  This, however,
ignores two key facts. Firstly, being  able to move to a more liberal
state hardly makes state laws less offensive (as they themselves will
be the first to  point out). Secondly, looking for a  new job or home
is not that  easy. Just a moving  to a new state  can involve drastic
upheavals,  so change  changing  jobs and  homes.  Moreover, the  job
market  is usually  a  buyers market  (it has  to  be in  capitalism,
otherwise profits  are squeezed  — see sections  C.7 and  10.2) and
this means  that workers are not  usually in a position  (unless they
organise) to demand increased liberties at work.</p>

<p>It   seems    somewhat   ironic,   to   say    the   least,   that
right-libertarians  place   rights  of   property  over   the  rights
of  self-ownership,   even  though  (according  to   their  ideology)
self-ownership is  the foundational right from  which property rights
are  derived. Thus  in  right-libertarianism the  rights of  property
owners  to  discriminate  and   govern  the  property-less  are  more
important than the freedom from  discrimination (i.e. to be yourself)
or the freedom to govern oneself at all times.</p>

<p>So, when  it boils down  to it, right-libertarians are  not really
bothered about restrictions on liberty  and, indeed, they will defend
private restrictions on liberty with all their might. This may seem a
strange  position  for  self-proclaimed “libertarians”  to  take,
but  it  flows  naturally  from  their  definition  of  freedom  (see
section  2 for  a  full discussion  of this).  but  by not  attacking
hierarchy  beyond certain  forms  of  statism, the  ‘libertarian’
right fundamentally  undermines its claim to  be libertarian. Freedom
cannot be compartmentalised,  but is holistic. The  denial of liberty
in, say, the workplace, quickly results in its being denied elsewhere
in society (due to the impact of the inequalities it would produce) ,
just as the degrading effects of wage labour and the hierarchies with
which is it bound up are felt by the worker outside work.</p>

<p>Neither      the     Libertarian      Party     nor      so-called
“anarcho”-capitalism         is        <strong>genuinely</strong>
anti-authoritarian, as those who are  truly dedicated to liberty must
be.</p>

<h4 id="toc18">1.2 How libertarian is right-Libertarian theory?</h4>

<p>The short answer  is, not very. Liberty not only  implies but also
requires  independent,  critical  thought (indeed,  anarchists  would
argue that  critical thought requires free  development and evolution
and  that  it  is precisely  <strong>this</strong>  which  capitalist
hierarchy crushes). For anarchists a  libertarian theory, if it is to
be  worthy of  the  name, must  be based  upon  critical thought  and
reflect the  key aspect  that characterises life  — change  and the
ability  to evolve.  To  hold  up dogma  and  base “theory”  upon
assumptions (as  opposed to facts)  is the opposite of  a libertarian
frame  of mind.  A  libertarian  theory must  be  based upon  reality
and  recognise the  need  for  change and  the  existence of  change.
Unfortunately, right-Libertarianism  is marked more by  ideology than
critical analysis.</p>

<p>Right-Libertarianism  is characterised  by  a  strong tendency  of
creating theories  based upon  assumptions and deductions  from these
axioms  (for  a  discussion  on the  pre-scientific  nature  of  this
methodology and of its dangers, see the next section). Robert Nozick,
for  example, in  <strong>Anarchy, State,  and Utopia</strong>  makes
no  attempt  to  provide  a  justification  of  the  property  rights
his  whole  theory  is  based  upon.  His  main  assumption  is  that
<em>“[i]ndividuals  have  rights,  and  there  are  certain  things
no  person  or  group  may   do  to  them  (without  violating  their
rights).”</em> [<strong>Anarchy, State  and Utopia</strong>, p. ix]
While this does have  its intuitive appeal, it is not  much to base a
political ideology  upon. After all,  what rights people  consider as
valid can  be pretty  subjective and  have constantly  evolved during
history. To say that “individuals have  rights” is to open up the
question “what rights?” Indeed, as  we argue in greater length in
section 2, such a rights based  system as Nozick desires can and does
lead to  situations developing  in which  people “consent”  to be
exploited and  oppressed and that, intuitively,  many people consider
supporting  the “violation”  of  these  “certain rights”  (by
creating other ones) simply because of their evil consequences.</p>

<p>In  other  words,  starting  from the  assumption  “people  have
[certain] rights” Nozick constructs a theory which, when faced with
the reality of unfreedom and domination it would create for the many,
justifies  this  unfreedom as  an  expression  of liberty.  In  other
words, regardless  of the outcome,  the initial assumptions  are what
matter.  Nozick’s intuitive  rights system  can lead  to some  very
non-intuitive outcomes.</p>

<p>And  does   Nozick  prove  the   theory  of  property   rights  he
assumes?  He   states  that   <em>“we  shall  not   formulate  [it]
here.”</em>  [<strong>Op. Cit.</strong>,  p. 150]  Moreover, it  is
not  formulated  anywhere  else  in  his  book.  And  if  it  is  not
formulated,  what is  there to  defend?  Surely this  means that  his
Libertarianism  is  without  foundations? As  Jonathan  Wolff  notes,
Nozick’s  <em>“Libertarian property  rights remain  substantially
undefended.”</em> [<strong>Robert Nozick: Property, Justice and the
Minimal  State</strong>, p.  117]  Given that  the  right to  acquire
property is critical to his whole theory you would think it important
enough to go  into in some detail (or at  least document). After all,
unless he provides us with a  firm basis for property rights then his
entitlement theory is  nonsense as no one has the  right to (private)
property.</p>

<p>It  could  be  argued that  Nozick  <strong>does</strong>  present
enough information to allow us  to piece together a possible argument
in  favour  of property  rights  based  on  his modification  of  the
<em>“Lockean  Proviso”</em> (although  he  does not  point us  to
these arguments). However, assuming this  is the case, such a defence
actually fails (see  section B.3.4 for more on  this). If individuals
<strong>do</strong> have rights, these rights do not include property
rights  in the  form  Nozick  assumes (but  does  not prove).  Nozick
appears initially convincing because what  he assumes with regards to
property is a  normal feature of the  society we are in  (we would be
forgiven when we note here  that feeble arguments pass for convincing
when they are on the same side as the prevailing sentiment).</p>

<p>Similarly, both Murray Rothbard and  Ayn Rand (who is infamous for
repeating  <em>“A  is  A”</em>  ad infinitum)  do  the  same  —
base  their ideologies  on assumptions  (see section  11 for  more on
this).</p>

<p>Therefore,  we  see that  most  of  the leading  right-Libertarian
ideologues base themselves on assumptions  about what “Man” is or
the rights  they should have  (usually in  the form that  people have
(certain) rights  because they are  people). From these  theorems and
assumptions  they  build  their respective  ideologies,  using  logic
to  deduce  the conclusions  that  their  assumptions imply.  Such  a
methodology  is  unscientific  and,  indeed,  a  relic  of  religious
(pre-scientific) society  (see next  section) but,  more importantly,
can have negative effects on maximising liberty. This is because this
“methodology” has distinct problems. Murray Bookchin argues:</p>

<blockquote class="citation">
“Conventional  reason rests  on  identity,  not change;  its
fundamental principle is that <strong>A equals A,</strong> the famous
‘principle of  identity,’ which  means that any  given phenomenon
can  be only  itself and  cannot be  other than  what we  immediately
perceive it to be at a given  moment in time. It does not address the
problem of change. A human being is an infant at one time, a child at
another, an adolescent  at still another, and finally a  youth and an
adult. When we analyse an infant  by means of conventional reason, we
are  not  exploring  what  it  is  <strong>becoming</strong>  in  the
process of  developing into a child.”</em>  [<em>“A Philosophical
Naturalism”</em>,  <strong>Society  and  Nature</strong>  No.2,  p.
64]
</blockquote>

<p>In other  words, right-Libertarian  theory is based  upon ignoring
the  fundamental aspect  of life  — namely  <strong>change</strong>
and  <strong>evolution.</strong>  Perhaps  it  will  be  argued  that
identity  also  accounts for  change  by  including potentiality  —
which  means,  that  we  have   the  strange  situation  that  A  can
<strong>potentially</strong> be A!  If A is not actually  A, but only
has the potential to be A, then A is not A. Thus to include change is
to  acknowledge that  A does  not equal  A —  that individuals  and
humanity evolves and so what  constitutes A also changes. To maintain
identity and then to deny it seems strange.</p>

<p>That change  is far from  the “A is  A” mentality can  be seen
from Murray  Rothbard who  goes so  far as  to state  that <em>“one
of  the  notable  attributes  of natural  law”</em>  is  <em>“its
applicability to  all men [sic!],  regardless of time or  place. Thus
ethical law  takes its  place alongside physical  or ‘scientific’
natural laws.”</em> [<strong>The Ethics of Liberty</strong>, p. 42]
Apparently the “nature of man” is the only living thing in nature
that does not evolve or change! Of course, it could be argued that by
“natural  law”  Rothbard  is  only referring  to  his  method  of
deducing his  (and, we  stress, they  are just  his —  not natural)
“ethical laws” — but his methodology starts by assuming certain
things about “man.” Whether these  assumptions seem far or not is
besides the  point, by using  the term “natural law”  Rothbard is
arguing that  any actions  that violate  <strong>his</strong> ethical
laws  are somehow  “against  nature” (but  if  they were  against
nature, they  could not occur —  see section 11 for  more on this).
Deductions from  assumptions is  a Procrustean  bed for  humanity (as
Rothbard’s ideology shows).</p>

<p>So, as  can be seen,  many leading right-Libertarians  place great
store by the axiom “A is  A” or that “man” has certain rights
simply because “he”  is a “man”. And as  Bookchin points out,
such conventional reason <em>“doubtless plays an indispensable role
in mathematical  thinking and  mathematical sciences  ... and  in the
nuts-and-bolts  of  dealing  with  everyday life”</em>  and  so  is
essential to <em>“understand or design mechanical entities.”</em>
[<strong>Ibid.</strong>,  p.67]  But  the question  arises,  is  such
reason useful when considering people and other forms of life?</p>

<p>Mechanical  entities are  but one  (small) aspect  of human  life.
Unfortunately for right-Libertarians (and fortunately for the rest of
humanity), human beings  are <strong>not</strong> mechanical entities
but  instead  are  living,   breathing,  feeling,  hoping,  dreaming,
<strong>changing</strong> living  organisms. They are  not mechanical
entities and any  theory that uses reason based  on such (non-living)
entities will flounder  when faced with living ones.  In other words,
right-Libertarian theory treats people as the capitalist system tries
to —  namely as  commodities, as things.  Instead of  human beings,
whose ideas, ideals  and ethics change, develop  and grow, capitalism
and capitalist  ideologues try to reduce  human life to the  level of
corn or iron  (by emphasising the unchanging “nature”  of man and
their starting assumptions/rights).</p>

<p>This can be seen from their support for wage labour, the reduction
of human  activity to  a commodity  on the  market. While  paying lip
service  to  liberty  and life,  right-libertarianism  justifies  the
commodification  of  labour  and  life,  which  within  a  system  of
capitalist property  rights can result  in the treating of  people as
means to an  end as opposed to  an end in themselves  (see sections 2
and 3.1).</p>

<p>And  as  Bookchin   points  out,  <em>“in  an   age  of  sharply
conflicting  values  and  emotionally  charges  ideals,  such  a  way
of  reasoning   is  often  repellent.   Dogmatism,  authoritarianism,
and  fear  seem  all-pervasive.”</em>  [<strong>Ibid.</strong>,  p.
68]  Right-Libertarianism  provides  more than  enough  evidence  for
Bookchin’s  summary  with  its  support  for  authoritarian  social
relationships, hierarchy and even slavery (see section 2).</p>

<p>This  mechanical viewpoint  is  also reflected  in  their lack  of
appreciation that  social institutions and relationships  evolve over
time  and, sometimes,  fundamentally change.  This can  best be  seen
from  property. Right-libertarians  fail to  see that  over time  (in
the words  of Proudhon)  property <em>“changed  its nature.”</em>
Originally, <em>“the word  <strong>property</strong> was synonymous
with  ...   <strong>individual  possession</strong>”</em>   but  it
became  more <em>“complex”</em>  and turned  into <strong>private
property</strong> — <em>“the right to use it by his neighbour’s
labour.”</em> The  changing of use-rights to  (capitalist) property
rights  created  relations  of domination  and  exploitation  between
people  absent  before. For  the  right-Libertarian,  both the  tools
of  the self-employed  artisan  and the  capital  of a  transnational
corporation  are  both forms  of  “property”  and (so)  basically
identical. In practice,  of course, the social  relations they create
and the impact  they have on society are totally  different. Thus the
mechanical mind-set  of right-Libertarianism fails to  understand how
institutions,  like property,  evolve  and come  to replace  whatever
freedom  enhancing features  they  had with  oppression (indeed,  von
Mises  argued that  <em>“[t]here may  possibly be  a difference  of
opinion about whether a particular institution is socially beneficial
or harmful. But once it has been judged [by whom, we ask] beneficial,
one can no longer contend that, for some inexplicable reason, it must
be condemned as  immoral”</em> [<strong>Liberalism</strong>, p. 34]
So much for evolution and change!).</p>

<p>Anarchism, in contrast,  is based upon the  importance of critical
thought informed by  an awareness that life is in  a constant process
of  change. This  means  that  our ideas  on  human  society must  be
informed by  the facts, not by  what we wish was  true. For Bookchin,
an  evaluation of  conventional  wisdom (as  expressed in  <em>“the
law  of  identity”</em>)  is  essential and  its  conclusions  have
<em>“enormous importance for  how we behave as  ethical beings, the
nature of  nature, and  our place in  the natural  world. Moreover...
these  issues  directly  affect  the kind  of  society,  sensibility,
and  lifeways  we  wish  to  foster.”</em>  [Bookchin,  <strong>Op.
Cit.</strong>, p. 69–70]</p>

<p>Bookchin is correct. While anarchists oppose hierarchy in the name
of  liberty,  right-libertarians  support  authority  and  hierarchy,
all  of  which  deny  freedom and  restrict  individual  development.
This is  unsurprising because the right-libertarian  ideology rejects
change  and critical  thought based  upon the  scientific method  and
so  is fundamentally  <strong>anti-life</strong>  in its  assumptions
and  <strong>anti-human</strong>  in its  method.  Far  from being  a
libertarian  set  of  ideas,  right-Libertarianism  is  a  mechanical
set  of dogmas  that  deny  the fundamental  nature  of life  (namely
change) and  of individuality (namely critical  thought and freedom).
Moreover, in practice their system  of (capitalist) rights would soon
result in extensive restrictions  on liberty and authoritarian social
relationships (see sections 2 and 3) — a strange result of a theory
proclaiming  itself “libertarian”  but  one  consistent with  its
methodology.</p>

<p>From  a   wider  viewpoint,  such   a  rejection  of   liberty  by
right-libertarians  is  unsurprising.  They do,  after  all,  support
capitalism. Capitalism  produces an  inverted set  of ethics,  one in
which capital  (dead labour)  is more  important that  people (living
labour).  After  all, workers  are  usually  easier to  replace  than
investments in capital  and the person who owns  capital commands the
person who “only” owns his  life and productive abilities. And as
Oscar Wilde once noted, crimes against property <em>“are the crimes
that the English law, valuing what a man has more than what a man is,
punishes  with  the  harshest  and  most  horrible  severity.”</em>
[<strong>The Soul of Man Under Socialism</strong>]</p>

<p>This mentality is reflected in right-libertarianism when it claims
that stealing  food is a  crime while starving  to death (due  to the
action of market forces/power and property rights) is no infringement
of your rights  (see section 4.2 for a similar  argument with regards
to water). It can also  be seen when right-libertarian’s claim that
the  taxation <em>“of  earnings  from labour”</em>  (e.g. of  one
dollar from  a millionaire) is <em>“<strong>on  a par with</strong>
forced  labour”</em> [Nozick,  <strong>Op.  Cit.</strong>, p.  169]
while  working in  a sweatshop  for 14  hours a  day (enriching  said
millionaire) does not affect your  liberty as you “consent” to it
due  to market  forces (although,  of course,  many rich  people have
earned their money  <strong>without</strong> labouring themselves —
their earnings derive from the wage  labour of others so would taxing
those, non-labour,  earnings be “forced  labour”?) Interestingly,
the Individualist Anarchist Ben Tucker  argued that an income tax was
<em>“a recognition of the fact that industrial freedom and equality
of opportunity no longer exist here [in the USA in the 1890s] even in
the imperfect state  in which they once did  exist”</em> [quoted by
James Martin,  <strong>Men Against the State</strong>,  p. 263] which
suggests a somewhat different viewpoint on this matter than Nozick or
Rothbard.</p>

<p>That capitalism  produces an  inverted set of  ethics can  be seen
when the Ford  produced the Pinto. The  Pinto had a flaw  in it which
meant that if  it was hit in a  certain way in a crash  the fuel tank
exploded.  The  Ford  company  decided it  was  more  “economically
viable”  to produce  that car  and pay  damages to  those who  were
injured or  the relatives of  those who died  than pay to  change the
invested  capital. The  needs for  the owners  of capital  to make  a
profit came before  the needs of the living.  Similarly, bosses often
hire people to  perform unsafe work in dangerous  conditions and fire
them if they protest. Right-libertarian ideology is the philosophical
equivalent. Its dogma is “capital” and it comes before life (i.e.
“labour”).</p>

<p>As  Bakunin  once   put  it,  <em>“you  will   always  find  the
idealists  in  the  very  act of  practical  materialism,  while  you
will   see  the   materialists  pursuing   and  realising   the  most
grandly  ideal  aspirations  and thoughts.”</em>  [<strong>God  and
the  State</strong>, p.  49]  Hence we  see right  “libertarians”
supporting sweat  shops and  opposing taxation —  for, in  the end,
money  (and the  power that  goes with  it) counts  far more  in that
ideology than ideals such as liberty, individual dignity, empowering,
creative and productive  work and so forth for all.  The central flaw
of  right-libertarianism  is that  it  does  not recognise  that  the
workings of the capitalist market can easily ensure that the majority
end  up  becoming a  resource  for  others  in  ways far  worse  than
that  associated with  taxation. The  legal rights  of self-ownership
supported by  right-libertarians does not  mean that people  have the
ability  to avoid  what  is  in effect  enslavement  to another  (see
sections 2 and 3).</p>

<p>Right-Libertarian  theory   is  not   based  upon   a  libertarian
methodology or perspective and so  it is hardly surprising it results
in  support  for  authoritarian  social  relationships  and,  indeed,
slavery (see section 2.6).</p>

<h4 id="toc19">1.3  Is   right-Libertarian  theory   scientific  in nature?</h4>

<p>Usually,      no.       The      scientific       approach      is
<strong>inductive,</strong> much of the right-libertarian approach is
<strong>deductive.</strong>  The  first  draws  generalisations  from
the  data, the  second  applies preconceived  generalisations to  the
data.  A completely  deductive approach  is pre-scientific,  however,
which  is why  many right-Libertarians  cannot legitimately  claim to
use  a  scientific  method.  Deduction does  occur  in  science,  but
the  generalisations are  primarily based  on other  data, not  <em>a
priori</em> assumptions, and are checked  against data to see if they
are accurate.  Anarchists tend  to fall into  the inductive  camp, as
Kropotkin put it:</p>

<blockquote class="citation">
“Precisely   this  natural-scientific   method  applied   to
economic facts, enables us to  prove that the so-called ‘laws’ of
middle-class sociology,  including also their political  economy, are
not  laws  at all,  but  simply  guesses,  or mere  assertions  which
have  never  been  verified  at  all.”</em>  [<strong>Kropotkin’s
Revolutionary Pamphlets</strong>, p. 153]
</blockquote>

<p>The  idea  that  natural-scientific  methods  can  be  applied  to
economic and social life is  one that many right-libertarians reject.
Instead they favour the  deductive (pre-scientific) approach (this we
must  note  is  not  limited  purely  to  Austrian  economists,  many
more  mainstream capitalist  economists also  embrace deduction  over
induction).</p>

<p>The tendency  for right-Libertarianism to fall  into dogmatism (or
<em>a priori</em> theorems, as they call it) and its implications can
best be seen  from the work of Ludwig von  Mises and other economists
from the right-Libertarian “Austrian  school.” Of course, not all
right-libertarians  necessarily subscribe  to  this approach  (Murray
Rothbard for one did)  but its use by so many  leading lights of both
schools of  thought is significant and  worthy of comment. And  as we
are concentrating on <strong>methodology</strong> it is not essential
to discuss the starting assumptions. The assumptions (such as, to use
Rothbard’s  words, the  Austrian’s <em>“fundamental  axiom that
individual  human beings  act”</em>) may  be correct,  incorrect or
incomplete —  but the method of  using them advocated by  von Mises
ensures that such considerations are irrelevant.</p>

<p>Von Mises (a  leading member of the Austrian  school of economics)
begins  by  noting that  social  and  economic theory  <em>“is  not
derived from  experience; it is prior  to experience...”</em> Which
is back  to front.  It is  obvious that  experience of  capitalism is
necessary in  order to develop  a viable  theory about how  it works.
Without  the experience,  any theory  is  just a  flight of  fantasy.
The  actual specific  theory  we develop  is  therefore derived  from
experience,  informed by  it and  will  have to  get checked  against
reality to see if it is viable. This is the scientific method — any
theory must be checked against the  facts. However, von Mises goes on
to argue at  length that <em>“no kind of experience  can ever force
us to discard or modify  <strong>a priori</strong> theorems; they are
logically prior  to it and  cannot be either proved  by corroborative
experience or disproved by experience to the contrary ...”</em></p>

<p>And  if this  does not  do  justice to  a full  exposition of  the
phantasmagoria of  von Mises’  <em>a priorism</em>, the  reader may
take some joy (or horror) from the following statement:</p>

<blockquote class="citation">
“If a contradiction appears between a theory and experience,
<strong>we must always assume</strong>  that a condition pre-supposed
by the  theory was not  present, or else there  is some error  in our
observation. The  disagreement between  the theory  and the  facts of
experience frequently forces us to  think through the problems of the
theory  again. <strong>But  so long  as  a rethinking  of the  theory
uncovers no errors in our thinking,  we are not entitled to doubt its
truth</strong>”</em> [emphasis added —  the quotes presented here
are  cited in  <strong>Ideology and  Method in  Economics</strong> by
Homa Katouzian, pp. 39–40]
</blockquote>

<p>In other words, if reality is  in conflict with your ideas, do not
adjust your  views because reality  must be at fault!  The scientific
method would be to revise the theory in light of the facts. It is not
scientific  to  reject  the  facts  in  light  of  the  theory!  This
anti-scientific  perspective is  at  the heart  of  his economics  as
experience  <em>“can never  ...  prove or  disprove any  particular
theorem”</em>:</p>

<blockquote class="citation">
 “What assigns economics to its peculiar and unique position
in the  orbit of pure knowledge  and of the practical  utilisation of
knowledge is  the fact that its  particular theorems are not  open to
any  verification  or  falsification  on the  grounds  of  experience
...... The ultimate yardstick  of an economic theorem’s correctness
or  incorrectness is  solely  reason  unaided by  experience.”</em>
[<strong>Human Action</strong>, p. 858]
</blockquote>

<p>Von   Mises   rejects   the   scientific  approach   as   do   all
Austrian   Economists.  Murray   Rothbard  states   approvingly  that
<em>“Mises  indeed held  not  only that  economic  theory does  not
need  to  be  ‘tested’  by  historical  fact  but  also  that  it
<strong>cannot</strong> be so tested.”</em> [<em>“Praxeology: The
Methodology of  Austrian Economics”</em> in  <strong>The Foundation
of Modern  Austrian Economics</strong>,  p. 32] Similarly,  von Hayek
wrote  that  economic  theories   can  <em>“never  be  verified  or
falsified by reference  to facts. All that we can  and must verify is
the  presence  of our  assumptions  in  the particular  case.”</em>
[<strong>Individualism and Economic Order</strong>, p. 73]</p>

<p>This may seen somewhat strange to non-Austrians. How can we ignore
reality when deciding  whether a theory is  a good one or  not? If we
cannot  evaluate our  ideas, how  can we  consider them  anything bar
dogma?  The  Austrian’s  maintain  that we  cannot  use  historical
evidence because every historical situation is unique. Thus we cannot
use  <em>“complex heterogeneous  historical facts  as if  they were
repeatable homogeneous  facts”</em> like  those in  a scientist’s
experiment [Rothbard, <strong>Op. Cit.</strong>, p. 33]. While such a
position <strong>does</strong> have an element of truth about it, the
extreme  <em>a  priorism</em> that  is  drawn  from this  element  is
radically false  (just as extreme  empiricism is also false,  but for
different reasons).</p>

<p>Those  who hold  such a  position ensure  that their  ideas cannot
be  evaluated  beyond  logical  analysis. As  Rothbard  makes  clear,
<em>“since praxeology begins with a true  axiom, A, all that can be
deduced  from this  axiom must  also be  true. For  if A  implies be,
and  A  is true,  then  B  must  also be  true.”</em>  [<strong>Op.
Cit.</strong>, pp.  19–20] But  such an  approach makes  the search
for  truth  a  game  without  rules.  The  Austrian  economists  (and
other right-libertarians)  who use this  method are free  to theorise
anything they  want, without such irritating  constrictions as facts,
statistics, data,  history or  experimental confirmation.  Their only
guide is logic. But this is  no different from what religions do when
they  assert the  logical existence  of God.  Theories ungrounded  in
facts  and data  are  easily spun  into any  belief  a person  wants.
Starting  assumptions and  trains of  logic may  contain inaccuracies
so  small  as to  be  undetectable,  yet  will yield  entirely  false
conclusions.</p>

<p>In  addition, trains  of  logic  may miss  things  which are  only
brought to light by actual experiences  (after all, the human mind is
not all  knowing or all  seeing). To  ignore actual experience  is to
loose that input when evaluating a  theory. Hence our comments on the
irrelevance of the assumptions used  — the methodology is such that
incomplete or incorrect assumptions or  steps cannot be identified in
light of  experience. This  is because  one way  of discovering  if a
given chain  of logic  requires checking is  to test  its conclusions
against available  evidence (although  von Mises  did argue  that the
<em>“ultimate yardstick”</em> was <em>“solely reason unaided by
experience”</em>). If  we <strong>do</strong> take  experience into
account  and rethink  a given  theory in  the light  of contradictory
evidence,  the problem  remains that  a  given logical  chain may  be
correct,  but incomplete  or concentrate  on or  stress inappropriate
factors. In  other words, our  logical deductions may be  correct but
our starting place or steps wrong and as the facts are to be rejected
in the light of the deductive method, we cannot revise our ideas.</p>

<p>Indeed, this  approach could  result in discarding  (certain forms
of) human behaviour  as irrelevant (which the  Austrian system claims
using empirical evidence does). For there are too many variables that
can  have  an influence  upon  individual  acts to  yield  conclusive
results explaining  human behaviour.  Indeed, the  deductive approach
may  ignore as  irrelevant  certain human  motivations  which have  a
decisive impact  on an outcome. There  could be a strong  tendency to
project “right-libertarian  person” onto the rest  of society and
history,  for  example,  and  draw inappropriate  insights  into  the
way  human  society works  or  has  worked.  This  can be  seen,  for
example, in attempts to claim pre-capitalist societies as examples of
“anarcho”-capitalism in action.</p>

<p>Moreover,  deductive   reasoning  cannot  indicate   the  relative
significance  of assumptions  or theoretical  factors. That  requires
empirical study.  It could be  that a factor considered  important in
the theory actually  turns out to have little effect  in practice and
so the derived axioms are so weak as to be seriously misleading.</p>

<p>In  such a  purely  ideal realm,  observation  and experience  are
distrusted (when  not ignored) and  instead theory is  the lodestone.
Given  the  bias   of  most  theorists  in  this   tradition,  it  is
unsurprising that  this style of  economics can always be  trusted to
produce results  proving free markets  to be the finest  principle of
social organisation. And,  as an added bonus, reality  can be ignored
as it  is <strong>never</strong>  “pure” enough according  to the
assumptions required  by the theory.  It could be argued,  because of
this,  that  many  right-libertarians insulate  their  theories  from
criticism  by  refusing  to  test them  or  acknowledge  the  results
of  such testing  (indeed,  it  could also  be  argued  that much  of
right-libertarianism is more a religion than a political theory as it
is set-up in  such a way that  it is either true or  false, with this
being determined  not by evaluating  facts but by whether  you accept
the assumptions and logical chains presented with them).</p>

<p>Strangely  enough,  while   dismissing  the  “testability”  of
theories   many   right-Libertarians  (including   Murray   Rothbard)
<strong>do</strong> investigate historical  situations and claim them
as examples  of how well their  ideas work in practice.  But why does
historical  fact  suddenly become  useful  when  it  can be  used  to
bolster the right-Libertarian  argument? Any such example  is just as
“complex” as any other and the  good results indicated may not be
accountable to the  assumptions and steps of the theory  but to other
factors  totally ignored  by it.  If  economic (or  other) theory  is
untestable  then <strong>no</strong>  conclusions can  be drawn  from
history,  including  claims  for  the  superiority  of  laissez-faire
capitalism. You cannot  have it both ways — although  we doubt that
right-libertarians  will stop  using history  as evidence  that their
ideas work.</p>

<p>Perhaps  the Austrian  desire  to investigate  history  is not  so
strange  after  all. Clashes  with  reality  make a-priori  deductive
systems  implode as  the  falsifications run  back  up the  deductive
changes  to shatter  the structure  built upon  the original  axioms.
Thus the  desire to  find <strong>some</strong> example  which proves
their ideology  must be  tremendous. However, the  deductive a-priori
methodology makes them unwilling to admit to being mistaken — hence
their attempts to  downplay examples which refute  their dogmas. Thus
we have  the desire for  historical examples  while at the  same time
they have  extensive ideological  justifications that  ensure reality
only enters their  world-view when it agrees with  them. In practice,
the latter  wins as real-life refuses  to be boxed into  their dogmas
and deductions.</p>

<p>Of    course    it    is    sometimes   argued    that    it    is
<strong>complex</strong>  data that  is the  problem. Let  use assume
that this  is the case. It  is argued that when  dealing with complex
information  it is  impossible to  use aggregate  data without  first
having more simple  assumptions (i.e. that “humans  act”). Due to
the complexity  of the situation, it  is argued, it is  impossible to
aggregate  data because  this  hides the  individual activities  that
creates  it. Thus  “complex” data  cannot be  used to  invalidate
assumptions or  theories. Hence,  according to Austrians,  the axioms
derived from the “simple fact” that “humans act” are the only
basis for thinking about the economy.</p>

<p>Such a position is false in two ways.</p>

<p>Firstly, the aggregation of data <strong>does</strong> allow us to
understand complex systems. If we look at a chair, we cannot find out
whether it is comfortable, its colour,  whether it is soft or hard by
looking at the atoms  that make it up. To suggest that  you can is to
imply the existence of green, soft, comfortable atoms. Similarly with
gases. They are composed to countless individual atoms but scientists
do not study them by looking at those atoms and their actions. Within
limits, this is also valid for human action. For example, it would be
crazy to maintain from historical data  that interest rates will be a
certain percentage a  week but it is valid to  maintain that interest
rates  are  known to  be  related  to  certain variables  in  certain
ways.  Or that  certain experiences  will tend  to result  in certain
forms  of psychological  damage. General  tendencies and  “rules of
thumb” can  be evolved  from such  study and these  can be  used to
<strong>guide</strong>  current practice  and theory.  By aggregating
data you can produce valid  information, rules of thumb, theories and
evidence which would be lost if you concentrated on “simple data”
(such as “humans act”). Therefore, empirical study produces facts
which vary  across time and  place, and yet underlying  and important
patterns can  be generated (patterns  which can be  evaluated against
<strong>new</strong> data and improved upon).</p>

<p>Secondly,  the   simple  actions  themselves  influence   and  are
influenced  in  turn  by  overall  (complex)  facts.  People  act  in
different  ways in  different circumstances  (something we  can agree
with Austrians about, although we refuse  to take it to their extreme
position of rejecting empirical evidence as such). To use simple acts
to understand complex systems means to  miss the fact that these acts
are not  independent of  their circumstances.  For example,  to claim
that the capitalist  market is “just” the  resultant of bilateral
exchanges ignores the fact that the market activity shapes the nature
and  form of  these  bilateral exchanges.  The  “simple” data  is
dependent on the  “complex” system — and so  the complex system
<strong>cannot</strong>  be  understood  by  looking  at  the  simple
actions  in isolation.  To  do so  would be  to  draw incomplete  and
misleading conclusions  (and it is  due to these  interrelations that
we  argue  that  aggregate  data should  be  used  critically).  This
is  particularly  important when  looking  at  capitalism, where  the
“simple” acts of exchange in the labour market are dependent upon
and shaped by circumstances outside these acts.</p>

<p>So  to claim  that (complex)  data cannot  be used  to evaluate  a
theory is false.  Data can be useful when seeing  whether a theory is
confirmed by reality. This is the nature of the scientific method —
you compare the  results expected by your theory to  the facts and if
they do  not match  you check  your facts  <strong>and</strong> check
your theory.  This may involve revising  the assumptions, methodology
and theories you  use if the evidence  is such as to  bring them into
question.  For example,  if you  claim  that capitalism  is based  on
freedom but that the net result of capitalism is to produce relations
of domination  between people then it  would be valid to  revise, for
example, your definition of freedom  rather than deny that domination
restricts freedom (see  section 2 on this). But  if actual experience
is  to be  distrusted when  evaluating theory,  we effectively  place
ideology above people — after  all, how the ideology affects people
in <strong>practice</strong>  is irrelevant as experiences  cannot be
used to  evaluate the  (logically sound  but actually  deeply flawed)
theory.</p>

<p>Moreover,  there  is  a  slight arrogance  in  the  “Austrian”
dismissal of  empirical evidence. If,  as they argue, the  economy is
just too complex  to allow us to generalise from  experience then how
can  one person  comprehend  it sufficiently  to  create an  economic
ideology as the  Austrian’s suggest? Surely no one  mind (or series
of  minds) can  produce  a  model which  accurately  reflects such  a
complex  system? To  suggest  that one  can deduce  a  theory for  an
exceedingly complex social system from  the theoretical work based on
an  analysis technique  which  deliberately ignores  that reality  as
being unreliable seems to require  a deliberate suspension of one’s
reasoning faculties. Of course, it may  be argued that such a task is
possible, given a small enough  subset of economic activity. However,
such a process is sure to lead its practitioners astray as the subset
is not independent of the  whole and, consequently, can be influenced
in ways the  ideologist does not (indeed, cannot)  take into account.
Simply put, even the greatest mind cannot comprehend the complexities
of real  life and so  empirical evidence  needs to inform  any theory
seeking to describe and explain it. To reject it is simply to retreat
into  dogmatism  and ideology,  which  is  precisely what  right-wing
libertarians generally do.</p>

<p>Ultimately, this dismissal of empirical evidence seems little more
than self-serving.  It’s utility to  the ideologist is  obvious. It
allows them to speculate to  their hearts content, building models of
the  economy  with  no  bearing  to reality.  Their  models  and  the
conclusions it generates need never  be bothered with reality — nor
the effects of their dogma. Which  shows its utility to the powerful.
It allows  them to  spout comments like  “the free  market benefits
all” while the  rich get richer and allows them  to brush aside any
one who points out such troublesome facts.</p>

<p>That this position is self-serving can  be seen from the fact that
most  right  libertarians  are  very  selective  about  applying  von
Mises’ argument.  As a rule of  thumb, it is only  applied when the
empirical evidence goes against capitalism. In such circumstances the
fact  that the  current system  is  not a  free market  will also  be
mentioned.  However,  if  the  evidence seems  to  bolster  the  case
for  propertarianism then  empirical evidence  becomes all  the rage.
Needless  to  say,  the fact  that  we  do  not  have a  free  market
will  be  conveniently  forgotten.  Depending on  the  needs  of  the
moment, fundamental  facts are dropped  and retrieved to  bolster the
ideology.</p>

<p>As we  indicated above (in section  1.2) and will discuss  in more
depth later  (in section  11) most  of the  leading right-Libertarian
theorists base  themselves on such deductive  methodologies, starting
from assumptions  and “logically” drawing conclusions  from them.
The religious  undertones of such  methodology can best be  seen from
the roots of right-Libertarian “Natural law” theory.</p>

<p>Carole  Pateman,  in  her  analysis of  Liberal  contract  theory,
indicates the religious  nature of the “Natural  Law” argument so
loved by the theorists of the “Radical Right.” She notes that for
Locke  (the main  source  of the  Libertarian  Right’s Natural  Law
cult)  <em>“natural law”</em>  was  equivalent of  <em>“God’s
Law”</em>  and   that  <em>“God’s  law  exists   externally  to
and  independently of  individuals.”</em>  [<strong>The Problem  of
Political Obligation</strong>,  p. 154] No role  for critical thought
there, only  obedience. Most modern day  “Natural Law” supporters
forget to  mention this  religious undercurrent  and instead  talk of
about “Nature”  (or “the market”)  as the deity  that creates
Law,  not  God,  in  order  to appear  “rational.”  So  much  for
science.</p>

<p>Such a basis in dogma and religion can hardly be a firm foundation
for  liberty  and  indeed  “Natural  Law” is  marked  by  a  deep
authoritarianism:</p>

<blockquote class="citation">
“Locke’s   traditional  view   of  natural   law  provided
individual’s with an external  standard which they could recognise,
but which  they did not  voluntarily choose to order  their political
life.”</em> [Pateman, <strong>Op. Cit.</strong>, p. 79]
</blockquote>

<p>In section  11 we discuss  the authoritarian nature  of “Natural
Law” and will not  do so here. However, here we  must point out the
political  conclusions Locke  draws  from his  ideas. In  Pateman’s
words, Locke  believed that  <em>“obedience lasts  only as  long as
protection. His  individuals are  able to  take action  themselves to
remedy their  political lot... but  this does  not mean, as  is often
assumed, that  Locke’s theory  gives direct support  to present-day
arguments for a right of  civil disobedience... His theory allows for
two alternatives only: either people  go peacefully about their daily
affairs under the protection of a liberal, constitutional government,
or they  are in revolt  against a government  which has ceased  to be
‘liberal’ and has become  arbitrary and tyrannical, so forfeiting
its  right  to   obedience.”</em>  [<strong>Op.  Cit.</strong>,  p.
77]</p>

<p>Locke’s    “rebellion”   exists    purely   to    reform   a
<strong>new</strong>  ‘liberal’  government,  not to  change  the
existing socio-economic structure  which the ‘liberal’ government
exists to protect. His theory,  therefore, indicates the results of a
priorism, namely  a denial of  any form  of social dissent  which may
change the  “natural law” as  defined by Locke.  This perspective
can be found  in Rothbard who lambasted  the individualist anarchists
for arguing  that juries should judge  the law as well  as the facts.
For Rothbard, the  law would be drawn up by  jurists and lawyers, not
ordinary people  (see section 1.4  for details). The idea  that those
subject to  laws should  have a  say in forming  them is  rejected in
favour of elite rule. As von Mises put it:</p>

<blockquote class="citation">
 “The flowering of human society depends on two factors: the
intellectual power  of outstanding men  to conceive sound  social and
economic theories,  and the  ability of  these or  other men  to make
these  ideologies palatable  to the  majority.”</em> [<strong>Human
Action</strong>, p. 864]
</blockquote>

<p>Yet such  a task would  require massive propaganda work  and would
only, ultimately,  succeed by removing  the majority from any  say in
the running  of society. Once  that is done  then we have  to believe
that the ruling elite will be altruistic in the extreme and not abuse
their  position to  create  laws and  processes  which defended  what
<strong>they</strong> thought was “legitimate” property, property
rights  and  what  constitutes “aggression.”  Which,  ironically,
contradicts  the key  capitalist  notion that  people  are driven  by
self-gain.  The obvious  conclusion from  such argument  is that  any
right-libertarian regime would have to  exclude change. If people can
change the  regime they  are under  they may change  it in  ways that
right  libertarian’s  do  not  support. The  provision  for  ending
amendments  to the  regime  or  the law  would  effectively ban  most
opposition groups or parties as, by definition, they could do nothing
once  in office  (for  minimal state  “libertarians”)  or in  the
market  for “defence”  agencies (for  “anarcho”-capitalists).
How this  differs from a dictatorship  is hard to say  — after all,
most  dictatorships have  parliamentary  bodies which  have no  power
but  which  can  talk  a  lot.  Perhaps  the  knowledge  that  it  is
<strong>private</strong>  police  enforcing  <strong>private</strong>
power will make those subject to the regime maximise their utility by
keeping quiet and not protesting. Given this, von Mises’ praise for
fascism in the 1920s may be  less contradictory than it first appears
(see  section 6.5)  as  it successfully  “deterred democracy”  by
crushing  the labour,  socialist and  anarchist movements  across the
world.</p>

<p>So, von  Mises, von Hayek  and most right-libertarians  reject the
scientific method  in favour  of ideological  correctness —  if the
facts  contradict your  theory  then  they can  be  dismissed as  too
“complex” or  “unique”. Facts, however, should  inform theory
and  any theory’s  methodology should  take this  into account.  To
dismiss facts out of hand is to promote dogma. This is not to suggest
that a theory  should be modified very time new  data comes along —
that would  be crazy as unique  situations <strong>do</strong> exist,
data can be wrong  and so forth — but it does  suggest that if your
theory <strong>continually</strong> comes into conflict with reality,
its  time to  rethink the  theory and  not assume  that facts  cannot
invalidate  it. A  true  libertarian would  approach a  contradiction
between  reality and  theory by  evaluating the  facts available  and
changing the theory  is this is required, not by  ignoring reality or
dismissing it as “complex”.</p>

<p>Thus,  much of  right-Libertarian  theory  is neither  libertarian
nor  scientific.   Much  of   right-libertarian  thought   is  highly
axiomatic,  being  logically deduced  from  such  starting axioms  as
<em>“self-ownership”</em> or <em>“no  one should initiate force
against another”</em>.  Hence the  importance of our  discussion of
von Mises as this indicates the  dangers of this approach, namely the
tendency to  ignore/dismiss the consequences of  these logical chains
and,  indeed,  to  justify  them  in terms  of  these  axioms  rather
than  from the  facts.  In  addition, the  methodology  used is  such
as  that  it would  be  fair  to  argue that  right-libertarians  get
to  critique  reality but  reality  can  never  be used  to  critique
right-libertarianism — for any empirical data presented as evidence
as  be  dismissed  as  “too   complex”  or  “unique”  and  so
irrelevant  (unless  it can  be  used  to  support their  claims,  of
course).</p>

<p>Hence  W. Duncan  Reekie’s  argument  (quoting leading  Austrian
economist  Israel  Kirzner)  that <em>“empirical  work  ‘has  the
function  of   establishing  the   <strong>applicability</strong>  of
particular  theorems,  and thus  <strong>illustrating</strong>  their
operation’ ... Confirmation of theory is not possible because there
is  no  constants  in  human  action, nor  is  it  necessary  because
theorems  themselves   describe  relationships   logically  developed
from  hypothesised   conditions.  Failure  of  a   logically  derived
axiom  to  fit the  facts  does  not  render  it invalid,  rather  it
‘might  merely  indicate  inapplicability’ to  the  circumstances
of   the   case.’”</em>   [<strong>Markets,   Entrepreneurs   and
Liberty</strong>, p. 31]</p>

<p>So,  if  facts confirm  your  theory,  your  theory is  right.  If
facts  do  not confirm  your  theory,  it  is  still right  but  just
not  applicable  in  this  case!  Which has  the  handy  side  effect
of  ensuring   that  facts  can  <strong>only</strong>   be  used  to
support  the ideology,  <strong>never</strong>  to  refute it  (which
is,  according  to  this  perspective, impossible  anyway).  As  Karl
Popper  argued,  a  <em>“theory  which  is  not  refutable  by  any
conceivable event is non-scientific.”</em> [<strong>Conjectures and
Refutations</strong>, p. 36]  In other words (as we  noted above), if
reality contradicts your theory, ignore reality!</p>

<p>Kropotkin hoped <em>“that those who believe in [current economic
doctrines] will themselves become convinced of their error as soon as
they  come  to see  the  necessity  of verifying  their  quantitative
deductions   by   quantitative  investigation.”</em>   [<strong>Op.
Cit.</strong>, p. 178] However, the  Austrian approach builds so many
barriers to  this that it is  doubtful that this will  occur. Indeed,
right-libertarianism,  with its  focus  on exchange  rather than  its
consequences, seems to  be based upon justifying  domination in terms
of their  deductions than  analysing what  freedom actually  means in
terms of human existence (see section 2 for a fuller discussion).</p>

<p>The real  question is  why are such  theories taken  seriously and
arouse such interest. Why are they  not simply dismissed out of hand,
given  their  methodology  and  the  authoritarian  conclusions  they
produce? The  answer is,  in part, that  feeble arguments  can easily
pass for convincing when they are  on the same side as the prevailing
sentiment and social system. And, of  course, there is the utility of
such theories  for ruling elites —  <em>“[a]n ideological defence
of  privileges, exploitation,  and  private power  will be  welcomed,
regardless of its merits.”</em>  [Noam Chomsky, <strong>The Chomsky
Reader</strong>, p. 188]</p>

<h4 id="toc20">  1.4  Is  “anarcho”-capitalism  a  new  form  of individualist anarchism?</h4>

<p>Some “anarcho”-capitalists shy away  from the term, preferring
such  expressions  as   “market  anarchist”  or  “individualist
anarchist.”  This   suggests  that  there  is   some  link  between
their  ideology  and   that  of  Tucker.  However,   the  founder  of
“anarcho”-capitalism,  Murray Rothbard,  refused that  label for,
while  <em>“strongly tempted,”</em>  he could  not do  so because
<em>“Spooner  and  Tucker have  in  a  sense pre-empted  that  name
for  their  doctrine and  that  from  that  doctrine I  have  certain
differences.”</em>  Somewhat  incredibly  Rothbard argued  that  on
the  whole politically  <em>“these  differences are  minor,”</em>
economically <em>“the  differences are substantial, and  this means
that  my  view of  the  consequences  of  putting  our more  of  less
common  system  into  practice   is  very  far  from  theirs.”</em>
[<em>“The Spooner-Tucker  Doctrine: An  Economist’s View”</em>,
<strong>Journal of  Libertarian Studies</strong>, vol. 20,  no. 1, p.
7]</p>

<p>What  an  understatement!  Individualist anarchists  advocated  an
economic system in which there would have been very little inequality
of wealth and so of power (and the accumulation of capital would have
been minimal without profit, interest and rent). Removing this social
and  economic basis  would  result in  <strong>substantially</strong>
different political regimes. This can be seen from the fate of Viking
Iceland, where  a substantially  communal and anarchistic  system was
destroyed from within by increasing inequality and the rise of tenant
farming  (see section  9 for  details). In  other words,  politics is
not  isolated  from  economics.  As  David  Wieck  put  it,  Rothbard
<em>“writes of society  as though some part of  it (government) can
be extracted and  replaced by another arrangement  while other things
go on before, and he constructs a system of police and judicial power
without any consideration of the influence of historical and economic
context.”</em> [<em>“Anarchist  Justice,”</em> in <strong>Nomos
XIX</strong>, Pennock and Chapman, eds., p. 227]</p>

<p>Unsurprisingly,   the   political    differences   he   highlights
<strong>are</strong>  significant,  namely  <em>“the  role  of  law
and  the jury  system”</em> and  <em>“the land  question.”</em>
The  former difference  relates to  the fact  that the  individualist
anarchists  <em>“allow[ed] each  individual free-market  court, and
more  specifically, each  free-market  jury, totally  free rein  over
judicial  decision.”</em> This  horrified Rothbard.  The reason  is
obvious, as  it allows real  people to judge the  law as well  as the
facts,  modifying the  former  as society  changes  and evolves.  For
Rothbard,  the  idea  that  ordinary  people should  have  a  say  in
the  law  is  dismissed.  Rather,  <em>“it  would  not  be  a  very
difficult task  for Libertarian  lawyers and jurists  to arrive  at a
rational  and  objective code  of  libertarian  legal principles  and
procedures.”</em> [<strong>Op. Cit.</strong>,  p. 7–8] Of course,
the fact  that <em>“lawyers”</em> and  <em>“jurists”</em> may
have a radically different idea of what is just than those subject to
their  laws is  not raised  by Rothbard,  never mind  answered. While
Rothbard notes that  juries may defend the people  against the state,
the notion that they may defend  the people against the authority and
power of  the rich  is not  even raised.  That is  why the  rich have
tended to oppose juries as well as popular assemblies.</p>

<p>Unsurprisingly,  the few  individualist  anarchists that  remained
pointed  this out.  Laurance  Labadie, the  son  of Tucker  associate
Joseph Labadie, argued in response to Rothbard as follows:</p>

<blockquote class="citation">
Mere common  sense would suggest  that any court would  be influenced
by  experience; and  any  free-market  court or  judge  would in  the
very  nature of  things have  some precedents  guiding them  in their
instructions to a jury. But since no case is exactly the same, a jury
would have considerable  say about the heinousness of  the offence in
each case, realising that  circumstances alter cases, and prescribing
penalty  accordingly. This  appeared to  Spooner and  Tucker to  be a
more  flexible and  equitable administration  of justice  possible or
feasible, human beings being what they are...
</blockquote>

<blockquote class="citation">
But when  Mr. Rothbard  quibbles about  the jurisprudential  ideas of
Spooner and Tucker,  and at the same  time upholds <strong>presumably
in his  courts</strong> the very  economic evils which are  at bottom
the very reason  for human contention and conflict, he  would seem to
be a man who chokes at a gnat while swallowing a camel.
</blockquote>

<blockquote class="citation_source">
<i>Quoted by Mildred J. Loomis and Mark A. Sullivan, «&nbsp;Laurance
Labadie: Keeper  Of The Flame&nbsp;», pp.  116–30,</i><br/>
Benjamin  R.  Tucker  and  the  Champions  of  Liberty<i>,  Coughlin,
Hamilton and Sullivan (eds.), p. 124</i>
</blockquote>

<p>In other  words, to  exclude the general  population from  any say
in  the law  and  how  it changes  is  hardly a  <em>“minor”</em>
difference!  Particularly if  you  are proposing  an economic  system
which is based on inequalities of wealth, power and influence and the
means  of accumulating  more. It  is like  a supporter  of the  state
saying that it  is a <em>“minor”</em> difference if  you favour a
dictatorship  rather than  a  democratically  elected government.  As
Tucker  argued,  <em>“it  is  precisely in  the  tempering  of  the
rigidity  of  enforcement  that  one  of  the  chief  excellences  of
Anarchism consists  ... under  Anarchism all rules  and laws  will be
little more than suggestions for the guidance of juries, and that all
disputes ...  will be submitted to  juries which will judge  not only
the facts but  the law, the justice of the  law, its applicability to
the given  circumstances, and the  penalty or damage to  be inflicted
because of  its infraction ...  under Anarchism  the law ...  will be
regarded as  <strong>just</strong> in proportion to  its flexibility,
instead of  now in  proportion to its  rigidity.”</em> [<strong>The
Individualist Anarchists</strong>,  pp. 160–1]  In others,  the law
will evolve to  take into account changing  social circumstances and,
as  a consequence,  public  opinion on  specific  events and  rights.
Tucker’s position is  fundamentally <strong>democratic</strong> and
evolutionary while Rothbard’s is autocratic and fossilised.</p>

<p>On  the   land  question,   Rothbard  opposed   the  individualist
position   of   “occupancy   and    use”   as   it   <em>“would
automatically  abolish  all  rent payments  for  land.”</em>  Which
was  <strong>precisely</strong>  why   the  individualist  anarchists
advocated it!  In a  predominantly rural  economy, this  would result
in  a  significant levelling  of  income  and  social power  as  well
as  bolstering  the  bargaining   position  of  non-land  workers  by
reducing unemployment.  He bemoans that landlords  cannot charge rent
on  their  <em>“justly-acquired  private  property”</em>  without
noticing  that  is  begging  the question  as  anarchists  deny  that
this is  <em>“justly-acquired”</em> land.  Unsurprising, Rothbard
considers  <em>“the property  theory”</em> of  land ownership  as
John  Locke’s, ignoring  the  fact that  the first  self-proclaimed
anarchist  book  was written  to  refute  that  kind of  theory.  His
argument simply  shows how  far from anarchism  his ideology  is. For
Rothbard, it goes without saying that the landlord’s <em>“freedom
of contract”</em> tops the worker’s  freedom to control their own
work  and live  and, of  course,  their right  to life.  [<strong>Op.
Cit.</strong>, p.  8 and  p. 9]  However, for  anarchists, <em>“the
land is indispensable to our  existence, consequently a common thing,
consequently   insusceptible  of   appropriation.”</em>  [Proudhon,
<strong>What is Property?</strong>, p. 107]</p>

<p>The   reason   question  is   why   Rothbard   considers  this   a
<strong>political</strong>  difference rather  than an  economic one.
Unfortunately, he does not explain. Perhaps because of the underlying
<strong>socialist</strong> perspective behind the anarchist position?
Or perhaps  the fact that feudalism  and monarchism was based  on the
owner of the land being its  ruler suggests a political aspect to the
ideology  best left  unexplored?  Given that  the  idea of  grounding
rulership  on  land ownership  receded  during  the Middle  Ages,  it
may  be  unwise  to  note  that  under  “anarcho”-capitalism  the
landlord and capitalist  would, likewise, be sovereign  over the land
<strong>and</strong> those  who used  it? As we  noted in  section 1,
this  is the  conclusion  that  Rothbard does  draw.  As such,  there
<strong>is</strong> a political aspect to this difference.</p>

<p>Moreover.  <em>“the  expropriation of  the  mass  of the  people
from  the   soil  forms   the  basis  of   the  capitalist   mode  of
production.”</em> [Marx, <strong>Capital</strong>,  vol. 1, p. 934]
For there are  <em>“two ways of oppressing men:  either directly by
brute force, by physical violence;  or indirectly by denying them the
means of life and this reducing them to a state of surrender.”</em>
In the second  case, government is <em>“an  organised instrument to
ensure that dominion and privilege will  be in the hands of those who
... have cornered all the means of life, first and foremost the land,
which they make use of to keep the people in bondage and to make them
work for their benefit.”</em> [Malatesta, <strong>Anarchy</strong>,
p. 21] Privatising  the coercive functions of  said government hardly
makes much difference.</p>

<p>Of  course, Rothbard  is simply  skimming the  surface. There  are
two  main ways  “anarcho”-capitalists  differ from  individualist
anarchists.  The  first  one  is  the  fact  that  the  individualist
anarchists  are socialists.  The  second is  on  whether equality  is
essential or not to anarchism. Each will be discussed in turn.</p>

<p>Unlike    both    Individualist     (and    social)    anarchists,
“anarcho”-capitalists  support  capitalism   (a  “pure”  free
market  type,   which  has  never   existed  although  it   has  been
approximated  occasionally).  This  means that  they  reject  totally
the  ideas  of  anarchists  with regards  to  property  and  economic
analysis.  For  example,  like  all supporters  of  capitalists  they
consider rent,  profit and  interest as  valid incomes.  In contrast,
all  Anarchists  consider  these   as  exploitation  and  agree  with
the   Individualist  Anarchist   Benjamin  Tucker   when  he   argued
that  <em>“<strong>[w]hoever</strong>   contributes  to  production
is  alone   entitled.  <strong>What</strong>   has  no   rights  that
<strong>who</strong> is bound to  respect. <strong>What</strong> is a
thing. <strong>Who</strong> is a person.  Things have no claims; they
exist  only to  be  claimed.  The possession  of  a  right cannot  be
predicted of dead material,  but only a living person.”</em>[quoted
by Wm. Gary Kline,  <strong>The Individualist Anarchists</strong>, p.
73]</p>

<p>This, we must note, is  the fundamental critique of the capitalist
theory that capital is productive.  In and of themselves, fixed costs
do  not  create  value.  Rather  value is  creation  depends  on  how
investments are  developed and  used once in  place. Because  of this
the  Individualist  Anarchists,  like  other  anarchists,  considered
non-labour derived income as usury, unlike “anarcho”-capitalists.
Similarly,  anarchists  reject  the  notion  of  capitalist  property
rights  in  favour  of  possession  (including  the  full  fruits  of
one’s labour). For example,  anarchists reject private ownership of
land  in favour  of  a “occupancy  and use”  regime.  In this  we
follow Proudhon’s <strong>What is Property?</strong> and argue that
<em>“property is  theft”</em>. Rothbard, as noted,  rejected this
perspective.</p>

<p>As these ideas are an <strong>essential</strong> part of anarchist
politics, they cannot be removed  without seriously damaging the rest
of  the  theory. This  can  be  seen  from Tucker’s  comments  that
<em>“<strong>Liberty</strong> insists... [on]  the abolition of the
State  and the  abolition  of usury;  on no  more  government of  man
by  man, and  no  more  exploitation of  man  by man.”</em>  [cited
by  Eunice Schuster  in  <strong>Native American  Anarchism</strong>,
p.  140].   He  indicates   that  anarchism  has   specific  economic
<strong>and</strong>  political  ideas,  that it  opposes  capitalism
along  with  the  state.  Therefore  anarchism  was  never  purely  a
“political”  concept,  but  always   combined  an  opposition  to
oppression with an opposition  to exploitation. The social anarchists
made exactly the same point. Which means that when Tucker argued that
<em>“<strong>Liberty</strong>  insists  on  Socialism...  —  true
Socialism, Anarchistic Socialism: the prevalence on earth of Liberty,
Equality, and Solidarity”</em>  he knew exactly what  he was saying
and meant it wholeheartedly.  [<strong>Instead of a Book</strong>, p.
363]</p>

<p>So because “anarcho”-capitalists embrace capitalism and reject
socialism,  they  cannot be  considered  anarchists  or part  of  the
anarchist tradition.</p>

<p>Which  brings us  nicely to  the second  point, namely  a lack  of
concern for equality. In stark contrast to anarchists of all schools,
inequality is not seen to be a problem with “anarcho”-capitalists
(see section 3).  However, it is a truism that  not all “traders”
are equally subject to the market  (i.e. have the same market power).
In  many  cases,  a  few  have sufficient  control  of  resources  to
influence  or determine  price and  in  such cases,  all others  must
submit to those terms or not buy the commodity. When the commodity is
labour power, even this option is  lacking — workers have to accept
a job  in order  to live. As  we argue in  section 10.2,  workers are
usually  at a  disadvantage on  the  labour market  when compared  to
capitalists, and this forces them to sell their liberty in return for
making  profits  for others.  These  profits  increase inequality  in
society  as  the property  owners  receive  the surplus  value  their
workers  produce. This  increases  inequality further,  consolidating
market  power  and so  weakens  the  bargaining position  of  workers
further, ensuring that even the freest competition possible could not
eliminate class power and society  (something B. Tucker recognised as
occurring with  the development of  trusts within capitalism  — see
section G.4).</p>

<p>By  removing  the  underlying  commitment  to  abolish  non-labour
income,  any  “anarchist”  capitalist  society  would  have  vast
differences in wealth  and so power. Instead of  a government imposed
monopolies  in land,  money and  so  on, the  economic power  flowing
from  private property  and capital  would ensure  that the  majority
remained  in  (to  use  Spooner’s words)  <em>“the  condition  of
servants”</em>  (see sections  2 and  3.1  for more  on this).  The
Individualist Anarchists were  aware of this danger  and so supported
economic ideas  that opposed usury  (i.e. rent, profit  and interest)
and ensured the worker the full value of her labour. While not all of
them called these ideas “socialist”  it is clear that these ideas
<strong>are</strong> socialist  in nature and in  aim (similarly, not
all  the Individualist  Anarchists called  themselves anarchists  but
their ideas are clearly anarchist in nature and in aim).</p>

<p>This combination  of the  political and  economic is  essential as
they mutually reinforce  each other. Without the  economic ideas, the
political  ideas would  be  meaningless as  inequality  would make  a
mockery  of them.  As  Kline notes,  the Individualist  Anarchists’
<em>“proposals  were   designed  to  establish  true   equality  of
opportunity  ... and  they expected  this would  result in  a society
without  great wealth  or  poverty. In  the  absence of  monopolistic
factors  which would  distort  competition, they  expected a  society
largely  of self-employed  workmen with  no significant  disparity of
wealth between  any of them  since all would  be required to  live at
their own  expense and not at  the expense of exploited  fellow human
beings.”</em> [<strong>Op. Cit.</strong>, pp. 103–4]</p>

<p>Because of the evil effects  of inequality on freedom, both social
and  individualist anarchists  desired  to create  an environment  in
which  circumstances would  not drive  people to  sell their  liberty
to  others  at  a  disadvantage.  In other  words,  they  desired  an
equalisation of  market power by  opposing interest, rent  and profit
and capitalist definitions of private property. Kline summarises this
by saying <em>“the American  [individualist] anarchists exposed the
tension existing in liberal thought  between private property and the
ideal  of equal  access. The  Individual Anarchists  were, at  least,
aware that existing  conditions were far from ideal,  that the system
itself working against  the majority of individuals  in their efforts
to attain  its promises. Lack of  capital, the means to  creation and
accumulation  of wealth,  usually  doomed  a labourer  to  a life  of
exploitation.  This the  anarchists  knew and  they  abhorred such  a
system.”</em> [<strong>Op. Cit.</strong>, p. 102]</p>

<p>And  this desire  for bargaining  equality is  reflected in  their
economic ideas and by removing these underlying economic ideas of the
individualist  anarchists, “anarcho”-capitalism  makes a  mockery
of  any ideas  they  do appropriate.  Essentially, the  Individualist
Anarchists  agreed with  Rousseau that  in order  to prevent  extreme
inequality of fortunes you deprive  people of the means to accumulate
in the first place and <strong>not</strong> take away wealth from the
rich.  An important  point  which  “anarcho”-capitalism fails  to
understand or appreciate.</p>

<p>There are, of course, overlaps between individualist anarchism and
“anarcho”-capitalism, just  as there are overlaps  between it and
Marxism  (and  social  anarchism,  of course).  However,  just  as  a
similar analysis of capitalism does not make individualist anarchists
Marxists,  so apparent  similarities between  individualist anarchism
does  not  make  it  a forerunner  of  “anarcho”-capitalism.  For
example, both schools support the idea of “free markets.” Yet the
question of markets is fundamentally  second to the issue of property
rights  for what  is exchanged  on the  market is  dependent on  what
is  considered  legitimate  property.  In this,  as  Rothbard  notes,
individualist  anarchists  and “anarcho”-capitalists  differ  and
different  property rights  produce different  market structures  and
dynamics. This  means that  capitalism is not  the only  economy with
markets and so support for markets cannot be equated with support for
capitalism.  Equally, opposition  to markets  is <strong>not</strong>
the  defining characteristic  of  socialism (as  we  note in  section
G.2.1).  As such,  it  <strong>is</strong> possible  to  be a  market
socialist (and many socialist are). This is because “markets” and
“property” do not equate to capitalism:</p>

<blockquote class="citation">
Political   economy  confuses,   on  principle,   two  very
different kinds of private property, one of which rests on the labour
of the  producers himself, and the  other on the exploitation  of the
labour of others.  It forgets that the latter is  not only the direct
antithesis  of the  former,  but  grows on  the  former’s tomb  and
nowhere else.
</blockquote>

<blockquote class="citation">
In  Western  Europe,  the homeland  of  political  economy,
the   process   of   primitive   accumulation   is   more   of   less
accomplished...
</blockquote>

<blockquote class="citation">
It  is  otherwise in  the  colonies.  There the  capitalist
regime  constantly comes  up against  the obstacle  presented by  the
producer, who, as owner of his own conditions of labour, employs that
labour to enrich himself instead of the capitalist. The contradiction
of these two diametrically opposed economic systems has its practical
manifestation here in the  struggle between them.
</blockquote>

<blockquote class="citation_author">Karl Marx</blockquote>
<blockquote class="citation_source">Capital,<i> vol. 1, p. 931</i></blockquote>

<p>Individualist anarchism  is obviously  an aspect of  this struggle
between  the  system  of  peasant and  artisan  production  of  early
America  and the  state  encouraged system  of  private property  and
wage  labour.  “Anarcho”-capitalists,  in contrast,  assume  that
generalised wage labour would remain under their system (while paying
lip-service  to the  possibilities  of co-operatives  —  and if  an
“anarcho”-capitalist  thinks that  co-operative  will become  the
dominant form of  workplace organisation, then they are  some kind of
market  socialist, <strong>not</strong>  a capitalist).  It is  clear
that  their  end point  (a  pure  capitalism, i.e.  generalised  wage
labour) is directly the opposite  of that desired by anarchists. This
was the case  of the Individualist Anarchists who  embraced the ideal
of (non-capitalist)  laissez faire  competition —  they did  so, as
noted, to <strong>end</strong>  exploitation, <strong>not</strong> to
maintain it. Indeed, their analysis of the change in American society
from  one  of mainly  independent  producers  into one  based  mainly
upon  wage  labour has  many  parallels  with,  of all  people,  Karl
Marx’s presented  in chapter 33 of  <strong>Capital</strong>. Marx,
correctly, argues  that <em>“the capitalist mode  of production and
accumulation,  and therefore  capitalist private  property, have  for
their fundamental condition the annihilation of that private property
which rests on the labour of  the individual himself; in other words,
the expropriation of  the worker.”</em> [<strong>Op. Cit.</strong>,
p. 940] He notes that to achieve this, the state is used:</p>

<blockquote class="citation">
“How then  can the anti-capitalistic cancer  of the colonies
be healed?  ... Let  the Government  set an  artificial price  on the
virgin soil, a  price independent of the law of  supply and demand, a
price that compels the immigrant to work a long time for wages before
he  can earn  enough money  to  buy land,  and turn  himself into  an
independent farmer.”</em> [<strong>Op. Cit.</strong>, p. 938]
</blockquote>

<p>Moreover,  tariffs are  introduced  with  <em>“the objective  of
manufacturing capitalists artificially”</em>  for the <em>“system
of protection was an artificial means of manufacturing manufacturers,
or expropriating  independent workers,  of capitalising  the national
means of  production and subsistence,  and of forcibly  cutting short
the  transition ...  to  the modern  mode  of production,”</em>  to
capitalism [<strong>Op. Cit.</strong>, p. 932 and pp. 921–2]</p>

<p>It  is  this  process   which  Individualist  Anarchism  protested
against, the use of the state  to favour the rising capitalist class.
However, unlike social anarchists, many individualist anarchists were
not consistently against  wage labour. This is  the other significant
overlap between “anarcho”-capitalism and individualist anarchism.
However,  they  were  opposed  to  exploitation  and  argued  (unlike
“anarcho”-capitalism)  that in  their  system workers  bargaining
powers would be  raised to such a level that  their wages would equal
the full product  of their labour. However, as we  discuss in section
G.1.1 the social  context the individualist anarchists  lived in must
be remembered. America at the times was a predominantly rural society
and industry  was not  as developed  as it is  now wage  labour would
have been  minimised (Spooner,  for example, explicitly  envisioned a
society made up  mostly entirely of self-employed  workers). As Kline
argues:</p>

<blockquote class="citation">
“Committed  as  they were  to  equality  in the  pursuit  of
property,  the objective  for the  anarchist became  the construction
of  a  society  providing  equal access  to  those  things  necessary
for  creating  wealth.  The  goal  of  the  anarchists  who  extolled
mutualism  and  the   abolition  of  all  monopolies   was,  then,  a
society  where  everyone  willing  to   work  would  have  the  tools
and  raw materials  necessary  for production  in a  non-exploitative
system  ...   the  dominant   vision  of   the  future   society  ...
[was]  underpinned  by   individual,  self-employed  workers.”</em>
[<strong>Op. Cit.</strong>, p. 95]
</blockquote>

<p>As such,  a limited amount  of wage labour within  a predominantly
self-employed  economy  does  not  make a  given  society  capitalist
any  more than  a  small amount  of  governmental communities  within
an  predominantly anarchist  world  would make  it  statist. As  Marx
argued. when <em>“the separation of  the worker from the conditions
of  labour  and  from the  soil  ...  does  not  yet exist,  or  only
sporadically,  or on  too limited  a  scale ...  Where, amongst  such
curious  characters,  is  the   ‘field  of  abstinence’  for  the
capitalists? ... Today’s  wage-labourer is tomorrow’s independent
peasant  or  artisan,  working  for himself.  He  vanishes  from  the
labour-market  — but  not into  the workhouse.”</em>  There is  a
<em>“constant  transformation  of wage-labourers  into  independent
producers, who work for themselves instead of for capital”</em> and
so <em>“the  degree of exploitation of  the wage-labourer remain[s]
indecently  low.”</em> In  addition, the  <em>“wage-labourer also
loses,  along  with  the  relation  of  dependence,  the  feeling  of
dependence   on  the   abstemious  capitalist.”</em>   [<strong>Op.
Cit.</strong>, pp. 935–6]</p>

<p>Saying that, as we discuss in section G.4, individualist anarchist
support for  wage labour is at  odds with the ideas  of Proudhon and,
far  more  importantly,  in  contradiction  to  many  of  the  stated
principles of the individualist anarchists themselves. In particular,
wage labour violates  “occupancy and use” as well  as having more
than a passing  similarity to the state. However,  these problems can
be solved  by consistently  applying the principles  of individualist
anarchism,  unlike  “anarcho”-capitalism,  and  that  is  why  it
is  a  real  school  of  anarchism.  In  other  words,  a  system  of
<strong>generalised</strong> wage  labour would not be  anarchist nor
would  it be  non-exploitative.  Moreover, the  social context  these
ideas were developed in and would have been applied ensure that these
contradictions would have been minimised. If they had been applied, a
genuine  anarchist society  of  self-employed workers  would, in  all
likelihood, have been created (at  least at first, whether the market
would increase inequalities is a moot point — see section G.4).</p>

<p>We  must stress  that  the  social situation  is  important as  it
shows  how  apparently  superficially   similar  arguments  can  have
radically  different  aims  and  results depending  on  who  suggests
them  and  in  what  circumstances.  As noted,  during  the  rise  of
capitalism the bourgeoisie were not  shy in urging state intervention
against the  masses. Unsurprisingly,  working class  people generally
took  an anti-state  position during  this period.  The individualist
anarchists were  part of  that tradition,  opposing what  Marx termed
<em>“primitive accumulation”</em> in favour of the pre-capitalist
forms of property and society it was destroying.</p>

<p>However,  when capitalism  found  its feet  and  could do  without
such  obvious intervention,  the possibility  of an  “anti-state”
capitalism could arise. Such a possibility became a definite once the
state started to intervene in ways which, while benefiting the system
as  a whole,  came  into  conflict with  the  property  and power  of
individual members of the capitalist  and landlord class. Thus social
legislation  which  attempted to  restrict  the  negative effects  of
unbridled exploitation and oppression  on workers and the environment
were having on the economy were the source of much outrage in certain
bourgeois circles:</p>

<blockquote class="citation">
“Quite independently  of these tendencies  [of individualist
anarchism]   ...   the   anti-state  bourgeoisie   (which   is   also
anti-statist, being hostile to any social intervention on the part of
the State to protect the victims of exploitation — in the matter of
working  hours,  hygienic working  conditions  and  so on),  and  the
greed  of  unlimited  exploitation,  had  stirred  up  in  England  a
certain agitation in favour  of pseudo-individualism, an unrestrained
exploitation. To this end, they  enlisted the services of a mercenary
pseudo-literature  ... which  played with  doctrinaire and  fanatical
ideas in order  to project a species of  ‘individualism’ that was
absolutely sterile,  and a species of  ‘non-interventionism’ that
would let a man die of hunger rather than offend his dignity.”</em>
[Max  Nettlau,  <strong>A  Short History  of  Anarchism</strong>,  p.
39]
</blockquote>

<p>This perspective can be seen when Tucker denounced Herbert Spencer
as a  champion of  the capitalistic  class for  his vocal  attacks on
social legislation which claimed to  benefit working class people but
stays  strangely  silent  on  the laws  passed  to  benefit  (usually
indirectly) capital and the rich. “Anarcho”-capitalism is part of
that tradition, the  tradition associated with a  capitalism which no
longer  needs obvious  state intervention  as enough  wealth as  been
accumulated  to  keep  workers  under  control  by  means  of  market
power.</p>

<p>As with  the original nineteenth century  British “anti-state”
capitalists  like  Spencer  and Herbert,  Rothbard  <em>“completely
overlooks  the  role of  the  state  in  building and  maintaining  a
capitalist economy in  the West. Privileged to live  in the twentieth
century, long  after the  battles to  establish capitalism  have been
fought and  won, Rothbard sees  the state solely  as a burden  on the
market  and  a vehicle  for  imposing  the  still greater  burden  of
socialism.  He manifests  a kind  of historical  nearsightedness that
allows him  to collapse many  centuries of human experience  into one
long night of tyranny that ended  only with the invention of the free
market  and  its  ‘spontaneous’  triumph over  the  past.  It  is
pointless to  argue, as Rothbard  seems ready to do,  that capitalism
would have  succeeded without the  bourgeois state; the fact  is that
all capitalist nations have relied  on the machinery of government to
create and preserve the political  and legal environments required by
their economic system.”</em>  That, of course, has  not stopped him
<em>“critis[ing] others for  being unhistorical.”</em> [Stephen L
Newman, <strong>Liberalism at Wit’s End</strong>, pp. 77–8 and p.
79]</p>

<p>In  other  words, there  is  substantial  differences between  the
victims  of  a  thief  trying  to  stop  being  robbed  and  be  left
alone  to  enjoy  their  property  and  the  successful  thief  doing
the  same!  Individualist  Anarchist’s  were  aware  of  this.  For
example,  Victor   Yarros  stressed   this  key   difference  between
individualist anarchism  and the  proto-“libertarian” capitalists
of “voluntaryism”:</p>

<blockquote class="citation">
[Auberon Herbert] believes in allowing people to retain all
their possessions, no matter how  unjustly and basely acquired, while
getting them, so to speak, to  swear off stealing and usurping and to
promise to  behave well in the  future. We, on the  other hand, while
insisting on  the principle of  private property, in  wealth honestly
obtained  under  the  reign  of  liberty,  do  not  think  it  either
unjust or  unwise to  dispossess the  landlords who  have monopolised
natural  wealth  by force  and  fraud.  We  hold  that the  poor  and
disinherited toilers  would be justified in  expropriating, not alone
the  landlords, who  notoriously have  no equitable  titles to  their
lands,  but  <strong>all</strong>  the financial  lords  and  rulers,
all  the  millionaires and  very  wealthy  individuals... Almost  all
possessors  of  great  wealth  enjoy  neither  what  they  nor  their
ancestors rightfully acquired (and if Mr. Herbert wishes to challenge
the correctness of this statement, we are ready to go with him into a
full discussion of the subject)...
</blockquote>

<blockquote class="citation">
If he holds that the landlords are justly entitled to their
lands, let him  make a defence of  the landlords or an  attack on our
unjust proposal.
</blockquote>

<blockquote class="citation_source">
<i>Quoted by Carl Watner,  «&nbsp;The English Individualists As They
Appear In Liberty&nbsp;», pp. 191–211,</i><br/>
Benjamin  R.  Tucker  and  the  Champions  of  Liberty<i>,  Coughlin,
Hamilton and Sullivan (eds.), pp. 199–200</i>
</blockquote>

<p>Significantly,  Tucker  and  other  individualist  anarchists  saw
state   intervention   has   a   result   of   capital   manipulating
legislation  to  gain  an  advantage on  the  so-called  free  market
which  allowed them  to exploit  labour  and, as  such, it  benefited
the  <strong>whole</strong>  capitalist  class.  Rothbard,  at  best,
acknowledges  that  <strong>some</strong>  sections of  big  business
benefit  from   the  current  system   and  so  fails  to   have  the
comprehensive  understanding  of  the  dynamics of  capitalism  as  a
<strong>system</strong>  (rather  as  an   ideology).  This  lack  of
understanding of capitalism  as a historic and  dynamic system rooted
in  class  rule  and  economic   power  is  important  in  evaluating
“anarcho”-capitalist  claims  to   anarchism.  Marxists  are  not
considered  anarchists  as they  support  the  state  as a  means  of
transition  to an  anarchist  society.  Much the  same  logic can  be
applied to right-wing  libertarians (even if they  do call themselves
“anarcho”-capitalists).  This  is because  they  do  not seek  to
correct  the inequalities  produced by  previous state  action before
ending it  nor do they seek  to change the definitions  of “private
property”  imposed by  the state.  In effect,  they argue  that the
“dictatorship of the bourgeoisie” should “wither away” and be
limited  to  defending  the  property accumulated  in  a  few  hands.
Needless  to say,  starting  from the  current (coercively  produced)
distribution  of property  and  then  eliminating “force”  simply
means defending the power and privilege of ruling minorities:</p>

<blockquote class="citation">
“The modern  Individualism initiated by Herbert  Spencer is,
like the critical  theory of Proudhon, a  powerful indictment against
the dangers and  wrongs of government, but its  practical solution of
the social  problem is miserable  — so miserable  as to lead  us to
inquire  if the  talk  of  ‘No force’  be  merely  an excuse  for
supporting landlord  and capitalist  domination.”</em> [<strong>Act
For Yourselves</strong>, p. 98]
</blockquote>

<h3   id="toc21">2   What   do  “anarcho”-capitalists   mean   by “freedom”?</h3>

<p>For “anarcho”-capitalists,  the concept of freedom  is limited
to the idea  of <em>“freedom from.”</em> For  them, freedom means
simply freedom  from the  <em>“initiation of force,”</em>  or the
<em>“non-aggression against anyone’s person and property.”</em>
[Murray  Rothbard, <strong>For  a  New Liberty</strong>,  p. 23]  The
notion that real freedom  must combine both freedom <em>“to”</em>
<strong>and</strong> freedom <em>“from”</em>  is missing in their
ideology,  as is  the social  context of  the so-called  freedom they
defend.</p>

<p>Before starting, it is useful to  quote Alan Haworth when he notes
that <em>“[i]n  fact, it is surprising  how <strong>little</strong>
close  attention the  concept  of freedom  receives from  libertarian
writers. Once again <strong>Anarchy,  State, and Utopia</strong> is a
case in  point. The word  ‘freedom’ doesn’t even appear  in the
index.  The  word  ‘liberty’  appears,  but  only  to  refer  the
reader  to  the  ‘Wilt  Chamberlain’  passage.  In  a  supposedly
‘libertarian’ work,  this is  more than  surprising. It  is truly
remarkable.”</em> [<strong>Anti-Libertarianism</strong>, p. 95]</p>

<p>Why   this   is   the   case    can   be   seen   from   how   the
“anarcho”-capitalist defines freedom.</p>

<p>In  a   right-libertarian  or   “anarcho”-capitalist  society,
freedom is considered to be a product of property. As Murray Rothbard
puts it, <em>“the libertarian  defines the concept of ‘freedom’
or ‘liberty’...[as  a] condition in which  a person’s ownership
rights in  his body and  his legitimate material property  rights are
not invaded,  are not  aggressed against... Freedom  and unrestricted
property rights go hand  in hand.”</em> [<strong>Op. Cit.</strong>,
p.41]</p>

<p>This definition has some problems, however. In such a society, one
cannot (legitimately) do anything with  or on another’s property if
the  owner  prohibits it.  This  means  that an  individual’s  only
<strong>guaranteed</strong> freedom  is determined  by the  amount of
property that he  or she owns. This has the  consequence that someone
with no property has no guaranteed freedom at all (beyond, of course,
the freedom not to be murdered  or otherwise harmed by the deliberate
acts of  others). In  other words,  a distribution  of property  is a
distribution of freedom, as  the right-libertarians themselves define
it. It strikes anarchists as strange  that an ideology that claims to
be committed  to promoting freedom  entails the conclusion  that some
people should be more free than  others. However, this is the logical
implication of their view, which raises a serious doubt as to whether
“anarcho”-capitalists are actually interested in freedom.</p>

<p>Looking at Rothbard’s definition  of “liberty” quoted above,
we can  see that  freedom is  actually no longer  considered to  be a
fundamental, independent  concept. Instead,  freedom is  a derivative
of   something  more   fundamental,   namely  the   <em>“legitimate
rights”</em>  of an  individual, which  are identified  as property
rights.  In other  words,  given  that “anarcho”-capitalists  and
right  libertarians in  general  consider the  right  to property  as
“absolute,”  it  follows that  freedom  and  property become  one
and  the  same. This  suggests  an  alternative  name for  the  right
Libertarian,   namely   <strong><em>“Propertarian.”</em></strong>
And, needless to  say, if we do not  accept the right-libertarians’
view of  what constitutes  “legitimate” “rights,”  then their
claim to be defenders of liberty is weak.</p>

<p>Another important  implication of  this “liberty  as property”
concept is that it produces a strangely alienated concept of freedom.
Liberty,  as  we noted,  is  no  longer  considered absolute,  but  a
derivative of property  — which has the  important consequence that
you can “sell”  your liberty and still be considered  free by the
ideology. This concept of  liberty (namely “liberty as property”)
is usually termed “self-ownership.” But,  to state the obvious, I
do not “own” myself, as if  were an object somehow separable from
my  subjectivity  —  I  <strong>am</strong>  myself.  However,  the
concept of “self-ownership” is handy for justifying various forms
of domination and  oppression — for by agreeing  (usually under the
force  of  circumstances, we  must  note)  to certain  contracts,  an
individual can  “sell” (or  rent out)  themselves to  others (for
example, when workers  sell their labour power to  capitalists on the
“free market”). In effect, “self-ownership” becomes the means
of justifying  treating people  as objects  — ironically,  the very
thing  the concept  was created  to stop!  As L.  Susan Brown  notes,
<em>“[a]t  the moment  an  individual ‘sells’  labour power  to
another,  he/she  loses  self-determination and  instead  is  treated
as  a  subjectless  instrument  for  the  fulfilment  of  another’s
will.”</em>  [<strong>The  Politics of  Individualism</strong>,  p.
4]</p>

<p>Given that  workers are paid  to obey,  you really have  to wonder
which planet Murray  Rothbard is on when he argues  that a person’s
<em>“labour  service is  alienable,  but his  <strong>will</strong>
is  not”</em>  and  that   he  [sic!]  <em>“cannot  alienate  his
<strong>will</strong>,  more particularly  his control  over his  own
mind and  body.”</em> [<strong>The  Ethics of  Liberty</strong>, p.
40,  p. 135]  He  contrasts private  property  and self-ownership  by
arguing  that <em>“[a]ll  physical property  owned by  a person  is
alienable ...  I can give  away or sell  to another person  my shoes,
my  house,  my car,  my  money,  etc.  But  there are  certain  vital
things  which,  in  natural  fact  and in  the  nature  of  man,  are
<strong>in</strong>alienable  ... [his]  will  and  control over  his
own person  are inalienable.”</em> [<strong>Op.  Cit.</strong>, pp.
134–5]</p>

<p>But   <em>“labour  services”</em>   are  unlike   the  private
possessions  Rothbard  lists as  being  alienable.  As we  argued  in
section B.1  (“Why do anarchists oppose  hierarchy”) a person’s
<em>“labour  services”</em>  and  <em>“will”</em>  cannot  be
divided — if  you sell your labour services, you  also have to give
control of your body and mind to another person! If a worker does not
obey the commands of her employer, she is fired. That Rothbard denies
this indicates  a total lack  of common-sense. Perhaps  Rothbard will
argue that as the  worker can quit at any time  she does not alienate
their will  (this seems to  be his  case against slave  contracts —
see  section  2.6).  But  this  ignores the  fact  that  between  the
signing  and breaking  of the  contract  and during  work hours  (and
perhaps outside  work hours, if  the boss has mandatory  drug testing
or  will fire  workers  who  attend union  or  anarchist meetings  or
those who  have an  “unnatural” sexuality and  so on)  the worker
<strong>does</strong> alienate  his will  and body.  In the  words of
Rudolf Rocker, <em>“under the  realities of the capitalist economic
form  ...  there can  be  no  talk of  a  ‘right  over one’s  own
person,’  for that  ends when  one is  compelled to  submit to  the
economic dictation of another if  he does not want to starve.”</em>
[<strong>Anarcho-Syndicalism</strong>, p. 17]</p>

<p>Ironically, the rights of property (which are said to flow from an
individual’s  self-ownership  of  themselves)  becomes  the  means,
under capitalism,  by which self-ownership of  non-property owners is
denied. The foundational right (self-ownership) becomes denied by the
derivative right (ownership  of things). Under capitalism,  a lack of
property can be just as oppressive  as a lack of legal rights because
of  the relationships  of  domination and  subjection this  situation
creates.</p>

<p>So Rothbard’s  argument (as well as  being contradictory) misses
the point  (and the reality of  capitalism). Yes, <strong>if</strong>
we define freedom as <em>“the  absence of coercion”</em> then the
idea that wage  labour does not restrict liberty  is unavoidable, but
such a definition is useless. This  is because it hides structures of
power  and  relations  of  domination and  subordination.  As  Carole
Pateman  argues, <em>“the  contract in  which the  worker allegedly
sells his  labour power is  a contract in  which, since he  cannot be
separated from his  capacities, he sells command over the  use of his
body  and  himself...  To  sell  command  over  the  use  of  oneself
for  a specified  period ...  is  to be  an unfree  labourer.”</em>
[<strong>The Sexual Contract</strong>, p. 151]</p>

<p>In other words, contracts about  property in the person inevitably
create  subordination. “Anarcho”-capitalism  defines this  source
of  unfreedom away,  but  it  still exists  and  has  a major  impact
on  people’s  liberty. Therefore  freedom  is  better described  as
“self-government”  or “self-management”  —  to  be able  to
govern  ones  own  actions  (if  alone)  or  to  participate  in  the
determination  of join  activity (if  part of  a group).  Freedom, to
put  it another  way,  is  not an  abstract  legal  concept, but  the
vital concrete  possibility for  every human being  to bring  to full
development all  their powers,  capacities, and talents  which nature
has endowed them. A  key aspect of this is to  govern one own actions
when  within associations  (self-management). If  we look  at freedom
this way, we see that coercion  is condemned but so is hierarchy (and
so is  capitalism for during  working hours,  people are not  free to
make their own  plans and have a  say in what affects  them. They are
order takers, <strong>not</strong> free individuals).</p>

<p>It  is  because  anarchists   have  recognised  the  authoritarian
nature  of  capitalist  firms  that they  have  opposed  wage  labour
and  capitalist  property rights  along  with  the state.  They  have
desired  to replace  institutions  structured  by subordination  with
institutions  constituted  by  free relationships  (based,  in  other
words,  on self-management)  in <strong>all</strong>  areas of  life,
including  economic organisations.  Hence Proudhon’s  argument that
the  <em>“workmen’s  associations  ...  are  full  of  hope  both
as  a  protest  against  the  wage  system,  and  as  an  affirmation
of  <strong>reciprocity</strong>”</em>  and that  their  importance
lies  <em>“in  their  denial  of the  rule  of  capitalists,  money
lenders  and governments.”</em>  [<strong>The General  Idea of  the
Revolution</strong>, pp. 98–99]</p>

<p>Unlike anarchists, the “anarcho”-capitalist account of freedom
allows  an  individual’s  freedom  to  be  rented  out  to  another
while  maintaining  that  the  person  is still  free.  It  may  seem
strange that  an ideology  proclaiming its  support for  liberty sees
nothing  wrong with  the alienation  and  denial of  liberty but,  in
actual  fact, it  is unsurprising.  After all,  contract theory  is a
<em>“theoretical strategy  that justifies subjection  by presenting
it as  freedom”</em> and  nothing more.  Little wonder,  then, that
contract <em>“creates  a relation of subordination”</em>  and not
of  freedom [Carole  Pateman,  <strong>Op. Cit.</strong>,  p. 39,  p.
59]</p>

<p>Any  attempt  to build  an  ethical  framework starting  from  the
abstract  individual (as  Rothbard  does  with his  <em>“legitimate
rights”</em>  method)  will  result in  domination  and  oppression
between  people,   <strong>not</strong>  freedom.   Indeed,  Rothbard
provides  an  example of  the  dangers  of idealist  philosophy  that
Bakunin warned  about when he argued  that while <em>“[m]aterialism
denies free will and ends  in the establishment of liberty; idealism,
in the name  of human dignity, proclaims free will,  and on the ruins
of  every  liberty  founds authority.”</em>  [<strong>God  and  the
State</strong>, p. 48] This is the case with “anarcho”-capitalism
can be  seen from Rothbard’s  wholehearted support for  wage labour
and the  rules imposed by  property owners on  those who use,  but do
not  own,  their  property.  Rothbard,  basing  himself  on  abstract
individualism, cannot help but justify authority over liberty.</p>

<p>Overall,  we  can see  that  the  logic of  the  right-libertarian
definition    of    “freedom”    ends   up    negating    itself,
because   it   results  in   the   creation   and  encouragement   of
<strong>authority,</strong> which is  an <strong>opposite</strong> of
freedom.  For example,  as Ayn  Rand  points out,  <em>“man has  to
sustain his life by  his own effort, the man who has  no right to the
product of his effort  has no means to sustain his  life. The man who
produces while  others dispose  of his  product, is  a slave.”</em>
[<strong>The Ayn Rand Lexicon: Objectivism  from A to Z</strong>, pp.
388–9] But, as  was shown in section C, capitalism  is based on, as
Proudhon  put  it, workers  working  <em>“for  an entrepreneur  who
pays  them and  keeps their  products,”</em> and  so is  a form  of
<strong>theft.</strong>  Thus,  by  “libertarian”  capitalism’s
<strong>own</strong> logic,  capitalism is based not  on freedom, but
on (wage) slavery;  for interest, profit and rent are  derived from a
worker’s <strong>unpaid</strong> labour, i.e. <em>“others dispose
of his [sic] product.”</em></p>

<p>And   if  a   society   <strong>is</strong>  run   on  the   wage-
and   profit-based  system   suggested  by   the  “anarcho”   and
“libertarian”  capitalists,  freedom  becomes  a  commodity.  The
more  money  you  have,  the   more  freedom  you  get.  Then,  since
money  is only  available to  those  who earn  it, Libertarianism  is
based  on that  classic  saying <em>“work  makes one  free!”</em>
(<strong><em>Arbeit  macht  frei!</em></strong>),   which  the  Nazis
placed on the gates of their concentration camps. Of course, since it
is  capitalism, this  motto is  somewhat different  for those  at the
top.  In this  case  it  is <em>“other  people’s  work makes  one
free!”</em> — a  truism in any society based  on private property
and the authority that stems from it.</p>

<p>Thus  it   is  debatable  that  a   libertarian  or  “anarcho”
capitalist  society  would have  less  unfreedom  or coercion  in  it
than “actually  existing capitalism.”  In contrast  to anarchism,
“anarcho”-capitalism,  with  its  narrow  definitions,  restricts
freedom to only  a few aspects of social life  and ignores domination
and  authority  beyond  those   aspects.  As  Peter  Marshall  points
out,  the  right-libertarian’s   <em>“definition  of  freedom  is
entirely  negative.  It  calls  for   the  absence  of  coercion  but
cannot  guarantee the  positive  freedom of  individual autonomy  and
independence.”</em> [<strong>Demanding  the Impossible</strong>, p.
564] By  confining freedom to  such a  narrow range of  human action,
“anarcho”-capitalism  is clearly  <strong>not</strong> a  form of
anarchism.  Real anarchists  support freedom  in every  aspect of  an
individual’s life.</p>

<h4 id="toc22">2.1 What  are the implications of  defining liberty in terms of (property) rights?</h4>

<p>The  change   from  defending  liberty  to   defending  (property)
rights has  important implications.  For one  thing, it  allows right
libertarians to imply  that private property is similar  to a “fact
of nature,”  and so  to conclude that  the restrictions  on freedom
produced by it can be ignored.  This can be seen in Robert Nozick’s
argument that decisions  are voluntary if the  limitations on one’s
actions are not  caused by human action which infringe  the rights of
others. Thus, in a “pure”  capitalist society the restrictions on
freedom caused  by wage slavery  are not really  restrictions because
the worker  voluntarily consents  to the contract.  The circumstances
that drive a worker to make  the contract are irrelevant because they
are created by people exercising their rights and not violating other
peoples’ ones (see the section on <em>“Voluntary Exchange”</em>
in <strong>Anarchy, State, and Utopia</strong>, pp. 262–265).</p>

<p>This  means that  within a  society <em>“[w]hether  a person’s
actions are  voluntary depends  on what  limits his  alternatives. If
facts of nature do so, the  actions are voluntary. (I may voluntarily
walk  to  someplace  I  would  prefer  to  fly  to  unaided).”</em>
[<strong>Anarchy, State, and Utopia</strong>,  p. 262] Similarly, the
results of voluntary actions and  the transference of property can be
considered  alongside  the “facts  of  nature”  (they are,  after
all, the  resultants of  “natural rights”).  This means  that the
circumstances created  by the  existence and use  of property  can be
considered, in essence, as a “natural” fact and so the actions we
take in response to these circumstances are therefore “voluntary”
and  we are  “free”  (Nozick  presents the  example  [p. 263]  of
someone  who marries  the  only  available person  —  all the  more
attractive people  having already chosen others  — as a case  of an
action  that  is voluntary  despite  removal  of  all but  the  least
attractive  alternative through  the  legitimate  actions of  others.
Needless  to say,  the example  can  be —  and is  — extended  to
workers on  the labour  market —  although, of  course, you  do not
starve to death if you decide not to marry).</p>

<p>However,  such  an  argument  fails to  notice  that  property  is
different from  gravity or biology. Of  course not being able  to fly
does not  restrict freedom. Neither  does not  being able to  jump 10
feet into the air. But unlike gravity (for example), private property
has  to  be protected  by  laws  and the  police.  No  one stops  you
from  flying,  but  laws  and  police forces  must  exist  to  ensure
that capitalist  property (and  the owners’  authority over  it) is
respected. The  claim, therefore,  that private property  in general,
and  capitalism  in particular,  can  be  considered as  “facts  of
nature,” like gravity,  ignores an important fact:  namely that the
people involved in an economy must  accept the rules of its operation
— rules that,  for example, allow contracts to  be enforced; forbid
using another’s  property without his or  her consent (“theft,”
trespass, copyright  infringement, etc.);  prohibit “conspiracy,”
unlawful assembly, rioting, and so  on; and create monopolies through
regulation,  licensing,  charters,  patents,  etc.  This  means  that
capitalism  has  to include  the  mechanisms  for deterring  property
crimes  as  well  as   mechanisms  for  compensation  and  punishment
should such  crimes be  committed. In other  words, capitalism  is in
fact  far  more  than  “voluntary  bilateral  exchange,”  because
it  <strong>must</strong>  include  the  policing,  arbitration,  and
legislating mechanisms required to  ensure its operation. Hence, like
the state,  the capitalist  market is a  social institution,  and the
distributions of goods  that result from its  operation are therefore
the  distributions sanctioned  by a  capitalist society.  As Benjamin
Franklin pointed  out, <em>“Private property  ... is a  Creature of
Society, and is subject to the Calls of that Society.”</em></p>

<p>Thus, to claim with Sir Isaiah Berlin (the main, modern, source of
the concepts  of <em>“negative”</em>  and <em>“positive”</em>
freedom   —  although   we  must   add  that   Berlin  was   not  a
right-Libertarian),  that  <em>“[i]f  my  poverty were  a  kind  of
disease,  which  prevented  me  from buying  bread  ...  as  lameness
prevents  me from  running,  this inability  would  not naturally  be
described  as a  lack  of freedom”</em>  totally  misses the  point
[<em>“Two Concepts  of Liberty”</em>,  in <strong>Four  Essays on
Liberty</strong>, p.  123]. If you  are lame, police officers  do not
come  round to  stop  you  running. They  do  not  have to.  However,
they <strong>are</strong>  required to  protect property  against the
dispossessed and those who reject capitalist property rights.</p>

<p>This  means   that  by  using  such   concepts  as  “negative”
liberty  and   ignoring  the  social  nature   of  private  property,
right-libertarians  are  trying  to  turn the  discussion  away  from
liberty  toward   “biology”  and  other  facts   of  nature.  And
conveniently, by placing property  rights alongside gravity and other
natural  laws,  they  also  succeed in  reducing  debate  even  about
rights.</p>

<p>Of    course,     coercion    and    restriction     of    liberty
<strong>can</strong>  be resisted,  unlike “natural  forces” like
gravity.  So if,  as Berlin  argues, <em>“negative”</em>  freedom
means  that  you  <em>“lack  political  freedom  only  if  you  are
prevented  from  attaining  a  goal  by  human  beings,”</em>  then
capitalism is indeed based on such a lack, since property rights need
to be enforced by human beings  (<em>“I am prevented by others from
doing what I could otherwise do”</em>). After all, as Proudhon long
ago noted, the market is manmade,  hence any constraint it imposes is
the coercion of man by man and so economic laws are not as inevitable
as natural ones [see Alan Ritter’s <strong>The Political Thought of
Pierre-Joseph  Proudhon</strong>, p.  122].  Or, to  put it  slightly
differently,  capitalism  requires coercion  in  order  to work,  and
hence,  is <strong>not</strong>  similar to  a “fact  of nature,”
regardless  of Nozick’s  claims (i.e.  property rights  have to  be
defined  and enforced  by human  beings, although  the nature  of the
labour market resulting from  capitalist property definitions is such
that  direct coercion  is usually  not needed).  This implication  is
actually recognised  by right-libertarians,  because they  argue that
the rights-framework  of society should be  set up in one  way rather
than  another. In  other words,  they recognise  that society  is not
independent of human interaction, and so can be changed.</p>

<p>Perhaps,   as  seems   the   case,  the   “anarcho”-capitalist
or    right-Libertarian    will    claim     that    it    is    only
<strong>deliberate</strong>  acts  which  violate  your  (libertarian
defined)  rights   by  other  humans  beings   that  cause  unfreedom
(<em>“we   define   freedom   ...   as   the   <strong>absence   of
invasion</strong>   by  another   man   of  an   man’s  person   or
property”</em> [Rothbard,  <strong>The Ethics  of Liberty</strong>,
p. 41]) and so if no-one  deliberately coerces you then you are free.
In  this way  the workings  of the  capitalist market  can be  placed
alongside  the “facts  of  nature”  and ignored  as  a source  of
unfreedom.  However, a  moments thought  shows that  this is  not the
case. Both  deliberate and non-deliberate acts  can leave individuals
lacking freedom.</p>

<p>Let us  assume (in  an example  paraphrased from  Alan Haworth’s
excellent  book  <strong>Anti-Libertarianism</strong>,  p.  49)  that
someone kidnaps  you and  places you down  a deep  (naturally formed)
pit, miles from anyway, which is impossible to climb up. No one would
deny that you  are unfree. Let us further assume  that another person
walks by and accidentally falls into the pit with you.</p>

<p>According  to right-libertarianism,  while  you  are unfree  (i.e.
subject to deliberate coercion)  your fellow pit-dweller is perfectly
free for they have subject to the “facts of nature” and not human
action (deliberate  or otherwise).  Or, perhaps,  they “voluntarily
choose”  to stay  in  the  pit, after  all,  it  is “only”  the
“facts  of  nature”  limiting   their  actions.  But,  obviously,
both  of  you  are  in <strong>exactly  the  same  position,</strong>
have   <strong>exactly  the   same   choices</strong>   and  so   are
<strong>equally</strong> unfree!  Thus a definition  of “liberty”
that maintains that  only deliberate acts of others  — for example,
coercion — reduces freedom misses the point totally.</p>

<p>Why is this example important? Let us consider Murray Rothbard’s
analysis of  the situation after  the abolition of serfdom  in Russia
and slavery in America. He writes:</p>

<blockquote class="citation">
“The  <strong>bodies</strong> of  the oppressed  were freed,
but the property which they had worked and eminently deserved to own,
remained  in the  hands  of their  former  oppressors. With  economic
power  thus remaining  in their  hands, the  former lords  soon found
themselves virtual masters once more of what were now free tenants or
farm  labourers. The  serfs and  slaves had  tasted freedom,  but had
been cruelly  derived of  its fruits.”</em> [<strong>The  Ethics of
Liberty</strong>, p. 74]
</blockquote>

<p>However,  contrast this  with Rothbard’s  claims that  if market
forces  (“voluntary exchanges”)  result in  the creation  of free
tenants  or  labourers then  these  labourers  and tenants  are  free
(see,  for  example,  <strong>The  Ethics  of  Liberty</strong>,  pp.
221–2  on  why  “economic  power” within  capitalism  does  not
exist).  But  the labourers  dispossessed  by  market forces  are  in
<strong>exactly</strong> the  same situation as the  former serfs and
slaves.  Rothbard  sees  the  obvious  <em>“economic  power”</em>
in  the   later  case,  but  denies   it  in  the  former.   But  the
<strong>conditions</strong> of  the people in question  are identical
and  it  is  these  conditions  that  horrify  us.  It  is  only  his
ideology  that  stops Rothbard  drawing  the  obvious conclusion  —
identical conditions produce identical social relationships and so if
the  formally  “free”  ex-serfs are  subject  to  <em>“economic
power”</em>  and <em>“masters”</em>  then so  are the  formally
“free” labourers within  capitalism! Both sets of  workers may be
formally  free,  but  their  circumstances are  such  that  they  are
“free” to  “consent” to  sell their  freedom to  others (i.e.
economic  power produces  relationships of  domination and  unfreedom
between formally free individuals).</p>

<p>Thus Rothbard’s definition  of liberty in terms  of rights fails
to provide us  with a realistic and viable  understanding of freedom.
Someone  can  be  a  virtual  slave while  still  having  her  rights
non-violated  (conversely, someone  can  have  their property  rights
violated and  still be free; for  example, the child who  enters your
backyard without your permission to get her ball hardly violates your
liberty —  indeed, you would never  know that she has  entered your
property unless  you happened  to see  her do it).  So the  idea that
freedom  means non-aggression  against  person  and their  legitimate
material  property  justifies extensive  <strong>non-freedom</strong>
for  the working  class. The  non-violation of  property rights  does
<strong>not</strong> imply freedom, as Rothbard’s discussion of the
former slaves shows. Anyone who, along with Rothbard, defines freedom
<em>“as the <strong>absence of  invasion</strong> by another man of
any man’s person or property”</em> in a deeply inequality society
is supporting, and justifying, capitalist and landlord domination. As
anarchists have long realised, in an unequal society, a contractarian
starting point implies an absolutist conclusion.</p>

<p>Why   is   this?  Simply   because   freedom   is  a   result   of
<strong>social</strong>   interaction,  not   the  product   of  some
isolated, abstract  individual (Rothbard  uses the model  of Robinson
Crusoe   to  construct   his  ideology).   But  as   Bakunin  argued,
<em>“the  freedom  of  the  individual  is a  function  of  men  in
society,  a necessary  consequence of  the collective  development of
mankind.”</em> He goes on to argue that <em>“man in isolation can
have no awareness  of his liberty ... Liberty is  therefore a feature
not of isolation  but of interaction, not of exclusion  but rather of
connection.”</em> [<strong>Selected  Writings</strong>, p.  146, p.
147] Right Libertarians, by building their definition of freedom from
the isolated person, end up by supporting restrictions of liberty due
to a neglect of an adequate recognition of the actual interdependence
of  human beings,  of  the fact  what each  person  does is  effected
by  and  affects  others.  People  become  aware  of  their  humanity
(liberty)  in  society, not  outside  it.  It is  the  <strong>social
relationships</strong> we  take part in  which determine how  free we
are and  any definition  of freedom which  builds upon  an individual
without social ties is doomed  to create relations of domination, not
freedom, between individuals — as  Rothbard’s theory does (to put
it  another  way,  voluntary  association is  a  necessary,  but  not
sufficient,  condition  for freedom.  Which  is  why anarchists  have
always  stressed the  importance of  equality —  see section  3 for
details).</p>

<p>So while facts of nature can restrict your options and freedom, it
is the circumstances within which they act and the options they limit
that are important (a person trapped at the bottom of a pit is unfree
as the options available are so  few; the lame person is free because
their available options are extensive). In the same manner, the facts
of society  can and  do restrict  your freedom  because they  are the
products  of human  action and  are  defined and  protected by  human
institutions, it  is the circumstances within  which individuals make
their decisions and the  social relationships these decisions produce
that are  important (the worker driven  by poverty to accept  a slave
contract in a sweat shop is unfree because the circumstances he faces
have limited his options and the  relations he accepts are based upon
hierarchy; the  person who  decides to join  an anarchist  commune is
free because the  commune is non-hierarchical and she  has the option
of joining another commune, working alone and so forth).</p>

<p>All in all,  the right-Libertarian concept of  freedom is lacking.
For  an  ideology that  takes  the  name “Libertarianism”  it  is
seems  happy to  ignore  actual liberty  and  instead concentrate  on
an  abstract  form  of  liberty  which ignores  so  many  sources  of
unfreedom  as to  narrow the  concept  until it  becomes little  more
than  a justification  for authoritarianism.  This can  be seen  from
right-Libertarian attitudes about private property and its effects on
liberty (as discussed in the next section).</p>

<h4 id="toc23">2.2 How does private property affect freedom?</h4>

<p>The right-libertarian  does not  address or even  acknowledge that
the  (absolute)  right of  private  property  may lead  to  extensive
control  by property  owners  over those  who use,  but  do not  own,
property (such as workers and tenants). Thus a free-market capitalist
system  leads  to a  very  selective  and class-based  protection  of
“rights”  and  “freedoms.”  For  example,  under  capitalism,
the  “freedom”   of  employers  inevitably  conflicts   with  the
“freedom”  of  employees.  When stockholders  or  their  managers
exercise  their  “freedom  of  enterprise” to  decide  how  their
company  will  operate,  they  violate their  employee’s  right  to
decide  how their  labouring capacities  will be  utilised. In  other
words, under capitalism, the  “property rights” of employers will
conflict  with and  restrict the  “human right”  of employees  to
manage  themselves. Capitalism  allows the  right of  self-management
only to the  few, not to all. Or, alternatively,  capitalism does not
recognise  certain human  rights as  <strong>universal</strong> which
anarchism does.</p>

<p>This  can be  seen from  Austrian Economist  W. Duncan  Reekie’s
defence of  wage labour. While referring  to <em>“intra-firm labour
markets”</em>  as <em>“hierarchies”</em>,  Reekie (in  his best
<em>ex  cathedra</em> tone)  states  that  <em>“[t]here is  nothing
authoritarian,  dictatorial  or  exploitative  in  the  relationship.
Employees order employers to pay them amounts specified in the hiring
contract just  as much as employers  order employees to abide  by the
terms  of the  contract.”</em> [<strong>Markets,  Entrepreneurs and
Liberty</strong>, p.  136, p.  137]. Given  that <em>“the  terms of
contract”</em> involve  the worker  agreeing to obey  the employers
orders and that they  will be fired if they do  not, its pretty clear
that  the  ordering that  goes  on  in the  <em>“intra-firm  labour
market”</em> is decidedly <strong>one way</strong>. Bosses have the
power,  workers  are  paid  to  obey. And  this  begs  the  question,
<strong>if</strong> the  employment contract  creates a  free worker,
why must she abandon her liberty during work hours?</p>

<p>Reekie  actually recognises  this lack  of freedom  in a  “round
about” way  when he notes  that <em>“employees  in a firm  at any
level in the hierarchy can exercise an entrepreneurial role. The area
within  which  that  role  can  be carried  out  increases  the  more
authority  the employee  has.”</em> [<strong>Op.  Cit.</strong>, p.
142] Which means workers <strong>are</strong> subject to control from
above  which restricts  the activities  they  are allowed  to do  and
so  they  are  <strong>not</strong>  free  to  act,  make  decisions,
participate in  the plans of  the organisation, to create  the future
and  so forth  within working  hours. And  it is  strange that  while
recognising  the firm  as  a  hierarchy, Reekie  tries  to deny  that
it  is authoritarian  or  dictatorial  — as  if  you  could have  a
hierarchy without authoritarian structures  or an unelected person in
authority who is not a dictator.  His confusion is shared by Austrian
guru  Ludwig  von Mises,  who  asserts  that the  <em>“entrepreneur
and  capitalist  are   not  irresponsible  autocrats”</em>  because
they  are <em>“unconditionally  subject to  the sovereignty  of the
consumer”</em> while, <strong>on  the next page</strong>, admitting
there  is   a  <em>“managerial  hierarchy”</em>   which  contains
<em>“the   average  subordinate   employee.”</em>  [<strong>Human
Action</strong>, p. 809  and p. 810] It does not  enter his mind that
the capitalist  may be subject  to some consumer control  while being
an  autocrat to  their  subordinated employees.  Again,  we find  the
right-“libertarian” acknowledging that  the capitalist managerial
structure is a  hierarchy and workers are  subordinated while denying
it  is autocratic  to the  workers! Thus  we have  “free” workers
within a relationship distinctly <strong>lacking</strong> freedom (in
the sense of self-government) —  a strange paradox. Indeed, if your
personal life  were as  closely monitored and  regulated as  the work
life  of millions  of  people  across the  world,  you would  rightly
consider it oppression.</p>

<p>Perhaps Reekie  (like most right-libertarians) will  maintain that
workers voluntarily agree (“consent”) to be subject to the bosses
dictatorship (he  writes that  <em>“each will  only enter  into the
contractual agreement  known as a  firm if  each believes he  will be
better off  thereby. The firm  is simply another example  of mutually
beneficial  exchange”</em>  [<strong>Op. Cit.</strong>,  p.  137]).
However, this does  not stop the relationship  being authoritarian or
dictatorial  (and so  exploitative as  it is  <strong>highly</strong>
unlikely that those at the top will not abuse their power). And as we
argue further in the next section (and also see sections B.4, 3.1 and
10.2), in a  capitalist society workers have the option  of finding a
job or facing abject poverty and/or starvation.</p>

<p>Little  wonder, then,  that  people  “voluntarily” sell  their
labour  and  “consent”  to authoritarian  structures!  They  have
little option to do otherwise. So, <strong>within</strong> the labour
market, workers <strong>can</strong> and <strong>do</strong> seek out
the best working conditions possible, but that does not mean that the
final contract  agreed is  “freely” accepted and  not due  to the
force of circumstances, that both parties have equal bargaining power
when drawing up  the contract or that the freedom  of both parties is
ensured. Which  means to argue  (as many right-libertarians  do) that
freedom cannot be restricted by wage labour because people enter into
relationships  they consider  will  lead to  improvements over  their
initial situation totally misses the points. As the initial situation
is not considered relevant, their argument fails. After all, agreeing
to  work  in  a  sweatshop  14 hours  a  day  <strong>is</strong>  an
improvement  over  starving  to  death  —  but  it  does  not  mean
that  those who  so agree  are free  when working  there or  actually
<strong>want</strong>  to  be there.  They  are  not  and it  is  the
circumstances, created  and enforced  by the  law, that  have ensured
that  they “consent”  to such  a regime  (given the  chance, they
would  desire to  <strong>change</strong> that  regime but  cannot as
this would  violate their  bosses property rights  and they  would be
repressed for trying).</p>

<p>So the  right-wing “libertarian”  right is interested  only in
a  narrow  concept  of  freedom  (rather  than  in  “freedom”  or
“liberty”  as  such).  This  can  be  seen  in  the  argument  of
Ayn  Rand  (a  leading  ideologue  of  “libertarian”  capitalism)
that <em>“<strong>Freedom</strong>,  in a political  context, means
freedom from  government coercion. It does  <strong>not</strong> mean
freedom from the  landlord, or freedom from the  employer, or freedom
from  the laws  of nature  which do  not provide  men with  automatic
prosperity. It  means freedom  from the coercive  power of  the state
—  and  nothing   else!”</em>  [<strong>Capitalism:  The  Unknown
Ideal</strong>, p.  192] By arguing  in this way,  right libertarians
ignore  the vast  number of  authoritarian social  relationships that
exist in capitalist society and, as  Rand does here, imply that these
social relationships are  like “the laws of  nature.” However, if
one  looks  at  the  world  without prejudice  but  with  an  eye  to
maximising freedom, the major coercive  institution is seen to be not
the  state  but  capitalist  social relationships  (as  indicated  in
section B.4).</p>

<p>The right  “libertarian,” then, far  from being a  defender of
freedom, is in fact a keen defender of certain forms of authority and
domination. As Peter Kropotkin noted, the <em>“modern Individualism
initiated  by  Herbert  Spencer  is,  like  the  critical  theory  of
Proudhon, a  powerful indictment  against the  dangers and  wrongs of
government,  but its  practical  solution of  the  social problem  is
miserable — so  miserable as to lead  us to inquire if  the talk of
‘No  force’  be merely  an  excuse  for supporting  landlord  and
capitalist domination.”</em>  [<strong>Act For Yourselves</strong>,
p. 98]</p>

<p>To  defend  the “freedom”  of  property  owners is  to  defend
authority  and  privilege  —  in   other  words,  statism.  So,  in
considering the concept of liberty as “freedom from,” it is clear
that by  defending private  property (as  opposed to  possession) the
“anarcho”-capitalist  is defending  the  power  and authority  of
property  owners  to  govern  those  who  use  “their”  property.
And  also, we  must  note,  defending all  the  petty tyrannies  that
make  the work  lives of  so many  people frustrating,  stressful and
unrewarding.</p>

<p>However, anarchism,  by definition, is in  favour of organisations
and   social    relationships   which   are    non-hierarchical   and
non-authoritarian. Otherwise, some people  are more free than others.
Failing  to  attack hierarchy  leads  to  massive contradiction.  For
example,  since  the British  Army  is  a  volunteer  one, it  is  an
“anarchist” organisation!  (see next section for  a discussion on
why the  “anarcho”-capitalism concept of freedom  also allows the
state to appear “libertarian”).</p>

<p>In  other  words, “full  capitalist  property  rights” do  not
protect freedom,  in fact  they actively  deny it.  But this  lack of
freedom is only  inevitable if we accept  capitalist private property
rights. If  we reject them,  we can try and  create a world  based on
freedom in all aspects of life, rather than just in a few.</p>

<h4 id="toc24">2.3 Can  “anarcho”-capitalist theory  justify the state?</h4>

<p>Ironically  enough,   “anarcho”-capitalist  ideology  actually
allows the  state to  be justified  along with  capitalist hierarchy.
This is because the reason  why capitalist authority is acceptable to
the “anarcho”-capitalist is because  it is “voluntary” — no
one forces  the worker to  join or  remain within a  specific company
(force  of  circumstances are  irrelevant  in  this viewpoint).  Thus
capitalist domination is  not really domination at all.  But the same
can be  said of all  democratic states as  well. Few such  states bar
exit for its citizens — they are free to leave at any time and join
any other  state that will have  them (exactly as employees  can with
companies). Of course  there <strong>are</strong> differences between
the two  kinds of authority —  anarchists do not deny  that — but
the similarities are all too clear.</p>

<p>The  “anarcho”-capitalist could  argue that  changing jobs  is
easier than changing  states and, sometimes, this is  correct — but
not always. Yes, changing states does  require the moving of home and
possessions over great distances but  so can changing job (indeed, if
a worker  has to  move half-way  across a country  or even  the world
to  get  a  job  “anarcho”-capitalists would  celebrate  this  as
an  example  of the  benefits  of  a “flexible”  labour  market).
Yes, states  often conscript  citizens and  send them  into dangerous
situations but bosses often force their employees to accept dangerous
working environments on pain of  firing. Yes, many states do restrict
freedom of association and speech, but  so do bosses. Yes, states tax
their citizens but landlords and  companies only let others use their
property if they get money in  return (i.e. rent or profits). Indeed,
if the employee  or tenant does not provide the  employer or landlord
with enough profits,  they will quickly be shown the  door. Of course
employees can start their own  companies but citizens can start their
own state if they  convince an existing state (the owner  of a set of
resources)  to sell/give  land to  them.  Setting up  a company  also
requires existing  owners to  sell/give resources  to those  who need
them. Of  course, in  a democratic state  citizens can  influence the
nature of laws and orders they obey. In a capitalist company, this is
not the case.</p>

<p>This means that, logically, “anarcho”-capitalism must consider
a  series of  freely exitable  states  as “anarchist”  and not  a
source of domination. If consent (not leaving) is what is required to
make capitalist domination  not domination then the same  can be said
of statist domination. Stephen L. Newman makes the same point:</p>

<blockquote class="citation">
“The  emphasis   [right-wing]  libertarians  place   on  the
opposition of liberty  and political power tends to  obscure the role
of  authority  in their  worldview  ...  the authority  exercised  in
private  relationships,  however  —  in  the  relationship  between
employer and  employee, for instance  — meets with  no objection...
[This]  reveals  a  curious  insensitivity  to  the  use  of  private
authority as a means of  social control. Comparing public and private
authority, we might  well ask of the  [right-wing] libertarians: When
the  price  of exercising  one’s  freedom  is terribly  high,  what
practical  difference is  there  between the  commands  of the  state
and  those issued  by  one’s employer?  ...  Though admittedly  the
circumstances are  not identical,  telling disgruntled  empowers that
they  are always  free  to leave  their jobs  seems  no different  in
principle from  telling political  dissidents that  they are  free to
emigrate.”</em>  [<strong>Liberalism at  Wit’s End</strong>,  pp.
45–46]
</blockquote>

<p>Murray Rothbard, in his own way, agrees:</p>

<blockquote class="citation">
“<strong>If</strong>  the State  may  be  said too  properly
<strong>own</strong>  its territory,  then  it is  proper  for it  to
make  rules for  everyone  who  presumes to  live  in  that area.  It
can  legitimately seize  or  control private  property because  there
<strong>is</strong>  no  private property  in  its  area, because  it
really owns the entire land  surface. <strong>So long</strong> as the
State  permits its  subjects to  leave  its territory,  then, it  can
be  said  to  act  as  does  any other  owner  who  sets  down  rules
for  people living  on his  property.”</em> [<strong>The  Ethics of
Liberty</strong>, p. 170]
</blockquote>

<p>Rothbard’s  argues that  this is  <strong>not</strong> the  case
simply  because  the  state  did   not  acquire  its  property  in  a
<em>“just”</em> manner and that it claims rights over virgin land
(both  of which  violates Rothbard’s  “homesteading” theory  of
property —  see section 4.1  for details and a  critique). Rothbard
argues that this defence of statism  (the state as property owner) is
unrealistic and ahistoric, but his account of the origins of property
is  equally unrealistic  and ahistoric  and  that does  not stop  him
supporting  capitalism.  People  in  glass houses  should  not  throw
stones!</p>

<p>Thus  he  claims  that  the  state  is  evil  and  its  claims  to
authority/power  false simply  because it  acquired the  resources it
claims to  own <em>“unjustly”</em>  — for example,  by violence
and  coercion  (see  <strong>The   Ethics  of  Liberty</strong>,  pp.
170–1, for Rothbard’s attempt to explain why the state should not
be considered as the owner of land). And even <strong>if</strong> the
state  <strong>was</strong> the  owner  of its  territory, it  cannot
appropriate  virgin  land  (although,  as  he  notes  elsewhere,  the
<em>“vast”</em> US frontier no  longer exists <em>“and there is
no point crying over  the fact”</em> [<strong>Op. Cit.</strong>, p.
240]).</p>

<p>So what  makes hierarchy  legitimate for  Rothbard is  whether the
property it derives from was acquired justly or unjustly. Which leads
us to a few <strong>very</strong> important points.</p>

<p>Firstly,  Rothbard is  explicitly  acknowledging the  similarities
between    statism   and    capitalism.    He    is   arguing    that
<strong>if</strong> the state had  developed in a <em>“just”</em>
way, then it is  perfectly justifiable in governing (<em>“set[ting]
down rules”</em>) those who “consent”  to live on its territory
in <strong>exactly</strong>  the same why  a property owner  does. In
other words,  private property  can be  considered as  a “justly”
created  state!  These  similarities  between  property  and  statism
have  long  been  recognised  by   anarchists  and  that  is  why  we
reject private  property along  with the  state (Proudhon  did, after
all,  note  that <em>“property  is  despotism”</em>  and well  as
<em>“theft”</em>). But, according to Rothbard, something can look
like a state (i.e. be a monopoly of decision making over an area) and
act like a state (i.e. set down rules for people, govern them, impose
a  monopoly of  force) but  not  be a  state.  But if  it looks  like
a  duck  and  sounds  like  a  duck, it  is  a  duck.  Claiming  that
the  origins of  the  thing are  what counts  is  irrelevant —  for
example, a  cloned duck is  just as much a  duck as a  naturally born
one.  A  statist  organisation  is  authoritarian  whether  it  comes
from  <em>“just”</em>  or   <em>“unjust”</em>  origins.  Does
transforming the  ownership of  the land  from states  to capitalists
<strong>really</strong> make  the relations of domination  created by
the  dispossession of  the  many less  authoritarian  and unfree?  Of
course not.</p>

<p>Secondly, much  property in “actually existing”  capitalism is
the  product (directly  or  indirectly) of  state  laws and  violence
(<em>“the  emergence of  both  agrarian  and industrial  capitalism
in  Britain  [and  elsewhere,  we   must  add]  ...  could  not  have
got  off  the   ground  without  resources  to   state  violence  —
legal  or  otherwise”</em>  [Brian  Morris,  <strong>Ecology  &amp;
Anarchism</strong>,  p.  190]).  If  state claims  of  ownership  are
invalid due to  their history, then so are  many others (particularly
those  which  claim  to  own  land).  As  the  initial  creation  was
illegitimate, so are the transactions which have sprung from it. Thus
if state claims  of property rights are invalid, so  are most (if not
all) capitalist claims. If the laws of the state are illegitimate, so
are the rules of the capitalist. If taxation is illegitimate, then so
are rent, interest and profit. Rothbard’s “historical” argument
against the state can also be  applied to private property and if the
one is unjustified, then so is the other.</p>

<p>Thirdly,  <strong>if</strong> the  state had  evolved “justly”
then  Rothbard would  actually  have nothing  against  it! A  strange
position  for  an  anarchist  to  take.  Logically  this  means  that
if  a  system  of  corporate  states evolved  from  the  workings  of
the capitalist  market then  the “anarcho”-capitalist  would have
nothing against  it. This  can be seen  from “anarcho”-capitalist
support  for  company towns  even  though  they have  correctly  been
described as  <em>“industrial feudalism”</em> (see section  6 for
more on this).</p>

<p>Fourthly, Rothbard’s argument implies that similar circumstances
producing  similar  relationships  of domination  and  unfreedom  are
somehow  different if  they  are created  by <em>“just”</em>  and
<em>“unjust”</em>  means.   Rothbard  claims  that   because  the
property is  <em>“justly”</em> acquired it means  the authority a
capitalist over  his employees  is totally different  from that  of a
state  over its  subject. But  such  a claim  is false  — both  the
subject/citizen and  the employee  are in  a similar  relationship of
domination and authoritarianism.  As we argued in section  2.2, how a
person got into  a situation is irrelevant when  considering how free
they  are. Thus,  the person  who  “consents” to  be governed  by
another because  all available  resources are  privately owned  is in
exactly  the same  situation as  a  person who  has to  join a  state
because all  available resources are  owned by one state  or another.
Both are  unfree and  are part  of authoritarian  relationships based
upon domination.</p>

<p>And, lastly,  while “anarcho”-capitalism  may be  a “just”
society, it is definitely <strong>not</strong> a free one. It will be
marked by  extensive hierarchy,  unfreedom and government,  but these
restrictions  of freedom  will be  of a  private nature.  As Rothbard
indicates, the  property owner  and the  state create/share  the same
authoritarian  relationships.  If  statism  is  unfree,  then  so  is
capitalism.  And, we  must  add,  how “just”  is  a system  which
undermines liberty.  Can “justice”  ever be met  in a  society in
which one class has more power and freedom than another. If one party
is in an inferior position, then they have little choice but to agree
to  the disadvantageous  terms  offered by  the  superior party  (see
section  3.1). In  such a  situation,  a “just”  outcome will  be
unlikely as  any contract agreed  will be  skewed to favour  one side
over the other.</p>

<p>The  implications of  these points  are important.  We can  easily
imagine  a  situation  within “anarcho”-capitalism  where  a  few
companies/people start  to buy up  land and form company  regions and
towns.  After  all,  this <strong>has</strong>  happened  continually
throughout capitalism. Thus a “natural” process may develop where
a  few  owners  start  to  accumulate larger  and  larger  tracks  of
land  “justly”.  Such  a  process  does not  need  to  result  in
<strong>one</strong> company  owning the world.  It is likely  that a
few hundred, perhaps a  few thousand, could do so. But  this is not a
cause  for  rejoicing  —  after all  the  current  “market”  in
“unjust” states  also has  a few hundred  competitors in  it. And
even if there is a large  multitude of property owners, the situation
for  the working  class  is exactly  the same  as  the citizen  under
current  statism! Does  the  fact that  it  is “justly”  acquired
property that faces the worker really change the fact she must submit
to the government and rules of another to gain access to the means of
life?</p>

<p>When      faced      with      anarchist      criticisms      that
<strong>circumstances</strong> force  workers to accept  wage slavery
the “anarcho”-capitalist  claims that these are  to be considered
as objective  facts of nature and  so wage labour is  not domination.
However, the same can be said of  states — we are born into a world
where  states claim  to own  all the  available land.  If states  are
replaced by individuals or groups of individuals does this change the
essential nature of our dispossession? Of course not.</p>

<p>Rothbard argues that <em>“[o]bviously,  in a free society, Smith
has the  ultimate decision-making power  over his own  just property,
Jones over his, etc.”</em> [<strong>Op. Cit.</strong>, p. 173] and,
equally  obviously, this  ultimate-decision making  power extends  to
those who  <strong>use,</strong> but do  not own, such  property. But
how “free”  is a  free society  where the  majority have  to sell
their  liberty to  another  in order  to  live? Rothbard  (correctly)
argues  that the  State  <em>“uses  its monopoly  of  force ...  to
control, regulate, and  coerce its hapless subjects.  Often it pushes
its  way into  controlling the  morality and  the very  lives of  its
subjects.”</em>  [<strong>Op.  Cit.</strong>,  p. 171]  However  he
fails  to note  that employers  do exactly  the same  thing to  their
employees. This, from an  anarchist perspective, is unsurprising, for
(after  all)  the  employer <strong>is</strong>  <em>“the  ultimate
decision-making  power over  his  just property”</em>  just as  the
state  is  over its  “unjust”  property.  That similar  forms  of
control and  regulation develop is  not a surprise given  the similar
hierarchical relations in both structures.</p>

<p>That there is  a choice in available states does  not make statism
any less unjust and unfree. Similarly,  just because we have a choice
between  employers does  not  make  wage labour  any  less unjust  or
unfree. But trying to dismiss one  form of domination as flowing from
“just” property while  attacking the other because  it flows from
“unjust” property  is not seeing the  wood for the trees.  If one
reduces liberty, so  does the other. Whether the situation  we are in
resulted from “just”  or “unjust” steps is  irrelevant to the
restrictions of freedom  we face because of them (and  as we argue in
section 2.5, “unjust” situations  can easily flow from “just”
steps).</p>

<p>The “anarcho”-capitalist insistence  that the voluntary nature
of  an association  determines whether  it is  anarchistic is  deeply
flawed —  so flawed in  fact that states and  state-like structures
(such  as  capitalist  firms)   can  be  considered  anarchistic!  In
contrast,  anarchists  think  that  the hierarchical  nature  of  the
associations we join is equally  as important as its voluntary nature
when determining whether  it is anarchistic or  statist. However this
option  is  not  available  to  the  “anarcho”-capitalist  as  it
logically  entails  that  capitalist  companies  are  to  be  opposed
along  with  the  state  as sources  of  domination,  oppression  and
exploitation.</p>

<h4 id="toc25">2.4  But  surely   transactions  on  the  market  are voluntary?</h4>

<p>Of course,  it is usually maintained  by “anarcho”-capitalists
that  no-one puts  a gun  to  a worker’s  head to  join a  specific
company. Yes,  indeed, this  is true  — workers  can apply  for any
job  they like.  But  the  point is  that  the  vast majority  cannot
avoid having  to sell  their liberty  to others  (self-employment and
co-operatives <strong>are</strong>  an option,  but they  account for
less than  10% of the working  population and are unlikely  to spread
due  to the  nature  of  capitalist market  forces  — see  sections
J.5.11  and  J.5.12 for  details).  And  as  Bob Black  pointed  out,
right  libertarians  argue that  <em>“‘one  can  at least  change
jobs.’  but you  can’t  avoid  having a  job  —  just as  under
statism one can  at least change nationalities but  you can’t avoid
subjection to  one nation-state  or another.  But freedom  means more
than the right to change masters.”</em> [<strong>The Libertarian as
Conservative</strong>]</p>

<p>So why do  workers agree to join a  company? Because circumstances
force  them  to  do  so  — circumstances  created,  we  must  note,
by  <strong>human</strong>  actions  and institutions  and  not  some
abstract “fact  of nature.” And  if the world that  humans create
by  their  activity is  detrimental  to  what  we should  value  most
(individual  liberty  and  individuality)  then  we  should  consider
how  to  <strong>change  that  world for  the  better.</strong>  Thus
“circumstances”  (current  “objective  reality”) is  a  valid
source of unfreedom and for human investigation and creative activity
— regardless of the claims of right-Libertarians.</p>

<p>Let us look at the circumstances created by capitalism. Capitalism
is marked  by a class of  dispossessed labourers who have  nothing to
sell by  their labour.  They are  legally barred  from access  to the
means of  life and  so have  little option  but to  take part  in the
labour market. As Alexander Berkman put it:</p>

<blockquote class="citation">
“The law says your employer does not sell anything from you,
because it  is done with  your consent. You  have agreed to  work for
your  boss  for  certain  pay,  he  to  have  all  that  you  produce
...</em></p>

<p><em>“But did you really consent?</em></p>

<p><em>“When the highway  man holds his gun to your  head, you turn
your  valuables  over  to  him.  You  ‘consent’  all  right,  but
you  do  so  because  you  cannot  help  yourself,  because  you  are
<strong>compelled</strong> by his gun.</em></p>

<p><em>“Are  you  not  <strong>compelled</strong> to  work  for  an
employer?  Your   need  compels   you  just  as   the  highwayman’s
gun.  You  must   live...  You  can’t  work   for  yourself  ...The
factories,  machinery,  and  tools  belong to  the  employing  class,
so  you <strong>must</strong>  hire  yourself out  to  that class  in
order  to  work  and  live.   Whatever  you  work  at,  whoever  your
employer  may  be,  it  is  always   comes  to  the  same:  you  must
work  <strong>for  him</strong>.  You   can’t  help  yourself.  You
are  <strong>compelled</strong>.”</em>  [<strong>What is  Communist
Anarchism?</strong>, p. 9]
</blockquote>

<p>Due  to  this class  monopoly  over  the  means of  life,  workers
(usually)  are at  a disadvantage  in terms  of bargaining  power —
there are  more workers than  jobs (see  sections B.4.3 and  10.2 for
a  discussion  why  this  is  the  normal  situation  on  the  labour
market).</p>

<p>As  was  indicated in  section  B.4  (How does  capitalism  affect
liberty?),   within  capitalism   there   is   no  equality   between
owners  and  the  dispossessed,  and  so  property  is  a  source  of
<strong>power.</strong>   To  claim   that  this   power  should   be
“left  alone” or  is  “fair” is  <em>“to the  anarchists...
preposterous.  Once a  State has  been established,  and most  of the
country’s capital  privatised, the threat  of physical force  is no
longer necessary to coerce workers into accepting jobs, even with low
pay  and  poor  conditions.  To use  Ayn  Rand’s  term,  ‘initial
force’ has  <strong>already taken place,</strong> by  those who now
have capital against  those who do not... In other  words, if a thief
died and willed his ‘ill-gotten  gain’ to his children, would the
children have  a right  to the  stolen property?  Not legally.  So if
‘property is theft,’  to borrow Proudhon’s quip,  and the fruit
of  exploited labour  is simply  legal  theft, then  the only  factor
giving the children  of a deceased capitalist a right  to inherit the
‘booty’  is  the law,  the  State.  As Bakunin  wrote,  ‘Ghosts
should  not  rule and  oppress  this  world,  which belongs  only  to
the  living’”</em> [Jeff  Draughn, <strong>Between  Anarchism and
Libertarianism</strong>].</p>

<p>Or, in other words,  right-Libertarianism fails to <em>“meet the
charge that normal operations of  the market systematically places an
entire class of  persons (wage earners) in  circumstances that compel
them to accept  the terms and conditions of labour  dictated by those
who offer work.  While it is true that individuals  are formally free
to seek better jobs or withhold their labour in the hope of receiving
higher wages, in  the end their position in the  market works against
them;  they  cannot  live  if  they  do  not  find  employment.  When
circumstances regularly  bestow a relative disadvantage  on one class
of  persons in  their dealings  with  another class,  members of  the
advantaged class  have little need  of coercive measures to  get what
they want.”</em> [Stephen L.  Newman, <strong>Liberalism at Wit’s
End</strong>, p. 130]</p>

<p>To ignore  the circumstances  which drive people  to seek  out the
most  “beneficial exchange”  is to  blind yourself  to the  power
relationships  inherent  within  capitalism —  power  relationships
created by the unequal bargaining power of the parties involved (also
see section  3.1). And  to argue  that “consent”  ensures freedom
is  false; if  you  are  “consenting” to  be  join a  dictatorial
organisation, you “consent” <strong>not</strong>  to be free (and
to  paraphrase Rousseau,  a  person who  renounces freedom  renounces
being human).</p>

<p>Which  is why  circumstances are  important —  if someone  truly
wants to  join an authoritarian  organisation, then  so be it.  It is
their  life. But  if  circumstances ensure  their “consent”  then
they  are not  free. The  danger is,  of course,  that people  become
<strong>accustomed</strong> to authoritarian relationships and end up
viewing them  as forms of freedom.  This can be seen  from the state,
which the vast  majority support and “consent” to.  And this also
applies  to  wage  labour,  which  many workers  today  accept  as  a
“necessary evil” (like the state)  but, as we indicate in section
8.6, the first wave of workers viewed with horror as a form of (wage)
slavery and did all that they  could to avoid. In such situations all
we can do is argue with them  and convince them that certain forms of
organisations (such  as the state  and capitalist firms) are  an evil
and urge them to change society to ensure their extinction.</p>

<p>So  due  to  this  lack  of  appreciation  of  circumstances  (and
the  fact   that  people  become   accustomed  to  certain   ways  of
life)  “anarcho”-capitalism  actively  supports  structures  that
restrict freedom  for the  many. And how  is “anarcho”-capitalism
<strong>anarchist</strong>   if   it  generates   extensive   amounts
of   archy?   It   is   for   this   reason   that   all   anarchists
support  self-management   within  free  association  —   that  way
we   maximise  freedom   both  inside   <strong>and</strong>  outside
organisations.  But  only  stressing freedom  outside  organisations,
“anarcho”-capitalism  ends  up  denying freedom  as  such  (after
all,   we   spend  most   of   our   waking   hours  at   work).   If
“anarcho”-capitalists  <strong>really</strong>  desired  freedom,
they  would  reject capitalism  and  become  anarchists —  only  in
a  libertarian  socialist  society   would  agreements  to  become  a
wage  worker be  truly  voluntary  as they  would  not  be driven  by
circumstances to sell their liberty.</p>

<p>This  means  that  while   right-Libertarianism  appears  to  make
“choice” an ideal (which sounds good, liberating and positive) in
practice it has become a  “dismal politics,” a politics of choice
where most  of the choices  are bad. And,  to state the  obvious, the
choices we  are “free” to make  are shaped by the  differences in
wealth and power in society (see  section 3.1) as well as such things
as “isolation paradoxes” (see section B.6) and the laws and other
human institutions that exist. If  we ignore the context within which
people make their  choices then we glorify abstract  processes at the
expense of real people. And, as importantly, we must add that many of
the  choices we  make under  capitalism (shaped  as they  are by  the
circumstances  within  which  they  are  made),  such  as  employment
contracts, result in our “choice” being narrowed to “love it or
leave it” in the organisations we  create/join as a result of these
“free” choices.</p>

<p>This     ideological     blind     spot     flows     from     the
“anarcho”-capitalist  definition of  “freedom” as  “absence
of  coercion” —  as  workers “freely  consent”  to joining  a
specific  workplace, their  freedom  is unrestricted.  But to  defend
<strong>only</strong>  “freedom  from”  in a  capitalist  society
means  to defend  the  power and  authority of  the  few against  the
attempts of  the many to claim  their freedom and rights.  To requote
Emma  Goldman, <em>“‘Rugged  individualism’ has  meant all  the
‘individualism’ for  the masters  ... ,  in whose  name political
tyranny and social oppression are  defended and held up as virtues’
while every  aspiration and  attempt of  man to  gain freedom  ... is
denounced as ... evil in the name of that same individualism.”</em>
[<strong>Red Emma Speaks</strong>, p. 112]</p>

<p>In   other   words,   its   all   fine   and   well   saying   (as
right-libertarians  do) that  you  aim to  abolish  force from  human
relationships but  if you  support an  economic system  which creates
hierarchy (and  so domination and  oppression) by its  very workings,
“defensive” force will always be required to maintain and enforce
that  domination. Moreover,  if one  class has  extensive power  over
another due  to the systematic  (and normal) workings of  the market,
any force used to defend that power is <strong>automatically</strong>
“defensive”. Thus  to argue against  the use of force  and ignore
the power relationships that exist within and shape a society (and so
also  shape the  individuals  within  it) is  to  defend and  justify
capitalist  and  landlord domination  and  denounce  any attempts  to
resist that domination as “initiation of force.”</p>

<p>Anarchists,  in contrast,  oppose <strong>hierarchy</strong>  (and
so  domination   within  relationships   —  bar   S&amp;M  personal
relationships, which  are a totally different  thing altogether; they
are truly  voluntary and they also  do not attempt to  hide the power
relationships involved  by using  economic jargon).  This opposition,
while also  including opposition to  the use of force  against equals
(for example, anarchists are opposed  to forcing workers and peasants
to join a  self-managed commune or syndicate),  also includes support
for  the attempts  of  those subject  to domination  to  end it  (for
example, workers striking for union recognition are not “initiating
force”, they are fighting for their freedom).</p>

<p>In  other words,  apparently  “voluntary”  agreements can  and
do  limit  freedom  and  so   the  circumstances  that  drive  people
into   them  <strong>must</strong>   be   considered  when   deciding
whether  any such  limitation  is valid.  By ignoring  circumstances,
“anarcho”-capitalism  ends  up  by  failing to  deliver  what  it
promises — a  society of free individuals —  and instead presents
us with  a society  of masters  and servants.  The question  is, what
do  we feel  moved  to  insist that  people  enjoy? Formal,  abstract
(bourgeois)  self-ownership  (“freedom”)  or a  more  substantive
control over one’s life (i.e. autonomy)?</p>

<h4 id="toc26">2.5 But surely circumstances are the result of liberty and so cannot be objected to?</h4>

<p>It is often argued by right-libertarians that the circumstances we
face within capitalism  are the result of  individual decisions (i.e.
individual liberty) and so we must  accept them as the expressions of
these acts (the most famous example of this argument is in Nozick’s
<strong>Anarchy, State,  and Utopia</strong>  pp. 161–163  where he
maintains  that  <em>“liberty  upsets  patterns”</em>).  This  is
because  whatever situation  evolves from  a just  situation by  just
(i.e. non-coercive steps) is also (by definition) just.</p>

<p>However,  it is  not apparent  that adding  just steps  to a  just
situation  will result  in a  just society.  We will  illustrate with
a  couple  of  banal  examples.   If  you  add  chemicals  which  are
non-combustible together you can  create a new, combustible, chemical
(i.e. X becomes not-X by adding new  X to it). Similarly, if you have
an  odd  number  and  add  another  odd  number  to  it,  it  becomes
even  (again, X  becomes  not-X by  adding  a  new X  to  it). So  it
<strong>is</strong>  very possible  to go  from an  just state  to an
unjust  state by  just  step (and  it  is possible  to  remain in  an
unjust  state by  just acts;  for example  if we  tried to  implement
“anarcho”-capitalism  on the  existing —  unjustly created  —
situation  of “actually  existing”  capitalism it  would be  like
having an odd number and adding  even numbers to it). In other words,
the  outcome  of  “just”  steps can  increase  inequality  within
society and  so ensure  that some acquire  an unacceptable  amount of
power  over  others,  via  their  control  over  resources.  Such  an
inequality of power would create  an “unjust” situation where the
major are free  to sell their liberty to others  due to inequality in
power and resources on the “free” market.</p>

<p>Ignoring    this   objection,    we   could    argue   (as    many
“anarcho”-capitalists   and  right-libertarians   do)  that   the
unforeseen results  of human  action are fine  unless we  assume that
these  human actions  are  in themselves  bad  (i.e. that  individual
choice is evil).</p>

<p>Such an argument is false for three reasons.</p>

<p>First,  when we  make our  choices the  aggregate impact  of these
choices are  unknown to  us —  and not  on offer  when we  make our
choices.  Thus we  cannot  be said  to  “choose” these  outcomes,
outcomes which  we may consider  deeply undesirable, and so  the fact
that these outcomes  are the result of individual  choices is besides
the point (if we knew the  outcome we could refrain from doing them).
The choices themselves, therefore, do not validate the outcome as the
outcome was  not part of the  choices when they where  made (i.e. the
means do  not justify the ends).  In other words, private  acts often
have  important public  consequences  (and “bilateral  exchanges”
often  involve externalities  for  third parties).  Secondly, if  the
outcome  of individual  choices  is to  deny  or restrict  individual
choice on a wider scale at a  later stage, then we are hardly arguing
that individual choice is a bad thing.  We want to arrange it so that
the  decisions we  make now  do not  result in  them restricting  our
ability to make choices in important areas of life at a latter stage.
Which means  we are in favour  of individual choices and  so liberty,
not against  them. Thirdly,  the unforeseen  or unplanned  results of
individual actions are not necessarily a good thing. If the aggregate
outcome of individual choices harms  individuals then we have a right
to modify the circumstances within  which choices are made and/or the
aggregate results of these choices.</p>

<p>An example  will show what  we mean (again drawn  from Haworth’s
excellent <strong>Anti-Libertarianism</strong>,  p. 35).  Millions of
people  across the  world bought  deodorants which  caused a  hole to
occur  in  the  ozone  layer surrounding  the  Earth.  The  resultant
of  these acts  created  a  situation in  which  individuals and  the
eco-system  they inhabited  were  in great  danger.  The actual  acts
themselves were  by no means wrong,  but the aggregate impact  was. A
similar argument can apply to any  form of pollution. Now, unless the
right-Libertarian argues that skin cancer or other forms of pollution
related illness are fine, its  clear that the resultant of individual
acts can be harmful to individuals.</p>

<p>The   right-Libertarian  could   argue   that   pollution  is   an
“initiation of  force” against an  individual’s property-rights
in  their  person and  so  individuals  can  sue the  polluters.  But
hierarchy also harms the individual (see  section B.1) — and so can
be  considered  as  an infringement  of  their  “property-rights”
(i.e.  liberty,  to get  away  from  the  insane property  fetish  of
right-Libertarianism). The loss of autonomy can be just as harmful to
an individual as lung cancer although very different in form. And the
differences in wealth resulting from  hierarchy is well known to have
serious impacts on life-span and health.</p>

<p>As  noted in  section  2.1,  the market  is  just  as man-made  as
pollution. This means that the “circumstances” we face are due to
aggregate of millions of individual  acts and these acts occur within
a specific  framework of rights, institutions  and ethics. Anarchists
think that a transformation of our  society and its rights and ideals
is required so that the resultant of individual choices does not have
the ironic  effect of  limiting individual  choice (freedom)  in many
important ways (such as in work, for example).</p>

<p>In   other  words,   the  <strong>circumstances</strong>   created
by     capitalist    rights     and    institutions     requires    a
<strong>transformation</strong> of  these rights and  institutions in
such a  way as to maximise  individual choice for all  — namely, to
abolish these  rights and  replace them with  new ones  (for example,
replace property rights with use rights). Thus Nozick’s claims that
<em>“Z does choose voluntarily if the other individuals A through Y
each acted  voluntarily and within their  rights”</em> [<strong>Op.
Cit.</strong>, p. 263]  misses the point — it is  these rights that
are  in question  (given that  Nozick <strong>assumes</strong>  these
rights then his whole thesis is begging the question).</p>

<p>And we  must add (before anyone  points it out) that,  yes, we are
aware that many  decisions will unavoidably limit  current and future
choices. For example, the decision to build a factory on a green-belt
area will  make it impossible  for people  to walk through  the woods
that are no longer there. But  such “limitations” (if they can be
called  that) of  choice are  different from  the limitations  we are
highlighting  here,  namely  the  lose of  freedom  that  accompanies
the  circumstances created  via  exchange in  the  market. The  human
actions which  build the factory  modify reality but do  not generate
social  relationships  of  domination  between people  in  so  doing.
The  human  actions  of  market exchange,  in  contrast,  modify  the
relative  strengths of  everyone in  society  and so  has a  distinct
impact  on the  social  relationships we  “voluntarily” agree  to
create.  Or, to  put it  another way,  the decision  to build  on the
green-belt site does  “limit” choice in the abstract  but it does
<strong>not</strong>  limit choice  in the  kind of  relationships we
form with other people nor create authoritarian relationships between
people due to inequality influencing  the content of the associations
we  form.  However,  the  profits produced  from  using  the  factory
increases inequality  (and so  market/economic power) and  so weakens
the position of the working class  in respect to the capitalist class
within society.  This increased inequality  will be reflected  in the
“free” contracts and  working regimes that are  created, with the
weaker “trader” having to compromise far more than before.</p>

<p>So, to try and defend wage slavery and other forms of hierarchy by
arguing that  “circumstances” are  created by  individual liberty
runs  aground on  its  own  logic. If  the  circumstances created  by
individual liberty  results in  pollution then  the right-Libertarian
will  be  the first  to  seek  to  change those  circumstances.  They
recognise that the  right to pollute while producing  is secondary to
our right to  be healthy. Similarly, if the  circumstances created by
individual liberty  results in hierarchy  (pollution of the  mind and
our relationships  with others  as opposed to  the body,  although it
affects that to)  then we are entitled to  change these circumstances
too and the means by which we get there (namely the institutional and
rights framework of society). Our  right to liberty is more important
than the rights of property  — sadly, the right-Libertarian refuses
to recognise this.</p>

<h4 id="toc27">2.6 Do Libertarian-capitalists support slavery?</h4>

<p>Yes.   It  may   come  as   a   surprise  to   many  people,   but
right-Libertarianism  is  one  of  the few  political  theories  that
justifies   slavery.  For   example,  Robert   Nozick  asks   whether
<em>“a free  system would  allow [the  individual] to  sell himself
into  slavery”</em>  and  he   answers  <em>“I  believe  that  it
would.”</em> [<strong>Anarchy,  State and Utopia</strong>,  p. 371]
While some right-Libertarians  do not agree with Nozick,  there is no
logical basis in their ideology for such disagreement.</p>

<p>The  logic  is simple,  you  cannot  really own  something  unless
you  can  sell it.  Self-ownership  is  one  of the  cornerstones  of
laissez-faire capitalist ideology. Therefore,  since you own yourself
you can sell yourself.</p>

<p>(For  Murray Rothbard’s  claims of  the <em>“unenforceability,
in  libertarian  theory,  of voluntary  slave  contracts”</em>  see
<strong>The Ethics of Liberty</strong>,  pp. 134–135 — of course,
<strong>other</strong> libertarian theorists claim the exact opposite
so  <em>“libertarian  theory”</em>  makes  no  such  claims,  but
nevermind! Essentially, his point  revolves around the assertion that
a person <em>“cannot, in nature, sell himself into slavery and have
this sale enforced — for this  would mean that his future will over
his own  body was  being surrendered in  advance”</em> and  that if
a  <em>“labourer  remains  totally subservient  to  his  master’s
will  voluntarily, he  is not  yet a  slave since  his submission  is
voluntary.”</em>  [p.  40]  However,  as we  noted  in  section  2,
Rothbard emphasis on  quitting fails to recognise  that actual denial
of  will and  control over  ones own  body that  is explicit  in wage
labour. It is this failure that pro-slave contract “libertarians”
stress —  as we will  see, they consider  the slave contract  as an
extended  wage  contract. Moreover,  a  modern  slave contract  would
likely take the form of a <em>“performance bond”</em> [p. 136] in
which the slave agrees to perform  X years labour or pay their master
substantial damages. The threat of damages that enforces the contract
and  such a  “contract” Rothbard  does agree  is enforceable  —
along with  <em>“conditional exchange”</em> [p. 141]  which could
be another way of creating slave contracts.)</p>

<p>Nozick’s defence of slavery should not come as a surprise to any
one familiar with classical liberalism. An elitist ideology, its main
rationale is to  defend the liberty and power of  property owners and
justify  unfree social  relationships  (such as  government and  wage
labour)  in terms  of  “consent.”  Nozick just  takes  it to  its
logical conclusion, a conclusion which Rothbard, while balking at the
label used, does not actually disagree with.</p>

<p>This is  because Nozick’s argument  is not  new but, as  with so
many others, can be found in  John Locke’s work. The key difference
is that  Locke refused  the term <em>“slavery”</em>  and favoured
<em>“drudgery”</em>  as, for  him,  slavery  mean a  relationship
<em>“between  a lawful  conqueror and  a captive”</em>  where the
former  has the  power of  life  and death  over the  latter. Once  a
<em>“compact”</em>  is agreed  between them,  <em>“an agreement
for a limited power  on the one side, and obedience  on the other ...
slavery  ceases.”</em>  As  long  as  the  master  could  not  kill
the  slave,  then it  was  <em>“drudgery.”</em>  Like Nozick,  he
acknowledges that <em>“men  did sell themselves; but,  it is plain,
this  was only  to  drudgery, not  to slavery:  for,  it is  evident,
the  person sold  was not  under an  absolute, arbitrary,  despotical
power:  for the  master could  not  have power  to kill  him, at  any
time,  whom,  at a  certain  time,  he was  obliged  to  let go  free
out  of  his  service.”</em>  [Locke,  <strong>Second  Treatise  of
Government</strong>,  Section  24]  In other  words,  like  Rothbard,
voluntary slavery was fine but just call it something else.</p>

<p>Not that Locke was bothered by involuntary slavery. He was heavily
involved in the  slave trade. He owned shares in  the “Royal Africa
Company” which  carried on  the slave trade  for England,  making a
profit when he sold them. He also held a significant share in another
slave  company, the  “Bahama Adventurers.”  In the  <em>“Second
Treatise”</em>, Locke justified slavery in terms of <em>“Captives
taken in a just war.”</em> [Section 85] In other words, a war waged
against  aggressors. That,  of course,  had  nothing to  do with  the
<strong>actual</strong> slavery Locke profited from (slave raids were
common, for example).  Nor did his “liberal”  principles stop him
suggesting a constitution that would ensure that <em>“every freeman
of Carolina  shall have absolute  power and authority over  his Negro
slaves.”</em> The constitution itself  was typically autocratic and
hierarchical, designed explicitly to <em>“avoid erecting a numerous
democracy.”</em> [<strong>The Works of John Locke</strong>, vol. X,
p. 196]</p>

<p>So the  notion of  contractual slavery has  a long  history within
right-wing liberalism, although most refuse  to call it by that name.
It is  of course simply  embarrassment that stops Rothbard  calling a
spade  a  spade.  He  incorrectly  assumes that  slavery  has  to  be
involuntary. In  fact, historically,  voluntary slave  contracts have
been  common (David  Ellerman’s  <strong>Property  and Contract  in
Economics</strong>  has  an  excellent  overview). Any  new  form  of
voluntary  slavery would  be a  “civilised” form  of slavery  and
could occur when  an individual would “agree”  to sell themselves
to themselves to another (as when a starving worker would “agree”
to become  a slave  in return  for food).  In addition,  the contract
would  be able  to be  broken  under certain  conditions (perhaps  in
return  for  breaking  the  contract, the  former  slave  would  have
pay  damages  to his  or  her  master  for  the labour  their  master
would  lose  — a  sizeable  amount  no  doubt  and such  a  payment
could  result in  debt  slavery, which  is the  most  common form  of
“civilised” slavery. Such  damages may be agreed  in the contract
as a “performance bond” or “conditional exchange”).</p>

<p>In summary,  right-Libertarians are talking  about “civilised”
slavery (or, in  other words, civil slavery) and  not forced slavery.
While some may have reservations about calling it slavery, they agree
with the basic concept that since people own themselves they can sell
themselves as well as selling their labour for a lifetime.</p>

<p>We must  stress that this  is no academic  debate. “Voluntary”
slavery has been a problem in many societies and still exists in many
countries today  (particularly third  world ones where  bonded labour
— i.e. where debt is used to  enslave people — is the most common
form).  With  the rise  of  sweat  shops  and  child labour  in  many
“developed” countries  such as  the USA,  “voluntary” slavery
(perhaps  via  debt and  bonded  labour)  may  become common  in  all
parts  of the  world  — an  ironic (if  not  surprising) result  of
“freeing” the market and being  indifferent to the actual freedom
of those within it.</p>

<p>And it  is interesting to  note that  even Murray Rothbard  is not
against  the selling  of  humans.  He argued  that  children are  the
property of their  parents. They can (bar actually  murdering them by
violence) do  whatever they  please with  them, even  sell them  on a
<em>“flourishing free child  market.”</em> [<strong>The Ethics of
Liberty</strong>, p. 102]  Combined with a whole  hearted support for
child  labour (after  all,  the child  can leave  its  parents if  it
objects to  working for  them) such a  “free child  market” could
easily  become  a “child  slave  market”  — with  entrepreneurs
making a  healthy profit selling  infants to other  entrepreneurs who
could  make  profits  from  the toil  of  “their”  children  (and
such  a  process  did  occur  in  19<sup>th</sup>  century  Britain).
Unsurprisingly, Rothbard ignores the possible nasty aspects of such a
market  in human  flesh  (such  as children  being  sold  to work  in
factories, homes and  brothels). And, of course, such  a market could
see women “specialising” in producing children for it (the use of
child  labour  during  the  Industrial Revolution  actually  made  it
economically  sensible  for  families  to have  more  children)  and,
perhaps,  gluts  and scarcities  of  babies  due to  changing  market
conditions. But this is besides the point.</p>

<p>Of course, this theoretical justification for slavery at the heart
of an ideology  calling itself “libertarianism” is  hard for many
right-Libertarians  to accept.  Some of  the “anarcho”-capitalist
type argue that such contracts would be very hard to enforce in their
system of  capitalism. This attempt  to get out of  the contradiction
fails simply because it ignores  the nature of the capitalist market.
If  there is  a  demand  for slave  contracts  to  be enforced,  then
companies will develop to provide that “service” (and it would be
interesting  to see  how  two “protection”  firms, one  defending
slave  contracts  and  another  not, could  compromise  and  reach  a
peaceful  agreement over  whether slave  contracts were  valid). Thus
we  could  see  a  so-called “anarchist”  or  “free”  society
producing companies whose  specific purpose was to  hunt down escaped
slaves  (i.e.  individuals  in  slave contracts  who  have  not  paid
damages to  their owners  for freedom).  Of course,  perhaps Rothbard
would  claim  that  such  slave  contracts  would  be  “outlawed”
under his  “general libertarian  law code” but  this is  a denial
of  market  “freedom”.  If slave  contracts  <strong>are</strong>
“banned” then  surely this  is paternalism,  stopping individuals
from contracting out their “labour  services” to whom and however
long they “desire”. You cannot have it both ways.</p>

<p>So,  ironically,   an  ideology  proclaiming  itself   to  support
“liberty” ends  up justifying and defending  slavery. Indeed, for
the  right-libertarian  the  slave contract  is  an  exemplification,
not  the  denial,   of  the  individual’s  liberty!   How  is  this
possible? How can  slavery be supported as an  expression of liberty?
Simple,  right-Libertarian support  for  slavery is  a  symptom of  a
<strong>deeper</strong>  authoritarianism,  namely  their  uncritical
acceptance of contract  theory. The central claim  of contract theory
is  that contract  is  the  means to  secure  and enhance  individual
freedom.  Slavery is  the antithesis  to freedom  and so,  in theory,
contract  and  slavery  must   be  mutually  exclusive.  However,  as
indicated  above, some  contract  theorists (past  and present)  have
included slave  contracts among  legitimate contracts.  This suggests
that contract theory cannot provide the theoretical support needed to
secure and enhance individual freedom. Why is this?</p>

<p>As  Carole Pateman  argues,  <em>“contract  theory is  primarily
about   a   way  of   creating   social   relations  constituted   by
subordination,  not about  exchange.”</em> Rather  than undermining
subordination,  contract  theorists  justify  modern  subjection  —
<em>“contract doctrine  has proclaimed that subjection  to a master
— a  boss, a  husband —  is freedom.”</em>  [<strong>The Sexual
Contract</strong>, p. 40 and p. 146] The question central to contract
theory (and so right-Libertarianism) is not “are people free” (as
one would expect) but “are people free to subordinate themselves in
any manner  they please.”  A radically  different question  and one
only fitting to someone who does not know what liberty means.</p>

<p>Anarchists argue that not all contracts are legitimate and no free
individual can make a contract that denies his or her own freedom. If
an individual is able to express themselves by making free agreements
then those free agreements must also be based upon freedom internally
as well. Any  agreement that creates domination  or hierarchy negates
the assumptions  underlying the agreement  and makes itself  null and
void. In  other words, voluntary  government is still  government and
the defining  chararacteristic of an  anarchy must be,  surely, “no
government” and “no rulers.”</p>

<p>This  is  most easily  seen  in  the  extreme  case of  the  slave
contract.  John Stuart  Mill stated  that  such a  contract would  be
“null and  void.” He  argued that  an individual  may voluntarily
choose to enter  such a contract but in so  doing <em>“he abdicates
his liberty; he foregoes any future use of it beyond that single act.
He therefore defeats, in his own  case, the very purpose which is the
justification of  allowing him to dispose  of himself...The principle
of freedom cannot require  that he should be free not  to be free. It
is  not  freedom, to  be  allowed  to alienate  his  freedom.”</em>
He  adds  that  <em>“these  reasons,  the  force  of  which  is  so
conspicuous  in this  particular  case, are  evidently  of far  wider
application.”</em> [quoted  by Pateman,  <strong>Op. Cit.</strong>,
pp. 171–2]</p>

<p>And it  is such an  application that defenders of  capitalism fear
(Mill did in fact apply these reasons wider and unsurprisingly became
a supporter of a market syndicalist  form of socialism). If we reject
slave contracts as illegitimate then,  logically, we must also reject
<strong>all</strong>  contracts  that  express qualities  similar  to
slavery (i.e.  deny freedom) including  wage slavery. Given  that, as
David Ellerman  points out,  <em>“the voluntary  slave ...  and the
employee  cannot in  fact take  their will  out of  their intentional
actions  so  that they  could  be  ‘employed’  by the  master  or
employer”</em>  we  are  left with  <em>“the  rather  implausible
assertion that a  person can vacate his  or her will for  eight or so
hours a  day for  weeks, months,  or years  on end  but cannot  do so
for  a working  lifetime.”</em> [<strong>Property  and Contract  in
Economics</strong>, p. 58]</p>

<p>The  implications   of  supporting  voluntary  slavery   is  quite
devastating for  all forms  of right-wing  “libertarianism.” This
was  proven by  Ellerman when  he wrote  an extremely  robust defence
of  it  under  the  pseudonym “J.  Philmore”  called  <strong>The
Libertarian Case for Slavery</strong> (first published in <strong>The
Philosophical  Forum</strong>,  xiv,  1982).  This  classic  rebuttal
takes the  form of “proof by  contradiction” (or <strong>reductio
ad   absurdum</strong>)   whereby   he   takes   the   arguments   of
right-libertarianism to  their logical end  and shows how  they reach
the memorably  conclusion that the  <em>“time has come  for liberal
economic and  political thinkers  to stop dodging  this issue  and to
critically re-examine their shared prejudices about certain voluntary
social institutions  ... this critical process  will inexorably drive
liberalism  to  its  only  logical  conclusion:  libertarianism  that
finally lays  the true  moral foundation  for economic  and political
slavery.”</em></p>

<p>Ellerman  shows how,  from  a right-“libertarian”  perspective
there  is  a  <em>“fundamental contradiction”</em>  in  a  modern
liberal society for  the state to prohibit slave  contracts. He notes
that there <em>“seems to be  a basic shared prejudice of liberalism
that slavery  is inherently  involuntary, so  the issue  of genuinely
voluntary slavery  has received little scrutiny.  The perfectly valid
liberal  argument that  involuntary slavery  is inherently  unjust is
thus taken to include voluntary slavery (in which case, the argument,
by definition, does not apply). This has resulted in an abridgment of
the freedom  of contract in  modern liberal society.”</em>  Thus it
is  possible to  argue  for a  <em>“civilised  form of  contractual
slavery.”</em> [“J. Philmore,”, <strong>Op. Cit.</strong>]</p>

<p>So  accurate and  logical was  Ellerman’s article  that many  of
its  readers  were convinced  it  <strong>was</strong>  written by  a
right-libertarian (including, we  have to say, us!).  One such writer
was Carole Pateman, who correctly noted that <em>“[t]here is a nice
historical irony here. In the American South, slaves were emancipated
and turned into wage labourers, and now American contractarians argue
that all workers should have  the opportunity to turn themselves into
civil slaves.”</em> [<strong>Op. Cit.</strong>, p. 63]).</p>

<p>The  aim of  Ellerman’s article  was to  show the  problems that
employment (wage labour) presents  for the concept of self-government
and how  contract need  not result in  social relationships  based on
freedom. As “Philmore” put it, <em>“[a]ny thorough and decisive
critique  of   voluntary  slavery  or   constitutional  nondemocratic
government would carry  over to the employment contract  — which is
the voluntary  contractual basis for the  free-market free-enterprise
system.  Such  a  critique  would   thus  be  a  <strong>reductio  ad
absurdum</strong>.”</em>  As <em>“contractual  slavery”</em> is
an  <em>“extension of  the  employer-employee contract,”</em>  he
shows  that  the  difference  between  wage  labour  and  slavery  is
the  time scale  rather than  the principle  or social  relationships
involved.  [<strong>Op. Cit.</strong>]  This  explains, firstly,  the
early  workers’  movement   called  capitalism  <em><strong>“wage
slavery”</strong></em>  (anarchists still  do)  and, secondly,  why
capitalists like Rothbard  support the concept but balk  at the name.
It exposes  the unfree nature  of the  system they support!  While it
is  possible to  present  wage  labour as  “freedom”  due to  its
“consensual” nature, it becomes much harder to do so when talking
about slavery  or dictatorship.  Then the contradictions  are exposed
for all to see and be horrified by.</p>

<p>All this  does not mean  that we  must reject free  agreement. Far
from it!  Free agreement is <strong>essential</strong>  for a society
based upon  individual dignity  and liberty. There  are a  variety of
forms  of free  agreement  and anarchists  support  those based  upon
co-operation and  self-management (i.e. individuals  working together
as equals).  Anarchists desire to create  relationships which reflect
(and so  express) the liberty  that is  the basis of  free agreement.
Capitalism creates  relationships that  deny liberty.  The opposition
between autonomy and  subjection can only be  maintained by modifying
or  rejecting contract  theory, something  that capitalism  cannot do
and  so the  right-wing  Libertarian rejects  autonomy  in favour  of
subjection (and so rejects socialism in favour of capitalism).</p>

<p>The real  contrast between  anarchism and  right-Libertarianism is
best  expressed in  their respective  opinions on  slavery. Anarchism
is  based  upon  the  individual  whose  individuality  depends  upon
the  maintenance of  free  relationships with  other individuals.  If
individuals deny their capacities for self-government from themselves
through a contract  the individuals bring about  a qualitative change
in their  relationship to others  — freedom is turned  into mastery
and subordination. For the anarchist, slavery is thus the paradigm of
what freedom  is <strong>not</strong>, instead of  an exemplification
of what it is (as right-Libertarians state). As Proudhon argued:</p>

<blockquote class="citation">
“If I were  asked to answer the following  question: What is
slavery? and  I should answer in  one word, It is  murder, my meaning
would be understood  at once. No extended argument  would be required
to show that the power to take  from a man his thought, his will, his
personality, is a power of life and  death; and that to enslave a man
is  to  kill  him.”</em> [<strong>What  is  Property?</strong>,  p.
37]
</blockquote>

<p>In contrast,  the right-Libertarian  effectively argues  that “I
support  slavery  because  I  believe  in liberty.”  It  is  a  sad
reflection of the ethical and  intellectual bankruptcy of our society
that such  an “argument”  is actually  taken seriously  by (some)
people. The concept of “slavery  as freedom” is far too Orwellian
to warrant a  critique — we will leave it  up to right Libertarians
to  corrupt our  language and  ethical standards  with an  attempt to
prove it.</p>

<p>From the  basic insight that  slavery is the opposite  of freedom,
the  anarchist rejection  of authoritarian  social relations  quickly
follows (the right-wing Libertarians fear):</p>

<blockquote class="citation">
“Liberty is inviolable.  I can neither sell  nor alienate my
liberty; every contract, every condition  of a contract, which has in
view the  alienation or  suspension of liberty,  is null:  the slave,
when he  plants his  foot upon  the soil of  liberty, at  that moment
becomes a  free man... Liberty is  the original condition of  man; to
renounce liberty  is to renounce the  nature of man: after  that, how
could we perform the acts of man?”</em> [P.J. Proudhon, <strong>Op.
Cit.</strong>, p. 67]
</blockquote>

<p>The employment contract (i.e.  wage slavery) abrogates liberty. It
is  based  upon inequality  of  power  and <em>“exploitation  is  a
consequence  of  the fact  that  the  sale  of labour  power  entails
the  worker’s subordination.”</em>  [Carole Pateman,  <strong>Op.
Cit.</strong>, P.  149] Hence Proudhon’s (and  Mill’s) support of
self-management  and opposition  to capitalism  — any  relationship
that resembles slavery is illegitimate and no contract that creates a
relationship of subordination  is valid. Thus in  a truly anarchistic
society,  slave contracts  would  be unenforceable  —  people in  a
truly free (i.e. non-capitalist) society would <strong>never</strong>
tolerate  such  a  horrible  institution   or  consider  it  a  valid
agreement. If someone was silly enough  to sign such a contract, they
would simply have to say they now rejected it in order to be free —
such contracts are made  to be broken and without the  force of a law
system (and private defence firms) to back it up, such contracts will
stay broken.</p>

<p>The  right-Libertarian  support  for  slave  contracts  (and  wage
slavery) indicates that their ideology  has little to do with liberty
and far  more to do with  justifying property and the  oppression and
exploitation it  produces. Their support and  theoretical support for
slavery  indicates  a  deeper authoritarianism  which  negates  their
claims to be libertarians.</p>

<h4 id="toc28">2.7  But surely  abolishing capitalism  would restrict liberty?</h4>

<p>Many “anarcho”-capitalists and  other supporters of capitalism
argue that it would be  “authoritarian” to restrict the number of
alternatives that people can choose between by abolishing capitalism.
If workers become wage labourers, so it is argued, it is because they
“value”  other things  more —  otherwise they  would not  agree
to  the  exchange. But  such  an  argument  ignores that  reality  of
capitalism.</p>

<p>By <strong>maintaining</strong>  capitalist private  property, the
options  available to  people <strong>are</strong>  restricted. In  a
fully  developed  capitalist  economy  the  vast  majority  have  the
“option” of  selling their  labour or starving/living  in poverty
— self-employed  workers account for  less than 10% of  the working
population.  Usually, workers  are at  a disadvantage  on the  labour
market  due to  the  existence  of unemployment  and  so accept  wage
labour because  otherwise they would  starve (see section 10.2  for a
discussion on  why this  is the  case). And as  we argue  in sections
J.5.11  and  J.5.12, even  <strong>if</strong>  the  majority of  the
working  population  desired  co-operative workplaces,  a  capitalist
market will not  provide them with that outcome due  to the nature of
the capitalist workplace (also see Juliet C. Schor’s excellent book
<strong>The  Overworked American</strong>  for  a  discussion of  why
workers desire  for more  free time  is not  reflected in  the labour
market).  In other  words, it  is a  myth to  claim that  wage labour
exists  or that  workplaces  are hierarchical  because workers  value
other things — they are hierarchical because bosses have more clout
on the market than workers  and, to use Schor’s expression, workers
end up wanting what they get rather than getting what they want.</p>

<p>Looking  at the  reality of  capitalism  we find  that because  of
inequality in  resources (protected  by the full  might of  the legal
system,  we should  note) those  with  property get  to govern  those
without it  during working hours (and  beyond in many cases).  If the
supporters of  capitalism were  actually concerned about  liberty (as
opposed to  property) that situation  would be abhorrent to  them —
after all, individuals  can no longer exercise their  ability to make
decisions, choices, and are reduced  to being order takers. If choice
and  liberty are  the  things  we value,  then  the  ability to  make
choices  in  all aspects  of  life  automatically follows  (including
during work hours). However,  the authoritarian relationships and the
continual violation of autonomy wage labour implies are irrelevant to
“anarcho”-capitalists (indeed, attempts  to change this situation
are denounced as violations of  the autonomy of the property owner!).
By purely concentrating on the moment  that a contract is signed they
blind  themselves to  the restricts  of liberty  that wage  contracts
create.</p>

<p>Of course, anarchists have  no desire to <strong>ban</strong> wage
labour —  we aim to  create a society  within which people  are not
forced by circumstances to sell their  liberty to others. In order to
do this, anarchists  propose a modification of  property and property
rights to ensure  true freedom of choice (a freedom  of choice denied
to  us by  capitalism). As  we  have noted  many times,  “bilateral
exchanges”  can  and do  adversely  effect  the position  of  third
parties if they result in the build-up of power/money in the hands of
a few.  And one of  these adverse effects  can be the  restriction of
workers options due to economic  power. Therefore it is the supporter
of capitalist who restricts options  by supporting an economic system
and rights framework  that by their very workings  reduce the options
available to the majority, who  then are “free to choose” between
those that  remain (see also  section B.4). Anarchists,  in contrast,
desire  to  expand the  available  options  by abolishing  capitalist
private property rights and removing inequalities in wealth and power
that help restrict our options and liberties artificially.</p>

<p>So does an anarchist society have  much to fear from the spread of
wage labour  within it? Probably  not. If  we look at  societies such
as  the early  United  States  or the  beginnings  of the  Industrial
Revolution in Britain,  for example, we find that,  given the choice,
most people  preferred to work  for themselves. Capitalists  found it
hard  to find  enough  workers  to employ  and  the  amount of  wages
that  had  to  be  offered  to  hire  workers  were  so  high  as  to
destroy any  profit margins.  Moreover, the  mobility of  workers and
their “laziness”  was frequently  commented upon,  with employers
despairing at  the fact workers  would just  work enough to  make end
meet and  then disappear. Thus,  left to  the actions of  the “free
market,” it is doubtful that wage  labour would have spread. But it
was not left to the “free market”.</p>

<p>In  response to  these “problems”,  the capitalists  turned to
the  state and  enforced various  restrictions on  society (the  most
important  being  the  land,  tariff and  money  monopolies  —  see
sections B.3  and 8).  In free competition  between artisan  and wage
labour, wage labour only succeeded due  to the use of state action to
create the required circumstances to  discipline the labour force and
to accumulate enough capital to give capitalists an edge over artisan
production (see section 8 for more details).</p>

<p>Thus an anarchist society would not  have to fear the spreading of
wage labour  within it. This  is simply because  would-be capitalists
(like those  in the  early United  States) would  have to  offer such
excellent conditions,  workers’ control and  high wages as  to make
the possibility  of extensive  profits from workers’  labour nearly
impossible. Without the state to support  them, they will not be able
to accumulate enough capital to give  them an advantage within a free
society. Moreover, it is somewhat  ironic to hear capitalists talking
about anarchism denying choice when we oppose wage labour considering
the fact workers were not given  any choice when the capitalists used
the state to develop wage labour in the first place!</p>

<h4 id="toc29">2.8 Why should  we reject the “anarcho”-capitalist definitions of freedom and justice?</h4>

<p>Simply because they  lead to the creation  of authoritarian social
relationships  and  so  to   restrictions  on  liberty.  A  political
theory  which, when  consistently  followed, has  evil or  iniquitous
consequences, is a bad theory.</p>

<p>For example,  any theory that  can justify slavery is  obviously a
bad theory — slavery does not cease  to stink the moment it is seen
to  follow  your  theory.  As right-Libertarians  can  justify  slave
contracts  as a  type of  wage labour  (see section  2.6) as  well as
numerous other authoritarian social  relationships, it is obviously a
bad theory.</p>

<p>It is worth quoting Noam Chomsky at length on this subject:</p>

<blockquote class="citation">
“Consider,  for   example,  the  ‘entitlement   theory  of
justice’... [a]ccording  to this  theory, a person  has a  right to
whatever  he has  acquired by  means that  are just.  If, by  luck or
labour or  ingenuity, a  person acquires  such and  such, then  he is
entitled to keep it and dispose of it as he wills, and a just society
will not infringe on this right.</em></p>

<p><em>“One can easily determine where such a principle might lead.
It  is entirely  possible  that  by legitimate  means  — say,  luck
supplemented  by  contractual  arrangements  ‘freely  undertaken’
under  pressure of  need —  one person  might gain  control of  the
necessities of life. Others are then  free to sell themselves to this
person as  slaves, if he is  willing to accept them.  Otherwise, they
are free  to perish.  Without extra question-begging  conditions, the
society is just.</em></p>

<p><em>“The argument  has all the  merits of a proof  that 2 +  2 =
5... Suppose  that some concept  of a ‘just society’  is advanced
that fails to characterise the  situation just described as unjust...
Then one  of two conclusions  is in order.  We may conclude  that the
concept  is simply  unimportant  and of  no interest  as  a guide  to
thought or action,  since it fails to apply properly  even in such an
elementary  case  as  this.  Or  we may  conclude  that  the  concept
advanced is  to be dismissed  in that it  fails to correspond  to the
pretheorectical notion that it intends  to capture in clear cases. If
our  intuitive concept  of justice  is  clear enough  to rule  social
arrangements of the  sort described as grossly unjust,  then the sole
interest of  a demonstration  that this  outcome might  be ‘just’
under  a given  ‘theory  of  justice’ lies  in  the inference  by
<strong>reductio  ad absurdum</strong>  to  the  conclusion that  the
theory is  hopelessly inadequate. While  it may capture  some partial
intuition regarding justice, it evidently neglects others.</em></p>

<p><em>“The real question to be  raised about theories that fail so
completely to capture  the concept of justice in  its significant and
intuitive sense  is why they arouse  such interest. Why are  they not
simply dismissed out of hand on the grounds of this failure, which is
striking in  clear cases?  Perhaps the  answer is,  in part,  the one
given by  Edward Greenberg  in a  discussion of  some recent  work on
the  entitlement theory  of  justice. After  reviewing empirical  and
conceptual  shortcomings,  he observes  that  such  work ‘plays  an
important function  in the process  of ... ‘blaming  the victim,’
and of protecting property  against egalitarian onslaughts by various
non-propertied  groups.’  An  ideological  defence  of  privileges,
exploitation, and private  power will be welcomed,  regardless of its
merits.</em></p>

<p><em>“These  matters  are of  no  small  importance to  poor  and
oppressed  people here  and  elsewhere.”</em> [<strong>The  Chomsky
Reader</strong>, pp. 187–188]
</blockquote>

<p>It may  be argued that  the reductions in liberty  associated with
capitalism is not really an  iniquitous outcome, but such an argument
is hardly fitting for  a theory proclaiming itself “libertarian.”
And the results of these authoritarian social relationships? To quote
Adam  Smith,  under the  capitalist  division  of labour  the  worker
<em>“has no  occasion to exert  his understanding, or  exercise his
invention”</em> and <em>“he naturally loses, therefore, the habit
of  such  exercise  and  generally becomes  as  stupid  and  ignorant
as  it  is possible  for  a  human  creature to  become.”</em>  The
worker’s mind falls <em>“into that  drowsy stupidity, which, in a
civilised society, seems to benumb the understanding of almost all of
the  inferior  [sic!] ranks  of  people.”</em>  [cited by  Chomsky,
<strong>Op. Cit.</strong>, p. 186]</p>

<p>Of course, it may be argued  that these evil effects of capitalist
authority relations on  individuals are also not  iniquitous (or that
the  very  real  domination  of  workers  by  bosses  is  not  really
domination) but that suggests a desire to sacrifice real individuals,
their hopes and  dreams and lives to an abstract  concept of liberty,
the  accumulative effect  of which  would  be to  impoverish all  our
lives. The  kind of  relationships we  create <strong>within</strong>
the  organisations  we  join  are   of  as  great  an  importance  as
their  voluntary  nature.   Social  relations  <strong>shape</strong>
the  individual  in  many  ways,  restricting  their  freedom,  their
perceptions of what freedom is and what their interests actually are.
This  means that,  in order  not  to be  farcical, any  relationships
we  create  must reflect  in  their  internal workings  the  critical
evaluation  and  self-government  that  created  them  in  the  first
place.  Sadly  capitalist  individualism masks  structures  of  power
and  relations  of  domination  and  subordination  within  seemingly
“voluntary” associations  — it fails  to note the  relations of
domination  resulting from  private property  and so  <em>“what has
been called  ‘individualism’ up  to now has  been only  a foolish
egoism which  belittles the  individual. Foolish  because it  was not
individualism  at  all. It  did  not  lead  to what  was  established
as  a  goal;  that  is   the  complete,  broad,  and  most  perfectly
attainable  development of  individuality.”</em> [Peter  Kropotkin,
<strong>Selected Writings</strong>, p. 297]</p>

<p>This  right-Libertarian lack  of concern  for concrete  individual
freedom  and  individuality is  a  reflection  of their  support  for
“free  markets”  (or  “economic liberty”  as  they  sometimes
phrase  it).   However,  as   Max  Stirner   noted,  this   fails  to
understand   that   <em>“[p]olitical   liberty   means   that   the
<strong>polis,</strong> the State, is  free; ... not, therefore, that
I  am free  of  the  State... It  does  not mean  <strong>my</strong>
liberty, but the liberty of a  power that rules and subjugates me; it
means that  one of  my <strong>despots</strong> ...  is free.”</em>
[<strong>The  Ego  and Its  Own</strong>,  p.  107] Thus  the  desire
for  “free  markets”  results  in  a  blindness  that  while  the
market may  be “free” the  individuals within  it may not  be (as
Stirner was well aware, <em>“[u]nder the <strong>regime</strong> of
the  commonality the  labourers always  fall  into the  hands of  the
possessors  ... of  the capitalists,  therefore.”</em> [<strong>Op.
Cit.</strong>, p. 115])</p>

<p>In other words, right-libertarians give the greatest importance to
an abstract concept of freedom and fail to take into account the fact
that real, concrete freedom is  the outcome of self-managed activity,
solidarity and voluntary co-operation. For liberty to be real it must
exist in all aspects of our  daily life and cannot be contracted away
without seriously  effecting our minds,  bodies and lives.  Thus, the
right-Libertarian’s  <em>“defence  of  freedom is  undermined  by
their insistence  on the concept  of negative liberty, which  all too
easily translates  in experience as the  negation of liberty.”</em>
[Stephan L.  Newman, <strong>Liberalism  as Wit’s  End</strong>, p.
161]</p>

<p>Thus   right-Libertarian’s    fundamental   fallacy    is   that
“contract”  does not  result in  the end  of power  or domination
(particularly   when  the   bargaining   power  or   wealth  of   the
would-be  contractors  is  not   equal).  As  Carole  Pateman  notes,
<em>“[i]ronically,   the  contractarian   ideal  cannot   encompass
capitalist  employment.  Employment  is  not a  continual  series  of
discrete contracts between employer and  worker, but ... one contract
in which a worker binds himself to enter an enterprise and follow the
directions of the  employer for the duration of the  contract. As Huw
Benyon has  bluntly stated,  ‘workers are paid  to obey.’”</em>
[<strong>The  Sexual  Contract</strong>,  p.  148]  This  means  that
<em>“the  employment  contract  (like  the  marriage  contract)  is
not  an  exchange;  both   contracts  create  social  relations  that
endure  over  time  — social  relations  of  subordination.”</em>
[<strong>Ibid.</strong>]</p>

<p>Authority  impoverishes us  all and  must, therefore,  be combated
wherever it  appears. That  is why  anarchists oppose  capitalism, so
that  there shall  be <em>“no  more government  of man  by man,  by
means  of  accumulation  of  capital.”</em>  [P-J  Proudhon,  cited
by  Woodcock in  <strong>Anarchism</strong>,  p. 110]  If, as  Murray
Bookchin  point it,  <em>“the object  of anarchism  is to  increase
choice”</em> [<strong>The Ecology of  Freedom</strong>, p. 70] then
this applies both to  when we are creating associations/relationships
with   others  and   when   we   are  <strong>within</strong>   these
associations/relationships — i.e. that they are consistent with the
liberty of  all, and  that implies participation  and self-management
<strong>not</strong>  hierarchy.  “Anarcho”-capitalism  fails  to
understand this  essential point and  by concentrating purely  on the
first condition for liberty ensures  a society based upon domination,
oppression and hierarchy and not freedom.</p>

<p>It  is unsurprising,  therefore, to  find that  the basic  unit of
analysis  of  the “anarcho”-capitalist/right-libertarian  is  the
transaction (the “trade,” the “contract”). The freedom of the
individual  is  seen  as  revolving  around  an  act,  the  contract,
and  <strong>not</strong>  in  our  relations with  others.  All  the
social facts  and mechanisms that  precede, surround and  result from
the  transaction are  omitted.  In particular,  the social  relations
that  result  from  the  transaction  are  ignored  (those,  and  the
circumstances that  make people contract, are  the two unmentionables
of right-libertarianism).</p>

<p>For anarchists it seems strange  to concentrate on the moment that
a contract is  signed and ignore the far longer  time the contract is
active for (as we noted in section A.2.14, if the worker is free when
they  sign  a  contract,  slavery  soon  overtakes  them).  Yes,  the
voluntary nature  of a decision is  important, but so are  the social
relationships we experience due to those decisions.</p>

<p>For the  anarchist, freedom is  based upon the insight  that other
people, apart from (indeed, <strong>because</strong> of) having their
own  intrinsic value,  also are  “means to  my end”,  that it  is
through their freedom that I gain my own — so enriching my life. As
Bakunin put it:</p>

<blockquote class="citation">
“I who want to be free  cannot be because all the men around
me do  not yet want  to be free,  and consequently they  become tools
of  oppression against  me.”</em>  [quoted by  Errico Malatesta  in
<strong>Anarchy</strong>, p. 27]
</blockquote>

<p>Therefore   anarchists    argue   that   we   must    reject   the
right-Libertarian theories of freedom and justice because they end up
supporting the denial  of liberty as the expression  of liberty. What
this fails to  recognise is that freedom is a  product of social life
and that (in  Bakunin’s words) <em>“[n]o man can  achieve his own
emancipation without at the same time working for the emancipation of
all men around him.  My freedom is the freedom of all  since I am not
truly free  in thought  and in  fact, except when  my freedom  and my
rights are  confirmed and approved in  the freedom and rights  of all
men who are my equals.”</em> [<strong>Ibid.</strong>]</p>

<p>Other people give  us the possibilities to develop  our full human
potentiality and thereby our freedom,  so when we destroy the freedom
of others  we limit our  own. <em>“To  treat others and  oneself as
property,”</em> argues anarchist L. Susan Brown, <em>“objectifies
the human individual, denies the unity of subject and object and is a
negation of individual will ... even  the freedom gained by the other
is  compromised by  this  relationship,  for to  negate  the will  of
another  to achieve  one’s own  freedom destroys  the very  freedom
one  sought  in the  first  place.”</em>  [<strong>The Politics  of
Individualism</strong>, p. 3]</p>

<p>Fundamentally, it  is for this  reason that anarchists  reject the
right-Libertarian theories  of freedom and  justice — it  just does
not ensure individual freedom or individuality.</p>

<h3  id="toc30">3 Why  do anarcho”-capitalists  place little  or no value on “equality”?</h3>

<p>Murray Rothbard argues  that <em>“the ‘rightist’ libertarian
is   not   opposed   to  inequality.”</em>   [<strong>For   a   New
Liberty</strong>,  p.  47]  In contrast,  “leftist”  libertarians
oppose  inequality  because  it  has harmful  effects  on  individual
liberty.</p>

<p>Part of  the reason  “anarcho”-capitalism places little  or no
value on “equality”  derives from their definition  of that term.
Murray Rothbard defines equality as:</p>

<blockquote class="citation">
“A and B are ‘equal’ if they are identical to each other
with respect to  a given attribute... There is one  and only one way,
then,  in which  any  two people  can really  be  ‘equal’ in  the
fullest sense:  they must be identical  in <strong>all</strong> their
attributes.”
</blockquote>

<p>He  then points  out  the  obvious fact  that  <em>“men are  not
uniform,...  the  species,  mankind,  is  uniquely  characterised  by
a  high  degree of  variety,  diversity,  differentiation: in  short,
inequality.”</em>  [<strong>Egalitarianism  as   a  Revolt  against
Nature and Other Essays</strong>, p. 4, p.5]</p>

<p>In  others  words,  every   individual  is  unique.  Something  no
egalitarian has  ever denied. On  the basis of this  amazing insight,
he  concludes  that equality  is  impossible  (except “equality  of
rights”)  and  that the  attempt  to  achieve “equality”  is  a
“revolt  against  nature”  —  as  if  any  anarchist  had  ever
advocated such a notion of equality as being identical!</p>

<p>And so, because we are all unique, the outcome of our actions will
not  be  identical  and  so  social  inequality  flows  from  natural
differences  and  not due  to  the  economic  system we  live  under.
Inequality of endowment  implies inequality of outcome  and so social
inequality. As individual differences are  a fact of nature, attempts
to create  a society  based on  “equality” (i.e.  making everyone
identical in  terms of  possessions and so  forth) is  impossible and
“unnatural.”</p>

<p>Before  continuing,  we  must  note that  Rothbard  is  destroying
language  to  make  his  point  and  that he  is  not  the  first  to
abuse  language   in  this  particular  way.   In  George  Orwell’s
<strong>1984</strong>,  the  expression  <em>“all men  are  created
equal”</em> could be translated into Newspeak, but it would make as
much sense as saying <em>“all men have red hair,”</em> an obvious
falsehood (see  <em>“The Principles of  Newspeak”</em> Appendix).
It’s nice to know that “Mr. Libertarian” is stealing ideas from
Big  Brother, and  for  the  same reason:  to  make critical  thought
impossible by restricting the meaning of words.</p>

<p>“Equality,”  in  the  context of  political  discussion,  does
not  mean  “identical,”  it  usually means  equality  of  rights,
respect,  worth, power  and  so  forth. It  does  not imply  treating
everyone  identically  (for example,  expecting  an  eighty year  old
man  to do  identical  work  to an  eighteen  violates treating  both
with  respect as  unique individuals).  For anarchists,  as Alexander
Berkman writes,  <em>“equality does  not mean  an equal  amount but
equal  <strong>opportunity</strong>... Do  not  make  the mistake  of
identifying  equality in  liberty  with the  forced  equality of  the
convict camp. True anarchist  equality implies freedom, not quantity.
It does  not mean that  every one must eat,  drink, or wear  the same
things, do the  same work, or live  in the same manner.  Far from it:
the very  reverse, in  fact. Individual needs  and tastes  differ, as
appetites differ. It is <strong>equal</strong> opportunity to satisfy
them  that  constitutes  true  equality.  Far  from  levelling,  such
equality opens the door for the greatest possible variety of activity
and  development.  For  human  character is  diverse,  and  only  the
repression of this free diversity results in levelling, in uniformity
and  sameness. Free  opportunity  and acting  out your  individuality
means development  of natural dissimilarities and  variations... Life
in freedom, in anarchy will do more than liberate man merely from his
present  political  and  economic  bondage. That  will  be  only  the
first  step,  the preliminary  to  a  truly human  existence.”</em>
[<strong>The ABC of Anarchism</strong>, p. 25]</p>

<p>Thus  anarchists  reject  the Rothbardian-Newspeak  definition  of
equality as  meaningless within  political discussion. No  two people
are identical  and so imposing “identical”  equality between them
would  mean  treating  them as  <strong>unequals</strong>,  i.e.  not
having equal  worth or giving  them equal  respect as befits  them as
human beings and fellow unique individuals.</p>

<p>So what should we make of  Rothbard’s claim? It is tempting just
to  quote  Rousseau  when  he  argued <em>“it  is  ...  useless  to
inquire whether  there is  any essential  connection between  the two
inequalities [social and natural]; for  this would be only asking, in
other words,  whether those who  command are necessarily  better than
those who obey, and if strength of body or of mind, wisdom, or virtue
are always  found in particular  individuals, in proportion  to their
power or wealth: a question fit  perhaps to be discussed by slaves in
the hearing of their masters, but highly unbecoming to reasonable and
free men in search of the truth.”</em> [<strong>The Social Contract
and  Discourses</strong>, p.  49] But  a  few more  points should  be
raised.</p>

<p>The  uniqueness of  individuals  has always  existed  but for  the
vast  majority of  human history  we have  lived in  very egalitarian
societies.  If  social  inequality  did, indeed,  flow  from  natural
inequalities then  <strong>all</strong> societies would be  marked by
it. This is not the case. Indeed, taking a relatively recent example,
many  visitors  to the  early  United  States noted  its  egalitarian
nature, something that soon changed with  the rise of wage labour and
industrial capitalism  (a rise dependent  upon state action,  we must
add, —  see section 8).  This implies that  the society we  live in
(its rights framework,  the social relationships it  generates and so
forth)  has a  far  more  of a  decisive  impact  on inequality  than
individual differences.  Thus certain rights frameworks  will tend to
magnify “natural”  inequalities (assuming  that is the  source of
the initial  inequality, rather  than, say,  violence and  force). As
Noam Chomsky argues:</p>

<blockquote class="citation">
“Presumably it is the case that in our ‘real world’ some
combination of  attributes is conducive  to success in  responding to
‘the  demands  of the  economic  system’  ... One  might  suppose
that  some  mixture of  avarice,  selfishness,  lack of  concern  for
others, aggressiveness,  and similar  characteristics play a  part in
getting ahead  [in capitalism]... Whatever the  correct collection of
attributes may be, we may ask what  follows from the fact, if it is a
fact, that  some partially inherited combination  of attributes tends
to  material success?  All  that  follows ...  is  a  comment on  our
particular social and economic arrangements ... The egalitarian might
responds, in all such cases, that  the social order should be changes
so that the  collection of attributes that tends to  bring success no
longer do  so ...  “</em> [<strong>The Chomsky  Reader</strong>, p.
190]
</blockquote>

<p>So, perhaps,  if we  change society  then the  social inequalities
we  see  today  would  disappear.  It  is  more  than  probable  that
natural   difference   has  been   long   ago   been  replaced   with
<strong>social</strong>  inequalities,   especially  inequalities  of
property  (which  will  tend   to  increase,  rather  than  decrease,
inequality).  And as  we argue  in  section 8  these inequalities  of
property  were initially  the result  of force,  <strong>not</strong>
differences in  ability. Thus to  claim that social  inequality flows
from natural differences is false as most social inequality has flown
from violence and  force. This initial inequality  has been magnified
by the framework of capitalist  property rights and so the inequality
within  capitalism is  far more  dependent upon,  say, the  existence
of  wage  labour,  rather   than  “natural”  differences  between
individuals.</p>

<p>If we  look at capitalism,  we see  that in workplaces  and across
industries many,  if not  most, unique individuals  receive identical
wages for  identical work (although  this often  is not the  case for
women and blacks,  who receive less wages than  male, white workers).
Similarly, capitalists have deliberately introduced wage inequalities
and hierarchies for no other reason  that to divide (and so rule) the
workforce  (see  section D.10).  Thus,  if  we assume  egalitarianism
<strong>is</strong> a revolt against  nature, then much of capitalist
economic  life  is  in  such  a  revolt (and  when  it  is  not,  the
“natural” inequalities have been imposed artificially by those in
power).</p>

<p>Thus  “natural”  differences  do  not  necessarily  result  in
inequality as  such. Given  a different social  system, “natural”
differences would  be encouraged and  celebrated far wider  than they
are under capitalism  (where, as we argued in  section B.1, hierarchy
ensures the crushing of  individuality rather than its encouragement)
without any change  in social equality. The  claim that “natural”
differences generates social inequalities  is question begging in the
extreme — it  takes the rights framework of society  as a given and
ignores  the initial  source  of inequality  in  property and  power.
Indeed,  inequality  of  outcome  or  reward is  more  likely  to  be
influenced by  social conditions  rather than  individual differences
(as would  be the  case in a  society based on  wage labour  or other
forms of exploitation).</p>

<p>Another  reason  for   “anarcho”-capitalist  lack  of  concern
for  equality   is  that   they  think  that   <em>“liberty  upsets
patterns”</em> (see  section 2.5, for  example). It is  argued that
equality  can only  be maintained  by restricting  individual freedom
to  make exchanges  or  by  taxation of  income.  However, what  this
argument  fails  to acknowledge  is  that  inequality also  restricts
individual  freedom (see  next  section, for  example)  and that  the
capitalist property  rights framework is  not the only  one possible.
After  all,  money  is  power  and inequalities  in  terms  of  power
easily  result  in restrictions  of  liberty  and the  transformation
of  the  majority  into  order takers  rather  than  free  producers.
In  other words,  once  a  certain level  of  inequality is  reached,
property  does not  promote, but  actually conflicts  with, the  ends
which render  private property  legitimate. Moreover, Nozick  (in his
“liberty upsets  patterns” argument)  <em>“has produced  ... an
argument for unrestricted private property using unrestricted private
property, and thus  he begs the question he  tries to answer.”</em>
[Andrew Kerhohan, <em>“Capitalism  and Self-Ownership”</em>, from
<strong>Capitalism</strong>, p. 71] For example, a worker employed by
a capitalist  cannot freely  exchange the  machines or  raw materials
they  have been  provided  with  to use  but  Nozick  does not  class
this distribution  of “restricted” property rights  as infringing
liberty  (nor  does  he  argue that  wage  slavery  itself  restricts
freedom, of course).</p>

<p>So in response to the claim that equality could only be maintained
by continuously  interfering with people’s lives,  anarchists would
say  that the  inequalities  produced by  capitalist property  rights
also involve  extensive and  continuous interference  with people’s
lives.  After  all,  as  Bob Black  notes  <em>“[y]our  foreman  or
supervisor  gives  you  more  or-else  orders  in  a  week  than  the
police  do  in  a  decade”</em>  nevermind  the  other  effects  of
inequality such as stress, ill  health and so on [<strong>Libertarian
as  Conservative</strong>].   Thus  claims  that   equality  involves
infringing liberty  ignores the  fact that inequality  also infringes
liberty.  A  reorganisation  of society  could  effectively  minimise
inequalities  by eliminating  the major  source of  such inequalities
(wage labour) by self-management (see section I.5.12 for a discussion
of “capitalistic acts”  within an anarchist society).  We have no
desire to restrict free exchanges  (after all, most anarchists desire
to see the  “gift economy” become a reality sooner  or later) but
we  argue  that free  exchanges  need  not involve  the  unrestricted
property rights  Nozick assumes. As we  argue in sections 2  and 3.1,
inequality  can  easily led  to  the  situation where  self-ownership
is   used  to   justify  its   own  negation   and  so   unrestricted
property  rights  may  undermine  the  meaningful  self-determination
(what  anarchists  would  usually   call  “freedom”  rather  than
self-ownership) which many people  intuitively understand by the term
“self-ownership”.</p>

<p>Thus,  for  anarchists,  the  “anarcho”-capitalist  opposition
to  equality misses  the  point and  is  extremely question  begging.
Anarchists   do  not   desire   to   make  humanity   “identical”
(which  would   be  impossible   and  a   total  denial   of  liberty
<strong>and</strong> equality)  but to make the  social relationships
between individuals equal in  <strong>power.</strong> In other words,
they  desire  a  situation  where people  interact  together  without
institutionalised  power  or hierarchy  and  are  influenced by  each
other  “naturally,”   in  proportion  to  how   the  (individual)
<strong>differences</strong> between (social) <strong>equals</strong>
are  applicable  in  a  given  context.  To  quote  Michael  Bakunin,
<em>“[t]he  greatest   intelligence  would   not  be  equal   to  a
comprehension of  the whole. Thence  results... the necessity  of the
division  and  association  of  labour.  I receive  and  I  give  —
such  is human  life.  Each  directs and  is  directed  in his  turn.
Therefore there is  no fixed and constant authority,  but a continual
exchange of  mutual, temporary,  and, above all,  voluntary authority
and  subordination.”</em> [<strong>God  and the  State</strong>, p.
33]</p>

<p>Such   an  environment   can   only   exist  within   self-managed
associations, for capitalism (i.e. wage labour) creates very specific
relations  and  institutions of  authority.  It  is for  this  reason
anarchists are socialists (i.e. opposed to wage labour, the existence
of  a  proletariat or  working  class).  In other  words,  anarchists
support equality precisely <strong>because</strong> we recognise that
everyone is unique. If we  are serious about “equality of rights”
or “equal freedom”  then conditions must be such  that people can
enjoy these rights  and liberties. If we assume the  right to develop
one’s capacities  to the fullest,  for example, then  inequality of
resources  and so  power within  society destroys  that right  simply
because  people  do not  have  the  means  to freely  exercise  their
capacities  (they are  subject  to  the authority  of  the boss,  for
example, during work hours).</p>

<p>So,  in  direct  contrast to  anarchism,  right-Libertarianism  is
unconcerned  about  any  form   of  equality  except  “equality  of
rights”. This blinds them to  the realities of life; in particular,
the impact of economic and social power on individuals within society
and the  social relationships of domination  they create. Individuals
may be “equal” before the law and  in rights, but they may not be
free due to the influence  of social inequality, the relationships it
creates and how  it affects the law and the  ability of the oppressed
to  use it.  Because of  this,  all anarchists  insist that  equality
is  essential  for  freedom,  including those  in  the  Individualist
Anarchist  tradition  the  “anarcho”-capitalist tries  to  co-opt
—  <em>“Spooner  and  Godwin   insist  that  inequality  corrupts
freedom. Their  anarchism is directed  as much against  inequality as
against tyranny”</em> and <em>“[w]hile sympathetic to Spooner’s
individualist anarchism,  they [Rothbard and David  Friedman] fail to
notice or conveniently overlook its egalitarian implications.”</em>
[Stephen L.  Newman, <strong>Liberalism  at Wit’s  End</strong>, p.
74, p. 76]</p>

<p>Why  equality is  important is  discussed more  fully in  the next
section. Here we just stress that without social equality, individual
freedom  is so  restricted  that it  becomes  a mockery  (essentially
limiting freedom  of the majority to  choosing <strong>which</strong>
employer will govern  them rather than being free  within and outside
work).</p>

<p>Of  course,  by  defining  “equality” in  such  a  restrictive
manner,  Rothbard’s  own ideology  is  proved  to be  nonsense.  As
L.A. Rollins  notes, <em>“Libertarianism,  the advocacy  of ‘free
society’  in which  people enjoy  ‘equal freedom’  and ‘equal
rights,’ is  actually a specific  form of egalitarianism.  As such,
Libertarianism itself is a revolt against nature. If people, by their
very biological nature,  are unequal in all  the attributes necessary
to achieving,  and preserving ‘freedom’ and  ‘rights’... then
there  is  no  way  that  people can  enjoy  ‘equal  freedom’  or
‘equal rights’.  If a free society  is conceived as a  society of
‘equal freedom,’  then there ain’t  no such thing as  ‘a free
society’.”</em>  [<strong>The Myth  of  Natural Law</strong>,  p.
36]</p>

<p>Under capitalism, freedom is a commodity like everything else. The
more money you  have, the greater your  freedom. “Equal” freedom,
in the Newspeak-Rothbardian  sense, <strong>cannot</strong> exist! As
for “equality  before the  law”, its  clear that  such a  hope is
always dashed against the rocks of  wealth and market power (see next
section for more on  this). As far as rights go,  of course, both the
rich and the  poor have an “equal right” to  sleep under a bridge
(assuming the bridge’s  owner agrees of course!); but  the owner of
the bridge  and the homeless have  <strong>different</strong> rights,
and  so  they cannot  be  said  to  have  “equal rights”  in  the
Newspeak-Rothbardian  sense either.  Needless to  say, poor  and rich
will not “equally”  use the “right” to sleep  under a bridge,
either.</p>

<p>Bob    Black    observes    in    <strong>The    Libertarian    as
Conservative</strong> that <em>“[t]he time of  your life is the one
commodity you  can sell  but never buy  back. Murray  Rothbard thinks
egalitarianism is  a revolt against nature,  but his day is  24 hours
long, just like everybody else’s.”</em></p>

<p>By   twisting  the   language  of   political  debate,   the  vast
differences in  power in capitalist  society can be  “blamed” not
on  an  unjust and  authoritarian  system  but on  “biology”  (we
are  all  unique  individuals,  after all).  Unlike  genes  (although
biotechnology corporations are working  on this, too!), human society
<strong>can</strong>  be changed,  by  the  individuals who  comprise
it,  to  reflect the  basic  features  we  all  share in  common  —
our  humanity, our  ability  to  think and  feel,  and  our need  for
freedom.</p>

<h4 id="toc31">3.1 Why is this disregard for equality important?</h4>

<p>Simply because  a disregard  for equality  soon ends  with liberty
for  the  majority  being  negated   in  many  important  ways.  Most
“anarcho”-capitalists  and right-Libertarians  deny  (or at  best
ignore)  market power.  Rothbard, for  example, claims  that economic
power does not exist;  what people call <em>“economic power”</em>
is  <em>“simply  the right  under  freedom  to  refuse to  make  an
exchange”</em> [<strong>The Ethics of Liberty</strong>, p. 222] and
so the concept is meaningless.</p>

<p>However,  the fact  is that  there are  substantial power  centres
in  society  (and  so  are  the  source  of  hierarchical  power  and
authoritarian   social   relations)   which   are   <strong>not   the
state.</strong>  The  central   fallacy  of  “anarcho”-capitalism
is  the  (unstated) assumption  that  the  various actors  within  an
economy  have  relatively  equal  power.  This  assumption  has  been
noted  by   many  readers   of  their   works.  For   example,  Peter
Marshall  notes  that  <em>“‘anarcho-capitalists’  like  Murray
Rothbard  assume individuals  would  have equal  bargaining power  in
a  [capitalist] market-based  society”</em> [<strong>Demanding  the
Impossible</strong>,  p. 46]  George  Walford also  makes this  clear
in  his  comments  on  David Friedman’s  <strong>The  Machinery  of
Freedom</strong>:</p>

<blockquote class="citation">
“The private ownership  envisages by the anarcho-capitalists
would be very  different from that which we know.  It is hardly going
too far to say that while the  one is nasty, the other would be nice.
In  anarcho-capitalism  there  would  be no  National  Insurance,  no
Social Security,  no National  Health Service  and not  even anything
corresponding to the Poor Laws;  there would be no public safety-nets
at all.  It would be a  rigorously competitive society: work,  beg or
die. But as one reads on, learning that each individual would have to
buy,  personally,  all goods  and  services  needed, not  only  food,
clothing  and  shelter  but  also  education,  medicine,  sanitation,
justice, police, all forms of security and insurance, even permission
to use the streets (for these  also would be privately owned), as one
reads about all this a  curious feature emerges: everybody always has
enough money to buy all these things.</em></p>

<p><em>“There are no public casual  wards or hospitals or hospices,
but neither is there anybody dying in the streets. There is no public
educational  system  but no  uneducated  children,  no public  police
service  but  nobody unable  to  buy  the  services of  an  efficient
security firm, no  public law but nobody  unable to buy the  use of a
private legal system. Neither is there  anybody able to buy much more
than anybody else;  no person or group possesses  economic power over
others.</em></p>

<p><em>“No explanation  is offered. The  anarcho-capitalists simply
take  it for  granted that  in  their favoured  society, although  it
possesses no  machinery for  restraining competition (for  this would
need  to  exercise  authority  over  the competitors  and  it  is  an
<strong>anarcho</strong>- capitalist  society) competition  would not
be  carried to  the point  where anybody  actually suffered  from it.
While  proclaiming their  system to  be a  competitive one,  in which
private  interest  rules  unchecked,  they show  it  operating  as  a
co-operative one, in which no person  or group profits at the cost of
another.”</em> [<strong>On the Capitalist Anarchists</strong>]
</blockquote>

<p>This assumption of (relative) equality comes to the fore in Murray
Rothbard’s  “Homesteading” concept  of  property (discussed  in
section  4.1). “Homesteading”  paints  a  picture of  individuals
and  families  doing   into  the  wilderness  to  make   a  home  for
themselves,  fighting against  the  elements and  so  forth. It  does
<strong>not</strong>  invoke the  idea of  transnational corporations
employing tens of  thousands of people or a  population without land,
resources and selling their labour to others. Indeed, Rothbard argues
that economic power does not exist  (at least under capitalism; as we
saw in section 2.1 he does make — highly illogical — exceptions).
Similarly,  David Friedman’s  example  of a  pro-death penalty  and
anti-death  penalty “defence”  firm coming  to an  agreement (see
section 6.3) assumes that the  firms have equal bargaining powers and
resources  — if  not, then  the  bargaining process  would be  very
one-sided and the smaller company  would think twice before taking on
the larger one  in battle (the likely outcome if  they cannot come to
an agreement on this issue) and so compromise.</p>

<p>However,  the   right-libertarian  denial   of  market   power  is
unsurprising.  The  necessity, not  the  redundancy,  of equality  is
required if the  inherent problems of contract are not  to become too
obvious.  If some  individuals <strong>are</strong>  assumed to  have
significantly  more  power  than  others,  and  if  they  are  always
self-interested,  then  a contract  that  creates  equal partners  is
impossible — the pact will  establish an association of masters and
servants. Needless  to say, the  strong will present the  contract as
being to the  advantage of both: the strong no  longer have to labour
(and become rich, i.e. even stronger)  and the weak receive an income
and so do not starve.</p>

<p>If freedom  is considered as  a function  of ownership then  it is
very clear that individuals lacking property (outside their own body,
of course) loses  effective control over their own  person and labour
(which  was,  lets not  forget,  the  basis  of their  equal  natural
rights). When ones  bargaining power is weak (which  is typically the
case in  the labour  market) exchanges  tend to  magnify inequalities
of  wealth  and  power  over  time rather  than  working  towards  an
equalisation.</p>

<p>In  other words,  “contract”  need not  replace  power if  the
bargaining position  and wealth of  the would-be contractors  are not
equal (for,  if the bargainers  had equal  power it is  doubtful they
would agree to  sell control of their liberty/time  to another). This
means that  “power” and “market” are  not antithetical terms.
While, in  an abstract sense,  all market relations are  voluntary in
practice  this  is not  the  case  within  a capitalist  market.  For
example, a large company has  a comparative advantage over small ones
and  communities  which will  definitely  shape  the outcome  of  any
contract.  For example,  a large  company  or rich  person will  have
access to more funds and so stretch out litigations and strikes until
their opponents  resources are exhausted.  Or, if a local  company is
polluting the  environment, the local  community may put up  with the
damage  caused  out of  fear  that  the  industry (which  it  depends
upon) would  relocate to  another area. If  members of  the community
<strong>did</strong> sue, then the company would be merely exercising
its property rights  when it threatened to move  to another location.
In such  circumstances, the  community would “freely”  consent to
its conditions or  face massive economic and  social disruption. And,
similarly, <em>“the  landlords’ agents who threaten  to discharge
agricultural workers and  tenants who failed to  vote the reactionary
ticket”</em>  in the  1936  Spanish election  were just  exercising
their legitimate property rights  when they threatened working people
and their  families with  economic uncertainty and  distress. [Murray
Bookchin, <strong>The Spanish Anarchists</strong>, p. 260]</p>

<p>If we  take the labour market,  it is clear that  the “buyers”
and “sellers” of labour power are  rarely on an equal footing (if
they were, then capitalism would soon  go into crisis — see section
10.2).  In fact,  competition <em>“in  labour markets  is typically
skewed in  favour of employers:  it is a  buyer’s market. And  in a
buyer’s,  it  is the  sellers  who  compromise.”</em> [Juliet  B.
Schor,  <strong>The Overworked  American</strong>, p.  129] Thus  the
ability to refuse an exchange weights  most heavily on one class than
another and so  ensures that “free exchange” works  to ensure the
domination (and so exploitation) of one party by the other.</p>

<p>Inequality  in  the  market  ensures that  the  decisions  of  the
majority of within it are shaped in accordance with that needs of the
powerful,  not the  needs of  all. It  was for  this reason  that the
Individual Anarchist  J.K. Ingalls opposed Henry  George’s proposal
of nationalising the land. Ingalls was well aware that the rich could
outbid the  poor for leases on  land and so the  dispossession of the
working classes would continue.</p>

<p>The market,  therefore, does not  end power or unfreedom  — they
are still  there, but in different  forms. And for an  exchange to be
truly  voluntary,  both parties  must  have  equal power  to  accept,
reject, or  influence its terms. Unfortunately,  these conditions are
rarely meet on  the labour market or within the  capitalist market in
general.  Thus Rothbard’s  argument  that economic  power does  not
exist fails  to acknowledge that  the rich  can out-bid the  poor for
resources and  that a  corporation generally  has greater  ability to
refuse a contract (with an  individual, union or community) than vice
versa (and  that the impact  of such a refusal  is such that  it will
encourage the  others involved  to “compromise” far  sooner). And
in  such  circumstances,  formally  free  individuals  will  have  to
“consent” to be unfree in order to survive.</p>

<p>As  Max  Stirner  pointed  out  in  the  1840s,  free  competition
<em>“is not ‘free,’ because  I lack the <strong>things</strong>
for competition.”</em>  [<strong>The Ego  and Its  Own</strong>, p.
262] Due to this basic inequality of wealth (of “things”) we find
that  <em>“[u]nder the  <strong>regime</strong> of  the commonality
the labourers always fall into the hands of the possessors ... of the
capitalists, therefore. The  labourer cannot <strong>realise</strong>
on  his labour  to  the extent  of  the  value that  it  has for  the
customer.”</em> [<strong>Op. Cit.</strong>, p. 115] Its interesting
to  note that  even  Stirner recognises  that  capitalism results  in
exploitation.  And  we may  add  that  value  the labourer  does  not
<em>“realise”</em> goes  into the  hands of the  capitalists, who
invest it in  more “things” and which  consolidates and increases
their advantage in “free” competition.</p>

<p>To quote Stephan L. Newman:</p>

<blockquote class="citation">
“Another disquieting  aspect of the  libertarians’ refusal
to acknowledge power  in the market is their failure  to confront the
tension between freedom and  autonomy... Wage labour under capitalism
is, of  course, formally  free labour.  No one is  forced to  work at
gun  point.  Economic circumstance,  however,  often  has the  effect
of  force;  it compels  the  relatively  poor  to accept  work  under
conditions  dictated by  owners and  managers. The  individual worker
retains freedom [i.e. negative  liberty] but loses autonomy [positive
liberty].”</em>  [<strong>Liberalism at  Wit’s End</strong>,  pp.
122–123]
</blockquote>

<p>(As an  aside, we  should point  out that  the full  Stirner quote
cited  above is  <em>“[u]nder  the  <strong>regime</strong> of  the
commonality  the  labourers  always  fall   into  the  hands  of  the
possessors, of those who have at their disposal some bit of the state
domains  (and  everything  possessible  in State  domain  belongs  to
the  State  and  is  only  a  fief  of  the  individual),  especially
money and  land; of the  capitalists, therefore. The  labourer cannot
<strong>realise</strong> on  his labour  to the  extent of  the value
that it has for the customer.”</em></p>

<p>It could be  argued that we misrepresenting  Stirner by truncating
the quote, but we feel that such a claim this is incorrect. Its clear
from his  book that  Stirner is  considering the  “minimal” state
(<em>“The State  is a  — commoners’ State  ... It  protects man
...according to whether the rights entrusted  to him by the State are
enjoyed and  managed in accordance with  the will, that is,  laws, of
the  State.”</em> The  State <em>“looks  on indifferently  as one
grows  poor  and  the  other rich,  unruffled  by  this  alternation.
As  <strong>individuals</strong> they  are  really  equal before  its
face.”</em>  [<strong>Op.  Cit.</strong>,  p.  115,  p.  252]).  As
“anarcho”-capitalists consider  their system to be  one of rights
and  laws  (particularly property  rights),  we  feel that  its  fair
to  generalise   Stirner’s  comments  into   capitalism  <strong>as
such</strong>  as opposed  to “minimum  state” capitalism.  If we
replace “State” by  “libertarian law code” you  will see what
we mean.  We have included  this aside before  any right-libertarians
claim that we are misrepresenting Stirner’ argument.)</p>

<p>If we  consider “equality before  the law” it is  obvious that
this also has  limitations in an (materially)  unequal society. Brian
Morris  notes  that  for  Ayn  Rand,  <em>“[u]nder  capitalism  ...
politics (state)  and economics (capitalism) are  separated ... This,
of course, is pure ideology,  for Rand’s justification of the state
is that it ‘protects’ private  property, that is, it supports and
upholds the economic power  of capitalists by coercive means.”</em>
[<strong>Ecology &amp;  Anarchism</strong>, p.  189] The same  can be
said  of “anarcho”-capitalism  and its  “protection agencies”
and “general libertarian law code.” If within a society a few own
all the  resources and  the majority are  dispossessed, then  any law
code which  protects private  property <strong>automatically</strong>
empowers the  owning class.  Workers will  <strong>always</strong> be
initiating force  if act against  the code and so  “equality before
the law” reinforces inequality of power and wealth.</p>

<p>This means that a system of property rights protects the liberties
of some  people in a way  which gives them an  unacceptable degree of
power over others.  And this cannot be met merely  by reaffirming the
rights  in question,  we have  to assess  the relative  importance of
various kinds of liberty and other values we how dear.</p>

<p>Therefore  right-libertarian disregard  for equality  is important
because it  allows “anarcho”-capitalism to ignore  many important
restrictions of  freedom in society.  In addition, it allows  them to
brush over the negative effects of their system by painting an unreal
picture of a  capitalist society without vast extremes  of wealth and
power  (indeed,  they  often  construe capitalist  society  in  terms
of  an  ideal  —  namely  artisan production  —  that  is  really
<strong>pre</strong>-capitalist  and  whose  social  basis  has  been
eroded by capitalist development). Inequality shapes the decisions we
have available and what ones we make:</p>

<blockquote class="citation">
“An  ‘incentive’ is  always available  in conditions  of
substantial social  inequality that ensure that  the ‘weak’ enter
into a  contract. When  social inequality prevails,  questions arises
about  what  counts  as  voluntary  entry into  a  contract  ...  Men
and  women ...  are now  juridically  free and  equal citizens,  but,
in  unequal  social  conditions,  the  possibility  cannot  be  ruled
out  that  some or  many  contracts  create relationships  that  bear
uncomfortable  resemblances  to  a  slave  contract.”</em>  [Carole
Pateman, <strong>The Sexual Contract</strong>, p. 62]
</blockquote>

<p>This  ideological confusion  of right-libertarianism  can also  be
seen from their  opposition to taxation. On the one  hand, they argue
that  taxation  is  wrong  because  it takes  money  from  those  who
“earn”  it  and  gives  it  to  the  poor.  On  the  other  hand,
“free market” capitalism  is assumed to be a  more equal society!
If  taxation  takes  from  the  rich  and  gives  to  the  poor,  how
will “anarcho”-capitalism be  more egalitarian? That equalisation
mechanism would  be gone  (of course,  it could  be claimed  that all
great riches are purely the  result of state intervention skewing the
“free  market” but  that places  all their  “rags to  riches”
stories in  a strange position).  Thus we  have a problem,  either we
have  relative  equality  or  we  do  not.  Either  we  have  riches,
and  so  market  power,  or  we  do  not.  And  its  clear  from  the
likes of  Rothbard, “anarcho”-capitalism will not  be without its
millionaires (there is, after  all, apparently nothing un-libertarian
about <em>“organisation, hierarchy, wage-work, granting of funds by
libertarian millionaires, and a libertarian party”</em>). And so we
are left with market power and so extensive unfreedom.</p>

<p>Thus,  for   a  ideology   that  denounces  egalitarianism   as  a
<em>“revolt  against   nature”</em>  it  is  pretty   funny  that
they  paint  a  picture  of  “anarcho”-capitalism  as  a  society
of  (relative) equals.  In  other words,  their  propaganda is  based
on  something that  has  never  existed, and  never  will, namely  an
egalitarian capitalist society.</p>

<h4 id="toc32">3.2 But  what about  “anarcho”-capitalist support for charity?</h4>

<p>Yes,  while  being blind  to  impact  of  inequality in  terms  of
economic  and social  power  and  influence, most  right-libertarians
<strong>do</strong> argue that the very  poor could depend on charity
in their system.  But such a recognition of poverty  does not reflect
an awareness of the need for  equality or the impact of inequality on
the agreements we  make. Quite the reverse in fact,  as the existence
of extensive  inequality is assumed  — after  all, in a  society of
relative  equals,  poverty would  not  exist,  nor would  charity  be
needed.</p>

<p>Ignoring the fact that their ideology hardly promotes a charitable
perspective, we will raise four  points. Firstly, charity will not be
enough to countermand  the existence and impact  of vast inequalities
of wealth (and so power). Secondly,  it will be likely that charities
will be concerned with “improving”  the moral quality of the poor
and so will divide them  into the “deserving” (i.e. obedient) and
“undeserving” (i.e. rebellious) poor. Charity will be forthcoming
to the  former, those who  agree to busy-bodies sticking  their noses
into their  lives. In this way  charity could become another  tool of
economic and  social power (see  Oscar Wilde’s <strong>The  Soul of
Man Under  Socialism</strong> for  more on  charity). Thirdly,  it is
unlikely  that  charity  will  be  able to  replace  all  the  social
spending  conducted  by the  state  —  to  do  so would  require  a
ten-fold  increase  in  charitable  donations (and  given  that  most
right-libertarians denounce the government  for making them pay taxes
to help  the poor, it  seems unlikely that  they will turn  round and
<strong>increase</strong> the amount they give). And, lastly, charity
is an  implicate recognition that,  under capitalism, no one  has the
right of life — its a privilege you have to pay for. That in itself
is enough to  reject the charity option. And, of  course, in a system
designed to secure the life and liberty of each person, how can it be
deemed  acceptable to  leave  the  life and  protection  of even  one
individual  to  the charitable  whims  of  others? (Perhaps  it  will
be  argued  that individual’s  have  the  right  to life,  but  not
a  right  to  be  a  parasite. This  ignores  the  fact  some  people
<strong>cannot</strong> work  — babies and some  handicapped people
— and that, in a functioning capitalist economy, many people cannot
find work all the time. Is  it this recognition of that babies cannot
work that prompts many right-libertarians to turn them into property?
Of course, rich folk  who have never done a days  work in their lives
are  never classed  as parasites,  even if  they inherited  all their
money).  All things  considered, little  wonder that  Proudhon argued
that:</p>

<blockquote class="citation">
“Even  charitable institutions  serve the  ends of  those in
authority marvellously well.</em></p>

<p><em>“Charity is the  strongest chain by which  privilege and the
Government, bound to protect them, holds down the lower classes. With
charity,  sweeter to  the  heart  of men,  more  intelligible to  the
poor  man  than the  abstruse  laws  of  Political Economy,  one  may
dispense  with  justice.”</em>  [<strong>The General  Idea  of  the
Revolution</strong>, pp. 69–70]
</blockquote>

<p>As  noted,  the  right-libertarian  (passing)  acknowledgement  of
poverty does  not mean  that they recognise  the existence  of market
power. They  never ask themselves  how can  someone be free  if their
social situation is such that they are drowning in a see of usury and
have to sell their labour (and so liberty) to survive.</p>


<h3 id="toc33">4  What is  the right-libertarian position  on private property?</h3>

<p>Right libertarians  are not  interested in  eliminating capitalist
private property and thus  the authority, oppression and exploitation
which goes  with it.  It is  true that they  call for  an end  to the
state, but this is not because they are concerned about workers being
exploited or  oppressed but  because they don’t  want the  state to
impede capitalists’  “freedom” to  exploit and  oppress workers
even more than is the case now!</p>

<p>They  make  an  idol  of  private property  and  claim  to  defend
absolute,  “unrestricted”  property  rights (i.e.  that  property
owners can do  anything they like with their property,  as long as it
does not damage  the property of others. In  particular, taxation and
theft are among the greatest  evils possible as they involve coercion
against “justly held” property). They  agree with John Adams that
<em>“[t]he moment that idea is  admitted into society that property
is not as sacred as the Laws of God, and that there is not a force of
law and public  justice to protect it, anarchy  and tyranny commence.
Property must be sacred or liberty cannot exist.”</em></p>

<p>But in their celebration of property as the source of liberty they
ignore the fact that private property is a source of “tyranny” in
itself (see  sections B.1 and  B.4, for  example — and  please note
that anarchists only object to private property, <strong>not</strong>
individual  possession,   see  section   B.3.1).  However,   as  much
anarchists  may disagree  about  other matters,  they  are united  in
condemning  private  property.  Thus Proudhon  argued  that  property
was <em>“theft”</em>  and <em>“despotism”</em>  while Stirner
indicated the  religious and statist  nature of private  property and
its impact on individual liberty when he wrote :</p>

<blockquote class="citation">
Property in  the civic sense  means <strong>sacred</strong>
property, such that I  must <strong>respect</strong> your property...
Be it ever so little, if one only has somewhat of his own — to wit,
a <strong>respected</strong>  property: The  more such  owners... the
more ‘free people and good patriots’ has the State.
</blockquote>

<blockquote class="citation">
Political  liberalism,  like everything  religious,  counts
on  <strong>respect,</strong>  humaneness,  the  virtues  of  love...
For  in  practice people  respect  nothing,  and everyday  the  small
possessions  are bought  up  again by  greater  proprietors, and  the
‘free people’ change into day labourers.
</blockquote>

<blockquote class="citation_source">
The Ego and Its Own<i>, p. 248</i>
</blockquote>

<p>Thus “anarcho”-capitalists  reject totally  one of  the common
(and  so  defining) features  of  all  anarchist traditions  —  the
opposition to capitalist property. From Individualist Anarchists like
Tucker to  Communist-Anarchists like  Bookchin, anarchists  have been
opposed  to what  Godwin termed  <em>“accumulated property.”</em>
This  was because  it was  in <em>“direct  contradiction”</em> to
property in  the form of  <em>“the produce of his  [the worker’s]
own  industry”</em>  and  so   it  allows  <em>“one  man...  [to]
dispos[e]  of  the  produce   of  another  man’s  industry.”</em>
[<strong>The  Anarchist  Reader</strong>,  pp. 129–131]  Thus,  for
anarchists,  capitalist   property  is  a  source   exploitation  and
domination, <strong>not</strong>  freedom (it undermines  the freedom
associated with possession by created relations of domination between
owner and employee).</p>

<p>Hardly  surprising  then  the   fact  that,  according  to  Murray
Bookchin,  Murray  Rothbard  <em>“attacked   me  [Bookchin]  as  an
anarchist with vigour because, as he  put it, I am opposed to private
property.”</em> [<strong>The Raven</strong>, no. 29, p. 343]</p>

<p>We will  discuss Rothbard’s “homesteading”  justification of
property in the  next section. However, we will note  here one aspect
of right-libertarian  defence of “unrestricted”  property rights,
namely that it  easily generates evil side effects  such as hierarchy
and starvation. As famine expert Amartya Sen notes:</p>

<blockquote class="citation">
“Take a theory  of entitlements based on a set  of rights of
‘ownership, transfer and rectification.’ In  this system a set of
holdings of  different people are  judged to  be just (or  unjust) by
looking at past history, and not by checking the consequences of that
set  of  holdings. But  what  if  the consequences  are  recognisably
terrible? ...[R]efer[ing]  to some  empirical findings  in a  work on
famines ...  evidence [is presented]  to indicate that in  many large
famines in  the recent past, in  which millions of people  have died,
there was  no over-all decline in  food availability at all,  and the
famines occurred precisely because of shifts in entitlement resulting
from  exercises  of rights  that  are  perfectly legitimate...  [Can]
famines  ... occur  with  a  system of  rights  of  the kind  morally
defended in various ethical theories, including Nozick’s. I believe
the answer is  straightforwardly yes, since for many  people the only
resource that they legitimately possess, viz. their labour-power, may
well  turn out  to be  unsaleable in  the market,  giving the  person
no  command  over food  ...  [i]f  results  such as  starvations  and
famines were  to occur, would  the distribution of holdings  still be
morally acceptable  despite their  disastrous consequences?  There is
something  deeply  implausible  in  the  affirmative  answer.”</em>
[<strong>Resources, Values and Development</strong>, pp. 311–2]
</blockquote>

<p>Thus  “unrestricted” property  rights can  have seriously  bad
consequences and so the existence  of “justly held” property need
not imply  a just or free  society — far from  it. The inequalities
property can generate  can have a serious on  individual freedom (see
section  3.1). Indeed,  Murray  Rothbard argued  that  the state  was
evil not  because it  restricted individual  freedom but  because the
resources  it claimed  to own  were not  “justly” acquired.  Thus
right-libertarian theory judges  property <strong>not</strong> on its
impact on  current freedom but by  looking at past history.  This has
the  interesting  side effect  of  allowing  its supporters  to  look
at  capitalist and  statist  hierarchies,  acknowledge their  similar
negative effects on the liberty of  those subjected to them but argue
that one is  legitimate and the other is not  simply because of their
history!  As  if  this  changed the  domination  and  unfreedom  that
both  inflict on  people living  today (see  section 2.3  for further
discussion and sections  2.8 and 4.2 for other  examples of “justly
acquired” property producing terrible consequences).</p>

<p>The  defence  of capitalist  property  does  have one  interesting
side  effect,  namely  the  need  arises  to  defend  inequality  and
the  authoritarian  relationships  inequality creates.  In  order  to
protect  the  private property  needed  by  capitalists in  order  to
continue  exploiting  the  working  class,  “anarcho”-capitalists
propose  private security  forces rather  than state  security forces
(police and military)  — a proposal that is  equivalent to bringing
back the state under another name.</p>

<p>Due  to (capitalist)  private  property, wage  labour would  still
exist under  “anarcho”-capitalism (it  is capitalism  after all).
This  means  that “defensive”  force,  a  state, is  required  to
“defend” exploitation,  oppression, hierarchy and  authority from
those who suffer  them. Inequality makes a mockery  of free agreement
and “consent” (see  section 3.1). As Peter  Kropotkin pointed out
long ago:</p>

<blockquote class="citation">
“When a workman sells his labour  to an employer ... it is a
mockery  to call  that a  free contract.  Modern economists  may call
it  free,  but  the  father  of  political  economy  —  Adam  Smith
—  was  never  guilty  of  such a  misrepresentation.  As  long  as
three-quarters  of humanity  are compelled  to enter  into agreements
of  that  description,  force  is,  of  course,  necessary,  both  to
enforce  the supposed  agreements and  to  maintain such  a state  of
things.  Force  —  and  a  good deal  of  force  —  is  necessary
to  prevent  the  labourers  from  taking  possession  of  what  they
consider  unjustly appropriated  by the  few... The  Spencerian party
[proto-right-libertarians] perfectly well  understand that; and while
they advocate  no force  for changing  the existing  conditions, they
advocate still more  force than is now used for  maintaining them. As
to Anarchy, it  is obviously as incompatible with  plutocracy as with
any other  kind of  -cracy.”</em> [<strong>Anarchism  and Anarchist
Communism</strong>, pp. 52–53]
</blockquote>

<p>Because   of   this   need   to  defend   privilege   and   power,
“anarcho”-capitalism    is   best    called   “private-state”
capitalism. This will be discussed in more detail in section 6.</p>

<p>By advocating private property, right libertarians contradict many
of their  other claims. For example,  they say that they  support the
right of individuals to travel where  they like. They make this claim
because  they assume  that only  the  state limits  free travel.  But
this  is  a  false  assumption.  Owners must  agree  to  let  you  on
their land  or property  (<em>“people only have  the right  to move
to  those  properties and  lands  where  the  owners desire  to  rent
or  sell to  them.”</em>  [Murray Rothbard,  <strong>The Ethics  of
Liberty</strong>, p. 119].  There is no “freedom  of travel” onto
private property (including private roads). Therefore immigration may
be just as hard under “anarcho”-capitalism as it is under statism
(after all, the  state, like the property owner, only  lets people in
whom  it wants  to let  in). People  will still  have to  get another
property owner  to agree to  let them in  before they can  travel —
exactly as now (and,  of course, they also have to  get the owners of
the road to  let them in as  well). Private property, as  can be seen
from this simple example, is the state writ small.</p>

<p>One  last point,  this ignoring  of (“politically  incorrect”)
economic and  other views  of dead  political thinkers  and activists
while claiming them as “libertarians”  seems to be commonplace in
right-Libertarian  circles. For  example, Aristotle  (beloved by  Ayn
Rand) <em>“thought that only living things could bear fruit. Money,
not a living thing, was by its nature barren, and any attempt to make
it bear fruit  (<strong>tokos</strong>, in Greek, the  same word used
for interest) was a crime against nature.”</em> [Marcello de Cecco,
quoted  by Doug  Henwood, <strong>Wall  Street</strong>, p.  41] Such
opposition  to interest  hardly  fits well  into  capitalism, and  so
either goes unmentioned  or gets classed as  an “error” (although
we could ask why Aristotle is in error while Rand is not). Similarly,
individualist anarchist  opposition to capitalist property  and rent,
interest and profits  is ignored or dismissed  as “bad economics”
without  realising  that these  ideas  played  a  key role  in  their
politics  and in  ensuring  that  an anarchy  would  not see  freedom
corrupted by  inequality. To  ignore such an  important concept  in a
person’s ideas  is to  distort the remainder  into something  it is
not.</p>

<h4 id="toc34">4.1 What is wrong  with a “homesteading” theory of property?</h4>

<p>So how  do “anarcho”-capitalists justify property?  Looking at
Murray  Rothbard,  we find  that  he  proposes a  <em>“homesteading
theory of property”</em>. In this theory it is argued that property
comes from occupancy and mixing  labour with natural resources (which
are  assumed to  be  unowned).  Thus the  world  is transformed  into
private  property,  for <em>“title  to  an  unowned resource  (such
as  land) comes  properly  only  from the  expenditure  of labour  to
transform  that resource  into  use.”</em>  [<strong>The Ethics  of
Liberty</strong>, p. 63]</p>

<p>Rothbard paints  a conceptual history of  individuals and families
forging a  home in the wilderness  by the sweat of  their labour (its
tempting  to rename  his theory  the <em>“immaculate  conception of
property”</em> as  his conceptual theory  is somewhat at  odds with
actual historical fact).</p>

<p>Sadly  for  Murray  Rothbard, his  “homesteading”  theory  was
refuted  by Proudhon  in <strong>What  is Property?</strong>  in 1840
(along with many other  justifications of property). Proudhon rightly
argues that  <em>“if the liberty  of man  is sacred, it  is equally
sacred  in  all individuals;  that,  if  it  needs property  for  its
objective  action,  that  is,  for its  life,  the  appropriation  of
material is equally necessary for all  ... Does it not follow that if
one  individual  cannot prevent  another  ...  from appropriating  an
amount  of  material  equal  to  his own,  no  more  can  he  prevent
individuals to come.”</em>  And if all the  available resources are
appropriated, and  the owner <em>“draws boundaries,  fences himself
in ... Here, then, is a piece  of land upon which, henceforth, no one
has a  right to  step, save  the proprietor and  his friends  ... Let
[this]...  multiply, and  soon the  people ...  will have  nowhere to
rest, no place  to shelter, no ground  to till. They will  die at the
proprietor’s door,  on the  edge of that  property which  was their
birthright.”</em> [<strong>What is Property?</strong>, pp. 84–85,
p. 118]</p>

<p>As Rothbard himself  noted in respect to the  aftermath of slavery
(see section 2.1), not having access  to the means of life places one
the  position of  unjust  dependency on  those  who do.  Rothbard’s
theory fails  because for  <em>“[w]e who  belong to  the proletaire
class, property excommunicates us!”</em> [P-J Proudhon, <strong>Op.
Cit.</strong>, p.  105] and  so the vast  majority of  the population
experience property as theft and despotism rather than as a source of
liberty and empowerment (which  possession gives). Thus, Rothbard’s
account fails to  take into account the Lockean  Proviso (see section
B.3.4)  and so,  for all  its  intuitive appeal,  ends up  justifying
capitalist  and landlord  domination  (see next  section  on why  the
Lockean Proviso is important).</p>

<p>It  also seems  strange  that while  (correctly) attacking  social
contract  theories  of  the   state  as  invalid  (because  <em>“no
past  generation  can  bind  later  generations”</em>  [<strong>Op.
Cit.</strong>, p. 145])  he fails to see he  is doing <strong>exactly
that</strong> with  his support  of private property  (similarly, Ayn
Rand argued that  <em>“[a]ny alleged ‘right’ of  one man, which
necessitates the violation of the right of another, is not and cannot
be a  right”</em> [<strong>Capitalism: The  Unknown Ideal</strong>,
p. 325] but  obviously appropriating land does violate  the rights of
others to walk, use or appropriate that land). Due to his support for
appropriation  and inheritance,  he is  clearly ensuring  that future
generations  are  <strong>not</strong>  born  as free  as  the  first
settlers were (after all, they cannot appropriate any land, it is all
taken!).  If  future  generations  cannot  be  bound  by  past  ones,
this  applies equally  to  resources and  property rights.  Something
anarchists have long  realised — there is no  defensible reason why
those who  first acquired property  should control its use  by future
generations.</p>

<p>However,  if we  take Rothbard’s  theory at  face value  we find
numerous problems  with it. If  title to unowned resources  comes via
the <em>“expenditure of labour”</em> on it, how can rivers, lakes
and  the oceans  be  appropriated? The  banks of  the  rivers can  be
transformed, but  can the river itself?  How can you mix  your labour
with water? “Anarcho”-capitalists usually  blame pollution on the
fact that  rivers, oceans, and so  forth are unowned, but  how can an
individual “transform” water by  their labour? Also, does fencing
in  land  mean you  have  “mixed  labour”  with  it? If  so  then
transnational corporations  can pay workers  to fence in  vast tracks
of  virgin  land  (such  as  rainforest) and  so  come  to  “own”
it.  Rothbard  argues  that  this  is  not  the  case  (he  expresses
opposition to  <em>“arbitrary claims”</em>). He notes  that it is
<strong>not</strong> the  case that  <em>“the first  discoverer ...
could properly lay claim  to [a piece of land] ...  [by] laying out a
boundary  for the  area.”</em>  He thinks  that <em>“their  claim
would still be no more than the boundary <strong>itself</strong>, and
not to any of  the land within, for only the  boundary will have been
transformed and  used by  men”</em> [<strong>Op.  Cit.</strong>, p.
50f]</p>

<p>However,  if  the  boundary <strong>is</strong>  private  property
and  the  owner refuses  others  permission  to  cross it,  then  the
enclosed  land is  inaccessible to  others! If  an “enterprising”
right-libertarian  builds  a  fence  around   the  only  oasis  in  a
desert  and  refuses permission  to  cross  it to  travellers  unless
they   pay  his   price   (which  is   everything   they  own)   then
the  person  <strong>has</strong>   appropriated  the  oasis  without
“transforming” it by  his labour. The travellers  have the choice
of paying the price or dying (and  the oasis owner is well within his
rights letting them die). Given Rothbard’s comments, it is probable
that  he will  claim that  such a  boundary is  null and  void as  it
allows  “arbitrary”  claims —  although  this  position is  not
at  all  clear. After  all,  the  fence builder  <strong>has</strong>
transformed the  boundary and  “unrestricted” property  rights is
what right-libertarianism is all about.</p>

<p>And, of  course, Rothbard ignores  the fact of economic  power —
a  transnational  corporation  can “transform”  far  more  virgin
resources in a  day than a family could in  a year. Transnational’s
“mixing their  labour” with  the land does  not spring  into mind
reading  Rothbard’s account  of property  growth, but  in the  real
world that is what will happen.</p>

<p>If  we take  the question  of wilderness  (a topic  close to  many
eco-anarchists’ and deep ecologists’  hearts) we run into similar
problems.  Rothbard  states  clearly that  <em>“libertarian  theory
must  invalidate  [any]  claim  to  ownership”</em>  of  land  that
has  <em>“never been  transformed  from  its natural  state”</em>
(he  presents  an   example  of  an  owner  who  has   left  a  piece
of  his  <em>“legally  owned”</em> land  untouched).  If  another
person  appears  who  <strong>does</strong> transform  the  land,  it
becomes  <em>“justly  owned  by another”</em>  and  the  original
owner  cannot stop  her  (and should  the  original owner  <em>“use
violence  to prevent  another settler  from entering  this never-used
land  and  transforming  it  into  use”</em>  they  also  become  a
<em>“criminal aggressor”</em>). Rothbard also stresses that he is
<strong>not</strong> saying that  land must continually be  in use to
be  valid property  [<strong>Op. Cit.</strong>,  pp. 63–64]  (after
all,  that  would justify  landless  workers  seizing the  land  from
landowners during a depression and working it themselves).</p>

<p>Now,   where  does   that   leave  wilderness?   In  response   to
ecologists   who   oppose   the  destruction   of   the   rainforest,
“anarcho”-capitalists  suggest that  they put  their money  where
their mouth is and <strong>buy</strong> rainforest land. In this way,
it is claimed, rainforest will be  protected (see section B.5 for why
such  arguments are  nonsense). As  ecologists desire  the rainforest
<strong>because  it  is  wilderness</strong>  they  are  unlikely  to
“transform” it by  human labour (its precisely that  they want to
stop). From Rothbard’s arguments it  is fair to ask whether logging
companies have a right to “transform” the virgin wilderness owned
by ecologists, after all it  meets Rothbard’s criteria (it is still
wilderness).  Perhaps  it  will  be claimed  that  fencing  off  land
“transforms” it (hardly what you imagine “mixing labour” with
to mean, but nevermind) — but  that allows large companies and rich
individuals to  hire workers  to fence  in vast  tracks of  land (and
recreate the land  monopoly by a “libertarian” route).  But as we
noted above, fencing off land does  not seem to imply that it becomes
property in Rothbard’s theory. And,  of course, fencing in areas of
rainforest disrupts  the local  eco-system — animals  cannot freely
travel, for  example — which,  again, is what ecologists  desire to
stop. Would  Rothbard accept a  piece of paper  as “transforming”
land? We  doubt it (after  all, in  his example the  wilderness owner
<strong>did</strong> legally own it) —  and so most ecologists will
have a hard time in  “anarcho”-capitalism (wilderness is just not
an option).</p>

<p>As an aside,  we must note that Rothbard fails  to realise — and
this  comes  from his  worship  of  the  market and  his  “Austrian
economics” — is that people value many things which do not appear
on the market.  He claims that wilderness  is <em>“valueless unused
natural  objects”</em>  (for  it  people valued  them,  they  would
use  —  i.e.  appropriate  —  them). But  unused  things  may  be
of  <strong>considerable</strong> value  to people,  wilderness being
a  classic  example.  And  if  something  <strong>cannot</strong>  be
transformed  into private  property,  does that  mean  people do  not
value it?  For example, people  value community, stress  free working
environments, meaningful work — if the market cannot provide these,
does that  mean they  do not  value them? Of  course not  (see Juliet
Schor’s  <strong>The Overworked  American</strong>  on how  working
people’s desire for shorter working  hours was not transformed into
options on the market).</p>

<p>Moreover, Rothbard’s “homesteading” theory actually violates
his  support for  unrestricted property  rights. What  if a  property
owner <strong>wants</strong>  part of her land  to remain wilderness?
Their desires are violated  by the “homesteading” theory (unless,
of course,  fencing things off equals  “transforming” them, which
it  apparently  does  not).  How  can  companies  provide  wilderness
holidays to people if they have  no right to stop settlers (including
large companies) “homesteading” that  wilderness? And, of course,
where  does   Rothbard’s  theory   leave  hunter-gather   or  nomad
societies. They <strong>use</strong> the resources of the wilderness,
but they do not “transform” them  (in this case you cannot easily
tell if  virgin land  is empty  or being  used as  a resource).  If a
troop  of nomads  find  its traditionally  used,  but natural,  oasis
appropriated by a homesteader what are they to do? If they ignore the
homesteaders claims he  can call upon his “defence”  firm to stop
them — and  then, in true Rothbardian fashion,  the homesteader can
refuse  to supply  water  to them  unless they  hand  over all  their
possessions  (see  section  4.2  on  this). And  if  the  history  of
the  United States  (which is  obviously the  model for  Rothbard’s
theory) is  anything to  go by, such  people will  become “criminal
aggressors” and removed from the picture.</p>

<p>Which  is  another  problem   with  Rothbard’s  account.  It  is
completely ahistoric  (and so,  as we  noted above,  is more  like an
<em>“immaculate conception of property”</em>). He has transported
“capitalist man” into the dawn  of time and constructed a history
of property based upon what he  is trying to justify (not surprising,
as he does this with his “Natural Law” theory too — see section
7). What <strong>is</strong> interesting to note, though, is that the
<strong>actual</strong> experience  of life  on the US  frontier (the
historic example  Rothbard seems to want  to claim) was far  from the
individualistic framework  he builds upon it  and (ironically enough)
it was destroyed by the development of capitalism.</p>

<p>As  Murray Bookchin  notes, <em>“the  independence that  the New
England yeomanry  enjoyed was itself  a function of  the co-operative
social  base  from  which  it emerged.  To  barter  home-grown  goods
and  objects, to  share tools  and  implements, to  engage in  common
labour  during harvesting  time in  a system  of mutual  aid, indeed,
to  help  new-comers   in  barn-raising,  corn-husking,  log-rolling,
and  the like,  was  the indispensable  cement  that bound  scattered
farmsteads  into  a   united  community.”</em>  [<strong>The  Third
Revolution</strong>,  vol.  1,  p.  233]  Bookchin  quotes  David  P.
Szatmary  (author of  a book  on Shay’  Rebellion) stating  that it
was a  society based  upon <em>“co-operative,  community orientated
interchanges”</em>   and   not   a   <em>“basically   competitive
society.”</em> [<strong>Ibid.</strong>]</p>

<p>Into this non-capitalist society  came capitalist elements. Market
forces and economic power soon resulted in the transformation of this
society. Merchants asked for payment  in specie which (and along with
taxes)  soon  resulted  in  indebtedness  and  the  dispossession  of
the  homesteaders from  their land  and goods.  In response  Shay’s
rebellion started, a  rebellion which was an important  factor in the
centralisation of state power in America to ensure that popular input
and control  over government were  marginalised and that  the wealthy
elite and their property rights  were protected against the many (see
Bookchin, <strong>Op. Cit.</strong>, for details). Thus the homestead
system was undermined,  essentially, by the need to  pay for services
in specie (as demanded by merchants).</p>

<p>So while  Rothbard’s theory as  a certain appeal  (reinforced by
watching  too many  Westerns, we  imagine)  it fails  to justify  the
“unrestricted” property rights theory  (and the theory of freedom
Rothbard  derives from  it).  All it  does is  to  end up  justifying
capitalist and  landlord domination  (which is  probably what  it was
intended to do).</p>

<h4 id="toc35">4.2 Why is the “Lockean Proviso” important?</h4>

<p>Robert   Nozick,  in   his   work   <strong>Anarchy,  State,   and
Utopia</strong> presented a case for private property rights that was
based on what he termed the <em>“Lockean Proviso”</em> — namely
that common (or unowned) land  and resources could be appropriated by
individuals as  long as the  position of others  is not worsen  by so
doing. However, if we <strong>do</strong> take this Proviso seriously
private  property rights  cannot be  defined (see  section B.3.4  for
details).  Thus Nozick’s  arguments  in favour  of property  rights
fail.</p>

<p>Some  right-libertarians, particularly  those associated  with the
Austrian school  of economics argue  that we must reject  the Lockean
Proviso (probably  due to the  fact it can  be used to  undermine the
case for absolute  property rights). Their argument  goes as follows:
if an individual appropriates and  uses a previously unused resource,
it is because it has value to him/her, as an individual, to engage in
such  action.  The  individual  has stolen  nothing  because  it  was
previously unowned and  we cannot know if other people  are better or
worse off,  all we know  is that, for  whatever reason, they  did not
appropriate the  resource (<em>“If  latecomers are worse  off, well
then  that is  their  proper  assumption of  risk  in  this free  and
uncertain world.  There is no  longer a  vast frontier in  the United
States, and there  is no point crying over  the fact.”</em> [Murray
Rothbard, <strong>The Ethics of Liberty</strong>, p. 240]).</p>

<p>Hence   the  appropriation   of   resources   is  an   essentially
individualistic,  asocial  act —  the  requirements  of others  are
either irrelevant or unknown. However, such an argument fails to take
into  account <strong>why</strong>  the Lockean  Proviso has  such an
appeal. When  we do this  we see that  rejecting it leads  to massive
injustice, even slavery.</p>

<p>However, let us start with a defence of rejecting the Proviso from
a leading Austrian economist:</p>

<blockquote class="citation">
“Consider ... the case ... of  the unheld sole water hole in
the desert (which <strong>everyone</strong>  in a group of travellers
knows about),  which one of  the travellers,  by racing ahead  of the
others,  succeeds in  appropriating ...  [This] clearly  and unjustly
violates the  Lockean proviso...  For use, however,  this view  is by
no  means  the  only  one  possible. We  notice  that  the  energetic
traveller  who appropriated  all  the water  was  not doing  anything
which  (always  ignoring,  of  course, prohibitions  resting  on  the
Lockean  proviso  itself)  the  other  travellers  were  not  equally
free  to  do. The  other  travellers,  too,  could have  raced  ahead
...  [they] did  <strong>not</strong> bother  to race  for the  water
...  It  does  not  seem  obvious that  these  other  travellers  can
claim that  they were <strong>hurt</strong>  by an action  which they
could  themselves  have  easily  taken”</em>  [Israel  M.  Kirzner,
<em>“Entrepreneurship,  Entitlement, and  Economic Justice”</em>,
pp. 385–413, in <strong>Reading Nozick</strong>, p. 406]
</blockquote>

<p>Murray Rothbard,  we should  note, takes a  similar position  in a
similar example, arguing that <em>“the owner [of the sole oasis] is
scarcely  being  ‘coercive’; in  fact  he  is supplying  a  vital
service,  and should  have  the  right to  refuse  a  sale or  charge
whatever the customers will pay. The situation may be unfortunate for
the customers,  as are many situations  in life.”</em> [<strong>The
Ethics of  Liberty</strong>, p.  221] (Rothbard,  we should  note, is
relying to the right-libertarian von Hayek  who — to his credit —
does  maintain that  this is  a  coercive situation;  but as  others,
including other right-libertarians,  point out, he has  to change his
definition of coercion/freedom to do so — see Stephan L. Newman’s
<strong>Liberalism  at Wit’s  End</strong>,  pp.  130–134 for  an
excellent summary of this debate).</p>

<p>Now, we could be tempted just to rant about the evils of the right
libertarian mind-frame but we will try  to present a clam analysis of
this position. Now,  what Kirzner (and Rothbard et al)  fails to note
is that without  the water the other travellers will  die in a matter
of days.  The monopolist  has the  power of life  and death  over his
fellow travellers. Perhaps he hates one of them and so raced ahead to
ensure their  death. Perhaps he  just recognised the vast  power that
his appropriation  would give  him and so,  correctly, sees  that the
other travellers would give up  all their possessions and property to
him in return for enough water to survive.</p>

<p>Either way,  its clear that  perhaps the other travellers  did not
<em>“race ahead”</em>  because they were ethical  people — they
would not desire to inflict such tyranny on others because they would
not like it inflicted upon them.</p>

<p>Thus we  can answer  Kirzner’s question  — <em>“What  ... is
so  obviously  acceptable  about  the  Lockean  proviso...  ?”</em>
[<strong>Ibid.</strong>]</p>

<p>It is  the means by  which human  actions are held  accountable to
social standards and ethics. It is  the means by which the greediest,
most evil  and debased humans are  stopped from dragging the  rest of
humanity down  to their level  (via a  “race to the  bottom”) and
inflicting untold tyranny  and domination on their  fellow humans. An
ideology that could  consider the oppression which  could result from
such an appropriation as “supplying  a vital service” and any act
to remove  this tyranny  as “coercion” is  obviously a  very sick
ideology. And we may note that the right-libertarian position on this
example is a good illustration of the dangers of deductive logic from
assumptions  (see  section 1.3  for  more  on this  right-libertarian
methodology)  — after  all W.  Duncan Reekie,  in his  introduction
to  Austrian Economics,  states  that  <em>“[t]o be  intellectually
consistent one must concede his  absolute right to the oasis.”</em>
[<strong>Markets,  Entrepreneurs  and  Liberty</strong>, p.  181]  To
place ideology  before people is  to ensure  humanity is placed  on a
Procrustean bed.</p>

<p>Which  brings us  to another  point. Often  right-libertarians say
that anarchists and other socialists are “lazy” or “do not want
to work”.  You could interpret  Kirzner’s example as  saying that
the  other  travellers  are  “lazy” for  not  rushing  ahead  and
appropriating the oasis. But this  is false. For under capitalism you
can only get rich by exploiting the labour of others via wage slavery
or,  within a  company,  get  better pay  by  taking “positions  of
responsibility” (i.e. management positions). If you have an ethical
objection to treating others as  objects (“means to an end”) then
these  options are  unavailable  to you.  Thus  anarchists and  other
socialists are not “lazy” because they are not rich — they just
have no desire to  get rich off the labour and  liberty of others (as
expressed in their  opposition to private property  and the relations
of  domination it  creates). In  other  words, Anarchism  is not  the
“politics of envy”; it is the  politics of liberty and the desire
to treat others as “ends in themselves”.</p>

<p>Rothbard   is   aware   of   what   is   involved   in   accepting
the   Lockean   Proviso  —   namely   the   existence  of   private
property  (<em>“Locke’s  proviso  may  lead to  the  outlawry  of
<strong>all</strong> private  property of land, since  one can always
say  that  the  reduction  of available  land  leaves  everyone  else
...  worse off”</em>,  <strong>The Ethics  of Liberty</strong>,  p.
240  — see  section  B.3.4  for a  discussion  on  why the  Proviso
<strong>does</strong> imply  the end of capitalist  property rights).
Which is why he, and other right-libertarians, reject it. Its simple.
Either you reject the Proviso  and embrace capitalist property rights
(and so  allow one  class of  people to  be dispossessed  and another
empowered at their expense) or  you reject private property in favour
of possession  and liberty. Anarchists, obviously,  favour the latter
option.</p>

<p>As  an aside,  we should  point out  that (following  Stirner) the
would-be monopolist is doing nothing wrong (as such) in attempting to
monopolise the oasis. He is,  after all, following his self-interest.
However, what  is objectionable  is the right-libertarian  attempt to
turn thus act into a “right” which must be respected by the other
travellers. Simply put,  if the other travellers gang  up and dispose
of this  would be tyrant then  they are right  to do so —  to argue
that this  is a violation  of the monopolists “rights”  is insane
and an indication of a  slave mentality (or, following Rousseau, that
the  others are  <em>“simple”</em>). Of  course, if  the would-be
monopolist has the necessary  <strong>force</strong> to withstand the
other  travellers then  his property  then the  matter is  closed —
might makes  right. But to  worship rights, even when  they obviously
result in  despotism, is  definitely a case  of <em>“spooks  in the
head”</em>  and “man  is created  for the  Sabbath” not  “the
Sabbath is created for man.”</p>

<h4 id="toc36">4.3    How    does    private    property    effect individualism?</h4>

<p>Private property is usually associated by “anarcho”-capitalism
with individualism. Usually  private property is seen as  the key way
of ensuring  individualism and  individual freedom (and  that private
property is the expression of  individualism). Therefore it is useful
to  indicate  how private  property  can  have  a serious  impact  on
individualism.</p>

<p>Usually     right-libertarians     contrast    the     joys     of
“individualism” with the evils of “collectivism” in which the
individual is sub-merged into the group  or collective and is made to
work for the benefit of the group  (see any Ayn Rand book or essay on
the evils of collectivism).</p>

<p>But what  is ironic is  that right-libertarian ideology  creates a
view of industry  which would (perhaps) shame even  the most die-hard
fan of Stalin. What do we mean? Simply that right-libertarians stress
the abilities of the people at the top of the company, the owner, the
entrepreneur, and tend to ignore the very real subordination of those
lower  down the  hierarchy  (see, again,  any Ayn  Rand  book on  the
worship of  business leaders). In  the Austrian school  of economics,
for example, the entrepreneur is  considered the driving force of the
market process and tend to  abstract away from the organisations they
govern.  This approach  is  usually  followed by  right-libertarians.
Often  you get  the impression  that  the accomplishments  of a  firm
are  the  personal  triumphs  of the  capitalists,  as  though  their
subordinates are merely  tools not unlike the machines  on which they
labour.</p>

<p>We  should   not,  of   course,  interpret   this  to   mean  that
right-libertarians  believe that  entrepreneurs  run their  companies
single-handedly  (although you  do get  that impression  sometimes!).
But  these  abstractions help  hide  the  fact  that the  economy  is
overwhelmingly  interdependent  and organised  hierarchically  within
industry.  Even in  their primary  role as  organisers, entrepreneurs
depend  on the  group. A  company  president can  only issue  general
guidelines to his  managers, who must inevitably  organise and direct
much of  their departments on their  own. The larger a  company gets,
the  less  personal  and  direct control  an  entrepreneur  has  over
it.  They must  delegate out  an  increasing share  of authority  and
responsibility, and  is more  dependent than ever  on others  to help
him  run  things, investigate  conditions,  inform  policy, and  make
recommendations.  Moreover, the  authority  structures  are from  the
“top-down” — indeed the firm  is essentially a command economy,
with all  members part of  a collective working  on a common  plan to
achieve a common goal (i.e.  it is essentially collectivist in nature
— which  means it is  not too  unsurprising that Lenin  argued that
state socialism could be considered as one big firm or office and why
the system he built on that model was so horrific).</p>

<p>So  the firm  (the key  component  of the  capitalist economy)  is
marked by  a distinct <strong>lack</strong> of  individualism, a lack
usually ignored  by right  libertarians (or,  at best,  considered as
“unavoidable”). As  these firms  are hierarchical  structures and
workers are  paid to obey,  it does make  <strong>some</strong> sense
— in a  capitalist environment — to assume  that the entrepreneur
is the  main actor, but  as an  individualistic model of  activity it
fails totally. Perhaps it would not  be unfair to say that capitalist
individualism  celebrates the  entrepreneur because  this reflects  a
hierarchical system in  which for the one to flourish,  the many must
obey? (Also see section 1.1).</p>

<p>Capitalist individualism  does not recognise the  power structures
that  exist within  capitalism and  how they  affect individuals.  In
Brian Morris’  words, what  they fail  <em>“to recognise  is that
most  productive relations  under capitalism  allow little  scope for
creativity  and self-expression  on the  part of  workers; that  such
relationships  are not  equitable;  nor are  they  freely engaged  in
for  the  mutual  benefit  of  both  parties,  for  workers  have  no
control  over the  production process  or over  the product  of their
labour.  Rand [like  other  right-libertarians] misleadingly  equates
trade,  artistic production  and  wage-slavery... [but]  wage-slavery
...  is  quite  different  from the  trade  principle”</em>  as  it
is  a  form  of <em>“exploitation.”</em>  [<strong>Ecology  &amp;
Anarchism</strong>, p. 190]</p>

<p>He further notes that <em>“[s]o called trade relations involving
human  labour are  contrary  to  the egoist  values  Rand [and  other
capitalist  individualists]  espouses  —  they  involve  little  in
the  way  of  independence, freedom,  integrity  or  justice.”</em>
[<strong>Ibid.</strong>, p. 191]</p>

<p>Moreover,         capitalist        individualism         actually
<strong>supports</strong> authority and hierarchy. As Joshua Chen and
Joel Rogers  point out, the <em>“achievement  of short-run material
satisfaction  often  makes  it   irrational  [from  an  individualist
perspective] to engage in more  radical struggle, since that struggle
is  by definition  against those  institutions which  provide one’s
current  gain.”</em>  In  other  words,  to  rise  up  the  company
structure, to  “better oneself,” (or  even get a  good reference)
you cannot be  a pain in the side of  management — obedient workers
do well, rebel workers do not.</p>

<p>Thus    the    hierarchical    structures    help    develop    an
“individualistic”  perspective  which actually  reinforces  those
authority  structures. This,  as Cohn  and Rogers  notes, means  that
<em>“the structure  in which [workers] find  themselves yields less
than  optimal social  results  from their  isolated but  economically
rational  decisions.”</em>   [quoted  by  Alfie   Kohn,  <strong>No
Contest</strong>, p. 67, p. 260f]</p>

<p>Steve Biko, a black activist  murdered by the South African police
in  the 1970s,  argued  that  <em>“the most  potent  weapon of  the
oppressor is the mind of the oppressed.”</em> And this is something
capitalists  have  long  recognised. Their  investment  in  “Public
Relations” and “education” programmes for their employees shows
this  clearly,  as does  the  hierarchical  nature  of the  firm.  By
having a  ladder to climb,  the firm rewards obedience  and penalises
rebellion. This aims at creating  a mind-set which views hierarchy as
good and so helps produce servile people.</p>

<p>This  is  why anarchists  would  agree  with  Alfie Kohn  when  he
argues  that  <em>“the  individualist  worldview  is  a  profoundly
conservative   doctrine:   it  inherently   stifles   change.”</em>
[<strong>Ibid.</strong>, p. 67]  So, what is the best way  for a boss
to  maintain  his  or  her power?  Create  a  hierarchical  workplace
and encourage  capitalist individualism (as  capitalist individualism
actually works <strong>against</strong>  attempts to increase freedom
from  hierarchy).  Needless to  say,  such  a technique  cannot  work
forever — hierarchy also encourages  revolt — but such divide and
conquer can be <strong>very</strong> effective.</p>

<p>And  as anarchist  author Michael  Moorcock put  it, <em>“Rugged
individualism  also  goes  hand  in  hand  with  a  strong  faith  in
paternalism —  albeit a  tolerant and somewhat  distant paternalism
—  and  many  otherwise   sharp-witted  libertarians  seem  to  see
nothing in  the morality  of a  John Wayne  Western to  conflict with
their  views.  Heinlein’s  paternalism  is at  heart  the  same  as
Wayne’s... To be  an anarchist, surely, is to  reject authority but
to  accept  self-discipline and  community  responsibility.  To be  a
rugged individualist  a la  Heinlein and  others is  to be  forever a
child who must obey, charm and cajole to be tolerated by some benign,
omniscient father:  Rooster Coburn shuffling  his feet in front  of a
judge he  respects for  his office (but  not necessarily  himself) in
True Grit.”</em> [<strong>Starship Stormtroopers</strong>]</p>

<p>One last thing, don’t be fooled into thinking that individualism
or  concern about  individuality —  not <strong>quite</strong>  the
same  thing  — is  restricted  to  the  right,  they are  not.  For
example,  the <em>“individualist  theory  of society  ... might  be
advanced  in a  capitalist  or  in an  anti-capitalist  form ...  the
theory as  developed by  critics of capitalism  such as  Hodgskin and
the  anarchist  Tucker saw  ownership  of  capital  by  a few  as  an
obstacle  to  genuine  individualism,  and  the  individualist  ideal
was  realisable  only  through  the  free  association  of  labourers
(Hodgskin)  or  independent proprietorship  (Tucker).”</em>  [David
Miller, <strong>Social Justice</strong>, pp. 290–1]</p>

<p>And the reason why social  anarchists oppose capitalism is that it
creates a <strong>false</strong> individualism, an abstract one which
crushes the  individuality of the  many and justifies  (and supports)
hierarchical  and authoritarian  social  relations. In  Kropotkin’s
words, <em>“what has been called  ‘individualism’ up to now has
been  only  a  foolish  egoism which  belittles  the  individual.  It
did  not  led  to  what  it  was  established  as  a  goal:  that  is
the  complete, broad,  and most  perfectly attainable  development of
individuality.”</em>  The new  individualism  desired by  Kropotkin
<em>“will not  consist ... in  the oppression of  one’s neighbour
...  [as this]  reduced the  [individualist]  ...to the  level of  an
animal  in a  herd.”</em>  [<strong>Selected Writings</strong>,  p,
295, p. 296]</p>

<h4 id="toc37">4.4    How    does    private    property    affect relationships?</h4>

<p>Obviously,  capitalist  private   property  affects  relationships
between people by creating structures  of power. Property, as we have
argued  all  through  this  FAQ,  creates  relationships  based  upon
domination —  and this cannot  help but produce  servile tendencies
within those subject to them  (it also produces rebellious tendencies
as well, the actual ratio between the two tendencies dependent on the
individual in question and the  community they are in). As anarchists
have long recognised,  power corrupts — both those  subjected to it
and those who exercise it.</p>

<p>While  few,  if  any,  anarchists  would  fail  to  recognise  the
importance  of  possession  —  which creates  the  necessary  space
all  individuals  need to  be  themselves  —  they all  agree  that
private property corrupts this liberatory aspect of “property” by
allowing relationships of domination and oppression to be built up on
top of it. Because of this  recognition, all anarchists have tried to
equalise property and turn it back into possession.</p>

<p>Also,  capitalist individualism  actively builds  barriers between
people. Under capitalism, money  rules and individuality is expressed
via consumption choices (i.e. money). But money does not encourage an
empathy with others. As Frank Stronach (chair of Magna International,
a Canadian  auto-parts maker that  shifted its production  to Mexico)
put it,  <em>“[t]o be  in business  your first  mandate is  to make
money, and money has no heart, no soul, conscience, homeland.”</em>
[cited by Doug Henwood, <strong>Wall Street</strong>, p. 113] And for
those who  study economics,  it seems  that this  dehumanising effect
also strikes them as well:</p>

<blockquote class="citation">
Studying  economics  also  seems  to  make  you  a  nastier
person.  Psychological studies  have  shown  that economics  graduate
students are more  likely to ‘free ride’  — shirk contributions
to  an experimental  ‘public  goods’ account  in  the pursuit  of
higher  private  returns  —  than the  general  public.  Economists
also are  less generous  that other  academics in  charitable giving.
Undergraduate  economics majors  are  more likely  to  defect in  the
classic prisoner’s dilemma game that are other majors. And on other
tests, students grow  less honest — expressing less  of a tendency,
for example, to return found  money — after studying economics, but
not studying a control subject like astronomy.
</blockquote>

<blockquote class="citation">
This  is  no  surprise,  really.  Mainstream  economics  is
built entirely  on a notion of  self-interested individuals, rational
self-maximisers  who can  order  their wants  and spend  accordingly.
There’s little  room for sentiment, uncertainty,  selflessness, and
social  institutions. Whether  this  is an  accurate  picture of  the
average human  is open  to question, but  there’s no  question that
capitalism  as a  system and  economics as  a discipline  both reward
people who  conform to the model.
</blockquote>

<blockquote class="citation_author">Doug Henwood</blockquote>
<blockquote class="citation_source">Op.cit<i>, p, 143</i></blockquote>

<p>Which, of course, highlights  the problems within the “trader”
model  advocated  by  Ayn  Rand.  According to  her,  the  trader  is
<strong>the</strong>  example   of  moral  behaviour  —   you  have
something I  want, I have  something you want,  we trade and  we both
benefit and so our activity  is self-interested and no-one sacrifices
themselves   for  another.   While  this   has  <strong>some</strong>
intuitive appeal it fails to note that in the real world it is a pure
fantasy.  The  trader  wants  to  get  the  best  deal  possible  for
themselves  and if  the  bargaining positions  are  unequal then  one
person will gain at the expense  of the other (if the “commodity”
being traded  is labour, the seller  may not even have  the option of
not  trading  at  all).  The  trader is  only  involved  in  economic
exchange, and has  no concern for the welfare of  the person they are
trading with.  They are a  bearer of things,  <strong>not</strong> an
individual  with  a wide  range  of  interests, concerns,  hopes  and
dreams. These are  irrelevant, unless you can make money  out of them
of course! Thus the trader is  often a manipulator and outside novels
it most definitely is a case of “buyer beware!”</p>

<p>If  the  trader model  is  taken  as  the basis  of  interpersonal
relationships, economic gain replaces respect and empathy for others.
It replaces  human relationships  with relationships based  on things
—  and  such  a  mentality does  not  encompass  how  interpersonal
relationships affect  both you and  the society  you life in.  In the
end, it impoverishes society and individuality. Yes, any relationship
must be based upon self-interest (mutual aid is, after all, something
we do because  we benefit from it  in some way) but  the trader model
presents  such a  <strong>narrow</strong>  self-interest  that it  is
useless  and  actively impoverishes  the  very  things it  should  be
protecting  — individuality  and  interpersonal relationships  (see
section I.7.4 on how capitalism does not protect individuality).</p>

 <h4 id="toc38">4.5   Does  private  property   co-ordinate  without hierarchy?</h4>

<p>It   is  usually   to   find   right-libertarians  maintain   that
private  property  (i.e.  capitalism)  allows  economic  activity  to
be  co-ordinated  by non-hierarchical  means.  In  other words,  they
maintain that  capitalism is  a system  of large  scale co-ordination
without  hierarchy.  These  claims   follow  the  argument  of  noted
right-wing, “free market” economist Milton Friedman who contrasts
<em>“central  planning  involving  the  use  of  coercion  —  the
technique of the  army or the modern  totalitarian state”</em> with
<em>“voluntary co-operation  between individuals —  the technique
of  the marketplace”</em>  as  two distinct  ways of  co-ordinating
the economic  activity of  large groups  (<em>“millions”</em>) of
people. [<strong>Capitalism and Freedom</strong>, p. 13].</p>

<p>However, this is just playing with words. As they themselves point
out the  internal structure  of a  corporation or  capitalist company
is  <strong>not</strong>   a  “market”   (i.e.  non-hierarchical)
structure,   it  is   a  “non-market”   (hierarchical)  structure
of  a  market  participant   (see  section  2.2).  However  “market
participants” are part of the market. In other words, capitalism is
<strong>not</strong>  a  system  of co-ordination  without  hierarchy
because it does contain  hierarchical organisations which <strong>are
an essential part of the system</strong>!</p>

<p>Indeed,  the  capitalist  company <strong>is</strong>  a  form  of
central planning and shares the  same “technique” as the army. As
the  pro-capitalist writer  Peter  Drucker noted  in  his history  of
General Motors, <em>“[t]here is a remarkably close parallel between
General  Motors’  scheme  of  organisation and  those  of  the  two
institutions most renowned for administrative efficiency: that of the
Catholic Church  and that of  the modern army ...”</em>  [quoted by
David  Enger,  <strong>Apostles of  Greed</strong>,  p.  66]. And  so
capitalism is  marked by a  series of totalitarian  organisations —
and since  when was  totalitarianism liberty enhancing?  Indeed, many
“anarcho”-capitalists actually  celebrate the command  economy of
the capitalist  firm as being more  “efficient” than self-managed
firms (usually because democracy stops  action with debate). The same
argument  is applied  by the  Fascists  to the  political sphere.  It
does  not change  much  — nor  does it  become  less fascistic  —
when  applied to  economic  structures. To  state  the obvious,  such
glorification of  workplace dictatorship seems somewhat  at odds with
an ideology  calling itself “libertarian” or  “anarchist”. Is
dictatorship  more liberty  enhancing  to those  subject  to it  than
democracy? Anarchists doubt it (see section A.2.11 for details).</p>

<p>In  order   to  claim  that  capitalism   co-ordinates  individual
activity without  hierarchy right-libertarians have to  abstract from
individuals and  how they interact  <strong>within</strong> companies
and  concentrate  purely  on  relationships  <strong>between</strong>
companies. This is pure sophistry. Like markets, companies require at
least  two or  more  people to  work  — both  are  forms of  social
co-operation. If co-ordination within companies is hierarchical, then
the system  they work within is  based upon hierarchy. To  claim that
capitalism  co-ordinates without  hierarchy is  simply false  — its
based  on hierarchy  and authoritarianism.  Capitalist companies  are
based upon denying workers self-government (i.e. freedom) during work
hours. The boss tells  workers what to do, when to do,  how to do and
for how long. This denial of freedom is discussed in greater depth in
sections B.1 and B.4.</p>

<p>Because  of  the relations  of  power  it creates,  opposition  to
capitalist private  property (and so  wage labour) and the  desire to
see it ended  is an essential aspect of anarchist  theory. Due to its
ideological  blind spot  with regards  to apparently  “voluntary”
relations  of  domination and  oppression  created  by the  force  of
circumstances (see  section 2 for  details), “anarcho”-capitalism
considers  wage  labour   as  a  form  of  freedom   and  ignore  its
fascistic  aspects   (when  not  celebrating  those   aspects).  Thus
“anarcho”-capitalism  is  not   anarchist.  By  concentrating  on
the  moment the  contract  is  signed, they  ignore  that freedom  is
restricted during  the contract itself. While  denouncing (correctly)
the totalitarianism of the army, they ignore it in the workplace. But
factory  fascism  is  just  as  freedom destroying  as  the  army  or
political fascism.</p>

<p>Due    to   this    basic   lack    of   concern    for   freedom,
“anarcho”-capitalists cannot  be considered as  anarchists. Their
total  lack  of concern  about  factory  fascism (i.e.  wage  labour)
places them totally outside  the anarchist tradition. Real anarchists
have  always been  aware of  that  private property  and wage  labour
restriction freedom and  desired to create a society  in which people
would be able to avoid it. In other words, where <strong>all</strong>
relations are non-hierarchical and truly co-operative.</p>

<p>To conclude,  to claim that private  property eliminates hierarchy
is  false.  Nor  does   capitalism  co-ordinate  economic  activities
without hierarchical  structures. For this reason  anarchists support
co-operative forms of production rather than capitalistic forms.</p>

<h3 id="toc39">5 Will privatising “the commons” increase liberty?</h3>

<p>“Anarcho”-capitalists    aim    for   a    situation    in
which   <em>“no   land   areas,   no  square   footage   in   the
world   shall   remain   ‘public,’”</em>   in   other   wordsp
<strong>everything</strong>   will  be   <em>“privatised.”</em>
[Murray Rothbard, <strong>Nations by  Consent</strong>, p. 84] They 
claim that privatising “the  commons” (e.g. roads, parks, etc.) 
which are  now freely  available to all  will increase  liberty. Is
this true?  We have shown  before why the claim  that privatisation
can  protect the  environment  is highly  implausible (see  section
E.2).  Here we  will concern  ourselves with  private ownership  of
commonly used “property” which we  all take for granted and pay
for with taxes.</p>

<p>Its  clear  from even  a  brief  consideration of  a  hypothetical
society  based  on “privatised”  roads  (as  suggested by  Murray
Rothbard  in <strong>For  a New  Liberty</strong>, pp.  202–203 and
David  Friedman in  <strong>The  Machinery  of Freedom</strong>,  pp.
98–101) that  the only increase of  liberty will be for  the ruling
elite. As  “anarcho”-capitalism is based  on paying for  what one
uses, privatisation  of roads would  require some method  of tracking
individuals to  ensure that they pay  for the roads they  use. In the
UK, for example, during the  1980s the British Tory government looked
into the  idea of toll-based motorways.  Obviously having toll-booths
on motorways would  hinder their use and  restrict “freedom,” and
so they  came up with the  idea of tracking cars  by satellite. Every
vehicle would have a tracking device  installed in it and a satellite
would record where people went and  which roads they used. They would
then be sent a bill or have their bank balances debited based on this
information (in the fascist city-state/company town of Singapore such
a scheme <strong>has</strong> been introduced).</p>

<p>If   we   extrapolate  from   this   example   to  a   system   of
<strong>fully</strong>  privatised  “commons,” it  would  clearly
require  all individuals  to have  tracking devices  on them  so they
could be properly billed for  use of roads, pavements, etc. Obviously
being  tracked  by  private  firms  would  be  a  serious  threat  to
individual liberty. Another, less costly, option would be for private
guards to  randomly stop and  question car-owners and  individuals to
make  sure they  had paid  for the  use of  the road  or pavement  in
question. “Parasites” would  be arrested and fined  or locked up.
Again, however, being stopped and questioned by uniformed individuals
has more  in common  with police  states than  liberty. Toll-boothing
<strong>every</strong> street  would be highly unfeasible  due to the
costs involved  and difficulties  for use that  it implies.  Thus the
idea of privatising  roads and charging drivers to  gain access seems
impractical at best and distinctly freedom endangering if implemented
at worse.</p>

<p>Of course, the option of owners  letting users have free access to
the roads and pavements they construct and run would be difficult for
a profit-based company.  No one could make a profit  in that case. If
companies paid  to construct  roads for their  customers/employees to
use, they  would be  financially hindered  in competition  with other
companies that did not, and thus would  be unlikely to do so. If they
restricted use  purely to their  own customers, the  tracking problem
appears again.</p>

<p>Some may  object that  this picture  of extensive  surveillance of
individuals  would  not  occur  or  be  impossible.  However,  Murray
Rothbard  (in a  slightly different  context) argued  that technology
would  be  available to  collate  information  about individuals.  He
argued that <em>“[i]t should be  pointed out that modern technology
makes  even  more  feasible   the  collection  and  dissemination  of
information about people’s credit ratings and records of keeping or
violating  their  contracts  or arbitration  agreements.  Presumably,
an  anarchist  [sic!]  society  would   see  the  expansion  of  this
sort  of dissemination  of  data.”</em>  [<em>“Society Without  A
State”</em>,  in <strong>Nomos  XIX</strong>,  Pennock and  Chapman
(eds.), p. 199] So, perhaps,  with the total privatisation of society
we  would also  see  the  rise of  private  Big Brothers,  collecting
information about individuals for use by property owners. The example
of  the  <strong>Economic  League</strong>  (a  British  company  who
provided the “service” of tracking the political affiliations and
activities of workers for employers) springs to mind.</p>

<p>And, of course, these privatisation suggestions ignore differences
in income and market power. If, for example, variable pricing is used
to discourage road use at times  of peak demand (to eliminate traffic
jams at rush-hour) as is suggested  both by Murray Rothbard and David
Friedman, then  the rich will  have far more “freedom”  to travel
than the rest of the population. And we may even see people having to
go into debt just to get to work or move to look for work.</p>

<p>Which raises  another problem with notion  of total privatisation,
the problem that it implies the  end of freedom of travel. Unless you
get permission  or (and this seems  more likely) pay for  access, you
will  not be  able to  travel <strong>anywhere.</strong>  As Rothbard
<strong>himself</strong> makes  clear, “anarcho”-capitalism means
the  end  of  the right  to  roam  or  even  travel. He  states  that
<em>“it became clear to me  that a totally privatised country would
not  have  open  borders  at  all.  If  every  piece  of  land  in  a
country  were  owned  ...  no  immigrant  could  enter  there  unless
invited to enter and allowed  to rent, or purchase, property.”</em>
[<strong>Nations by  Consent</strong>, p.  84] What happens  to those
who cannot <strong>afford</strong> to pay for access is not addressed
(perhaps, being unable to exit  a given capitalist’s land they will
become  bonded  labourers? Or  be  imprisoned  and used  to  undercut
workers’ wages  via prison labour?  Perhaps they will just  be shot
as  trespassers?  Who  can  tell?).  Nor is  it  addressed  how  this
situation actually <strong>increases</strong>  freedom. For Rothbard,
a  <em>“totally  privatised  country  would be  as  closed  as  the
particular inhabitants and  property owners [<strong>not</strong> the
same thing, we must point out] desire. It seems clear, then, that the
regime of open  borders that exists <strong>de  facto</strong> in the
US really amounts to a compulsory opening by the central state... and
does  not genuinely  reflect the  wishes of  the proprietors.”</em>
[<strong>Op.  Cit.</strong>,   p.  85]  Of  course,   the  wishes  of
<strong>non</strong>-proprietors (the vast majority) do not matter in
the  slightest. Thus,  it is  clear, that  with the  privatisation of
“the  commons” the  right  to  roam, to  travel,  would become  a
privilege, subject to the laws and rules of the property owners. This
can hardly  be said  to <strong>increase</strong> freedom  for anyone
bar the capitalist class.</p>

<p>Rothbard  acknowledges that  <em>“in a  fully privatised  world,
access   rights  would   obviously  be   a  crucial   part  of   land
ownership.”</em> [<strong>Nations by Consent</strong>, p. 86] Given
that there is no free lunch, we  can imagine we would have to pay for
such “rights.” The implications of this are obviously unappealing
and an  obvious danger to  individual freedom. The problem  of access
associated with the idea of privatising the roads can only be avoided
by  having  a “right  of  passage”  encoded into  the  “general
libertarian  law code.”  This  would mean  that  road owners  would
be  required,  by  law,  to  let  anyone  use  them.  But  where  are
“absolute” property rights in this  case? Are the owners of roads
not  to have  the same  rights as  other owners?  And if  “right of
passage” is  enforced, what  would this mean  for road  owners when
people sue  them for car-pollution  related illnesses? (The  right of
those  injured  by  pollution  to  sue  polluters  is  the  main  way
“anarcho”-capitalists  propose to  protect  the environment.  See
sections E.2  and E.3). It  is unlikely  that those wishing  to bring
suit  could find,  never mind  sue,  the millions  of individual  car
owners who  could have  potentially caused  their illness.  Hence the
road-owners would be sued for letting polluting (or unsafe) cars onto
“their” roads. The road-owners would therefore desire to restrict
pollution levels by restricting the  right to use their property, and
so  would resist  the “right  of passage”  as an  “attack” on
their “absolute”  property rights.  If the road-owners  got their
way (which would  be highly likely given the  need for “absolute”
property rights and is suggested by the variable pricing way to avoid
traffic jams mentioned above) and were able to control who used their
property, freedom to travel would be <strong>very</strong> restricted
and  limited  to those  whom  the  owner considered  “desirable.”
Indeed, Murray  Rothbard supports such  a regime (<em>“In  the free
[sic!] society, they [travellers] would,  in the first instance, have
the right to travel only on  those streets whose owners agree to have
them there”</em> [<strong>The Ethics of Liberty</strong>, p. 119]).
The threat  to liberty  in such a  system is obvious  — to  all but
Rothbard and other right-libertarians, of course.</p>

<p>To  take another  example, let  us consider  the privatisation  of
parks, streets and other public areas. Currently, individuals can use
these  areas to  hold  political demonstrations,  hand out  leaflets,
picket and so on.  However, under “anarcho”-capitalism the owners
of such property can restrict  such liberties if they desire, calling
such  activities  “initiation  of force”  (although  they  cannot
explain  how  speaking  your  mind is  an  example  of  “force”).
Therefore, freedom of speech, assembly  and a host of other liberties
we take  for granted  would be  reduced (if  not eliminated)  under a
right-“libertarian” regime.  Or, taking  the case of  pickets and
other forms  of social  struggle, its  clear that  privatising “the
commons” would only benefit the bosses. Strikers or other activists
picketing or handing out leaflets  in shopping centre’s are quickly
ejected by private security even today. Think about how much worse it
would  become under  “anarcho”-capitalism  when  the whole  world
becomes a series of malls — it would be impossible to hold a picket
when  the owner  of the  pavement objects,  for example  (as Rothbard
himself argues, <strong>Op.  Cit.</strong>, p. 132) and  if the owner
of  the  pavement  also  happens  to  be  the  boss  being  picketed,
then  workers’ rights  would be  zero.  Perhaps we  could also  see
capitalists  suing working  class organisations  for littering  their
property if they do hand out leaflets (so placing even greater stress
on limited resources).</p>

<p>The  I.W.W. went  down  in  history for  its  rigorous defence  of
freedom of  speech because  of its  rightly famous  “free speech”
fights  in  numerous  American   cities  and  towns.  Repression  was
inflicted  upon  wobblies  who  joined  the  struggle  by  “private
citizens,” but in  the end the I.W.W. won. Consider  the case under
“anarcho”-capitalism.  The wobblies  would have  been “criminal
aggressors”  as the  owners of  the streets  have refused  to allow
“undesirables” to use  them to argue their case.  If they refused
to acknowledge the decree of  the property owners, private cops would
have taken them away. Given that those who controlled city government
in the historical  example were the wealthiest citizens  in town, its
likely that the same people would have been involved in the fictional
(“anarcho”-capitalist) account.  Is it a  good thing that  in the
real account the wobblies are hailed  as heroes of freedom but in the
fictional  one they  are “criminal  aggressors”? Does  converting
public  spaces  into  private property  <strong>really</strong>  stop
restrictions on free speech being a bad thing?</p>

<p>Of  course,  Rothbard  (and other  right-libertarians)  are  aware
that  privatisation  will  not  remove  restrictions  on  freedom  of
speech, association  and so on  (while, at  the same time,  trying to
portray themselves  as supporters  of such liberties!).  However, for
right-libertarians  such  restrictions  are  of  no  consequence.  As
Rothbard argues, any <em>“prohibitions  would not be state imposed,
but would  simply be requirements  for residence  or for use  of some
person’s  or community’s  land area.”</em>  [<strong>Nations by
Consent</strong>,  p. 85]  Thus we  yet  again see  the blindness  of
right-libertarians to  the commonality  between private  property and
the state. The state also  maintains that submitting to its authority
is the requirement for taking up residence in its territory (see also
section  2.3  for  more  on  this). As  Benjamin  Tucker  noted,  the
state  can be  defined as  (in  part) <em>“the  assumption of  sole
authority over a  given area and all  within it.”</em> [<strong>The
Individualist Anarchists</strong>, p. 24]  If the property owners can
determine “prohibitions” (i.e. laws and  rules) for those who use
the property  then they  are the <em>“sole  authority over  a given
area and all within it,”</em> i.e. a state. Thus privatising “the
commons” means subjecting the non-property  owners to the rules and
laws of the property owners —  in effect, privatising the state and
turning the world into a series of Monarchies and oligarchies without
the pretence of democracy and democratic rights.</p>

<p>These examples  can hardly  be said to  be increasing  liberty for
society  as  a  whole,  although “anarcho”  capitalists  seem  to
think  they would.  So far  from <strong>increasing</strong>  liberty
for  all,  then,  privatising  the commons  would  only  increase  it
for  the ruling  elite,  by  giving them  yet  another monopoly  from
which  to collect  income and  exercise  their power  over. It  would
<strong>reduce</strong> freedom for everyone  else. As Peter Marshall
notes, <em>“[i]n the name of freedom, the anarcho-capitalists would
like to  turn public spaces  into private property, but  freedom does
not  flourish  behind  high  fences protected  by  private  companies
but  expands in  the  open  air when  it  is  enjoyed by  all”</em>
[<strong>Demanding the Impossible</strong>, p. 564].</p>

<p>Little wonder Proudhon argued that <em>“if the public highway is
nothing but an  accessory of private property; if  the communal lands
are converted into private property;  if the public domain, in short,
is guarded,  exploited, leased,  and sold  like private  property —
what  remains for  the proletaire?  Of what  advantage is  it to  him
that  society has  left  the state  of  war to  enter  the regime  of
police?”</em> [<strong>System  of Economic Contradictions</strong>,
p. 371]</p>

<h3 id="toc40">6 Is “anarcho”-capitalism against the state?</h3>

<p>No. Due to its basis in private property, “anarcho”-capitalism
implies  a class  division of  society into  bosses and  workers. Any
such  division will  require  a  state to  maintain  it. However,  it
need  not be  the same  state as  exists now.  Regarding this  point,
“anarcho”-capitalism plainly advocates “defence associations”
to  protect  property.  For  the  “anarcho”-capitalist,  however,
these private  companies are  not states.  For anarchists,  they most
definitely are.</p>

<p>According   to   Murray   Rothbard   [<em>“Society   Without   A
State”</em>,  in <strong>Nomos  XIX</strong>, Pennock  and Chapman,
eds.,  p. 192.],  a state  must  have one  or both  of the  following
characteristics:</p>

<ol>
<li>The ability  to  tax  those who  live  within it.</li>
<li>It  asserts  and  usually  obtains  a  coerced  monopoly  of  the
provision of defence over a given area.</li>
</ol>

<p>He makes the same point  in <strong>The Ethics of Liberty</strong>
[p. 171].</p>

<p>Instead of  this, the “anarcho”-capitalist thinks  that people
should be  able to  select their  own “defence  companies” (which
would provide the  needed police) and courts from the  free market in
“defence” which would spring up after the state monopoly has been
eliminated. These companies <em>“all... would  have to abide by the
basic law  code”</em> [<em>“Society  Without A  State”</em>, p.
206]. Thus a <em>“general libertarian law code”</em> would govern
the actions  of these companies.  This “law code”  would prohibit
coercive aggression  at the very  least, although  to do so  it would
have to specify what counted as  legitimate property, how said can be
owned and  what actually  constitutes aggression.  Thus the  law code
would be quite extensive.</p>

<p>How is  this law code to  be actually specified? Would  these laws
be  democratically decided?  Would  they reflect  common usage  (i.e.
custom)? “supply and demand”? “Natural law”? Given the strong
dislike of democracy shown  by “anarcho”-capitalists, we think we
can safely  say that some combination  of the last two  options would
be  used. Murray  Rothbard,  as  noted in  section  1.4, opposed  the
individualist anarchist  principle that  juries would judge  both the
facts and the law, suggesting instead that <em>“Libertarian lawyers
and jurists”</em>  would determine a <em>“rational  and objective
code  of libertarian  legal  principles  and procedures.”</em>  The
judges in his system would <em>“not [be] making the law but finding
it  on  the  basis  of agreed-upon  principles  derived  either  from
custom  or reason.”</em>  [<em>“Society without  a State”</em>,
<strong>Op. Cit.</strong>, p. 206] David Friedman, on the other hand,
argues  that  different defence  firms  would  sell their  own  laws.
[<strong>The Machinery  of Freedom</strong>, p. 116]  It is sometimes
acknowledged that non-libertarian laws may be demanded (and supplied)
in such a market.</p>

<p>Around this system of “defence  companies” is a free market in
“arbitrators” and  “appeal judges” to administer  justice and
the “basic law code.” Rothbard  believes that such a system would
see <em>“arbitrators  with the  best reputation for  efficiency and
probity...[being] chosen by the various parties in the market...[and]
will  come to  be  given an  increasing  amount of  business.”</em>
[Rothbard,  <strong>Op.  Cit.</strong>,   p.199]  Judges  <em>“will
prosper  on  the  market  in   proportion  to  their  reputation  for
efficiency and  impartiality.”</em> [<strong>Op.  Cit.</strong>, p.
204]</p>

<p>Therefore,  like  any  other  company,  arbitrators  would  strive
for  profits  and wealth,  with  the  most successful  ones  becoming
<em>“prosperous.”</em>  Of  course,  such wealth  would  have  no
impact on the decisions of the  judges, and if it did, the population
(in theory) are free to select  any other judge (although, of course,
they  would also  <em>“strive  for profits  and wealth”</em>  —
which means the choice of character  may be somewhat limited! — and
the laws  which they were  using to  guide their judgements  would be
enforcing capitalist rights).</p>

<p>Whether or not  this system would work as desired  is discussed in
the following sections. We think that  it will not. Moreover, we will
argue that “anarcho”-capitalist  “defence companies” meet not
only the criteria  of statehood we outlined in section  B.2, but also
Rothbard’s own criteria for the state, quoted above.</p>

<p>As regards  the anarchist criterion,  it is clear  that “defence
companies”  exist  to  defend   private  property;  that  they  are
hierarchical (in that they are  capitalist companies which defend the
power of those who employ  them); that they are professional coercive
bodies; and that they exercise a  monopoly of force over a given area
(the area, initially, being the property of the person or company who
is employing the  “association”). If, as Ayn Rand  noted (using a
Weberian  definition of  the state)  a government  is an  institution
<em>“that  holds the  exclusive  power to  <strong>enforce</strong>
certain  rules  of  conduct  in  a  given  geographical  area”</em>
[<strong>Capitalism: The  Unknown Ideal</strong>, p. 239]  then these
“defence companies”  are the  means by  which the  property owner
(who  exercises a  monopoly to  determine the  rules governing  their
property) enforce their rules.</p>

<p>For   this    (and   other   reasons),   we    should   call   the
“anarcho”-capitalist defence firms  “private states” — that
is what they are — and “anarcho”-capitalism “private state”
capitalism.</p>

<p>Before discussing these  points further, it is  necessary to point
out a relatively common fallacy of “anarcho”-capitalists. This is
the  idea that  “defence” under  the system  they advocate  means
defending people, not  territorial areas. This, for  some, means that
defence  companies  are  not  “states.” However,  as  people  and
their  property  and  possessions  do not  exist  merely  in  thought
but  on  the Earth,  it  is  obvious  that  these companies  will  be
administering “justice”  over a given  area of the planet.  It is
also obvious,  therefore, that these “defence  associations” will
operate  over a  (property-owner defined)  area of  land and  enforce
the  property-owner’s  laws,  rules  and  regulations.  The  deeply
anti-libertarian, indeed fascistic, aspects of this “arrangement”
will be examined in the following sections.</p>

<h4 id="toc41">6.1  What’s  wrong   with  this  “free  market” justice?</h4>

<p>It does  not take much  imagination to figure out  whose interests
<em>“prosperous”</em> arbitrators,  judges and  defence companies
would defend:  their own, as  well as those  who pay their  wages —
which is to say,  other members of the rich elite.  As the law exists
to  defend property,  then it  (by definition)  exists to  defend the
power of capitalists against their workers.</p>

<p>Rothbard  argues that  the <em>“judges”</em>  would <em>“not
[be]  making the  law  but finding  it on  the  basis of  agreed-upon
principles derived  either from  custom or  reason”</em> [Rothbard,
<strong>Op. Cit.</strong>, p. 206].  However, this begs the question:
<strong>whose</strong>  reason?  <strong>whose</strong>  customs?  Do
individuals in  different classes  share the  same customs?  The same
ideas of right and wrong? Would rich  and poor desire the same from a
<em>“basic  law code”</em>?  Obviously not.  The rich  would only
support a code which defended their power over the poor.</p>

<p>Although only <em>“finding”</em> the  law, the arbitrators and
judges  still exert  an influence  in the  “justice” process,  an
influence  not impartial  or neutral.  As the  arbitrators themselves
would be  part of  a profession,  with specific  companies developing
within  the  market, it  does  not  take  a  genius to  realise  that
when  “interpreting” the  “basic  law  code,” such  companies
would  hardly  act  against  their own  interests  as  companies.  In
addition, if the “justice” system was based on “one dollar, one
vote,”  the  “law”  would  best  defend  those  with  the  most
“votes”  (the question  of  market forces  will  be discussed  in
section  6.3). Moreover,  even  if “market  forces” would  ensure
that  “impartial”  judges  were  dominant, all  judges  would  be
enforcing a  <strong>very</strong> partial law code  (namely one that
defended  <strong>capitalist</strong> property  rights). Impartiality
when enforcing partial laws hardly makes judgements less unfair.</p>

<p>Thus,  due  to   these  three  pressures  —   the  interests  of
arbitrators/judges, the influence of money  and the nature of the law
— the terms of “free agreements”  under such a law system would
be tilted in favour of  lenders over debtors, landlords over tenants,
employers over  employees, and  in general, the  rich over  the poor,
just as  we have  today. This is  what one would  expect in  a system
based on  “unrestricted” property rights and  a (capitalist) free
market. A similar  tendency towards the standardisation  of output in
an industry in response to influences  of wealth can be seen from the
current media system  (see section D.3 — How  does wealth influence
the mass media?)</p>

<p>Some  “anarcho”-capitalists,  however,   claim  that  just  as
cheaper  cars  were developed  to  meet  demand, so  cheaper  defence
associations and  “people’s arbitrators”  would develop  on the
market  for the  working  class.  In this  way  impartiality will  be
ensured. This argument overlooks a few key points:</p>

<p>Firstly,  the   general  “libertarian”   law  code   would  be
applicable to  <strong>all</strong> associations, so they  would have
to  operate  within  a  system  determined  by  the  power  of  money
and  of capital.  The  law code  would  reflect, therefore,  property
<strong>not</strong>  labour  and   so  “socialistic”  law  codes
would  be  classed as  “outlaw”  ones.  The options  then  facing
working  people  is  to  select   a  firm  which  best  enforced  the
<strong>capitalist</strong> law in their  favour. And as noted above,
the impartial  enforcement of  a biased law  code will  hardly ensure
freedom or justice for all.</p>

<p>Secondly, in a race between a  Jaguar and a Volkswagen Beetle, who
is more likely to win? The  rich would have “the best justice money
can buy,” as they do now.  Members of the capitalist class would be
able to select the firms with the best lawyers, best private cops and
most resources. Those without the financial clout to purchase quality
“justice” would simply be out of luck — such is the “magic”
of the marketplace.</p>

<p>Thirdly,   because   of   the   tendency   toward   concentration,
centralisation,  and oligopoly  under capitalism  (due to  increasing
capital  costs for  new firms  entering the  market, as  discussed in
section C.4), a few companies would soon dominate the market — with
obvious implications for “justice.”</p>

<p>Different firms will have different  resources. In other words, in
a conflict between a small firm and  a larger one, the smaller one is
at  a disadvantage  in  terms of  resources.  They may  not  be in  a
position to fight the larger company if it rejects arbitration and so
may  give  in simply  because,  as  the “anarcho”-capitalists  so
rightly point out,  conflict and violence will push  up a company’s
costs and so  they would have to be avoided  by smaller companies. It
is ironic  that the “anarcho”-capitalist implicitly  assumes that
every “defence company”  is approximately of the  same size, with
the  same resources  behind  it.  In real  life,  this would  clearly
<strong>not</strong> the case.</p>

<p>Fourthly, it  is <strong>very</strong> likely that  many companies
would make subscription  to a specific “defence” firm  or court a
requirement of employment. Just as today many (most?) workers have to
sign no-union  contracts (and face  being fired if they  change their
minds), it does not take much  imagination to see that the same could
apply  to “defence”  firms and  courts. This  was/is the  case in
company  towns  (indeed,  you  can  consider  unions  as  a  form  of
“defence” firm and these companies refused to recognise them). As
the labour  market is  almost always  a buyer’s  market, it  is not
enough  to  argue that  workers  can  find  a  new job  without  this
condition. They  may not and so  have to put up  with this situation.
And if  (as seems likely)  the laws  and rules of  the property-owner
will take precedence  in any conflict, then workers  and tenants will
be at a disadvantage no matter how “impartial” the judges.</p>

<p>Ironically,  some “anarcho”-capitalists  point to  current day
company/union  negotiations as  an example  of how  different defence
firms would  work out  their differences  peacefully. Sadly  for this
argument, union rights under  “actually existing capitalism” were
created and enforced by the  state in direct opposition to capitalist
“freedom of  contract.” Before the  law was changed,  unions were
often  crushed by  force —  the  companies were  better armed,  had
more  resources and  had  the  law on  their  side.  Today, with  the
“downsizing” of companies we can  see what happens to “peaceful
negotiation”  and “co-operation”  between unions  and companies
when it is no longer required  (i.e. when the resources of both sides
are unequal). The market power of  companies far exceeds those of the
unions  and the  law, by  definition,  favours the  companies. As  an
example of  how competing “protection  agencies” will work  in an
“anarcho”-capitalist  society, it  is  far  more insightful  than
originally intended!</p>

<p>Now  let us  consider  the <em>“basic  law code”</em>  itself.
How  the  laws in  the  <em>“general  libertarian law  code”</em>
would  actually  be  selected  is  anyone’s  guess,  although  many
“anarcho”-capitalists support the myth  of “natural law,” and
this  would  suggest  an  unchangeable law  code  selected  by  those
considered  as “the  voice  of  nature” (see  section  11. for  a
discussion of its authoritarian  implications). David Friedman argues
that as well as  a market in defence companies, there  will also be a
market in  laws and rights.  However, there will be  extensive market
pressure to  unify these  differing law codes  into one  standard one
(imagine what would  happen if ever CD manufacturer  created a unique
CD player, or every computer manufacturer different sized floppy-disk
drivers — little wonder, then, that over time companies standardise
their products).  Friedman himself acknowledges that  this process is
likely (and uses the example of standard paper sizes to indicate such
a process).</p>

<p>In any event, the laws would not be decided on the basis of “one
person, one vote”; hence, as  market forces worked their magic, the
“general”  law code  would  reflect vested  interests  and so  be
very  hard  to change.  As  rights  and  laws  would be  a  commodity
like  everything else  in  capitalism, they  would  soon reflect  the
interests of the rich —  particularly if those interpreting the law
are  wealthy professionals  and  companies with  vested interests  of
their own.  Little wonder that the  individualist anarchists proposed
“trial by  jury” as  the only  basis for real  justice in  a free
society. For,  unlike professional  “arbitrators,” juries  are ad
hoc, made up of ordinary people  and do not reflect power, authority,
or the  influence of wealth.  And by being able  to judge the  law as
well as  a conflict, they can  ensure a populist revision  of laws as
society progresses.</p>

<p>Thus  a system  of “defence”  on the  market will  continue to
reflect the influence and power of property owners and wealth and not
be subject  to popular control  beyond choosing between  companies to
enforce the capitalist laws.</p>

<h4 id="toc42">6.2  What  are  the social  consequences  of  such  a system?</h4>

<p>The “anarcho”  capitalist imagines  that there will  be police
agencies, “defence associations,” courts,  and appeals courts all
organised on  a free-market  basis and available  for hire.  As David
Weick points out, however, the major problem with such a system would
not  be the  corruption  of “private”  courts  and police  forces
(although, as suggested above, this could indeed be a problem):</p>

<blockquote class="citation">
There is something more serious than the ‘Mafia danger’, and this
other problem concerns the role of such ‘defence’ institutions in
a given social and economic context.
</blockquote>

<blockquote class="citation">
[The] context...  is one of  a free-market economy  with no
restraints upon  accumulation of  property. Now,  we had  an American
experience, roughly from the end of the Civil War to the 1930’s, in
what were  in effect private  courts, private police,  indeed private
governments. We had the experience  of the (private) Pinkerton police
which, by its spies, by its <strong>agents provocateurs,</strong> and
by methods that included violence and kidnapping, was one of the most
powerful tools of large corporations  and an instrument of oppression
of working people. We had the experience as well of the police forces
established  to  the  same  end,  within  corporations,  by  numerous
companies... (The  automobile companies  drew upon  additional covert
instruments of  a private nature,  usually termed vigilante,  such as
the Black  Legion). These were,  in effect, private armies,  and were
sometimes described as such. The territories owned by coal companies,
which frequently included entire towns and their environs, the stores
the miners were obliged by economic coercion to patronise, the houses
they lived  in, were commonly  policed by  the private police  of the
United  States  Steel  Corporation  or  whatever  company  owned  the
properties.  The chief  practical function  of these  police was,  of
course, to prevent labour organisation and preserve a certain balance
of ‘bargaining.’
</blockquote>

<blockquote class="citation">
These  complexes  were  a  law  unto  themselves,  powerful
enough  to ignore,  when they  did not  purchase, the  governments of
various jurisdictions of the American federal system. This industrial
system was, at the  time, often characterised as feudalism...”
</blockquote>

<blockquote class="citation_source">
<i>«&nbsp;Anarchist  Justice&nbsp;»,</i> Op.cit.<i>,  pp. 223–224</i>
</blockquote>

<p>For a description of the  weaponry and activities of these private
armies, the  economic historian  Maurice Dobbs presents  an excellent
summary   in  <strong>Studies   in  Capitalist   Development</strong>
[pp.  353–357].  According to  a  report  on <em>“Private  Police
Systems”</em> cited  by Dobbs,  in a  town dominated  by Republican
Steel,  the <em>“civil  liberties  and the  rights  of labour  were
suppressed by  company police.  Union organisers  were driven  out of
town.”</em>  Company  towns  had  their  own  (company-run)  money,
stores, houses and  jails and many corporations  had machine-guns and
tear-gas along  with the usual  shot-guns, rifles and  revolvers. The
<em>“usurpation of  police powers  by privately paid  ‘guards and
‘deputies’,  often  hired  from  detective  agencies,  many  with
criminal records”</em> was <em>“a  general practice in many parts
of the country.”</em></p>

<p>The local (state-run) law  enforcement agencies turned a blind-eye
to what  was going  on (after  all, the  workers <strong>had</strong>
broken their contracts and  so were “criminal aggressors” against
the companies) even  when union members and strikers  were beaten and
killed.  The workers  own defence  organisations were  the only  ones
willing to  help them, and if  the workers seemed to  be winning then
troops were called in to “restore  the peace” (as happened in the
Ludlow strike,  when strikers originally  cheered the troops  as they
thought they would  defend their civil rights; needless  to say, they
were wrong).</p>

<p>Here   we   have   a   society    which   is   claimed   by   many
“anarcho”-capitalists  as one  of the  closest examples  to their
“ideal,” with limited state intervention, free reign for property
owners, etc. What  happened? The rich reduced the working  class to a
serf-like  existence,  capitalist production  undermined  independent
producers (much to  the annoyance of individualist  anarchists at the
time), and the result was the emergence of the corporate America that
“anarcho”-capitalists say they oppose.</p>

<p>Are we to expect  that “anarcho”-capitalism will be different?
That, unlike before, “defence” firms  will intervene on behalf of
strikers? Given that  the “general libertarian law  code” will be
enforcing capitalist property rights, workers  will be in exactly the
same  situation as  they  were then.  Support  of strikers  violating
property rights  would be a  violation of the  “general libertarian
law  code” and  be costly  for profit  making firms  to do  (if not
dangerous  as  they  could  be  “outlawed”  by  the  rest).  Thus
“anarcho”-capitalism will  extend extensive rights and  powers to
bosses,  but  few if  any  rights  to  rebellious workers.  And  this
difference in power is  enshrined within the fundamental institutions
of the system.</p>

<p>In   evaluating  “anarcho”-capitalism’s   claim   to  be   a
form  of   anarchism,  Peter   Marshall  notes   that  <em>“private
protection  agencies  would  merely  serve  the  interests  of  their
paymasters.”</em>  [<strong>Demanding  the Impossible</strong>,  p.
653] With  the increase  of private “defence  associations” under
“really  existing  capitalism”   today  (associations  that  many
“anarcho”-capitalists point  to as  examples of their  ideas), we
see  a  vindication  of  Marshall’s claim.  There  have  been  many
documented experiences  of protesters  being badly beaten  by private
security  guards. As  far as  market theory  goes, the  companies are
only  supplying what  the buyer  is demanding.  The rights  of others
are  <strong>not  a  factor</strong> (yet  more  “externalities,”
obviously).  Even  if  the  victims  successfully  sue  the  company,
the  message  is  clear  — social  activism  can  seriously  damage
your  health.  With  a  reversion to  “a  general  libertarian  law
code” enforced by private companies,  this form of “defence” of
“absolute”  property rights  can  only increase,  perhaps to  the
levels  previously  attained  in  the heyday  of  US  capitalism,  as
described above by Weick.</p>

<h4 id="toc43">6.3 But  surely market forces will stop  abuses by the rich?</h4>

<p>Unlikely.  The  rise  of  corporations  within  America  indicates
exactly how  a “general libertarian  law code” would  reflect the
interests of the rich and powerful. The laws recognising corporations
as “legal persons” were  <strong>not</strong> primarily a product
of “the  state” but of  private lawyers hired  by the rich  — a
result  with which  Rothbard would  have no  problem. As  Howard Zinn
notes:</p>

<blockquote class="citation">
“the   American  Bar   Association,  organised   by  lawyers
accustomed  to serving  the  wealthy, began  a  national campaign  of
education  to reverse  the [Supreme]  Court decision  [that companies
could not be considered as a  person]... By 1886... the Supreme Court
had accepted  the argument  that corporations were  ‘persons’ and
their  money was  property protected  by  the process  clause of  the
Fourteenth  Amendment...  The  justices  of the  Supreme  Court  were
not  simply  interpreters  of  the Constitution.  They  were  men  of
certain backgrounds, of certain [class] interests.”</em> [<strong>A
People’s History of the United States</strong>, p. 255]
</blockquote>

<p>Of course it  will be argued that the Supreme  Court is a monopoly
and so our  analysis is flawed. In  “anarcho”-capitalism there is
no  monopoly.  But  the  corporate  laws  came  about  because  there
was  a demand  for  them. That  demand would  still  have existed  in
“anarcho”-capitalism. Now,  while there may be  no Supreme Court,
Rothbard  does  maintain  that  <em>“the basic  Law  Code  ...would
have  to  be  agreed  upon   by  all  the  judicial  agencies”</em>
but  he  maintains that  this  <em>“would  imply no  unified  legal
system”</em>! Even  though <em>“[a]ny agencies  that transgressed
the  basic libertarian  law code  would be  open outlaws”</em>  and
soon crushed  this is  <strong>not</strong>, apparently,  a monopoly.
[<strong>The  Ethics  of Liberty</strong>,  p.  234]  So, you  either
agree  to the  law  code or  you  go  out of  business.  And that  is
<strong>not</strong> a monopoly! Therefore, we think, our comments on
the Supreme Court decision are valid.</p>

<p>If all the available defence firms  enforce the same laws, then it
can  hardly be  called “competitive”!  And  if this  is the  case
(and  it  is) <em>“when  private  wealth  is uncontrolled,  then  a
police-judicial complex enjoying a  clientele of wealthy corporations
whose  motto is  self-interest is  hardly an  innocuous social  force
controllable  by  the  possibility  of forming  or  affiliating  with
competing ‘companies.’”</em> [Weick, <strong>Op. Cit.</strong>,
p. 225]</p>

<p>This is  particularly true if  these companies are  themselves Big
Business and so  have a large impact on the  laws they are enforcing.
If the  law code recognises  and protects capitalist  power, property
and wealth as fundamental <strong>any</strong> attempt to change this
is “initiation of force” and so  the power of the rich is written
into the system from the start!</p>

<p>(And, we must  add, if there is a general  libertarian law code to
which all  must subscribe,  where does that  put customer  demand? If
people demand a  non-libertarian law code, will  defence firms refuse
to supply it?  If so, will not new firms,  looking for profit, spring
up that  will supply what  is being demanded?  And will that  not put
them in direct conflict with the existing, pro-general law code ones?
And will  a market in law  codes not just reflect  economic power and
wealth? David Friedman, who is for a market in law codes, argues that
<em>“[i]f almost  everyone believes strongly that  heroin addiction
is  so  horrible that  it  should  not  be permitted  anywhere  under
any circumstances  anarcho-capitalist institutions will  produce laws
against heroin.  Laws are being produced  on the market, and  that is
what the market wants.”</em> And he adds that <em>“market demands
are in dollars, not votes. The legality of heroin will be determined,
not by how many  are for or against but how high a  cost each side is
willing  to  bear  in  order to  get  its  way.”</em>  [<strong>The
Machinery of  Freedom</strong>, p.  127] And, as  the market  is less
than equal in  terms of income and wealth, such  a position will mean
that the  capitalist class will  have a higher effective  demand than
the working class,  and more resources to pay for  any conflicts that
arise.  Thus any  law codes  that develop  will tend  to reflect  the
interests of the wealthy.)</p>

<p>Which brings  us nicely  on to the  next problem  regarding market
forces.</p>

<p>As  well  as  the  obvious influence  of  economic  interests  and
differences in  wealth, another  problem faces the  “free market”
justice  of  “anarcho”-capitalism.  This  is  the  <em>“general
libertarian  law code”</em>  itself.  Even if  we  assume that  the
system  actually works  like it  should  in theory,  the simple  fact
remains that  these “defence companies” are  enforcing laws which
explicitly  defend capitalist  property  (and  so social  relations).
Capitalists  own  the  means  of  production  upon  which  they  hire
wage-labourers  to  work  and   this  is  an  inequality  established
<strong>prior</strong>  to any  specific  transaction  in the  labour
market. This  inequality reflects itself  in terms of  differences in
power  within (and  outside) the  company and  in the  “law code”
of  “anarcho”-capitalism which  protects that  power against  the
dispossessed.</p>

<p>In other  words, the law  code within which the  defence companies
work assumes  that capitalist property  is legitimate and  that force
can legitimately  be used to defend  it. This means that,  in effect,
“anarcho”-capitalism is  based on a  monopoly of law,  a monopoly
which  explicitly exists  to  defend  the power  and  capital of  the
wealthy. The  major difference is  that the agencies used  to protect
that wealth  will be  in a  weaker position  to act  independently of
their pay-masters.  Unlike the state,  the “defence” firm  is not
remotely accountable to the general  population and cannot be used to
equalise  even slightly  the power  relationships between  worker and
capitalist.</p>

<p>And, needless  to say, it is  very likely that the  private police
forces  <strong>will</strong> give  preferential  treatment to  their
wealthier customers (what  business does not?) and that  the law code
will  reflect  the interests  of  the  wealthier sectors  of  society
(particularly  if  <em>“prosperous”</em> judges  administer  that
code)  in  reality, even  if  not  in  theory. Since,  in  capitalist
practice,  “the  customer  is   always  right,”  the  best-paying
customers will get their way in “anarcho”-capitalist society.</p>

<p>For   example,  in   chapter  29   of  <strong>The   Machinery  of
Freedom</strong>, David Friedman  presents an example of  how a clash
of different law codes could be resolved by a bargaining process (the
law in question is the death penalty). This process would involve one
defence firm  giving a sum of  money to the other  for them accepting
the appropriate (anti/pro capital  punishment) court. Friedman claims
that <em>“[a]s in  any good trade, everyone  gains”</em> but this
is obviously  not true. Assuming the  anti-capital punishment defence
firm pays  the pro  one to accept  an anti-capital  punishment court,
then, yes,  both defence firms have  made money and so  are happy, so
are the  anti-capital punishment consumers but  the pro-death penalty
customers have  only (perhaps) received  a cut in their  bills. Their
desire to see criminals hanged (for whatever reason) has been ignored
(if they were not in favour of the death penalty, they would not have
subscribed  to  that company).  Friedman  claims  that the  deal,  by
allowing the  anti-death penalty firm  to cut its costs,  will ensure
that it <em>“keep its customers  and even get more”</em> but this
is just an assumption.  It is just as likely to  loose customers to a
defence  firm  that refuses  to  compromise  (and has  the  resources
to  back  it  up).  Friedman’s assumption  that  lower  costs  will
automatically win  over people’s passions  is unfounded. As  is the
assumption that both firms have equal resources and bargaining power.
If the  pro-capital punishment  firm demands more  than the  anti can
provide and  has larger  weaponry and troops,  then the  anti defence
firm may have to agree to let the pro one have its way.</p>

<p>So,   all  in   all,   it  is   <strong>not</strong>  clear   that
<em>“everyone gains”</em> — there  may be a sizeable percentage
of those involved  who do not “gain” as their  desire for capital
punishment is  traded away  by those who  claimed they  would enforce
it.</p>

<p>In other  words, a  system of competing  law codes  and privatised
rights does not ensure  that <strong>all</strong> consumers interests
are meet.  Given unequal resources  within society, it is  also clear
that the “effective demand” of  the parties involved to see their
law codes  enforced is drastically  different. The wealthy head  of a
transnational corporation  will have far more  resources available to
him to pay  for <strong>his</strong> laws to be enforced  than one of
his employees on the assembly line. Moreover, as we argue in sections
3.1  and 10.2,  the  labour market  is usually  skewed  in favour  of
capitalists. This means  that workers have to compromise  to get work
and  such  compromises  may  involve  agreeing  to  join  a  specific
“defence” firm or not join one  at all (just as workers are often
forced to  sign non-union contracts today  in order to get  work). In
other  words, a  privatised law  system is  very likely  to skew  the
enforcement of laws in line with  the skewing of income and wealth in
society. At the  very least, unlike every other  market, the customer
is <strong>not</strong>  guaranteed to  get exactly what  they demand
simply because the  product they “consume” is  dependent on other
within the same  market to ensure its supply. The  unique workings of
the law/defence market  are such as to deny customer  choice (we will
discuss other aspects of this unique market shortly).</p>

<p>Weick sums  up by  saying <em>“any judicial  system is  going to
exist in  the context  of economic institutions.  If there  are gross
inequalities of power in the economic  and social domains, one has to
imagine society  as strangely  compartmentalised in order  to believe
that  those  inequalities will  fail  to  reflect themselves  in  the
judicial and  legal domain, and  that the economically  powerful will
be  unable to  manipulate  the  legal and  judicial  system to  their
advantage.  To abstract  from such  influences of  context, and  then
consider the merits of an abstract  judicial system... is to follow a
method that  is not likely  to take  us far. This,  by the way,  is a
criticism that applies...to  any theory that relies on a  rule of law
to override  the tendencies inherent  in a given social  and economic
system”</em>  [Weick, <strong>Op.  Cit.</strong>,  p.  225] (For  a
discussion of this problem as it would surface in attempts to protect
the environment under “anarcho”-capitalism,  see sections E.2 and
E.3).</p>

<p>There  is another  reason why  “market forces”  will not  stop
abuse  by the  rich,  or indeed  stop the  system  from turning  from
private  to  public  statism.  This  is due  to  the  nature  of  the
“defence”  market (for  a similar  analysis of  the “defence”
market see Tyler Cowen’s <em>“Law as a Public Good: The Economics
of  Anarchy”</em>  in  <strong>Economics  and  Philosophy</strong>,
no.   8  (1992),   pp.  249–267   and  <em>“Rejoinder   to  David
Friedman  on the  Economics of  Anarchy”</em> in  <strong>Economics
and   Philosophy</strong>,  no.   10  (1994),   pp.  329–332).   In
“anarcho”-capitalist  theory it  is  assumed  that the  competing
“defence companies” have a vested interest in peacefully settling
differences between themselves  by means of arbitration.  In order to
be competitive on  the market, companies will have  to co-operate via
contractual  relations otherwise  the  higher  price associated  with
conflict will  make the company  uncompetitive and it will  go under.
Those companies  that ignore decisions  made in arbitration  would be
outlawed by  others, ostracised  and their  rulings ignored.  By this
process, it is argued, a  system of competing “defence” companies
will be  stable and not turn  into a civil war  between agencies with
each  enforcing the  interests  of their  clients  against others  by
force.</p>

<p>However,   there  is   a   catch.  Unlike   every  other   market,
the  businesses   in  competition   in  the   “defence”  industry
<strong>must</strong> co-operate with its fellows in order to provide
its services  for its  customers. They  need to be  able to  agree to
courts and judges,  agree to abide by decisions and  law codes and so
forth. In economics there are other, more accurate, terms to describe
co-operative  activity  between  companies:  collusion  and  cartels.
Collusion and cartels  is where companies in a  specific market agree
to work  together to  restrict competition and  reap the  benefits of
monopoly power  by working  to achieve the  same ends  in partnership
with  each  other.  In  other  words this  means  that  collusion  is
built  into  the system,  with  the  necessary contractual  relations
between agencies in the  “protection” market requiring that firms
co-operate and,  by so  doing, to behave  (effectively) as  one large
firm (and  so, effectively,  resemble the state  even more  than they
already do). Quoting Adam Smith seems appropriate here: <em>“People
of  the same  trade  seldom  meet together,  even  for merriment  and
diversion,  but the  conversation ends  in a  conspiracy against  the
public, or in some  contrivance to raise prices.”</em> [<strong>The
Wealth of Nations</strong>, p. 117]</p>

<p>For  example, when  buying food  it  does not  matter whether  the
supermarkets I visit  have good relations with each  other. The goods
I  buy  are  independent  of the  relationships  that  exist  between
competing companies. However, in the  case of private states, this is
<strong>not</strong> the  case. If  a specific  “defence” company
has bad relationships  with other companies in the market  then it is
against my self-interest to subscribe to it. Why join a private state
if its judgements are  ignored by the others and it  has to resort to
violence to be  heard? This, as well as  being potentially dangerous,
will also push up the prices I have to pay. Arbitration is one of the
most important  services a defence  firm can offer its  customers and
its  market share  is based  upon  being able  to settle  interagency
disputes without  risk of war  or uncertainty that the  final outcome
will not be accepted by all parties.</p>

<p>Therefore, the  market set-up within  the “anarcho”-capitalist
“defence”  market is  such  that private  states <strong>have  to
co-operate</strong> with the others (or  go out of business fast) and
this means  collusion can  take place.  In other  words, a  system of
private  states will  have  to agree  to work  together  in order  to
provide the service  of “law enforcement” to  their customers and
the  result of  such co-operation  is  to create  a cartel.  However,
unlike cartels in other industries,  the “defence” cartel will be
a  stable body  simply because  its members  <strong>have</strong> to
work with their competitors in order to survive.</p>

<p>Let us look at what would happen  after such a cartel is formed in
a specific area and a new  “defence company” desired to enter the
market. This  new company will have  to work with the  members of the
cartel in order  to provide its services to its  customers (note that
“anarcho”-capitalists already  assume that they  <em>“will have
to”</em>  subscribe to  the  same  law code).  If  the new  defence
firm tries  to under-cut  the cartel’s  monopoly prices,  the other
companies  would refuse  to work  with  it. Having  to face  constant
conflict or the  possibility of conflict, seeing  its decisions being
ignored by other  agencies and being uncertain what the  results of a
dispute would be, few would  patronise the new “defence company.”
The new company’s prices would go  up and so face either folding or
joining  the  cartel. Unlike  every  other  market, if  a  “defence
company” does not have  friendly, co-operative relations with other
firms in the same industry then it will go out of business.</p>

<p>This means that the firms that  are co-operating have but to agree
not to  deal with  new firms  which are  attempting to  undermine the
cartel in  order for them to  fail. A “cartel busting”  firm goes
out of  business in the  same way an outlaw  one does —  the higher
costs associated with having to solve all its conflicts by force, not
arbitration,  increases its  production  costs much  higher than  the
competitors and  the firm  faces insurmountable  difficulties selling
its products at a profit (ignoring any drop of demand due to fears of
conflict by actual  and potential customers). Even if  we assume that
many people will happily join the new firm in spite of the dangers to
protect themselves against the cartel and its taxation (i.e. monopoly
profits), enough will remain members of the cartel (perhaps they will
be fired  if they change, perhaps  they dislike change and  think the
extra money is worth peace, perhaps they fear that by joining the new
company  their peace  will  be  disrupted or  the  outcomes of  their
problems with  others too  unsure to  be worth  it, perhaps  they are
shareholders and want to maintain  their income) so that co-operation
will still be needed and  conflict unprofitable and dangerous (and as
the  cartel will  have more  resources than  the new  firm, it  could
usually hold out longer than the new firm could). In effect, breaking
the cartel may take  the form of an armed revolution  — as it would
with any state.</p>

<p>The  forces  that  break  up   cartels  and  monopolies  in  other
industries  (such  as   free  entry  —  although,   of  course  the
“defence” market  will be subject to  oligopolistic tendencies as
any other and this will create barriers to entry, see section C.4) do
not work  here and so  new firms have  to co-operate or  loose market
share and/or  profits. This  means that “defence  companies” will
reap monopoly profits and, more importantly, have a monopoly of force
over a given area.</p>

<p>Hence a monopoly of private states will develop in addition to the
existing  monopoly  of  law  and  this is  a  de  facto  monopoly  of
force  over a  given area  (i.e.  some kind  of public  state run  by
share holders).  New companies attempting to  enter the “defence”
industry  will have  to work  with the  existing cartel  in order  to
provide the services  it offers to its customers. The  cartel is in a
dominant position and new entries  into the market either become part
of it  or fail.  This is  exactly the position  with the  state, with
“private agencies”  free to operate as  long as they work  to the
state’s guidelines.  As with the monopolist  “general libertarian
law code”,  if you  do not  toe the  line, you  go out  of business
fast.</p>

<p>It is also likely that a  multitude of cartels would develop, with
a given  cartel operating in  a given  locality. This is  because law
enforcement would  be localised in  given areas as most  crime occurs
where the  criminal lives. Few criminals  would live in New  York and
commit  crimes in  Portland. However,  as defence  companies have  to
co-operate  to provide  their  services, so  would  the cartels.  Few
people live all  their lives in one area and  so firms from different
cartels would come into contact, so forming a cartel of cartels.</p>

<p>A cartel  of cartels may (perhaps)  be less powerful than  a local
cartel,  but it  would still  be required  and for  exactly the  same
reasons  a local  one is.  Therefore “anarcho”-capitalism  would,
like “actually  existing capitalism,”  be marked  by a  series of
public states covering given areas,  co-ordinated by larger states at
higher levels. Such a set up would parallel the United States in many
ways except it would be  run directly by wealthy shareholders without
the sham of  “democratic” elections. Moreover, as in  the USA and
other states  there will still be  a monopoly of rules  and laws (the
“general libertarian law code”).</p>

<p>Some  “anarcho”-capitalists claim  that this  will not  occur,
but  that the  co-operation  needed  to provide  the  service of  law
enforcement  will somehow  <strong>not</strong>  turn into  collusion
between companies.  However, they  are quick  to argue  that renegade
“agencies”  (for example,  the so-called  “Mafia problem”  or
those who reject  judgements) will go out of business  because of the
higher costs  associated with conflict and  not arbitration. However,
these higher costs  are ensured because the firms in  question do not
co-operate  with  others.  If  other  agencies  boycott  a  firm  but
co-operate with  all the others, then  the boycotted firm will  be at
the same disadvantage — regardless of whether it is a cartel buster
or a renegade.</p>

<p>The  “anarcho”-capitalist  is trying  to  have  it both  ways.
If  the  punishment  of   non-conforming  firms  cannot  occur,  then
“anarcho”-capitalism  will turn  into a  war of  all against  all
or,  at  the  very  least,  the  service  of  social  peace  and  law
enforcement cannot  be provided.  If firms  cannot deter  others from
disrupting  the social  peace (one  service the  firm provides)  then
“anarcho”-capitalism is not stable and will not remain orderly as
agencies develop  which favour the  interests of their  own customers
and  enforce  their own  law  codes  at  the  expense of  others.  If
collusion  cannot occur  (or  is  too costly)  then  neither can  the
punishment of non-conforming  firms and “anarcho”-capitalism will
prove to be unstable.</p>

<p>So,  to sum  up, the  “defence” market  of private  states has
powerful forces within it to turn it  into a monopoly of force over a
given area. From a privately chosen monopoly of force over a specific
(privately owned) area, the market of private states will turn into a
monopoly of force  over a general area.  This is due to  the need for
peaceful relations  between companies,  relations which  are required
for a  firm to  secure market  share. The  unique market  forces that
exist within this market ensure collusion and monopoly.</p>

<p>In other words, the system of  private states will become a cartel
and so a public state —  unaccountable to all but its shareholders,
a state  of the wealthy,  by the wealthy,  for the wealthy.  In other
words, fascism.</p>

<h4 id="toc44">6.4  Why   are   these  “defence   associations” states?</h4>

<p>It  is clear  that  “anarcho”-capitalist defence  associations
meet the  criteria of statehood  outlined in section B.2  (“Why are
anarchists against  the state”). They defend  property and preserve
authority relationships, they practice coercion, and are hierarchical
institutions which govern  those under them on behalf  of a “ruling
elite,” i.e. those  who employ both the governing  forces and those
they govern.  Thus, from  an anarchist perspective,  these “defence
associations” as most definitely states.</p>

<p>What is interesting,  however, is that by their  own definitions a
very  good case  can be  made that  these “defence  associations”
as  states  in  the “anarcho”-capitalist  sense  too.  Capitalist
apologists usually define a “government”  (or state) as those who
have a monopoly  of force and coercion within a  given area. Relative
to the rest  of the society, these defence associations  would have a
monopoly of force and coercion of a given piece of property; thus, by
the  “anarcho”-capitalists’ <strong>own  definition</strong> of
statehood, these associations would qualify!</p>

<p>If  we  look  at   Rothbard’s  definition  of  statehood,  which
requires  (a)   the  power  to   tax  and/or  (b)   a  <em>“coerced
monopoly  of the  provision  of defence  over  a given  area”</em>,
“anarcho”-capitalism runs into trouble.</p>

<p>In the first place, the  costs of hiring defence associations will
be deducted from the wealth created by those who use, but do not own,
the  property of  capitalists and  landlords. Let  not forget  that a
capitalist will only employ a worker  or rent out land and housing if
they make a  profit from so doing. Without the  labour of the worker,
there would be nothing  to sell and no wages to pay  for rent. Thus a
company’s or landlord’s “defence” firm  will be paid from the
revenue gathered from the capitalists power to extract a tribute from
those who  use, but do not  own, a property. In  other words, workers
would pay for the agencies  that enforce their employers’ authority
over  them via  the  wage system  and  rent —  taxation  in a  more
insidious form.</p>

<p>In the second, under capitalism most  people spend a large part of
their day  on other people’s  property —  that is, they  work for
capitalists and/or  live in  rented accommodation. Hence  if property
owners select a “defence association” to protect their factories,
farms, rental housing, etc., their employees and tenants will view it
as a <em>“coerced monopoly of the provision of defence over a given
area.”</em>  For  certainly  the  employees and  tenants  will  not
be  able to  hire  their  own defence  companies  to expropriate  the
capitalists and landlords.  So, from the standpoint  of the employees
and tenants, the owners do have  a monopoly of “defence” over the
areas in question. Of course, the “anarcho”-capitalist will argue
that the  tenants and  workers “consent”  to <strong>all</strong>
the rules and conditions  of a contract when they sign  it and so the
property  owner’s  monopoly  is  not  “coerced.”  However,  the
“consent” argument is  so weak in conditions of  inequality as to
be useless (see sections 2.4 and  3.1, for example) and, moreover, it
can  and  has  been  used  to justify  the  state.  In  other  words,
“consent” in and of itself does not ensure that a given regime is
not statist (see section 2.3 for  more on this). So an argument along
these  lines is  deeply flawed  and can  be used  to justify  regimes
which  are little  better than  “industrial feudalism”  (such as,
as  indicated in  section  B.4,  company towns,  for  example —  an
institution which right-libertarianism has no problem with). Even the
<em>“general libertarian  law code,”</em>  could be  considered a
“monopoly of government over  a particular area,” particularly if
ordinary people have no real means  of affecting the law code, either
because it is market-driven and so is money-determined, or because it
will be “natural” law and so unchangeable by mere mortals.</p>

<p>In other words, <strong>if</strong>  the state <em>“arrogates to
itself a monopoly of force, of ultimate decision-making power, over a
given area territorial area”</em>  [Rothbard, <strong>The Ethics of
Liberty</strong>, p.  170] then  its pretty  clear that  the property
owner shares this power. The owner is, after all, the <em>“ultimate
decision-making power”</em> in their workplace or on their land. If
the boss takes a dislike to you (for example, you do not follow their
orders) then you get fired. If you  cannot get a job or rent the land
without agreeing to  certain conditions (such as not  joining a union
or subscribing to  the “defence firm” approved  by your employer)
then you  either sign  the contract  or look  for something  else. Of
course Rothbard fails to note that bosses have this monopoly of power
and is instead referring to <em>“prohibiting the voluntary purchase
and  sale  of  defence and  judicial  services.”</em>  [<strong>Op.
Cit.</strong>, p.  171] But  just as  surely as  the law  of contract
allows the banning  of unions from a property, it  can just as surely
ban the sale and purchase of  defence and judicial services (it could
be argued  that market forces will  stop this happening, but  this is
unlikely as  bosses usually have  the advantage on the  labour market
and workers have to  compromise to get a job —  see section 10.2 on
why this is the case). After  all, in the company towns, only company
money was legal tender and company police the only law enforcers.</p>

<p>Therefore, it is obvious  that the “anarcho”-capitalist system
meets  the  Weberian  criteria  of  a  monopoly  to  enforce  certain
rules in  a given  area of land.  The <em>“general  libertarian law
code”</em> is  a monopoly and  property owners determine  the rules
that apply  to their property.  Moreover, if the rules  that property
owners enforce  are subject  to rules  contained in  the monopolistic
<em>“general libertarian  law code”</em> (for example,  that they
cannot ban  the sale  and purchase  of certain  products —  such as
defence  — on  their own  territory) then  “anarcho”-capitalism
<strong>definitely</strong>  meets  the  Weberian definition  of  the
state (as described  by Ayn Rand as an  institution <em>“that holds
the  exclusive power  to  <strong>enforce</strong>  certain rules  of
conduct in a given geographical area”</em> [<strong>Capitalism: The
Unknown Ideal</strong>, p. 239]) as  its “law code” overrides the
desires  of  property owners  to  do  what  they  like on  their  own
property.</p>

<p>Therefore, no matter how  you look at it, “anarcho”-capitalism
and   its  “defence”   market  promotes   a  <em>“monopoly   of
ultimate   decision   making   power”</em>  over   a   <em>“given
territorial  area”</em>. It  is  obvious that  for anarchists,  the
“anarcho”-capitalist system  is a state  system. As, as  we note,
a  reasonable  case  can  be  made  for it  also  being  a  state  in
“anarcho”-capitalist theory as well.</p>

<p>So,     in     effect,      “anarcho”-capitalism     has     a
<strong>different</strong>  sort  of  state,   one  in  which  bosses
hire  and   fire  the   policeman.  As   Peter  Sabatini   notes  [in
<strong>Libertarianism:   Bogus  Anarchy</strong>],   <em>“[w]ithin
Libertarianism,  Rothbard  represents  a  minority  perspective  that
actually  argues for  the  total elimination  of  the state.  However
Rothbard’s claim as an anarchist is quickly voided when it is shown
that he only wants an end to the public state. In its place he allows
countless private states, with each person supplying their own police
force,  army,  and  law,  or  else  purchasing  these  services  from
capitalist vendors...  Rothbard sees  nothing at  all wrong  with the
amassing of wealth, therefore those with more capital will inevitably
have  greater coercive  force  at  their disposal,  just  as they  do
now.”</em></p>

<p>Far    from    wanting    to     abolish    the    state,    then,
“anarcho”-capitalists  only desire  to privatise  it —  to make
it  solely accountable  to capitalist  wealth. Their  “companies”
perform the same  services as the state, for the  same people, in the
same manner. However, there is one slight difference. Property owners
would  be  able  to  select between  competing  companies  for  their
“services.”  Because such  “companies”  are  employed by  the
boss,  they would  be used  to reinforce  the totalitarian  nature of
capitalist firms by ensuring that the police and the law they enforce
are not even slightly accountable to ordinary people.</p>

<p>Looking beyond the “defence association” to the defence market
itself (as we argued in the  last section), this will become a cartel
and  so become  some kind  of public  state. The  very nature  of the
private  state,  its need  to  co-operate  with  others in  the  same
industry,  push it  towards  a monopoly  network of  firms  and so  a
monopoly of  force over a given  area. Given the assumptions  used to
defend “anarcho”-capitalism,  its system of private  statism will
develop into public  statism — a state run  by managers accountable
only to the share-holding elite.</p>

<p>To  quote  Peter  Marshall  again,  the  “anarcho”-capitalists
<em>“claim  that all  would benefit  from  a free  exchange on  the
market,  it is  by no  means  certain; any  unfettered market  system
would  most likely  sponsor a  reversion to  an unequal  society with
defence associations perpetuating exploitation and privilege.”</em>
[<strong>Demanding  the  Impossible</strong>,  p. 565]  History,  and
current practice, prove this point.</p>

<p>In  short, “anarcho”-capitalists  are not  anarchists at  all,
they are  just capitalists who  desire to see private  states develop
— states which are strictly accountable to their paymasters without
even the sham of democracy we have today. Hence a far better name for
“anarcho”-capitalism would be  “private-state” capitalism. At
least  that  way  we get  a  fairer  idea  of  what they  are  trying
to  sell  us. As  Bob  Black  writes  in <strong>The  Libertarian  as
Conservative</strong>, <em>“To  my mind  a right-wing  anarchist is
just a minarchist  who’d abolish the state to  his own satisfaction
by calling it something else...  They don’t denounce what the state
does, they just object to who’s doing it.”</em></p>

<h4 id="toc45">6.5 What other effects would “free market” justice have?</h4>

<p>Such  a system  would be  dangerous  simply because  of the  power
it  places  in the  hands  of  companies.  As Michael  Taylor  notes,
<em>“whether the [protection] market is competitive or not, it must
be remembered that the product is a peculiar one: when we buy cars or
shoes or  telephone services we do  not give the firm  power based on
force, but armed protection agencies,  like the state, make customers
(their own and others’) vulnerable,  and having given them power we
cannot be sure that they will use it only for our protection.”</em>
[<strong>Community, Anarchy and Liberty</strong>, p. 65]</p>

<p>As  we argued  above, there  are many  reasons to  believe that  a
“protection” market will  place most of society  (bar the wealthy
elite)  in  a  “vulnerable”  position. One  such  reason  is  the
assumptions  of  the  “anarcho”-capitalists themselves.  As  they
note,  capitalism  is  marked  by  an  extreme  division  of  labour.
Instead of  everyone having  all the skills  they need,  these skills
are  distributed  throughout  society  and all  (so  it  is  claimed)
benefit.</p>

<p>This applies equally to the “defence” market. People subscribe
to a “defence firm” because they either cannot or do not want the
labour of having to protect their own property and person. The skills
of defence,  therefore, are  concentrated in  these companies  and so
these firms will have an advantage  in terms of experience and mental
state  (they are  trained  to fight)  as well  as,  as seems  likely,
weaponry. This  means that most normal  people will be somewhat  at a
disadvantage if a cartel of  defence firms decides to act coercively.
The division of  labour society will discourage the  spread of skills
required for  sustained warfare  throughout society and  so, perhaps,
ensure that  customers remain “vulnerable.” The  price of liberty
may  be eternal  vigilance, but  are most  people willing  to include
eternal preparation  of war as  well? For modern society,  the answer
seems to be no,  they prefer to let others do  that (namely the state
and its armed forces). And, we should note, an armed society may be a
polite one, but its  politeness comes from fear, <strong>not</strong>
mutual respect and so totally phoney and soul destroying.</p>

<p>If  we  look  at  inequality  within  society,  this  may  produce
a  ghettoisation  effect  within “anarcho”-capitalism.  As  David
Friedman notes, conflict  between defence firms is  bad for business.
Conflict costs  money both  in terms of  weaponry used  and increased
(“danger money”) wages.  For this reason he  thinks that peaceful
co-operation will  exist between firms.  However, if we look  at poor
areas with high crime rates then its  clear that such an area will be
a dangerous place.  In other words, it  is very likely to  be high in
conflict. But conflict increases costs, and so prices. Does this mean
that those  areas which need police  most will also have  the highest
prices for law  enforcement? That is the case with  insurance now, so
perhaps we will see whole areas turning into Hobbesian anarchy simply
because the high costs associated  with dangerous areas will make the
effective demand for their services approach zero.</p>

<p>In a  system based  on “private  statism,” police  and justice
would  be determined  by “free  market” forces.  As indicated  in
section B.4.1,  right-libertarians maintain  that one would  have few
rights on other peoples’ property,  and so the owner’s will would
be the law (possibly restricted  somewhat by a “general libertarian
law code”,  perhaps not —  see last section). In  this situation,
those who could not afford  police protection would become victims of
roving bandits  and rampant crime,  resulting in a society  where the
wealthy are securely  protected in their bastions by  their own armed
forces, with a bunch of poor crowded around them for protection. This
would be very similar to feudal Europe.</p>

<p>The competing  police forces would  also be attempting  to execute
the laws of their  sponsors in areas that may not  be theirs to begin
with, which would lead to  conflicts unless everyone agreed to follow
a “general libertarian  law code” (as Rothbard,  for one, wants).
If there were competing law codes, the problem of whose “laws” to
select and  enforce would  arise, with each  of the  wealthy security
sponsors desiring  that their law  control all  of the land.  And, as
noted earlier, if there  were <strong>one</strong> “libertarian law
code,” this would be a <em>“monopoly of government”</em> over a
given area, and therefore statist.</p>

<p>In addition, it  should be noted that  the right-libertarian claim
that under their system anarchistic  associations would be allowed as
long as they are formed voluntarily just reflects their usual vacuous
concept of  freedom. This  is because  such associations  would exist
within  and be  subject  to the  “general  libertarian law  code”
of  “anarcho”-capitalist society.  These laws  would reflect  and
protect the  interests and power  of those with  capitalist property,
meaning that unless  these owners agree, trying to  live an anarchist
life  would be  nearly  impossible  (its all  fine  and  well to  say
that  those with  property  can do  what  they like,  if  you do  not
have  property then  experimentation  could prove  difficult —  not
to  mention,  of course,  few  areas  are completely  self-sufficient
meaning  that  anarchistic associations  will  be  subject to  market
forces,  market  forces  which  stress and  reward  the  opposite  of
the  values these  communes  were set  up to  create).  Thus we  must
<strong>buy</strong> the right to be free!</p>

<p>If,  as  anarchists  desire,   most  people  refuse  to  recognise
or  defend  the  rights  of private  property  and  freely  associate
accordingly  to organise  their own  lives and  ignore their  bosses,
this   would  still   be   classed  as   “initiation  of   force”
under   “anarcho”-capitalism,  and   thus  repressed.   In  other
words,  like   any  authoritarian  system,  the   “rules”  within
“anarcho”-capitalism do not evolve  with society and its changing
concepts  (this  can  be  seen  from  the  popularity  of  “natural
law” with right-libertarians, the  authoritarian nature of which is
discussed in section 11).</p>

<p>Therefore, in “anarcho”-capitalism you  are free to follow the
(capitalist) laws and  to act within the limits of  these laws. It is
only within this  context that you can experiment (if  you can afford
to).  If you  act outside  these laws,  then you  will be  subject to
coercion. The  amount of  coercion required  to prevent  such actions
depends on  how willing people are  to respect the laws.  Hence it is
not the case that an “anarcho”-capitalist society is particularly
conducive  to  social  experimentation  and free  evolution,  as  its
advocates like to claim. Indeed, the opposite may be the case, as any
capitalist  system will  have vast  differences of  wealth and  power
within it,  thus ensuring that  the ability to experiment  is limited
to  those who  can  afford  it. As  Jonathan  Wolff  points out,  the
<em>“image of people freely moving from one utopia to another until
they find their heaven, ignores  the thought that certain choices may
be irreversible... This thought may lead to speculation about whether
a law of evolution would apply to the plural utopias. Perhaps, in the
long run, we may find the  framework regulated by the law of survival
of  the economically  most  fit, and  so  we would  expect  to see  a
development not  of diversity  but of homogeneity.  Those communities
with great  market power would  eventually soak  up all but  the most
resistant of  those communities around  them.”</em> [<strong>Robert
Nozick: Property, Justice and the Minimal State</strong>, p. 135]</p>

<p>And  if  the  initial  distribution of  resources  is  similar  to
that already  existing then the <em>“economically  most fit”</em>
will be  capitalistic (as  argued in  section J.5.12,  the capitalist
market actively  selects against  co-operatives even though  they are
more  productive).  Given the  head  start  provided by  statism,  it
seems likely  that explicitly capitalist utopia’s  would remain the
dominant type  (particularly as  the rights framework  is such  as to
protect  capitalist property  rights). Moreover,  we doubt  that most
“anarcho”-capitalists  would  embrace  the  ideology  if  it  was
more  than  likely that  non-capitalist  utopias  would overcome  the
capitalist ones (after all, they <strong>are</strong> self-proclaimed
capitalists).</p>

<p>So,  given   that  “anarcho”-capitalists  who   follow  Murray
Rothbard’s ideas and  minimal-statist right-libertarians agree that
<strong>all</strong> must follow the basic <em>“general libertarian
law  code”</em> which  defends capitalist  property rights,  we can
safely  say  that  the  economically  <em>“most  fit”</em>  would
be  capitalist  ones. Hardly  surprising  if  the law  code  reflects
capitalist ideas of  right and wrong. In addition,  as George Reitzer
has  argued (see  <strong>The  McDonaldization of  Society</strong>),
capitalism is  driven towards  standardisation and conformity  by its
own logic. This suggests that  plurality of communities would soon be
replaced  by  a series  of  “communities”  which share  the  same
features of  hierarchy and ruling  elites. (“Anarcho”-capitalists
who  follow David  Friedman’s ideas  consider it  possible, perhaps
likely, that  a free market in  laws will result in  one standard law
code and so this also applies to that school as well)</p>

<p>So, in the end, the  “anarcho” capitalists argue that in their
system  you are  free  to follow  the (capitalist)  law  and work  in
the  (capitalist) economy,  and  if you  are lucky,  take  part in  a
“commune” as  a collective capitalist.  How <strong>very</strong>
generous of  them! Of  course, any  attempt to  change said  rules or
economy are illegal and would be stopped by private states.</p>

<p>As  well as  indicating  the  falsity of  “anarcho”-capitalist
claims   to  support   “experimentation,”  this   discussion  has
also   indicated   that   coercion   would   not   be   absent   from
“anarcho”-capitalism.  This would  be the  case only  if everyone
voluntarily respected private  property rights and abided  by the law
(i.e. acted in a capitalist-approved way).  As long as you follow the
law, you will be  fine — which is exactly the  same as under public
statism.  Moreover, if  the  citizens  of a  society  do  not want  a
capitalist order, it may require a lot of coercion to impose it. This
can be seen  from the experiences of the  Italian factory occupations
in  1920 (see  section A.5.5),  in  which workers  refused to  accept
capitalist property or authority as valid and ignored it. In response
to  this change  of  thought  within a  large  part  of society,  the
capitalists backed fascism in order  to stop the evolutionary process
within society.</p>

<p>The socialist  economic historian  Maurice Dobbs,  after reviewing
the private  armies in  1920s and  1930s America  made much  the same
point:</p>

<blockquote class="citation">
“When business policy takes the step of financing and arming
a mass political movement to  capture the machinery of government, to
outlaw opposing  forms of organisation and  suppress hostile opinions
we  have merely  a further  and  more logical  stage beyond  [private
armies]”</em> [<strong>Op, Cit.</strong>, p. 357]
</blockquote>

<p>(Noted  Austrian   Economist  Ludwig   von  Mises   whose  extreme
free   market  liberal   political   and   economic  ideas   inspired
right-libertarianism  in many  ways had  this to  say about  fascism:
<em>“It cannot be denied that  Fascism and similar movements aiming
at the establishment of dictatorships are full of the best intentions
and  that their  intervention  has, for  the  moment, saved  European
civilisation. The merit that Fascism  has thereby won for itself will
live eternally  in history.”</em>  [<strong>Liberalism</strong>, p.
51])</p>

<p>This  example illustrates  the  fact  that capitalism  <strong>per
se</strong> is  essentially authoritarian, because it  is necessarily
based on coercion and hierarchy,  which explains why capitalists have
resorted to the most extreme  forms of authoritarianism — including
totalitarian  dictatorship  —  during crises  that  threatened  the
fundamental rules of  the system itself. There is no  reason to think
that “anarcho”-capitalism would be any different.</p>

<p>Since “anarcho”-capitalism, with its  private states, does not
actually want to get rid of hierarchical forms of authority, the need
for one government to unify the enforcement activities of the various
defence  companies  becomes  apparent.  In  the  end,  that  is  what
“anarcho”-capitalism recognises  with its  “general libertarian
law code”  (based either  on market  forces or  “natural law”).
Thus it appears that one  government/hierarchy over a given territory
is inevitable under  any form of capitalism. That being  the case, it
is obvious  that a democratic  form of  statism, with its  checks and
balances, is preferable to a dictatorship that imposes “absolute”
property rights and so “absolute” power.</p>

<p>Of course, we do have another option than either private or public
statism. This is anarchism, the end of hierarchical authority and its
replacement by the “natural”  authority of communal and workplace
self-management.</p>

<h3  id="toc46">7 How  does the  history of  “anarcho”-capitalism show that it is not anarchist?</h3>

<p>Of course, “anarcho”-capitalism  does have historic precedents
and  “anarcho”-capitalists  spend  considerable  time  trying  to
co-opt various  individuals into  their self-proclaimed  tradition of
“anti-statist” liberalism.  That, in itself, should  be enough to
show  that  anarchism  and “anarcho”-capitalism  have  little  in
common  as  anarchism  developed  in  opposition  to  liberalism  and
its  defence of  capitalism.  Unsurprisingly, these  “anti-state”
liberals tended to, at best, refuse to call themselves anarchists or,
at worse, explicitly deny they were anarchists.</p>

<p>One  “anarcho”-capitalist  overview   of  their  tradition  is
presented by David  M. Hart. His perspective on  anarchism is typical
of  the school,  noting  that  in his  essay  anarchism or  anarchist
<em>“are used  in the sense  of a political theory  which advocates
the  maximum  amount of  individual  liberty,  a necessary  condition
of  which  is the  elimination  of  governmental or  other  organised
force.”</em>  [David M.  Hart, <em>“Gustave  de Molinari  and the
Anti-statist  Liberal  Tradition:   Part  I”</em>,  pp.  263–290,
<strong>Journal of  Libertarian Studies</strong>,  vol. V, no.  3, p.
284] Yet  anarchism has <strong>never</strong> been  solely concerned
with  abolishing the  state.  Rather, anarchists  have always  raised
economic and social demands and  goals along with their opposition to
the state. As  such, anti-statism may be a necessary  condition to be
an anarchist, but not a sufficient one to count a specific individual
or theory as anarchist.</p>

<p>Specifically, anarchists  have turned their analysis  onto private
property noting that the hierarchical social relationships created by
inequality of wealth (for  example, wage labour) restricts individual
freedom.  This  means  that  if  we do  seek  <em>“the  maximum  of
individual  liberty”</em> then  our analysis  cannot be  limited to
just the  state or  government. Consequently,  to limit  anarchism as
Hart does requires  substantial rewriting of history, as  can be seen
from his account of William Godwin.</p>

<p>Hart  tries  to  co-opt  of  William  Godwin  into  the  ranks  of
“anti-state”   liberalism,   arguing  that   he   <em>“defended
individualism  and   the  right  to   property.”</em>  [<strong>Op.
Cit.</strong>, p. 265]  He, of course, quotes from  Godwin to support
his claim yet strangely truncates  Godwin’s argument to exclude his
conclusion that <em>“[w]hen  the laws of morality  shall be clearly
understood, their excellence  universally apprehended, and themselves
seen  to  be coincident  with  each  man’s private  advantage,  the
idea  of  property  in  this  sense will  remain,  but  no  man  will
have  the least  desire, for  purposes of  ostentation or  luxury, to
possess more  than his  neighbours.”</em> [<strong>An  Enquiry into
Political Justice</strong>, p. 199] In other words, personal property
(possession) would still exist but  not private property in the sense
of capital or inequality of wealth.</p>

<p>This analysis  is confirmed in  book 8 of Godwin’s  classic work
entitled  <em><strong>“On  Property.”</strong></em>  Needless  to
say,  Hart fails  to mention  this analysis,  unsurprising as  it was
later  reprinted as  a socialist  pamphlet. Godwin  thought that  the
<em>“subject of property is the key-stone that completes the fabric
of political justice.”</em> Like Proudhon, Godwin subjects property
as well as the state to an anarchist analysis. For Godwin, there were
<em>“three degrees”</em> of property.  The first is possession of
things you  need to live.  The second  is <em>“the empire  to which
every man is  entitled over the produce of  his own industry.”</em>
The third is  <em>“that which occupies the  most vigilant attention
in the civilised states of Europe. It is a system, in whatever manner
established, by which one man enters into the faculty of disposing of
the  produce  of  another  man’s industry.”</em>  He  notes  that
it  is <em>“clear  therefore  that the  third  species of  property
is  in  direct  contradiction  to  the  second.”</em>  [<strong>Op.
Cit.</strong>, p. 701 and p. 710–2]</p>

<p>Godwin, unlike  classical liberals,  saw the need  to <em>“point
out the evils  of accumulated property,”</em> arguing  that the the
<em>“spirit  of  oppression,  the  spirit  of  servility,  and  the
spirit  of fraud  ... are  the  immediate growth  of the  established
administration of  property. They  are alike hostile  to intellectual
and  moral  improvement.”</em>  Like the  socialists  he  inspired,
Godwin argued that <em>“it is to be considered that this injustice,
the unequal distribution of property, the grasping and selfish spirit
of individuals, is  to be regarded as one of  the original sources of
government,  and,  as  it  rises  in  its  excesses,  is  continually
demanding  and necessitating  new  injustice, new  penalties and  new
slavery.”</em>  He  stressed,  <em>“let  it  never  be  forgotten
that   accumulated  property   is  usurpation.”</em>   [<strong>Op.
Cit.</strong>, p. 732, pp. 717–8, and p. 718]</p>

<p>Godwin argued against the current system of property and in favour
of <em>“the justice of an equal  distribution of the good things of
life.”</em> This would be based on <em>“[e]quality of conditions,
or, in  other words, an equal  admission to the means  of improvement
and pleasure”</em> as this <em>“is a law rigorously enjoined upon
mankind by the voice  of justice.”</em> [<strong>Op. Cit.</strong>,
p. 725 and  p. 736] Thus his anarchist ideas  were applied to private
property, noting like subsequent  anarchists that economic inequality
resulted in  the loss of liberty  for the many and,  consequently, an
anarchist  society  would  see  a  radical  change  in  property  and
property rights. As Kropotkin noted,  Godwin <em>“stated in 1793 in
a  quite  definite  form  the political  and  economic  principle  of
Anarchism.”</em> Little wonder he, like so many others, argued that
Godwin was <em>“the first theoriser of Socialism without government
— that  is to  say, of Anarchism.”</em>  [<strong>Environment and
Evolution</strong>, p. 62 and p.  26] For Kropotkin, anarchism was by
definition  not  restricted  to  purely  political  issues  but  also
attacked  economic  hierarchy,  inequality and  injustice.  As  Peter
Marshall  confirms, <em>“Godwin’s  economics, like  his politics,
are  an  extension  of  his  ethics.”</em>  [<strong>Demanding  the
Impossible</strong>, p. 210]</p>

<p>Godwin’s theory of property  is significant because it reflected
what was to  become standard nineteenth century  socialist thought on
the matter.  In Britain, his ideas  influenced Robert Owen and,  as a
result, the early socialist movement in that country. His analysis of
property, as noted, predated Proudhon’s classic anarchist analysis.
As such, to  state, as Hart did, that  Godwin simply <em>“concluded
that the state  was an evil which  had to be reduced in  power if not
eliminated  completely”</em>  while  not  noting  his  analysis  of
property gives  a radically false  presentation of his  ideas. [Hart,
<strong>Op.  Cit.</strong>, p.  265] However,  it does  fit into  his
flawed assertion that  anarchism is purely concerned  with the state.
Any evidence to the contrary is simply ignored.</p>

<h4 id="toc47">7.1 Are competing governments anarchism?</h4>

<p>No, of  course not. Yet according  to “anarcho”-capitalism, it
is. This can be seen from the ideas of Gustave de Molinari.</p>

<p>Hart is on  firmer ground when he argues  that the 19<sup>th</sup>
century French economist  Gustave de Molinari is the  true founder of
“anarcho”-capitalism.  With Molinari,  he argues,  <em>“the two
different  currents  of  anarchist  thought  converged:  he  combined
the  political  anarchism  of  Burke  and  Godwin  with  the  nascent
economic anarchism  of Adam Smith  and Say to  create a new  forms of
anarchism”</em> that has  been called <em>“anarcho-capitalism, or
free market anarchism.”</em> [<strong>Op. Cit.</strong>, p. 269] Of
course, Godwin  (like other anarchists)  did not limit  his anarchism
purely to “political” issues  and so he discussed <em>“economic
anarchism”</em>  as  well  in  his  critique  of  private  property
(as  Proudhon  also  did  later).  As  such,  to  artificially  split
anarchism into  political and  economic spheres is  both historically
and logically  flawed. While some dictionaries  limit “anarchism”
to opposition to the state, anarchists did and do not.</p>

<p>The  key  problem  for  Hart  is that  Molinari  refused  to  call
himself  an anarchist.  He did  not even  oppose government,  as Hart
himself  notes  Molinari proposed  a  system  of insurance  companies
to  provide defence  of  property and  <em>“called these  insurance
companies  ‘governments’   even  though  they  did   not  have  a
monopoly  within a  given geographical  area.”</em> As  Hart notes,
Molinari  was  the  sole  defender of  such  free-market  justice  at
the  time  in France.  [David  M.  Hart, <em>“Gustave  de  Molinari
and  the   Anti-statist  Liberal  Tradition:  Part   II”</em>,  pp.
399–434,<strong>Journal  of Libertarian  Studies</strong>, vol.  V,
no. 4, p. 415 and p. 411]  Molinari was clear that he wanted <em>“a
regime of  free government,”</em>  counterpoising <em>“monopolist
or communist governments”</em>  to <em>“free governments.”</em>
This would  lead to <em>“freedom of  government”</em> rather than
its  abolition (not  freedom  <strong>from</strong> government).  For
Molinarie the  future would not  bring <em>“the suppression  of the
state which  is the  dream of  the anarchists ...  It will  bring the
diffusion  of  the  state  within  society. That  is  ...  ‘a  free
state  in a  free  society.’”</em> [quoted  by Hart,  <strong>Op.
Cit.</strong>,  p. 429,  p. 411  and p.  422] As  such, Molinari  can
hardly be considered an anarchist, even if “anarchist” is limited
to purely being against government.</p>

<p>Moreover, in another sense Molinari was in favour of the state. As
we discuss in section 6, these companies would have a monopoly within
a given  geographical area — they  have to in order  to enforce the
property  owner’s  power  over  those  who use,  but  do  not  own,
the  property in  question.  The  key contradiction  can  be seen  in
Molinari’s advocating of company towns, privately owned communities
(his  term  was  a <em>“proprietary  company”</em>).  Instead  of
taxes, people  would pay  rent and  the <em>“administration  of the
community would be either left in  the hands of the company itself or
handled special organisations set up for this purpose.”</em> Within
such a regime <em>“those with  the most property had proportionally
the  greater say  in  matters which  affected the  community.”</em>
If  the poor  objected  then they  could  simply leave.  [<strong>Op.
Cit.</strong>, pp. 421–2 and p. 422]</p>

<p>Given this,  the idea that Molinari  was an anarchist in  any form
can be dismissed. His system was based on privatising government, not
abolishing it (as he himself  admitted). This would be different from
the current system, of course,  as landlords and capitalists would be
hiring force directly to enforce  their decisions rather than relying
on a state  which they control indirectly. This system,  as we proved
in section  6, would not  be anarchist as  can be seen  from American
history. There  capitalists and  landlords created their  own private
police forces and armies, which regularly attacked and murdered union
organisers  and strikers.  As  an example,  there  is Henry  Ford’s
Service Department (private police force):</p>

<blockquote class="citation">
“In 1932  a hunger  march of the  unemployed was  planned to
march up  to the gates of  the Ford plant at  Dearborn... The machine
guns of  the Dearborn police  and the Ford Motor  Company’s Service
Department killed [four]  and wounded over a score  of others... Ford
was fundamentally and  entirely opposed to trade unions.  The idea of
working men questioning  his prerogatives as an  owner was outrageous
... [T]he River Rouge plant... was dominated by the autocratic regime
of Bennett’s service men. Bennett . . organise[d] and train[ed] the
three and  a half  thousand private policemen  employed by  Ford. His
task  was to  maintain  discipline amongst  the  work force,  protect
Ford’s  property [and  power],  and  prevent unionisation...  Frank
Murphy,  the mayor  of Detroit,  claimed that  ‘Henry Ford  employs
some  of the  worst  gangsters in  our city.’  The  claim was  well
based. Ford’s Service  Department policed the gates  of his plants,
infiltrated emergent groups  of union activists, posed  as workers to
spy on men on  the line... Under this tyranny the  Ford worker had no
security, no rights. So much so  that any information about the state
of things within the plant could only be freely obtained from ex-Ford
workers.”</em> [Huw Beynon,  <strong>Working for Ford</strong>, pp.
29–30]
</blockquote>

<p>The private  police attacked  women workers handing  out pro-union
handbills and  gave them  <em>“a severe beating.”</em>  At Kansas
and  Dallas <em>“similar  beatings  were handed  out  to the  union
men.”</em> This use of private police to control the work force was
not  unique.  General  Motors  <em>“spent one  million  dollars  on
espionage,  employing fourteen  detective  agencies  and two  hundred
spies at  one time [between  1933 and 1936]. The  Pinkerton Detective
Agency  found  anti-unionism  its  most  lucrative  activity.”</em>
[<strong>Op. Cit.</strong>, p.  34 and p. 32] We must  also note that
the  Pinkerton’s had  been  selling their  private police  services
for  decades  before the  1930s.  For  over  60 years  the  Pinkerton
Detective  Agency had  <em>“specialised in  providing spies,  agent
provocateurs, and private armed forces for employers combating labour
organisations.”</em> By  1892 it  <em>“had provided  its services
for management in seventy major labour disputes, and its 2 000 active
agents and  30 000 reserves totalled  more than the standing  army of
the nation.”</em> [Jeremy Brecher, <strong>Strike!</strong>, p. 55]
With this force  available, little wonder unions found it  so hard to
survive in the USA.</p>

<p>Only an “anarcho”-capitalist would deny that this is a private
government, employing private police  to enforce private power. Given
that  unions  could  be  considered  as  “defence”  agencies  for
workers,  this suggests  a  picture  of how  “anarcho”-capitalism
may  work   in  practice   radically  different  from   the  pictures
painted  by  its  advocates.  The  reason  is  simple,  it  does  not
ignore inequality  and subjects  economics to an  anarchist analysis.
Little wonder,  then, that  Proudhon stressed that  it <em>“becomes
necessary  for  the  workers   to  form  themselves  into  democratic
societies,  with equal  conditions  for  all members,  on  pain of  a
relapse  into feudalism.”</em>  Anarchism,  in  other words,  would
see  <em>“[c]apitalistic   and  proprietary   exploitation  stopped
everywhere,  the  wage  system abolished”</em>  and  so  <em>“the
economic organisation [would] replac[e] the governmental and military
system.”</em> [<strong>The General Idea of the Revolution</strong>,
p. 227  and p. 281] Clearly,  the idea that Proudhon  shared the same
political goal as Molinari is a  joke. He would have dismissed such a
system as little more than an  updated form of feudalism in which the
property owner is sovereign and the workers subjects (see section B.4
for more details).</p>

<p>Unsurprisingly,  Molinari  (unlike the  individualist  anarchists)
attacked  the  jury  system,  arguing  that  its  obliged  people  to
<em>“perform the duties of  judges. This is pure communism.”</em>
People would <em>“judge according to  the colour of their opinions,
than  according  to  justice.”</em> [quoted  by  Hart,  <strong>Op.
Cit.</strong>,  p.  409]  As  the jury  system  used  amateurs  (i.e.
ordinary  people)  rather  than   full-time  professionals  it  could
not  be relied  upon  to  defend the  power  and  property rights  of
the  rich.  As we  noted  in  section  1.4, Rothbard  criticised  the
individualist anarchists  for supporting  juries for  essentially the
same reasons.</p>

<p>But,  as  is clear  from  Hart’s  account, Molinari  had  little
concern that  working class  people should  have a  say in  their own
lives beyond  consuming goods. His  perspective can be seen  from his
lament about  those <em>“colonies where slavery  has been abolished
without  the  compulsory labour  being  replaced  with an  equivalent
quantity  of  free  [sic!]  labour [i.e.,  wage  labour],  there  has
occurred  the opposite  of  what happens  everyday  before our  eyes.
Simple workers have been seen to exploit in their turn the industrial
<strong>entrepreneurs,</strong> demanding from  them wages which bear
absolutely no relation  to the legitimate share in  the product which
they ought to  receive. The planters were unable to  obtain for their
sugar a  sufficient price to  cover the  increase in wages,  and were
obliged to furnish  the extra amount, at first out  of their profits,
and then out of their very capital. A considerable number of planters
have been  ruined as a result  ... It is doubtless  better that these
accumulations of capital should be destroyed than that generations of
men should perish [Marx: ‘how generous of M. Molinari’] but would
it  not be  better if  both survived?”</em>  [quoted by  Karl Marx,
<strong>Capital</strong>, vol. 1, p. 937f]</p>

<p>So  workers  exploiting capital  is  the  <em>“opposite of  what
happens  everyday  before  our   eyes”</em>?  In  other  words,  it
is  normal that  entrepreneurs  <em>“exploit”</em> workers  under
capitalism?  Similarly,  what  is a  <em>“legitimate  share”</em>
which  workers  <em>“ought  to   receive”</em>?  Surely  that  is
determined by  the eternal  laws of  supply and  demand and  not what
the  capitalists  (or  Molinari)  thinks is  right?  And  those  poor
former  slave drivers,  they  really do  deserve  our sympathy.  What
horrors they face  from the impositions subjected upon  them by their
ex-chattels  — they  had to  reduce their  profits! How  dare their
ex-slaves  refuse to  obey them  in return  for what  their ex-owners
think  was  their  <em>“legitimate share  in  the  produce”</em>!
How <em>“simple”</em>  these workers  are, not  understanding the
sacrifices  their former  masters  suffer nor  appreciating how  much
more  difficult  it is  for  their  ex-masters to  create  <em>“the
product”</em> without the  whip and the branding iron  to aid them!
As  Marx so  rightly comments:  <em>“And  what, if  you please,  is
this ‘legitimate  share’, which, according to  [Molinari’s] own
admission,  the capitalist  in  Europe daily  neglects  to pay?  Over
yonder, in the colonies, where the  workers are so ‘simple’ as to
‘exploit’  the  capitalist, M.  Molinari  feels  a powerful  itch
to  use  police  methods  to  set  on the  right  road  that  law  of
supply and demand which  works automatically everywhere else.”</em>
[<strong>Op. Cit.</strong>, p. 937f]</p>

<p>An added difficulty  in arguing that Molinari was  an anarchist is
that  he was  a  contemporary of  Proudhon,  the first  self-declared
anarchist, and lived in a country with a vigorous anarchist movement.
Surely if  he was really an  anarchist, he would have  proclaimed his
kinship with Proudhon  and joined in the wider movement.  He did not,
as Hart notes as regards Proudhon:</p>

<blockquote class="citation">
“their differences in economic theory were considerable, and
it is probably for this reason  that Molinari refused to call himself
an  anarchist  in  spite  of their  many  similarities  in  political
theory. Molinari  refused to accept  the socialist economic  ideas of
Proudhon  ...  in Molinari’s  mind,  the  term ‘anarchist’  was
intimately linked with socialist  and statist economic views.”</em>
[<strong>Op. Cit.</strong>, p. 415]
</blockquote>

<p>Yet  Proudhon’s economic  views,  like  Godwin’s, flowed  from
his  anarchist analysis  and principles.  They cannot  be arbitrarily
separated as Hart suggests. So while arguing that <em>“Molinari was
just as much  an anarchist as Proudhon,”</em> Hart  forgets the key
issue.  Proudhon was  aware that  private property  ensured that  the
proletarian  did not  exercise <em>“self-government”</em>  during
working hours, i.e. was not  a self-governing individual. As for Hart
claiming that  Proudhon had <em>“statist economic  views”</em> it
simply shows how far  an “anarcho”-capitalist perspective is from
genuine anarchism.  Proudhon’s economic  analysis, his  critique of
private property and capitalism, flowed from his anarchism and was an
integral aspect of it.</p>

<p>To restrict anarchism  purely to opposition to the  state, Hart is
impoverishing anarchist  theory and  denying its history.  Given that
anarchism was  born from a  critique of  private property as  well as
government,  this  shows the  false  nature  of Hart’s  claim  that
<em>“Molinari was  the first  to develop  a theory  of free-market,
proprietary  anarchism  that extended  the  laws  of the  market  and
a  rigorous  defence  of  property to  its  logical  extreme.”</em>
[<strong>Op. Cit.</strong>,  p. 415  and p. 416]  Hart shows  how far
from  anarchism Molinari  was as  Proudhon had  turned his  anarchist
analysis to property, showing that <em>“defence of property”</em>
lead to the oppression of the many by the few in social relationships
identical to those  which mark the state.  Moreover, Proudhon, argued
the state would  always be required to defend  such social relations.
Privatising it would hardly be a step forward.</p>

<p>Unsurprisingly, Proudhon dismissed the idea that the laissez faire
capitalists  shared  his  goals. <em>“The  school  of  Say,”</em>
Proudhon  argued, was  <em>“the chief  focus of  counter-revolution
next  to  the  Jesuits”</em>  and <em>“has  for  ten  years  past
seemed  to exist  only  to  protect and  applaud  the execrable  work
of  the monopolists  of  money and  necessities,  deepening more  and
more  the obscurity  of a  science  naturally difficult  and full  of
complications.”</em> Much  the same  can be said  of “anarcho”-
capitalists,  incidentally.  For  Proudhon, <em>“the  disciples  of
Malthus and of Say, who oppose  with all their might any intervention
of the  State in  matters commercial  or industrial,  do not  fail to
avail  themselves of  this seemingly  liberal attitude,  and to  show
themselves  more revolutionary  than  the Revolution.  More than  one
honest  searcher has  been  deceived  thereby.”</em> However,  this
apparent “anti-statist”  attitude of supporters of  capitalism is
false  as  pure  free  market  capitalism  cannot  solve  the  social
question,  which  arises  because  of  capitalism  itself.  As  such,
it  was  impossible  to  abolish the  state  under  capitalism.  Thus
<em>“this inaction of Power in  economic matters was the foundation
of government. What need should  we have of a political organisation,
if  Power   once  permitted  us  to   enjoy  economic  order?”</em>
Instead  of capitalism,  Proudhon  advocated the  <em>“constitution
of  Value,”</em>  the  <em>“organisation of  credit,”</em>  the
elimination of  interest, the <em>“establishment  of workingmen’s
associations”</em>  and <em>“the  use  of  a just  price.”</em>
[<strong>The General Idea of the  Revolution</strong>, p. 225, p. 226
and p. 233]</p>

<p>Clearly, then, the  claims that Molinari was an  anarchist fail as
he, unlike his followers, were aware of what anarchism actually stood
for. Hart, in his own way, acknowledges this:</p>

<blockquote class="citation">
“In  spite of  his protestations  to the  contrary, Molinari
should  be  considered  an  anarchist  thinker.  His  attack  on  the
state’s monopoly of defence must  surely warrant the description of
anarchism. His reluctance to accept  this label stemmed from the fact
that  the  socialists  had  used  it first  to  describe  a  form  of
non-statist  society which  Molinari  definitely  opposed. Like  many
original  thinkers, Molinari  had to  use the  concepts developed  by
others to  describe his  theories. In  his case, he  had come  to the
same  political  conclusions  as the  communist  anarchists  although
he  had  been  working  within  the  liberal  tradition,  and  it  is
therefore not surprising that the terms  used by the two schools were
not  compatible.  It would  not  be  until  the  latter half  of  the
twentieth  century that  radical, free-trade  liberals would  use the
word ‘anarchist’ to  describe their beliefs.”</em> [<strong>Op.
Cit.</strong>, p. 416]
</blockquote>

<p>It  should  be  noted  that Proudhon  was  <strong>not</strong>  a
communist-anarchist,  but the  point remains.  The aims  of anarchism
were  recognised   by  Molinari   as  being  inconsistent   with  his
ideology. Consequently, he  (rightly) refused the label.  If only his
self-proclaimed followers in the  <em>“latter half of the twentieth
century”</em> did the same anarchists would not have to bother with
them!</p>

<p>As    such,    it   seems    ironic    that    the   founder    of
“anarcho”-capitalism should  have come to the  same conclusion as
modern day anarchists on the subject  of whether his ideas are a form
of anarchism or not!</p>

<h4 id="toc48">7.2 Is government compatible with anarchism?</h4>

<p>Of course not, but ironically this is the conclusion arrived at by
Hart’s  analyst of  the  British “voluntaryists,”  particularly
Auberon Herbert.  Voluntaryism was  a fringe  part of  the right-wing
individualist movement  inspired by Herbert Spencer,  a spokesman for
free market capitalism  in the later half of  the nineteenth century.
As with  Molinari, there is  a problem with presenting  this ideology
as  anarchist, namely  that  its leading  light, Herbert,  explicitly
rejected the label “anarchist.”</p>

<p>Herbert  was   clearly  aware   of  individualist   anarchism  and
distanced himself  from it.  He argued  that such  a system  would be
<em>“pandemonium.”</em> He thought  that people should <em>“not
direct  our attacks  —  as the  anarchists  do —  <strong>against
all government</strong>  , against  government in  itself”</em> but
<em>“only  against the  overgrown, the  exaggerated, the  insolent,
unreasonable and  indefensible forms  of government, which  are found
everywhere  today.”</em>   Government  should   be  <em>“strictly
limited  to  its  legitimate  duties  in  defence  of  self-ownership
and  individual  rights.”</em>  He   stressed  that  <em>“we  are
governmentalists ... formally constituted by the nation, employing in
this matter  of force the majority  method.”</em> Moreover, Herbert
knew of, and rejected, individualist  anarchism, considering it to be
<em>“founded  on a  fatal  mistake.”</em>  [<strong>Essay X:  The
Principles Of  Voluntaryism And  Free Life</strong>] As  such, claims
that  he  was  an  anarchist or  “anarcho”-capitalist  cannot  be
justified.</p>

<p>Hart  is  aware  of   this  slight  problem,  quoting  Herbert’s
claim  that he  aimed  for  <em>“regularly constituted  government,
generally  accepted  by  all  citizens  for  the  protection  of  the
individual.”</em>  [quoted by  Hart, <strong>Op.  Cit.</strong>, p.
86] Like  Molinari, Herbert was  aware that  anarchism was a  form of
socialism  and that  the  political aims  could  not be  artificially
separated  from  its  economic  and  social aims.  As  such,  he  was
right <strong>not</strong>  to call his  ideas anarchism as  it would
result  in confusion  (particularly as  anarchism was  a much  larger
movement than  his). As  Hart acknowledges, <em>“Herbert  faced the
same  problems  that  Molinari  had with  labelling  his  philosophy.
Like  Molinari,  he  rejected  the  term  ‘anarchism,’  which  he
associated with the socialism  of Proudhon and ... terrorism.”</em>
While  <em>“quite  tolerant”</em>   of  individualist  anarchism,
he  thought   they  <em>“were  mistaken  in   their  rejections  of
‘government.’”</em>  However, Hart  knows  better than  Herbert
about his  own ideas, arguing that  his ideology <em>“is in  fact a
new form of anarchism, since the  most important aspect of the modern
state,  the  monopoly  of the  use  of  force  in  a given  area,  is
rejected  in no  uncertain  terms by  both men.”</em>  [<strong>Op.
Cit.</strong>, p.  86] He  does mention  that Benjamin  Tucker called
Herbert a  <em>“true anarchist in everything  but name,”</em> but
Tucker denied that Kropotkin was  an anarchist suggesting that he was
hardly a reliable guide.  [quoted by Hart, <strong>Op. Cit.</strong>,
p.  87] As  it  stands, it  seems  that Tucker  was  mistaken in  his
evaluation of Herbert’s politics.</p>

<p>Economically, Herbert was not an anarchist, arguing that the state
should protect  Lockean property  rights. Of  course, Hart  may argue
that  these economic  differences are  not relevant  to the  issue of
Herbert’s anarchism  but that  is simply to  repeat the  claim that
anarchism is simply concerned with  government, a claim which is hard
to  support. This  position  cannot be  maintained,  given that  both
Herbert and Molinari defended the  right of capitalists and landlords
to force  their employees and  tenants to follow their  orders. Their
“governments” existed  to defend  the capitalist  from rebellious
workers, to  break unions, strikes  and occupations. In  other words,
they were a monopoly  of the use of force in a  given area to enforce
the monopoly  of power  in a  given area (namely,  the wishes  of the
property owner). While they may  have argued that this was “defence
of liberty,” in reality it is defence of power and authority.</p>

<p>What about if we just look  at the political aspects of his ideas?
Did Herbert actually advocate anarchism?  No, far from it. He clearly
demanded a minimal state based on voluntary taxation. The state would
not use force of any  kind, <em>“except for purposes of restraining
force.”</em> He argued  that in his system,  while <em>“the state
should compel no services and  exact no payments by force,”</em> it
<em>“should  be  free  to  conduct  many  useful  undertakings  ...
in  competition with  all  voluntary agencies  ...  in dependence  on
voluntary payments.”</em>  [Herbert, <strong>Op.  Cit.</strong>] As
such, <em>“the  state”</em> would remain  and unless he  is using
the term “state” in some highly  unusual way, it is clear that he
means  a  system  where  individuals  live  under  a  single  elected
government as  their common  law maker, judge  and defender  within a
given territory.</p>

<p>This  becomes  clearer  once  we  look  at  how  the  state  would
be  organised.  In   his  essay  <em>“A  Politician   in  Sight  of
Haven,”</em> Herbert  does discuss the franchise,  stating it would
be limited to  those who paid a  voluntary <em>“income tax,”</em>
anyone <em>“paying it  would have the right to vote;  those who did
not pay it would  be — as is just —  without the franchise. There
would be no other tax.”</em> The  law would be strictly limited, of
course, and the  <em>“government ... must confine  itself simply to
the  defense of  life and  property, whether  as regards  internal or
external  defense.”</em>  In other  words,  Herbert  was a  minimal
statist,  with his  government elected  by  a majority  of those  who
choose to pay their  income tax and funded by that  (and by any other
voluntary  taxes  they  decided  to  pay).  Whether  individuals  and
companies could  hire their own  private police  in such a  regime is
irrelevant in determining whether it is an anarchy.</p>

<p>This  can be  best seen  by comparing  Herbert with  Ayn Rand.  No
one  would ever  claim  Rand was  an anarchist,  yet  her ideas  were
extremely  similar  to  Herbert’s.  Like  Herbert,  Rand  supported
laissez-faire  capitalism  and  was   against  the  “initiation  of
force.”  Like Herbert,  she  extended this  principle  to favour  a
government funded by voluntary  means [<em>“Government Financing in
a Free  Society,”</em> <strong>The Virtue  of Selfishness</strong>,
pp. 116–20] Moreover, like Herbert,  she explicitly denied being an
anarchist  and, again  like Herbert,  thought the  idea of  competing
defence  agencies  (“governments”)  would result  in  chaos.  The
similarities with Herbert are  clear, yet no “anarcho”-capitalist
would  claim that  Rand  was an  anarchist, yet  they  do claim  that
Herbert was.</p>

<p>This  position is,  of  course, deeply  illogical  and flows  from
the   non-anarchist  nature   of  “anarcho”-capitalism.   Perhaps
unsurprisingly,   when   Rothbard   discusses  the   ideas   of   the
“voluntaryists”  he  fails  to  address  the  key  issue  of  who
determines the laws being enforced  in society. For Rothbard, the key
issue is  <strong>who</strong> is enforcing  the law, not  where that
law  comes  from  (as long,  of  course,  as  it  is a  law  code  he
approves of). The implications of  this is significant, as it implies
that  “anarchism” need  not be  opposed to  either the  state nor
government! This  can be clearly  seen from Rothbard’s  analysis of
voluntary taxation.</p>

<p>Rothbard,  correctly,  notes   that  Herbert  advocated  voluntary
taxation as  the means  of funding  a state whose  basic role  was to
enforce  Lockean property  rights. For  Rothbard, the  key issue  was
<strong>not</strong> who determines the law  but who enforces it. For
Rothbard, it should  be privatised police and courts  and he suggests
that the <em>“voluntary taxationists have never attempted to answer
this problem; they  have rather stubbornly assumed that  no one would
set  up a  competing defence  agency within  a State’s  territorial
limits.”</em>  If the  state <strong>did</strong>  bar such  firms,
then that system is not a genuine free market. However, <em>“if the
government  <strong>did</strong> permit  free competition  in defence
service, there would soon no longer  be a central government over the
territory.  Defence  agencies,  police and  judicial,  would  compete
with  one another  in  the  same uncoerced  manner  as the  producers
of  any  other  service  on the  market.”</em>  [<strong>Power  and
Market</strong>, p. 122 and p. 123]</p>

<p>However,  this  misses  the  point totally.  The  key  issue  that
Rothbard  ignores is  who  determines the  laws  which these  private
“defence” agencies would enforce. If the laws are determined by a
central government, then  the fact that citizen’s  can hire private
police  and attend  private courts  does  not stop  the regime  being
statist. We  can safely assume Rand,  for example, would have  had no
problem  with  companies providing  private  security  guards or  the
hiring of private detectives within the context of her minimal state.
Ironically,  Rothbard stresses  the need  for such  a monopoly  legal
system:</p>

<blockquote class="citation">
“While ‘the  government’ would cease to  exist, the same
cannot be said for  a constitution or a rule of  law, which, in fact,
would  take on  in the  free society  a far  more important  function
than at  present. For  the freely  competing judicial  agencies would
have  to be  guided by  a  body of  absolute  law to  enable them  to
distinguish  objectively  between  defence and  invasion.  This  law,
embodying elaborations upon the basic injunction to defend person and
property from acts of invasion, would  be codified in the basic legal
code. Failure  to establish such  a code of  law would tend  to break
down  the  free  market,  for then  defence  against  invasion  could
not  be adequately  achieved.”</em> [<strong>Op.  Cit.</strong>, p.
123–4]
</blockquote>

<p>So  if  you  violate  the  <em>“absolute  law”</em>  defending
(absolute) property rights then you  would be in trouble. The problem
now lies  in determining  who sets  that law.  Rothbard is  silent on
how  his  system  of  monopoly  laws  are  determined  or  specified.
The  “voluntaryists” did  propose  a solution,  namely a  central
government elected by  the majority of those  who voluntarily decided
to pay an income tax. In the words of Herbert:</p>

<blockquote class="citation">
“We  agree that  there  must  be a  central  agency to  deal
with  crime —  an  agency  that defends  the  liberty  of all  men,
and  employs  force  against  the  uses  of  force;  but  my  central
agency  rests upon  voluntary  support, whilst  Mr. Levy’s  central
agency rests  on compulsory support.”</em> [quoted  by Carl Watner,
<em>“The English Individualists As  They Appear In Liberty,”</em>
pp.  191–211,  <strong>Benjamin  R.  Tucker and  the  Champions  of
Liberty</strong>, p. 194]
</blockquote>

<p>And all  Rothbard is  concerned over private  cops would  exist or
not!  This lack  of  concern  over the  existence  of  the state  and
government flows from the strange fact that “anarcho”-capitalists
commonly use the term “anarchism” to refer to any philosophy that
opposes all forms of initiatory coercion. Notice that government does
not  play  a part  in  this  definition,  thus Rothbard  can  analyse
Herbert’s politics without commenting on who determines the law his
private  “defence”  agencies  enforce.  For  Rothbard,  <em>“an
anarchist society”</em> is defined <em>“as  one where there is no
legal  possibility for  coercive  aggression against  the person  and
property of  any individual.”</em>  He then  moved onto  the state,
defining that  as an <em>“institution  which possesses one  or both
(almost always  both) of  the following  properties: (1)  it acquires
its  income by  the physical  coercion known  as ‘taxation’;  and
(2)  it  acquires and  usually  obtains  a  coerced monopoly  of  the
provision  of  defence  service  (police and  courts)  over  a  given
territorial area.”</em> [<em>“Society without a State”</em>, in
<strong>Nomos XIX</strong>, Pennock and Chapman (eds.)., p. 192]</p>

<p>This  is  highly  unusual  definition  of  “anarchism,”  given
that  it  utterly  fails  to  mention  or  define  government.  This,
perhaps,  is understandable  as any  attempt  to define  it in  terms
of  <em>“monopoly  of   decision-making  power”</em>  results  in
showing that  capitalism is  statist (see section  1 for  a summary).
The  key issue  here  is the  term <em>“legal  possibility.”</em>
That  suggestions   a  system  of   laws  which  determine   what  is
<em>“coercive aggression”</em>  and what constitutes what  is and
what  is  not legitimate  “property.”  Herbert  is considered  by
“anarcho”-capitalists  as  one of  them.  Which  brings us  to  a
strange conclusion, that for “anarcho”-capitalists you can have a
system of  “anarchism” in which  there is a government  and state
— as  long as the state  does not impose taxation  nor stop private
police forces from operating!</p>

<p>As  Rothbard  argues <em>“if  a  government  based on  voluntary
taxation  permits free  competition, the  result will  be the  purely
free-market system  ... The previous  government would now  simply be
one  competing  defence  agency  among many  on  the  market.”</em>
[<strong>Power and  Market</strong>, p.  124] That the  government is
specifying what is  and is not legal  does not seem to  bother him or
even cross his mind. Why should  it, when the existence of government
is  irrelevant to  his definition  of anarchism  and the  state? That
private  police  are  enforcing  a monopoly  law  determined  by  the
government seems hardly  a step in the right direction  nor can it be
considered as anarchism. Perhaps this  is unsurprising, for under his
system there would  be <em>“a basic, common  Law Code”</em> which
<em>“all would have to abide  by”</em> as well as <em>“some way
of resolving disputes that will  gain a majority consensus in society
... whose  decision will  be accepted  by the  great majority  of the
public.”</em> [<em>“Society without  a State,”</em> <strong>Op.
Cit.</strong>, p. 205]</p>

<p>At least Herbert is clear that  this would be a government system,
unlike Rothbard  who assumes a monopoly  law but seems to  think that
this is not a  government or a state. As David  Wieck argued, this is
illogical for  according to  Rothbard <em>“all ‘would  have to’
conform to the same legal code”</em>  and this can only be achieved
by  means of  <em>“the forceful  action  of adherents  to the  code
against  those  who flout  it”</em>  and  so <em>“in  his  system
<strong>there  would stand  over against  every individual  the legal
authority  of all  the  others.</strong> An  individual  who did  not
recognise private  property as legitimate would  surely perceive this
as  a tyranny  of law,  a  tyranny of  the  majority or  of the  most
powerful  — in  short, a  hydra-headed state.  If the  law code  is
itself  unitary, then  this  multiple  state might  be  said to  have
properly a  single head —  the law ... But  it looks as  though one
might still  call this ‘a state,’  under Rothbard’s definition,
by satisfying <strong>de facto</strong> one of his pair of sufficient
conditions: ‘It asserts  and usually obtains a  coerced monopoly of
provision  of  defence  service  (police and  courts)  over  a  given
territorial area’  ... Hobbes’s  individual sovereign  would seem
to  have  become many  sovereigns  —  with  but one  law,  however,
and  in  truth, therefore,  a  single  sovereign in  Hobbes’s  more
important  sense of  the  latter  term. One  might  better, and  less
confusingly, call  this a libertarian state  than an anarchy.”</em>
[<em>“Anarchist  Justice”</em>,  in  <strong>Nomos  XIX</strong>,
Pennock and Chapman (eds.)., pp. 216–7]</p>

<p>The obvious recipients  of the coercion of the new  state would be
those who rejected the authority of their bosses and landlords, those
who  reject the  Lockean property  rights Rothbard  and Herbert  hold
dear. In such  cases, the rebels and any  “defence agency” (like,
say, a union) which defended them  would be driven out of business as
it  violated  the  law  of  the land.  How  this  is  different  from
a  state  banning  competing  agencies is  hard  to  determine.  This
is  a <em>“difficulty”</em>  argues  Wieck, which  <em>“results
from  the attachment  of  a  principle of  private  property, and  of
unrestricted accumulation  of wealth, to the  principle of individual
liberty. This increases sharply  the possibility that many reasonable
people who  respect their fellow  men and women will  find themselves
outside the  law because  of dissent  from a  property interpretation
of  liberty.”</em>   Similarly,  there  is  the   economic  results
of  capitalism.  <em>“One  can  imagine,”</em>  Wieck  continues,
<em>“that  those who  lose out  badly  in the  free competition  of
Rothbard’s economic  system, perhaps  a considerable  number, might
regard the legal  authority as an alien power, state  for them, based
on violence,  and might be  quite unmoved by  the fact that,  just as
under nineteenth century  capitalism, a principle of  liberty was the
justification for it all.”</em>  [<strong>Op. Cit.</strong>, p. 217
and pp. 217–8]</p>

<h4 id="toc49">7.3 Can there be a “right-wing” anarchism?</h4>

<p>Hart,   of   course,   mentions  the   individualist   anarchists,
calling   Tucker’s   ideas  <em>“<strong>laissez   faire</strong>
liberalism.”</em>  [<strong>Op.  Cit.</strong>,   p.  87]  However,
Tucker  called his  ideas  <em>“socialism”</em>  and presented  a
left-wing critique  of most  aspects of liberalism,  particularly its
Lockean based private property rights. Tucker based much of his ideas
on  property on  Proudhon,  so  if Hart  dismisses  the  latter as  a
socialist then  this must apply  to the  former. Given that  he notes
that there  are <em>“two  main kinds of  anarchist thought,”</em>
namely  <em>“communist  anarchism  which  denies the  right  of  an
individual  to  seek profit,  charge  rent  or  interest and  to  own
property”</em> and a <em>“‘right-wing’ proprietary anarchism,
which  vigorously  defends  these rights”</em>  then  Tucker,  like
Godwin, would have to be placed in the <em>“left-wing”</em> camp.
[<em>“Gustave de  Molinari and the Anti-statist  Liberal Tradition:
Part  II”</em>, <strong>Op.  Cit.</strong>, p.  427] Tucker,  after
all, argued  that he aimed for  the end of profit,  interest and rent
and attacked private property in land and housing beyond “occupancy
and use.”</p>

<p>As   can   be  seen,   Hart’s   account   of  the   history   of
“anti-state”  liberalism is  flawed. Godwin  is included  only by
ignoring his views on property, views which in many ways reflects the
later “socialist” (i.e. anarchist)  analysis of Proudhon. He then
discusses a  few individuals  who were alone  in their  opinions even
within extreme  free market right and  all of whom knew  of anarchism
and explicitly rejected the name  for their respective ideologies. In
fact, they  preferred the term <em>“government”</em>  to describe
their systems  which, on the face  of it, would be  hard to reconcile
with the  usual “anarcho”-capitalist  definition of  anarchism as
being  “no  government.”  Hart’s  discussion  of  individualist
anarchism is equally flawed, failing  to discuss their economic views
(just as  well, as  its links to  “left-wing” anarchism  would be
obvious).</p>

<p>However, the  similarities of  Molinari’s views with  what later
became  known  as  “anarcho”-capitalism  are  clear.  Hart  notes
that  with Molinari’s  death in  1912, <em>“liberal  anti-statism
virtually  disappeared until  it  was rediscovered  by the  economist
Murray  Rothbard  in  the late  1950’s”</em>  [<em>“Gustave  de
Molinari and  the Anti-statist  Liberal Tradition:  Part III”</em>,
<strong>Op.  Cit.</strong>,  p. 88]  While  this  fringe is  somewhat
bigger than  previously, the  fact remains  that the  ideas expounded
by  Rothbard  are  just  as  alien  to  the  anarchist  tradition  as
Molinari’s. It is a shame that Rothbard, like his predecessors, did
not call his ideology something  other than anarchism. Not only would
it have  been more  accurate, it  would also have  lead to  much less
confusion and no need to write this section of the FAQ! As it stands,
the only reason why “anarcho”-capitalism  is considered a form of
“anarchism” by some  is because one person  (Rothbard) decided to
steal the  name of  a well established  and widespread  political and
social theory and  movement and apply it to an  ideology with little,
if anything, in common with it.</p>

<p>As  Hart inadvertently  shows,  it is  not a  firm  base to  build
a  claim.  That  anyone   can  consider  “anarcho”-capitalism  as
anarchist simply flows  from a lack of knowledge  about anarchism. As
numerous  anarchists have  argued.  For example,  <em>“Rothbard’s
conjunction of anarchism with  capitalism,”</em> according to David
Wieck, <em>“results  in a conception  that is entirely  outside the
mainstream of anarchist theoretical  writings or social movements ...
this conjunction  is a  self-contradiction.”</em> He  stressed that
<em>“the main traditions of anarchism are entirely different. These
traditions, and  theoretical writings  associated with  them, express
the  perspectives  and  the  aspirations, and  also,  sometimes,  the
rage,  of the  oppressed  people  in human  society:  not only  those
economically oppressed,  although the major anarchist  movements have
been  mainly  movements  of  workers and  peasants,  but  also  those
oppressed by  power in all  those social dimensions ...  including of
course that  of political  power expressed  in the  state.”</em> In
other  words,  <em>“anarchism  represents ...  a  moral  commitment
(Rothbard’s anarchism I take to be diametrically opposite).”</em>
[<em>“Anarchist  Justice”</em>,  in  <strong>Nomos  XIX</strong>,
Pennock and Chapman (eds.), p. 215, p. 229 and p. 234]</p>

<p>It  is  a  shame  that  some  academics  consider  only  the  word
Rothbard uses  as relevant rather  than the content and  its relation
to  anarchist  theory and  history.  If  they  did, they  would  soon
realise  that  the expressed  opposition  of  so many  anarchists  to
“anarcho”-capitalism  is something  which  cannot  be ignored  or
dismissed.  In  other  words,  a  “right-wing”  anarchist  cannot
and  does not  exist,  no matter  how  often they  use  that word  to
describe their  ideology. As  Bob Black  put it,  <em>“a right-wing
anarchist is just  a minarchist who’d abolish the state  to his own
satisfaction by calling  it something else ...  They don’t denounce
what the  state does, they  just object to who’s  doing it.”</em>
[<strong>Libertarian as Conservative</strong>]</p>

<p>The reason is  simple. Anarchist economics and  politics cannot be
artificially separated, they are linked.  Godwin and Proudhon did not
stop  their  analysis at  the  state.  They  extended it  the  social
relationships produced  by inequality of wealth,  i.e. economic power
as  well  as political  power.  To  see  why,  we need  only  consult
Rothbard’s work. As noted in the last section, for Rothbard the key
issue with  the “voluntary  taxationists” was not  who determined
the <em>“body of  absolute law”</em> but rather  who enforced it.
In his discussion,  he argued that a  democratic “defence agency”
is  at a  disadvantage in  his “free  market” system.  As he  put
it:</p>

<blockquote class="citation">
“It would, in  fact, be competing at  a severe disadvantage,
having been established on  the principle of ‘democratic voting.’
Looked at as  a market phenomenon, ‘democratic  voting’ (one vote
per person) is simply the  method of the consumer ‘co-operative.’
Empirically,   it  has   been  demonstrated   time  and   again  that
co-operatives   cannot  compete   successfully  against   stock-owned
companies, especially when both are equal before the law. There is no
reason to  believe that co-operatives  for defence would be  any more
efficient. Hence,  we may expect  the old co-operative  government to
‘wither  away’ through  loss of  customers on  the market,  while
joint-stock  (i.e.,  corporate)  defence agencies  would  become  the
prevailing market form.”
</blockquote>

<p>Notice how  he assumes  that both  a co-operative  and corporation
would  be <em>“equal  before  the law.”</em>  But who  determines
that  law? Obviously  <strong>not</strong>  a democratically  elected
government, as the idea of  “one person, one vote” in determining
the common law all are  subject to is <em>“inefficient.”</em> Nor
does  he  think, like  the  individualist  anarchists, that  the  law
would  be judged  by  juries along  with  the facts.  As  we note  in
section 1.4,  he rejects  that in  favour of  it being  determined by
<em>“Libertarian  lawyers  and  jurists.”</em> Thus  the  law  is
unchangeable  by  ordinary people  and  enforced  by private  defence
agencies  hired to  protect the  liberty and  property of  the owning
class.  In the  case of  a capitalist  economy, this  means defending
the  power of  landlords and  capitalists against  rebel tenants  and
workers.</p>

<p>This means  that Rothbard’s <em>“common Law  Code”</em> will
be  determined, interpreted,  enforced  and  amended by  corporations
based on  the will of  the majority  of shareholders, i.e.  the rich.
That hardly  seems likely to produce  equality before the law.  As he
argues in a footnote:</p>

<blockquote class="citation">
“There  is a  strong  <strong>a  priori</strong> reason  for
believing that corporations will be  superior to co-operatives in any
given situation. For if each  owner receives only one vote regardless
of how  much money  he has  invested in a  project (and  earnings are
divided in the  same way), there is no incentive  to invest more than
the  next man;  in  fact,  every incentive  is  the  other way.  This
hampering of  investment militates strongly against  the co-operative
form.”
</blockquote>

<p>So  <strong>if</strong>  the  law  is determined  by  the  defence
agencies and  courts then  it will  be determined  by those  who have
invested most  in these companies.  As it  is unlikely that  the rich
will  invest in  defence firms  which do  not support  their property
rights, power, profits and definition of property rights, it is clear
that agencies  which favour the  wealthy will survive on  the market.
The  idea that  market  demand  will counter  this  class rule  seems
unlikely, given  Rothbard’s own  argument. After  all, in  order to
compete successfully  you need more  than demand, you need  source of
investment. If co-operative defence agencies do form, they will be at
a  market  disadvantage due  to  lack  of  investment. As  argued  in
section  J.5.12, even  though co-operatives  are more  efficient than
capitalist firms lack of investment (caused by the lack of control by
capitalists Rothbard  notes) stops them replacing  wage slavery. Thus
capitalist  wealth  and  power  inhibits the  spread  of  freedom  in
production. If we  apply his own argument to  Rothbard’s system, we
suggest that the market in “defence” will also stop the spread of
more libertarian associations thanks  to capitalist power and wealth.
In other  words, like  any market, Rothbard’s  “defence” market
will simply reflect the interests of the elite, not the masses.</p>

<p>Moreover,  we  can  expect  any democratic  defence  agency  (like
a   union)   to  support,   say,   striking   workers  or   squatting
tenants,  to  be crushed.  This  is  because, as  Rothbard  stresses,
<strong>all</strong> “defence”  firms would be expected  to apply
the  <em>“common”</em>  law,  as  written  by  <em>“Libertarian
lawyers and  jurists.”</em> If they  did not they would  quickly be
labelled “outlaw” agencies and crushed by the others. Ironically,
Tucker would join  Bakunin and Kropotkin in  an “anarchist” court
accused to violating “anarchist” law by practising and advocating
“occupancy and use” rather than the approved Rothbardian property
rights. Even if these democratic “defence” agencies could survive
and not  be driven  out of  the market  by a  combination of  lack of
investment and  violence due to  their “outlaw” status,  there is
another  problem.  As  we  discussed  in  section  1,  landlords  and
capitalists  have a  monopoly  of decision  making  power over  their
property. As such, they can simply refuse to recognise any democratic
agency as a  legitimate defence association and use  the same tactics
perfected against unions  to ensure that it does not  gain a foothold
in their domain (see section 6 for more details).</p>

<p>Clearly, then,  a “right-wing” anarchism is  impossible as any
system  based  on  capitalist  property  rights  will  simply  be  an
oligarchy run by and for the  wealthy. As Rothbard notes, any defence
agency  based  on  democratic  principles will  not  survive  in  the
“market” for defence simply because it does not allow the wealthy
to control it  and its decisions. Little wonder  Proudhon argued that
laissez-faire  capitalism  meant  <em>“the victory  of  the  strong
over  the  weak,  of  those  who own  property  over  those  who  own
nothing.”</em>  [quoted by  Peter  Marshall, <strong>Demanding  the
Impossible</strong>, p. 259]</p>

<h3 id="toc50">8 What role did the state take in the creation of capitalism?</h3>

<p>If the “anarcho”-capitalist is  to claim with any plausibility
that  “real”  capitalism is  non-statist  or  that it  can  exist
without a state, it must  be shown that capitalism evolved naturally,
in  opposition  to  state  intervention.  However,  in  reality,  the
opposite is  the case.  Capitalism was  born from  state intervention
and,  in   the  words   of  Kropotkin,   <em>“the  State   ...  and
capitalism  ...  developed  side  by side,  mutually  supporting  and
re-enforcing each other.”</em> [<strong>Kropotkin’s Revolutionary
Pamphlets</strong>, p. 181]</p>

<p>Numerous   writers   have   made    this   point.   For   example,
in   Karl   Polanyi’s    flawed   masterpiece   <strong>The   Great
Transformation</strong>  we   read  that   <em>“the  road   to  the
free   market   was   opened   and   kept   open   by   an   enormous
increase   in   continuous,   centrally  organised   and   controlled
interventionism”</em> by the state [p. 140]. This intervention took
many forms — for  example, state support during “mercantilism,”
which allowed  the “manufactures” (i.e. industry)  to survive and
develop, enclosures  of common land,  and so forth. In  addition, the
slave trade,  the invasion  and brutal conquest  of the  Americas and
other “primitive” nations,  and the looting of  gold, slaves, and
raw materials from abroad also  enriched the European economy, giving
the development of capitalism an added boost. Thus Kropotkin:</p>

<blockquote class="citation">
“The history of the genesis of capital has already been told
by socialists  many times.  They have  described how  it was  born of
war  and  pillage,  of  slavery  and serfdom,  of  modern  fraud  and
exploitation. They have shown how it is nourished by the blood of the
worker,  and  how  little  by  little  it  has  conquered  the  whole
world.”</em> [<strong>Op. Cit.</strong>,p. 207]
</blockquote>

<p>Or, if  Kropotkin seems  too committed  to be  fair, we  have John
Stuart Mill’s statement that:</p>

<blockquote class="citation">
“The social  arrangements of modern Europe  commenced from a
distribution of property which was the result, not of just partition,
or acquisition by industry, but  of conquest and violence... “</em>
[<strong>Principles of Political Economy</strong>, p. 15]
</blockquote>

<p>Therefore,  when supporters  of  “libertarian” capitalism  say
they  are  against the  “initiation  of  force,” they  mean  only
<strong>new</strong>  initiations  of  force;  for  the  system  they
support was born from numerous initiations of force in the past. And,
as  can be  seen from  the history  of the  last 100  years, it  also
requires state  intervention to  keep it  going (section  D.1, “Why
does  state  intervention occur?,”  addresses  this  point in  some
detail). Indeed, many thinkers have argued that it was precisely this
state  support and  coercion (particularly  the separation  of people
from the land) that played  the <strong>key</strong> role in allowing
capitalism  to develop  rather than  the theory  that <em>“previous
savings”</em> did so. As the noted German thinker Franz Oppenheimer
argued,  <em>“the concept  of  a  ‘primitive accumulation,’  or
an  original  store of  wealth,  in  land  and in  movable  property,
brought  about  by  means  of purely  economic  forces”</em>  while
<em>“seem[ing]  quite plausible”</em>  is in  fact <em>“utterly
mistaken; it is a ‘fairly tale,’ or  it is a class theory used to
justify  the privileges  of the  upper classes.”</em>  [<strong>The
State</strong>, pp. 5–6]</p>

<p>This thesis will be discussed in the following sections. It is, of
course, ironic to hear right-wing  libertarians sing the praises of a
capitalism that never  existed and urge its adoption  by all nations,
in  spite  of the  historical  evidence  suggesting that  only  state
intervention made capitalist economies viable  — even in that Mecca
of “free enterprise,” the United  States. As Noam Chomsky argues,
<em>“who  but a  lunatic could  have opposed  the development  of a
textile industry in New England in the early nineteenth century, when
British textile production  was so much more efficient  that half the
New England industrial  sector would have gone  bankrupt without very
high protective  tariffs, thus terminating industrial  development in
the  United States?  Or the  high tariffs  that radically  undermined
economic  efficiency to  allow  the United  States  to develop  steel
and  other  manufacturing capacities?  Or  the  gross distortions  of
the  market that  created modern  electronics?”</em> [<strong>World
Orders,  Old and  New</strong>, p.  168]. To  claim, therefore,  that
“mercantilism”  is not  capitalism  makes  little sense.  Without
mercantilism, “proper” capitalism would never have developed, and
any attempt  to divorce a social  system from its roots  is ahistoric
and makes a mockery of critical thought.</p>

<p>Similarly,  it is  somewhat ironic  when “anarcho”-capitalists
and  right  libertarians  claim  that they  support  the  freedom  of
individuals to choose  how to live. After all, the  working class was
not given <strong>that</strong> particular choice when capitalism was
developing. Indeed, their  right to choose their own way  of life was
constantly violated and denied. So to claim that <strong>now</strong>
(after capitalism has been created) we get the chance to try and live
as we like is insulting in the extreme. The available options we have
are not  independent of  the society  we live  in and  are decisively
shaped by  the past. To  claim we are “free”  to live as  we like
(within the  laws of capitalism)  is basically  to argue that  we are
able to “buy” the freedom that every individual is due from those
who have stolen it from us in the first place!</p>

<p>Needless to say, some  right-libertarians recognise that the state
played a massive role  in encouraging industrialisation (more correct
to say “proletarianisation”  as it created a  working class which
did not own the tools they used, although we stress that this process
started on the land and not  in industry). So they contrast “bad”
business  people  (who took  state  aid)  and “good”  ones.  Thus
Rothbard’s  comment that  Marxists have  <em>“made no  particular
distinction between ‘bourgeoisie’ who made  use of the state, and
bourgeoisie who acted on the free market.”</em> [<strong>The Ethics
of Liberty</strong>, p. 72]</p>

<p>But such an  argument is nonsense as it ignores  the fact that the
“free  market” is  a network  (and defined  by the  state by  the
property rights it enforces). For example, the owners of the American
steel  and other  companies who  grew  rich and  their companies  big
behind protectionist  walls are obviously “bad”  bourgeoisie. But
are  the bourgeoisie  who  supplied the  steel  companies with  coal,
machinery, food,  “defence” and  so on  not also  benefiting from
state action?  And the suppliers of  the luxury goods to  the wealthy
steel company owners, did they not  benefit from state action? Or the
suppliers of  commodities to the  workers that laboured in  the steel
factories that the  tariffs made possible, did they  not benefit? And
the  suppliers  to  these  suppliers?  And  the  suppliers  to  these
suppliers?  Did not  the users  of technology  first introduced  into
industry by companies protected by state orders also not benefit? Did
not the  capitalists who had  a large  and landless working  class to
select from  benefit from  the “land  monopoly” even  though they
may  not  have,  unlike  other capitalists,  directly  advocated  it?
It  increased  the  pool  of  wage  labour  for  <strong>all</strong>
capitalists  and increased  their  bargaining  position/power in  the
labour market  at the expense of  the working class. In  other words,
such a  policy helped maintain capitalist  market power, irrespective
of whether  individual capitalists encouraged politicians  to vote to
create/maintain it. And,  similarly, <strong>all</strong> capitalists
benefited from  the changes  in common law  to recognise  and protect
capitalist private property and rights that the state enforced during
the 19<sup>th</sup> century (see section B.2.5).</p>

<p>It  appears  that,  for  Rothbard,  the  collusion  between  state
and  business is  the fault,  not  of capitalism,  but of  particular
capitalists. The system  is pure; only individuals  are corrupt. But,
for  anarchists, the  origins of  the modern  state-capitalist system
lies not  in the individual qualities  of capitalists as such  but in
the  dynamic  and  evolution  of  capitalism  itself  —  a  complex
interaction of class interest, class struggle, social defence against
the destructive  actions of the  market, individual qualities  and so
forth. In other  words, Rothbard’s claims are flawed  — they fail
to understand capitalism as a <strong>system</strong> and its dynamic
nature.</p>

<p>Indeed,  if  we  look  at  the  role  of  the  state  in  creating
capitalism  we could  be tempted  to rename  “anarcho”-capitalism
“marxian-capitalism”.  This  is  because,  given  the  historical
evidence,  a  political   theory  can  be  developed   by  which  the
“dictatorship  of  the  bourgeoisie”  is created  and  that  this
capitalist  state “withers  away” into  anarchy. That  this means
rejecting  the  economic  and  social  ideas  of  Marxism  and  their
replacement by their  direct opposite should not mean  that we should
reject  the idea  (after all,  that is  what “anarcho”-capitalism
has  done  to  Individualist  Anarchism!). But  we  doubt  that  many
“anarcho”-capitalists will accept such a name change (even though
this would reflect  their politics far better; after all  they do not
object to  past initiations of force,  just current ones and  many do
seem to think that the modern state <strong>will</strong> wither away
due to market forces).</p>

<p>But this is  beside the point. The fact remains  that state action
was required to create and maintain capitalism. Without state support
it is doubtful that capitalism would have developed at all.</p>

<p>So, when  the right  suggests that  “we” be  “left alone,”
what they  mean by “we” comes  into clear focus when  we consider
how  capitalism developed.  Artisans and  peasants were  only “left
alone” to starve, and the  working classes of industrial capitalism
were only “left alone” outside work  and for only as long as they
respected the rules of their “betters.”  As for the other side of
the  class divide,  they desire  to be  “left alone”  to exercise
their power over others, as we will see. That modern “capitalism”
is, in  effect, a kind  of “corporate mercantilism,”  with states
providing the  conditions that  allow corporations to  flourish (e.g.
tax breaks,  subsidies, bailouts,  anti-labour laws, etc.)  says more
about the statist roots of  capitalism than the ideologically correct
definition of capitalism used by its supporters.</p>

<h4 id="toc51">8.1 What social forces lay behind the rise of capitalism?</h4>

<p>Capitalist society  is a relatively recent  development. As Murray
Bookchin points  out, for  a <em>“long  era, perhaps  spanning more
than five centuries,”</em>  capitalism <em>“coexisted with feudal
and simple commodity relationships”</em>  in Europe. He argues that
this  period <em>“simply  cannot be  treated as  ‘transitional’
without reading back the present into the past.”</em> [<strong>From
Urbanisation to  Cities</strong>, p. 179] In  other words, capitalism
was   not   a  inevitable   outcome   of   “history”  or   social
evolution.</p>

<p>He   goes  on   to  note   that  capitalism   existed  <em>“with
growing  significance in  the  mixed  economy of  the  West from  the
fourteenth  century  up  to   the  seventeenth”</em>  but  that  it
<em>“literally exploded into being in Europe, particularly England,
during  the eighteenth  and especially  nineteenth centuries.”</em>
[<strong>Op.  Cit.</strong>, p.  181] The  question arises,  what lay
behind  this  <em>“growing   significance”</em>?  Did  capitalism
<em>“explode”</em> due to its inherently more efficient nature or
where there other, non-economic, forces at  work? As we will show, it
was most  definitely the later one  — capitalism was born  not from
economic  forces  but  from  the  political  actions  of  the  social
elites which  its usury  enriched. Unlike artisan  (simple commodity)
production, wage labour generates inequalities and wealth for the few
and  so will  be  selected,  protected and  encouraged  by those  who
control the state in their own economic and social interests.</p>

<p>The  development  of capitalism  in  Europe  was favoured  by  two
social elites,  the rising  capitalist class within  the degenerating
medieval  cities and  the  absolutist state.  The  medieval city  was
<em>“thoroughly changed  by the  gradual increase  in the  power of
commercial capital,  due primarily  to foreign  trade... By  this the
inner unity  of the commune was  loosened, giving place to  a growing
caste  system and  leading  necessarily to  a progressive  inequality
of  social interests.  The  privileged minorities  pressed ever  more
definitely  towards  a  centralisation  of the  political  forces  of
the  community... Mercantilism  in the  perishing city  republics led
logically to a demand for  larger economic units [i.e. to nationalise
the market]; and by this the  desire for stronger political forms was
greatly strengthened... Thus the city gradually became a small state,
paving the way for the coming national state.”</em> [Rudolf Rocker,
<strong>Nationalism and Culture</strong>, p. 94]</p>

<p>The rising economic power of the proto-capitalists conflicted with
that of the  feudal lords, which meant that the  former required help
to  consolidate their  position. That  aid came  in the  form of  the
monarchical state.  With the force  of absolutism behind  it, capital
could  start the  process of  increasing its  power and  influence by
expanding the “market” through state action.</p>

<p>As  far  as the  absolutist  state  was concerned,  it  <em>“was
dependent  upon   the  help  of   these  new  economic   forces,  and
vice  versa...”  “The  absolutist state,”</em>  Rocker  argues,
<em>“whose coffers the  expansion of commerce filled  ..., at first
furthered the plans of commercial  capital. Its armies and fleets ...
contributed to  the expansion  of industrial production  because they
demanded  a number  of things  for whose  large-scale production  the
shops of small tradesmen were no longer adapted. Thus gradually arose
the  so-called  manufactures,  the  forerunners of  the  later  large
industries.”</em> [<strong>Op. Cit.</strong>, p. 117–8]</p>

<p>Some of  the most important  state actions from the  standpoint of
early  industry  were the  so-called  Enclosure  Acts, by  which  the
“commons” — the free farmland shared communally by the peasants
in most  rural villages —  was “enclosed” or  incorporated into
the estates  of various  landlords as  private property  (see section
8.3).  This ensured  a pool  of landless  workers who  had no  option
but  to sell  their  labour to  capitalists.  Indeed, the  widespread
independence caused by  the possession of the  majority of households
of land caused the rising  class of merchants to complain <em>“that
men who should  work as wage-labourers cling to the  soil, and in the
naughtiness  of  their hearts  prefer  independence  as squatters  to
employment by a master.”</em> [R.H  Tawney, cited by Allan Elgar in
<strong>The Apostles of Greed</strong>, p. 12]</p>

<p>In  addition, other  forms of  state aid  ensured that  capitalist
firms got a head start, so  ensuring their dominance over other forms
of  work (such  as co-operatives).  A major  way of  creating a  pool
of  resources that  could  be  used for  investment  was  the use  of
mercantilist  policies which  used protectionist  measures to  enrich
capitalists  and landlords  at  the expense  of  consumers and  their
workers.  For  example,  one  of  most  common  complaints  of  early
capitalists was  that workers  could not turn  up to  work regularly.
Once they had worked a few  days, they disappeared as they had earned
enough  money to  live on.  With higher  prices for  food, caused  by
protectionist measures, workers had to  work longer and harder and so
became  accustomed  to  factory  labour.  In  addition,  mercantilism
allowed  native industry  to develop  by barring  foreign competition
and  so allowed  industrialists  to reap  excess  profits which  they
could  then  use to  increase  their  investments.  In the  words  of
Marian-socialist economic historian Maurice Dobbs:</p>

<blockquote class="citation">
“In   short,  the   Mercantile  System   was  a   system  of
State-regulated  exploitation through  trade  which  played a  highly
important   rule  in   the   adolescence   of  capitalist   industry:
it   was   essentially   the   economic   policy   of   an   age   of
primitive   accumulation.”</em>   [<strong>Studies  in   Capitalism
Development</strong>, p. 209]
</blockquote>

<p>As  Rocker  summarises,  <em>“when  abolutism  had  victoriously
overcome all  opposition to national unification,  but its furthering
of  mercantilism  and economic  monopoly  it  gave the  whole  social
evolution a  direction which  could only lead  to capitalism.”</em>
[<strong>Op. Cit.</strong>, pp. 116–7]</p>

<p>This process of state aid  in capitalist development was also seen
in the  United States of  America. As  Edward Herman points  out, the
<em>“level  of government  involvement  in business  in the  United
States from the late eighteenth century to the present has followed a
U-shaped pattern: There was  extensive government intervention in the
pre-Civil  War period  (major subsidies,  joint ventures  with active
government participation  and direct  government production),  then a
quasi-laissez faire period  between the Civil War and the  end of the
nineteenth  century [a  period  marked by  “the  aggressive use  of
tariff protection” and state  supported railway construction, a key
factor in  capitalist expansion  in the USA],  followed by  a gradual
upswing of  government intervention  in the twentieth  century, which
accelerated after 1930.”</em> [<strong>Corporate Control, Corporate
Power</strong>, p. 162]</p>

<p>Such intervention ensured that income was transferred from workers
to  capitalists. Under  state protection,  America industrialised  by
forcing the  consumer to  enrich the  capitalists and  increase their
capital  stock. <em>“According  to  one study,  of  the tariff  had
been  removed  in  the  1830s ‘about  half  the  industrial  sector
of  New  England  would  have   been  bankrupted’  ...  the  tariff
became a near-permanent political institution representing government
assistance to manufacturing.  It kept price levels  from being driven
down  by foreign  competition  and thereby  shifted the  distribution
of  income  in  favour  of  owners  of  industrial  property  to  the
disadvantage of  workers and customers.”</em> [Richard  B. Du Boff,
<strong>Accumulation and Power</strong>, p. 56]</p>

<p>This  protection  was  essential,  for   as  Du  Boff  notes,  the
<em>“end  of   the  European   wars  in   1814  ...   reopened  the
United  States  to  a  flood  of  British  imports  that  drove  many
American competitors  out of  business. Large  portions of  the newly
expanded  manufacturing  base  were  wiped  out,  bringing  a  decade
of   near-stagnation.”</em>  Unsurprisingly,   the  <em>“era   of
protectionism  began  in 1816,  with  northern  agitation for  higher
tariffs... “</em> [<strong>Op. Cit.</strong>, p. 14, p. 55]</p>

<p>Combined  with  ready  repression   of  the  labour  movement  and
government “homesteading”  acts (see  section 8.5),  tariffs were
the American equivalent of mercantilism  (which, after all, was above
all else  a policy of  protectionism, i.e.  the use of  government to
stimulate the  growth of native  industry). Only once America  was at
the top of the economic pile did it renounce state intervention (just
as Britain did, we must note).</p>

<p>This  is  <strong>not</strong>  to  suggest  that  government  aid
was  limited  to  tariffs.  The  state  played  a  key  role  in  the
development of industry and manufacturing.  As John Zerzan notes, the
<em>“role  of the  State is  tellingly reflected  by the  fact that
the  ‘armoury system’  now  rivals the  older ‘American  system
of  manufactures’  term  as  the  more  accurate  to  describe  the
new  system  of production  methods”</em>  developed  in the  early
1800s. [<strong>Elements  of Refusal</strong>, p. 100]  Moreover, the
<em>“lead  in technological  innovation [during  the US  Industrial
Revolution]  came  in  armaments   where  assured  government  orders
justified  high fixed-cost  investments  in special-pursue  machinery
and  managerial personnel.  Indeed,  some of  the pioneering  effects
occurred  in government-owned  armouries.”</em> [William  Lazonick,
<strong>Competitive  Advantage on  the Shop  Floor</strong>, p.  218]
The  government  also  <em>“actively  furthered  this  process  [of
“commercial  revolution”]  with  public works  in  transportation
and   communication.”</em>  [Richard   B.   Du  Boff,   <strong>Op.
Cit.</strong>, p. 15]</p>

<p>In addition  to this  “physical” aid,  <em>“state government
provided   critical   help,   with   devices   like   the   chartered
corporation”</em>  [<strong>Ibid.</strong>]  and,  as we  noted  in
section B.2.5, changes in the  legal system which favoured capitalist
interests over the rest of society.</p>

<p>Interestingly, there  was increasing  inequality between  1840 and
1860 in  the USA This coincided  with the victory of  wage labour and
industrial capitalism — the 1820s <em>“constituted a watershed in
U.S. life.  By the  end of that  decade ...industrialism  assured its
decisive  American victory,  by  the  end of  the  1830s  all of  its
cardinal  features  were  definitely present.”</em>  [John  Zerzan,
<strong>Op. Cit.</strong>,  p. 99]  This is  unsurprising, for  as we
have argued many times, the  capitalist market tends to increase, not
reduce, inequalities  between individuals and classes.  Little wonder
the  Individualist Anarchists  at  the time  denounced  the way  that
property had  been transformed  into <em>“a  power [with  which] to
accumulate an income”</em> (to use the words of J.K. Ingalls).</p>

<p>Over all,  as Paul  Ormerod puts it,  the <em>“advice  to follow
pure  free-market polices  seems ...  to be  contrary to  the lessons
of  virtually the  whole  of economic  history  since the  Industrial
Revolution  ...  every  country  which  has  moved  into  ...  strong
sustained  growth ...  has done  so  in outright  violation of  pure,
free-market principles.”  “The model of  entrepreneurial activity
in the product  market, with judicious state  support plus repression
in  the  labour  market,  seems  to  be  a  good  model  of  economic
development.”</em>  [<strong>The  Death of  Economics</strong>,  p.
63]</p>

<p>Thus  the  social  forces  at   work  creating  capitalism  was  a
combination of capitalist activity and  state action. But without the
support of the  state, it is doubtful that  capitalist activity would
have been  enough to  generate the  initial accumulation  required to
start the economic ball rolling.  Hence the necessity of Mercantilism
in  Europe  and  its  modified  cousin  of  state  aid,  tariffs  and
“homestead acts” in America.</p>

<h4 id="toc52">8.2 What was the social context of the statement “laissez-faire?”</h4>

<p>The  honeymoon  of interests  between  the  early capitalists  and
autocratic kings  did not  last long. <em>“This  selfsame monarchy,
which for  weighty reasons sought  to further the aims  of commercial
capital and was...  itself aided in its development  by capital, grew
at  last into  a crippling  obstacle  to any  further development  of
European  industry.”</em> [Rudolf  Rocker, <strong>Nationalism  and
Culture</strong>, p. 117]</p>

<p>This    is    the    social     context    of    the    expression
<em>“laissez-faire”</em>  — a  system  which  has outgrown  the
supports that  protected it in  its early  stages of growth.  Just as
children  eventually  rebel  against  the  protection  and  rules  of
their parents,  so the capitalists rebelled  against the over-bearing
support of the absolutist  state. Mercantilist policies favoured some
industries and harmed the growth  of industrial capitalism in others.
The rules  and regulations imposed  upon those it did  favour reduced
the flexibility  of capitalists  to changing environments.  As Rocker
argues, <em>“no matter  how the abolutist state strove,  in its own
interest, to meety the demands of  commerce, it still put on industry
countless fetters which became gradually more and more oppressive ...
[it] became  an unbearable  burden ...  which paralysed  all economic
and  social life.”</em>  [<em>Op. Cit.</em>,  p. 119]  All in  all,
mercantilism became more of a hindrance than  a help and so had to be
replaced.  With  the growth  of  economic  and  social power  by  the
capitalist class, this replacement was made easier.</p>

<p>Errico  Malatesta notes,  <em>“[t]he development  of production,
the vast  expansion of  commerce, the  immeasurable power  assumed by
money ... have guaranteed this  supremacy [of economic power over the
political power]  to the  capitalist class  which, no  longer content
with enjoying the support of the government, demanded that government
arise from its  own ranks. A government which owed  its origin to the
right of conquest ... though subject by existing circumstances to the
capitalist  class,  went  on  maintaining a  proud  and  contemptuous
attitude towards its  now wealthy former slaves,  and had pretensions
to  independence  of  domination.  That  government  was  indeed  the
defender, the property owners’ gendarme,  but the kind of gendarmes
who think they are somebody, and behave in an arrogant manner towards
the people they  have to escort and defend, when  they don’t rob or
kill them at the next street corner; and the capitalist class got rid
of it ...  [and replaced it] by  a government [and state]  ... at all
times under  its control  and specifically  organised to  defend that
class  against  any  possible demands  by  the  disinherited.”</em>
[<strong>Anarchy</strong>, pp. 19–20]</p>

<p>Malatesta  here   indicates  the  true  meaning   of  <em>“leave
us     alone,”</em>     or    <em>“laissez-faire.”</em>     The
<strong>absolutist</strong>  state  (not   “the  state”  per  se)
began to  interfere with capitalists’ profit-making  activities and
authority, so  they determined  that it  had to  go —  as happened,
for  example,  in  the  English,  French  and  American  revolutions.
However, in other ways, state  intervention in society was encouraged
and  applauded by  capitalists.  <em>“It is  ironic  that the  main
protagonists  of  the  State,  in its  political  and  administrative
authority, were the  middle-class Utilitarians, on the  other side of
whose  Statist  banner  were  inscribed  the  doctrines  of  economic
Laissez  Faire”</em>  [E.P.  Thompson, <strong>The  Making  of  the
English  Working Class</strong>,  p. 90].  Capitalists simply  wanted
<strong>capitalist</strong> states to  replace monarchical states, so
that  heads  of  government  would  follow  state  economic  policies
regarded by capitalists as beneficial to  their class as a whole. And
as development economist Lance Taylor argues:</p>

<blockquote class="citation">
“In the long run, there  are no laissez-faire transitions to
modern economic growth.  The state has always intervened  to create a
capitalist class, and  then it has to regulate  the capitalist class,
and  then the  state  has to  worry  about being  taken  over by  the
capitalist  class,  but  the  state has  always  been  there.”</em>
[quoted by Noam Chomsky, <strong>Year 501</strong>, p. 104]
</blockquote>

<p>In  order to  attack mercantilism,  the early  capitalists had  to
ignore the successful  impact of its policies  in developing industry
and a “store of wealth”  for future economic activity. As William
Lazonick points out, <em>“the political purpose of [Adam Smith’s]
the <strong>Wealth of Nations</strong> was to attack the mercantilist
institutions that the British economy  had built up over the previous
two  hundred years...  In  his attack  on  these institutions,  Smith
might have  asked why  the extent  of the  world market  available to
Britain  in  the  late  eighteenth century  was  <strong>so  uniquely
under  British  control.</strong>  If  Smith had  asked  this  ‘big
question,’ he might  have been forced to grant credit  for [it] ...
to the  very mercantilist  institutions he was  attacking ...”</em>
Moreover,  he  <em>“might  have recognised  the  integral  relation
between  economic and  political  power  in the  rise  of Britain  to
international dominance.”</em>  Overall, <em>“[w]hat  the British
advocates of laissez-faire neglected to  talk about was the role that
a  system of  national power  had played  in creating  conditions for
Britain to  embark on its dynamic  development path ... They  did not
bother to ask how Britain had attained th[e] position [of ‘workshop
of the world’], while they conveniently ignored the on going system
of national  power — the British  Empire — that ...  continued to
support Britain’s  position.”</em> [<strong>Business Organisation
and the Myth of the Market Economy</strong>, p. 2, p. 3, p.5]</p>

<p>Similar comments are applicable  to American supporters of laissez
faire who fail to notice  that the “traditional” American support
for world-wide  free trade is  quite a recent phenomenon.  It started
only  at the  end  of  the Second  World  War  (although, of  course,
<strong>within</strong>  America  military  Keynesian  policies  were
utilised). While American industry was developing, the country had no
time for laissez-faire. After it  had grown strong, the United States
began preaching laissez-faire to the rest  of the world — and began
to  kid itself  about its  own history,  believing its  slogans about
laissez-faire  as the  secret  of  its success.  In  addition to  the
tariff,  nineteenth-century America  went in  heavily for  industrial
planning — occasionally under that name  but more often in the name
of  national  defence.  The  military  was the  excuse  for  what  is
today  termed rebuilding  infrastructure, picking  winners, promoting
research, and  co-ordinating industrial  growth (as  it still  is, we
should add).</p>

<p>As Richard B. Du Boff points out, the “anti-state” backlash of
the 1840s  onwards in  America was highly  selective, as  the general
opinion  was  that  <em>“[h]enceforth,  if  governments  wished  to
subsidise private  business operations, there would  be no objection.
But if public power were to be used to control business actions or if
the public sector were to  undertake economic initiatives on its own,
it  would  run  up  against  the  determined  opposition  of  private
capital.”</em> [<strong>Accumulation and  Power</strong>, p. 26] In
other words, the state could aid capitalists indirectly (via tariffs,
land  policy,  repression  of  the  labour  movement,  infrastructure
subsidy and so on) and it would “leave them alone” to oppress and
exploit workers,  exploit consumers,  build their  industrial empires
and so forth.</p>

<p>So, the expression “laissez-faire”  dates from the period when
capitalists  were objecting  to the  restrictions that  helped create
them  in  the first  place.  It  has little  to  do  with freedom  as
such  and far  more to  do  with the  needs of  capitalist power  and
profits  (as  Murray  Bookchin  argues,  it is  an  error  to  depict
this  <em>“revolutionary  era  and its  democratic  aspirations  as
‘bourgeois,’  an  imagery that  makes  capitalism  a system  more
committed to freedom,  or even ordinary civil liberties,  than it was
historically”</em>  [<strong>From Urbanisation  to Cities</strong>,
p. 180f]). Takis Fotopoules, in his essay <em>“The Nation-state and
the  Market”</em>, indicates  that  the social  forces  at work  in
“freeing” the market did  not represent a “natural” evolution
towards freedom:</p>

<blockquote class="citation">
“Contrary   to   what    liberals   and   Marxists   assert,
marketisation of  the economy was  not just an  evolutionary process,
following  the  expansion  of  trade under  mercantilism  ...  modern
[i.e.  capitalist]  markets  did  not evolve  out  of  local  markets
and/or  markets  for  foreign   goods  ...  the  nation-state,  which
was  just  emerging  at  the  end   of  the  Middle  Ages,  played  a
crucial role  creating the  conditions for  the ‘nationalisation’
of  the  market  ...  and  ...   by  freeing  the  market  [i.e.  the
rich  and proto-capitalists]  from effective  social control.”</em>
[<strong>Society and Nature</strong>, Vol. 3, pp. 44–45]
</blockquote>

<p>The  “freeing” of  the  market thus  means  freeing those  who
“own”  most  of   the  market  (i.e.  the   wealthy  elite)  from
<em>“effective social control,”</em> but  the rest of society was
not as lucky. Peter Kropotkin makes a similar point in <strong>Modern
Science and Anarchism</strong>,  <em>“[w]hile giving the capitalist
any degree of  free scope to amass  his wealth at the  expense of the
helpless labourers,  the government has  <strong>nowhere</strong> and
<strong>never</strong>...afforded the labourers the opportunity ‘to
do as  they pleased’.”</em>  [<strong>Kropotkin’s Revolutionary
Pamphlets</strong>, p. 182]</p>

<p>The  one essential  form  of support  the “Libertarian”  right
wants the state (or “defence” firms) to provide capitalism is the
enforcement of  property rights —  the right of property  owners to
“do as they  like” on their own property, which  can have obvious
and  extensive  social  impacts. What  “libertarian”  capitalists
object to is attempts by others  — workers, society as a whole, the
state, etc. — to interfere with  the authority of bosses. That this
is just the defence of  privilege and power (and <strong>not</strong>
freedom) has been discussed in section  B and elsewhere in section F,
so we will not repeat ourselves here.</p>

<p>Samuel   Johnson   once   observed   that   <em>“we   hear   the
loudest  <strong>yelps</strong>  for  liberty among  the  drivers  of
Negroes.”</em> Our  modern “libertarian” capitalist  drivers of
wage-slaves are yelping  for exactly the same  kind of “liberty.”
[Johnson  quoted  in  Noam  Chomsky,  <strong>Year  501</strong>,  p.
141]</p>

<h4 id="toc53">8.3 What other forms did state intervention in creating capitalism take?</h4>

<p>Beyond being  a paymaster for  new forms of production  and social
relations  and defending  the owners’  power, the  state intervened
economically in other ways as well. As we noted in section B.2.5, the
state played a key role in transforming the law codes of society in a
capitalistic  fashion,  ignoring custom  and  common  law to  do  so.
Similarly,  the use  of tariffs  and  the granting  of monopolies  to
companies played  an important  role in  accumulating capital  at the
expense of working people, as did  the breaking of unions and strikes
by force.</p>

<p>However, one of  the most blatant of these acts  was the enclosure
of common land. In Britain, by means of the Enclosure Acts, land that
had been freely used by poor  peasants for farming their small family
plots  was  claimed  by  large  landlords  as  private  property.  As
E.P.  Thompson notes,  <em>“Parliament and  law imposed  capitalist
definitions to exclusive property in land”</em> [<strong>Customs in
Common</strong>, p. 163]. Property rights, which exclusively favoured
the  rich,  replaced the  use  rights  and  free agreement  that  had
governed peasant’s  use of  the commons.  Unlike use  rights, which
rest in the individual, property rights require state intervention to
create and maintain.</p>

<p>This stealing of  the land should not be  under estimated. Without
land, you cannot  live and have to sell your  liberty to others. This
places  those  with capital  at  an  advantage,  which will  tend  to
increase, rather than  decrease, the inequalities in  society (and so
place the landless workers at  an increasing disadvantage over time).
This process  can be seen from  early stages of capitalism.  With the
enclosure of  the land, an  agricultural workforce was  created which
had to travel where the work was. This influx of landless ex-peasants
into the towns ensured that the traditional guild system crumbled and
was  transformed  into capitalistic  industry  with  bosses and  wage
slaves rather than  master craftsmen and their  journeymen. Hence the
enclosure of  land played  a key  role, for  <em>“it is  clear that
economic inequalities  are unlikely to  create a division  of society
into  an employing  master class  and a  subject wage-earning  class,
unless  access to  the  mans  of production,  including  land, is  by
some  means  or  another  barred  to a  substantial  section  of  the
community.”</em>  [Maurice  Dobbs,  <strong>Studies  in  Capitalist
Development</strong>, p. 253]</p>

<p>The importance of access to land is summarised by this limerick by
the followers of  Henry George (a 19<sup>th</sup>  century writer who
argued  for a  <em>“single tax”</em>  and the  nationalisation of
land). The  Georgites got their  basic argument on the  importance of
land down these few, excellent lines:</p>

<blockquote class="citation">
A college  economist planned<br />
To live without  access to land<br />
He would have succeeded<br  />
But found that he needed<br />
Food, shelter and somewhere to stand.
</blockquote>

<p>Thus the Individualist (and  other) anarchists’ concern over the
<em>“land monopoly”</em> of which the Enclosure Acts were but one
part. The land monopoly, to  use Tucker’s words, <em>“consists in
the enforcement by  government of land titles which do  not rest upon
personal  occupancy and  cultivation.”</em> [<strong>The  Anarchist
Reader</strong>, p. 150] It is important to remember that wage labour
first developed on the land and  it was the protection of land titles
of landlords and nobility, combined with enclosure, that meant people
could not just work their own land.</p>

<p>In  other words,  the circumstances  so created  by enclosing  the
land  and enforcing  property rights  to large  estates ensured  that
capitalists  did not  have to  point  a gun  at workers  head to  get
them to  work long  hours in authoritarian,  dehumanising conditions.
In  such  circumstances,  when  the  majority  are  dispossessed  and
face  the threat  of  starvation, poverty,  homelessness  and so  on,
“initiation of force” is  <strong>not required.</strong> But guns
<strong>were</strong>  required  to  enforce the  system  of  private
property  that created  the  labour  market in  the  first place,  to
enforce the enclosure  of common land and protect the  estates of the
nobility and wealthy.</p>

<p>In addition to increasing the  availability of land on the market,
the  enclosures  also  had  the effect  of  destroying  working-class
independence. Through these Acts,  innumerable peasants were excluded
from  access to  their former  means of  livelihood, forcing  them to
migrate to  the cities to seek  work in the newly  emerging factories
of  the budding  capitalist  class,  who were  thus  provided with  a
ready source  of cheap  labour. The capitalists,  of course,  did not
describe the results  this way, but attempted to  obfuscate the issue
with  their  usual rhetoric  about  civilisation  and progress.  Thus
John  Bellers,  a  17<sup>th</sup>-century supporter  of  enclosures,
claimed that  commons were  <em>“a hindrance  to Industry,  and ...
Nurseries  of  Idleness  and  Insolence.”</em>  The  <em>“forests
and  great  Commons  make  the  Poor that  are  upon  them  too  much
like  the  <strong>indians.</strong>”</em>   [quoted  by  Thompson,
<strong>Op. Cit.</strong>, p. 163] Elsewhere Thompson argues that the
commons <em>“were  now seen as  a dangerous centre  of indiscipline
...  Ideology was  added  to  self-interest. It  became  a matter  of
public-spirited  policy for  gentlemen to  remove cottagers  from the
commons, reduce his labourers  to dependence ...”</em> [<strong>The
Making of the English Working Class</strong>, pp. 242–3]</p>

<p>The  commons gave  working-class people  a degree  of independence
which allowed them to be “insolent” to their betters. This had to
be  stopped,  as  it  undermined  to  the  very  roots  of  authority
relationships within society.  The commons <strong>increased</strong>
freedom  for ordinary  people and  made them  less willing  to follow
orders  and accept  wage labour.  The reference  to “Indians”  is
important, as  the independence  and freedom  of Native  Americans is
well documented.  The common  feature of  both cultures  was communal
ownership  of  the  means  of   production  and  free  access  to  it
(usufruct).  This  is  discussed  further  in  section  I.7  (Won’t
Libertarian Socialism destroy individuality?)</p>

<p>As the  early American economist  Edward Wakefield noted  in 1833,
<em>“where land is  cheap and all are free, where  every one who so
pleases can  easily obtain a piece  of land for himself,  not only is
labour dear, as  respects the labourer’s share of  the product, but
the difficulty  is to  obtain combined  labour at  any price.”</em>
[<strong>England and  America</strong>, quoted by Jeremy  Brecher and
Tim Costello, <strong>Commonsense for Hard Times</strong>, p. 24]</p>

<p>The enclosure  of the commons  (in whatever  form it took  — see
section  8.5 for  the US  equivalent)  solved both  problems —  the
high  cost of  labour, and  the freedom  and dignity  of the  worker.
The  enclosures perfectly  illustrate the  principle that  capitalism
requires a  state to ensure that  the majority of people  do not have
free access to any means of livelihood and so must sell themselves to
capitalists in order to survive. There  is no doubt that if the state
had  “left  alone”  the  European  peasantry,  allowing  them  to
continue their collective farming practices (“collective farming”
because,  as  Kropotkin  shows in  <strong>Mutual  Aid</strong>,  the
peasants not  only shared  the land  but much of  the farm  labour as
well),  capitalism  could not  have  taken  hold (see  <strong>Mutual
Aid</strong>, pp. 184–189, for more on the European enclosures). As
Kropotkin notes, <em>“[i]nstances  of commoners themselves dividing
their lands were  rare, everywhere the State coerced  them to enforce
the  division,  or  simply  favoured  the  private  appropriation  of
their  lands”</em>  by  the  nobles  and  wealthy.  [<strong>Mutual
Aid</strong>, p. 188]</p>

<p>Thus Kropotkin’s  statement that <em>“to speak  of the natural
death  of  the  village  community  [or the  commons]  in  virtue  of
economical  law  is  as grim  a  joke  as  to  speak of  the  natural
death of soldiers slaughtered  on a battlefield.”</em> [<strong>Op.
Cit.</strong>, p. 189]</p>

<p>Like  the more  recent case  of fascist  Chile, “free  market”
capitalism was imposed  on the majority of society by  an elite using
the authoritarian  state. This was  recognised by Adam Smith  when he
opposed state intervention in <strong>The Wealth of Nations</strong>.
In  Smith’s  day, the  government  was  openly and  unashamedly  an
instrument of  wealth owners. Less  than 10  per cent of  British men
(and  no women)  had  the right  to vote.  When  Smith opposed  state
interference,  he was  opposing  the imposition  of wealth  owners’
interests  on  everybody else  (and,  of  course, how  “liberal”,
nevermind “libertarian”, is a political  system in which the many
follow the rules and laws set-down  in the so-called interests of all
by the few?  As history shows, any minority given,  or who take, such
power <strong>will</strong> abuse it  in their own interests). Today,
the situation  is reversed, with neo-liberals  and right libertarians
opposing state  interference in the  economy (e.g. regulation  of Big
Business) so as to prevent the public from having even a minor impact
on the power or interests of the elite.</p>

<p>The  fact  that  “free   market”  capitalism  always  requires
introduction  by  an  authoritarian  state  should  make  all  honest
“Libertarians” ask: How “free”  is the “free market”? And
why,  when it  is introduced,  do the  rich get  richer and  the poor
poorer?  This was  the  case in  Chile (see  Section  C.11). For  the
poverty  associated  with  the  rise of  capitalism  in  England  200
years  ago,  E.P.  Thompson’s  <strong>The Making  of  the  English
Working  Class</strong>   provides  a  detailed   discussion.  Howard
Zinn’s <strong>A  People’s History of the  United States</strong>
describes  the  poverty  associated with  19<sup>th</sup>-century  US
capitalism.</p>

<h4 id="toc54">8.4 Aren’t the enclosures a socialist myth?</h4>

<p>The short  answer is no, they  are not. While a  lot of historical
analysis has  been spent in trying  to deny the extent  and impact of
the enclosures, the  simple fact is (in the words  of noted historian
E.P.  Thompson) enclosure  <em>“was a  plain enough  case of  class
robbery,  played according  to the  fair  rules of  property and  law
laid  down by  a parliament  of property-owners  and lawyers.”</em>
[<strong>The  Making  of  the  English  Working  Class</strong>,  pp.
237–8]</p>

<p>The  enclosures  were  one  of   the  ways  that  the  <em>“land
monopoly”</em> was created. The land  monopoly was used to refer to
capitalist property  rights and ownership  of land by  (among others)
the  Individualist Anarchists.  Instead  of  an <em>“occupancy  and
use”</em> regime advocated by anarchists, the land monopoly allowed
a few to bar the many from the land — so creating a class of people
with nothing  to sell but their  labour. While this monopoly  is less
important these  days in  developed nations (few  people know  how to
farm) it was essential as  a means of consolidating capitalism. Given
the  choice,  most people  preferred  to  become independent  farmers
rather than wage workers (see next section).</p>

<p>However, the importance of the enclosure movement is downplayed by
supporters of  capitalism. Little wonder,  for it is something  of an
embarrassment for them to acknowledge that the creation of capitalism
was somewhat less than “immaculate”  — after all, capitalism is
portrayed  as  an  almost  ideal  society of  freedom.  To  find  out
that  an  idol  has  feet  of  clay and  that  we  are  still  living
with  the impact  of its  origins is  something pro-capitalists  must
deny. So  <strong>is</strong> the  enclosures a socialist  myth? Most
claims  that  it  is  flow  from  the  work  of  the  historian  J.D.
Chambers’  famous essay  <em>“Enclosures  and  the Labour  Supply
in  the  Industrial   Revolution.”</em>  [<strong>Economic  History
Review</strong>,  2<sup>nd</sup>  series,  no.  5,  August  1953]  In
this  essay,  Chambers  attempts  to  refute  Karl  Marx’s  account
of  the  enclosures and  the  role  it  played  in what  Marx  called
<em>“primitive accumulation.”</em></p>

<p>We  cannot be  expected to  provide  an extensive  account of  the
debate  that has  raged over  this issue.  All we  can do  is provide
a  summary  of  the  work   of  William  Lazonick  who  presented  an
excellent  reply to  those  who  claim that  the  enclosures were  an
unimportant historical event. We are  drawing upon his summary of his
excellent essay  <em>“Karl Marx  and Enclosures  in England”</em>
[<strong>Review of Radical Political Economy</strong>, no. 6, Summer,
1974] which can  be found in his  books <strong>Competitive Advantage
on the Shop Floor</strong>  and <strong>Business Organisation and the
Myth  of the  Market Economy</strong>.  There are  three main  claims
against the socialist  account of the enclosures. We  will cover each
in turn.</p>

<p>Firstly,  it  is  often  claimed that  the  enclosures  drove  the
uprooted cottager and small peasant  into industry. However, this was
never  claimed.  It  is  correct  that  the  agricultural  revolution
associated with the  enclosures <strong>increased</strong> the demand
for farm  labour as claimed by  Chambers and others. And  this is the
whole point —  enclosures created a pool  of dispossessed labourers
who had  to sell their  time/liberty to survive.  The <em>“critical
transformation was  not the  level of agricultural  employment before
and after  enclosure but the  changes in employment  relations caused
by  the  reorganisation  of  landholdings  and  the  reallocation  of
access to  land.”</em> [<strong>Competitive  Advantage on  the Shop
Floor</strong>, p.  30] Thus  the key feature  of the  enclosures was
that it created a supply for farm labour, a supply that had no choice
but to  work for another.  This would  drive down wages  and increase
demand. Moreover, freed from the land, these workers could later move
to the towns in search for better work.</p>

<p>Secondly,  it is  argued  that  the number  of  small farm  owners
increased, or at least did not  greatly decline, and so the enclosure
movement was  unimportant. Again, this  misses the point.  Small farm
owners can still employ wage  workers (i.e. become capitalist farmers
as opposed  to “yeomen”  — independent peasant  proprietor). As
Lazonick  notes,  <em>“[i]t is  true  that  after 1750  some  petty
proprietors continued  to occupy and  work their  own land. But  in a
world of  capitalist agriculture,  the yeomanry  no longer  played an
important role  in determining the course  of capitalist agriculture.
As  a social  class that  could  influence the  evolution of  British
economy society,  the yeomanry had  disappeared.”</em> [<strong>Op.
Cit.</strong>, p. 32]</p>

<p>Thirdly, it is often claimed that it was population growth, rather
than  enclosures, that  caused the  supply  of wage  workers. So  was
population  growth  more  important that  enclosures?  Maurice  Dobbs
argues  that <em>“the  centuries in  which a  proletariat was  most
rapidly recruited were  apt to be those of slow  rather than of rapid
natural increase  of population,  and the paucity  or plenitude  of a
labour  reserve  in  different  countries  was  not  correlated  with
comparable  difference in  their rates  of population-growth.”</em>
[<strong>Studies   in   Capitalist  Development</strong>,   p.   223]
Moreover,  the population  argument ignores  the question  of whether
the  changes  in  society  caused  by  enclosures  and  the  rise  of
capitalism  have an  impact on  the observed  trends towards  earlier
marriage  and  larger  families  after  1750.  Lazonick  argues  that
<em>“[t]here   is  reason   to  believe   that  they   did.”</em>
[<strong>Op. Cit.</strong>, p. 33] Also,  of course, the use of child
labour in  the factories created  an economic incentive to  have more
children, an  incentive created by the  developing capitalist system.
Overall,  Lazonick  notes  that  <em>“[t]o  argue  that  population
growth  created  the industrial  labour  supply  is to  ignore  these
momentous social transformations”</em> associated  with the rise of
capitalism [<strong>Business Organisation and  the Myth of the Market
Economy</strong>, p. 273].</p>

<p>In other words, there is good reason to think that the enclosures,
far  from  being some  kind  of  socialist  myth,  in fact  played  a
key  role  in the  development  of  capitalism. As  Lazonick  himself
notes,  <em>“Chambers misunderstood”  “the argument  concerning
the ‘institutional creation’ of a proletarianised (i.e. landless)
workforce.  Indeed,  Chamber’s  own  evidence  and  logic  tend  to
support the  Marxian [and anarchist!]  argument, when it  is properly
understood.”</em> [<strong>Op. Cit.</strong>, p. 273]</p>

<h4 id="toc55">8.5  What  about  the   lack  of  enclosures  in  the Americas?</h4>

<p>The enclosure movement was but one way of creating the <em>“land
monopoly”</em> which ensured  the creation of a  working class. The
circumstances facing the ruling class in the Americas were distinctly
different than in the Old World and so the “land monopoly” took a
different form there. In the Americas, enclosures were unimportant as
customary land rights did not really exist. Here the problem was that
(after the  original users  of the land  were eliminated,  of course)
there were vast tracts of land available for people to use.</p>

<p>Unsurprisingly, there  was a movement towards  independent farming
and  this pushed  up the  price of  labour, by  reducing the  supply.
Capitalists found  it difficult to  find workers willing to  work for
them at  wages low  enough to provide  them with  sufficient profits.
It  was  due the  difficulty  in  finding  cheap enough  labour  that
capitalists in  America turned  to slavery.  All things  being equal,
wage labour <strong>is</strong> more  productive than slavery. But in
early  America all  things  were  <strong>not</strong> equal.  Having
access to  cheap (indeed,  free) land meant  that working  people had
a  choice,  and  few  desired  to  become  wage  slaves.  Because  of
this,  capitalists turned  to slavery  in the  South and  the “land
monopoly” in the North and West.</p>

<p>This was because, in the  words of Maurice Dobbs, it <em>“became
clear  to   those  who  wished  to   reproduce  capitalist  relations
of  production  in  the  new country  that  the  foundation-stone  of
their  endeavour  must  be   the  restriction  of  land-ownership  to
a  minority  and  the  exclusion  of  the  majority  from  any  share
in  [productive]  property.”</em>  [<strong>Studies  in  Capitalist
Development</strong>, pp. 221–2] As  one radical historian puts it,
<em>“[w]hen  land  is  ‘free’  or ‘cheap’.  as  it  was  in
different regions of the United States before the 1830s, there was no
compulsion for  farmers to  introduce labour-saving technology.  As a
result,  ‘independent  household  production’  ...  hindered  the
development of  capitalism ...  [by] allowing  large portions  of the
population to  escape wage labour.”</em> [Charlie  Post, <em>“The
‘Agricultural  Revolution’  in  the  United  States”</em>,  pp.
216–228, <strong>Science  and Society</strong>, vol. 61,  no. 2, p.
221]</p>

<p>It was precisely this option (i.e. of independent production) that
had to be destroyed in order  for capitalist industry to develop. The
state had  to violate  the holy  laws of  “supply and  demand” by
controlling  the  access  to  land  in order  to  ensure  the  normal
workings of “supply  and demand” in the labour  market (i.e. that
the  bargaining  position  on  the labour  market  favoured  employer
over  employee). Once  this situation  became the  typical one  (i.e.
when  the option  of  self-employment was  effectively eliminated)  a
(protectionist based)  “laissez-faire” approach could  be adopted
and  state action  used only  to  protect private  property from  the
actions of the dispossessed.</p>

<p>So how was this transformation of land ownership achieved?</p>

<p>Instead of allowing settlers to appropriate their own farms as was
the case  before the 1830s,  the state stepped  in once the  army had
cleared out the  original users. Its first major role  was to enforce
legal rights of property on unused  land. Land stolen from the Native
Americans  was  sold  at  auction  to  the  highest  bidders,  namely
speculators, who  then sold  it on to  farmers. This  process started
right <em>“after the revolution, [when]  huge sections of land were
bought up by  rich speculators”</em> and their  claims supported by
the  law [Howard  Zinn, <strong>A  People’s History  of the  United
States</strong>, p.  125] Thus land  which should have been  free was
sold to  land-hungry farmers and  the few enriched themselves  at the
expense of  the many.  Not only did  this increase  inequality within
society, it also encouraged the development of wage labour — having
to pay for  land would have ensured that many  immigrants remained on
the East  Coast until they  had enough money.  Thus a pool  of people
with little  option but  to sell  their labour  was increased  due to
state protection of  unoccupied land. That the land  usually ended up
in the hands of farmers did  not (could not) countermand the shift in
class forces that this policy created.</p>

<p>This was  also the essential  role of the  various “Homesteading
Acts”  and,  in  general,  the   <em>“Federal  land  law  in  the
19<sup>th</sup>  century  provided  for  the  sale  of  most  of  the
public  domain at  public auction  to  the higher  bidder ...  Actual
settlers  were  forced  to  buy  land  from  speculators,  at  prices
considerably  above  the  federal minimal  price”</em>  (which  few
people could afford anyway) [Charlie Post, <strong>Op. Cit.</strong>,
p.  222]. Little  wonder  the Individualist  Anarchists supported  an
<em>“occupancy and  use”</em> system of  land ownership as  a key
way  of  stopping  capitalist  and  landlord usury  as  well  as  the
development of capitalism itself.</p>

<p>This change in  the appropriation of land  had significant effects
on  agriculture  and  the  desirability  of  taking  up  farming  for
immigrants. As  Post notes,  <em>“[w]hen the social  conditions for
obtaining and maintaining  possession of land change, as  they did in
the midwest  between 1830 and  1840, pursuing the goal  of preserving
[family ownership  and control] ... produced  very different results.
In order  to pay growing  mortgages, debts and taxes,  family farmers
were  compelled  to  specialise  production  toward  cash  crops  and
to  market  more  and  more  of  their  output.”</em>  [<strong>Op.
Cit.</strong>, p. 221–2]</p>

<p>So, in order to pay for  land which was formerly free, farmers got
themselves into debt and increasingly turned  to the market to pay it
off. Thus, the <em>“Federal land  system, by transforming land into
a  commodity and  stimulating land  speculation, made  the midwestern
farmers dependent upon markets for  the continual possession of their
farms.”</em> [Charlie Post, <strong>Op. Cit.</strong>, p. 223] Once
on the market,  farmers had to invest in new  machinery and this also
got them into debt. In the face of a bad harvest or market glut, they
could not repay their  loans and their farms had to be  sold to so do
so. By 1880, 25% of all farms were rented by tenants, and the numbers
kept rising.</p>

<p>This means  that Murray Rothbard’s comment  that <em>“once the
land was purchased by  the settler, the injustice disappeared”</em>
is  nonsense —  the injustice  was  transmitted to  other parts  of
society and  this, along with  the legacy of the  original injustice,
lived on and helped transform society towards capitalism [<strong>The
Ethics of Liberty</strong>,  p. 73]. In addition,  his comments about
<em>“the establishment in North America of a truly libertarian land
system”</em> would  be one the Individualist  Anarchists would have
seriously disagreed with! [<strong>Ibid.</strong>]</p>

<p>Thus  state  action,  in  restricting free  access  to  the  land,
ensured  that workers  were dependent  on wage  labour. In  addition,
the <em>“transformation  of social  property relations  in northern
agriculture set  the stage  for the ‘agricultural  revolution’ of
the 1840s  and 1850s ...  [R]ising debts and taxes  forced midwestern
family farmers to compete as commodity producers in order to maintain
their  land-holding  ...  The  transformation  ...  was  the  central
precondition  for the  development  of industrial  capitalism in  the
United  States.”</em>  [Charlie  Post,  <strong>Ibid.</strong>,  p.
226]</p>

<p>In addition to seizing the land  and distributing it in such a way
as to  benefit capitalist industry, the  <em>“government played its
part in  helping the  bankers and  hurting the  farmers; it  kept the
amount of  money — based  in the gold  supply — steady  while the
population rose, so there was less and less money in circulation. The
farmer had to pay  off his debts in dollars that  were harder to get.
The bankers, getting loans back, were getting dollars worth more than
when they loaned them out — a  kind of interest on top of interest.
That  was  why  ...  farmers’  movements  [like  the  Individualist
Anarchists, we  must add]  ... [talked about]  putting more  money in
circulation.”</em>  [Howard  Zinn,  <strong>Op.  Cit.</strong>,  p.
278]</p>

<p>Overall,  therefore, state  action ensured  the transformation  of
America from a society of independent workers to a capitalist one. By
creating  and  enforcing  the  “land monopoly”  (of  which  state
ownership of unoccupied  land and its enforcement  of landlord rights
were the most important) the state  ensured that the balance of class
forces  tipped in  favour of  the capitalist  class. By  removing the
option of  farming your own land,  the US government created  its own
form  of enclosure  and the  creation  of a  landless workforce  with
little option but to sell its liberty on the “free market”. This,
combined with  protectionism, ensured the transformation  of American
society from  a pre-capitalist  one into a  capitalist one.  They was
nothing “natural” about it.</p>

<p>Little wonder  the Individualist  Anarchist J.K.  Ingalls attacked
the “land monopoly” in the following words:</p>

<blockquote class="citation">
“The earth, with  its vast resources of  mineral wealth, its
spontaneous productions  and its fertile  soil, the free gift  of God
and the common patrimony of mankind, has for long centuries been held
in the grasp of  one set of oppressors by right  of conquest or right
of discovery;  and it is  now held by  another, through the  right of
purchase from them. All of  man’s natural possessions ... have been
claimed as property;  nor has man himself escaped  the insatiate jaws
of greed. The invasion of his rights and possessions has resulted ...
in clothing  property with a  power to accumulate  an income.”</em>
[quoted by  James Martin, <strong>Men Against  the State</strong>, p.
142]
</blockquote>

<h4 id="toc56">8.6  How   did  working  people  view   the  rise  of capitalism?</h4>

<p>The best  example of how hated  capitalism was can be  seen by the
rise and  spread of the  socialist movement,  in all its  many forms,
across  the world.  It  is  no coincidence  that  the development  of
capitalism also saw the rise of socialist theories. However, in order
to  fully  understand  how  different capitalism  was  from  previous
economic systems, we will consider  early capitalism in the US, which
for many  “Libertarians” is  <strong>the</strong> example  of the
“capitalism-equals-freedom” argument.</p>

<p>Early America  was pervaded  by artisan production  — individual
ownership  of  the  means  of  production.  Unlike  capitalism,  this
system  is  <strong>not</strong>  marked  by the  separation  of  the
worker  from the  means of  life. Most  people did  not have  to work
for  another, and  so  did  not. As  Jeremy  Brecher  notes, in  1831
the  <em>“great majority  of Americans  were farmers  working their
own  land, primarily  for  their own  needs. Most  of  the rest  were
self-employed artisans, merchants,  traders, and professionals. Other
classes  —  employees  and  industrialists  in  the  North,  slaves
and  planters in  the  South  — were  relatively  small. The  great
majority  of Americans  were  independent and  free from  anybody’s
command.”</em> [<strong>Strike!</strong>, p.  xxi] These conditions
created the  high cost  of combined (wage)  labour which  ensured the
practice of slavery existed.</p>

<p>However,  toward   the  middle  of  the   19<sup>th</sup>  century
the  economy  began  to  change.  Capitalism  began  to  be  imported
into  American  society as  the  infrastructure  was improved,  which
allowed  markets  for  manufactured  goods  to  grow.  Soon,  due  to
(state-supported)  capitalist  competition,  artisan  production  was
replaced  by  wage  labour.  Thus  “evolved”  modern  capitalism.
Many  workers  understood,  resented, and  opposed  their  increasing
subjugation to  their employers  (<em>“the masters”</em>,  to use
Adam Smith’s  expression), which could  not be reconciled  with the
principles  of  freedom and  economic  independence  that had  marked
American life and sunk deeply into mass consciousness during the days
of the early economy. In 1854,  for example, a group of skilled piano
makers  wrote that  <em>“the day  is  far distant  when they  [wage
earners] will so far  forget what is due to manhood as  to glory in a
system forced upon them by their necessity and in opposition to their
feelings of  independence and  self-respect. May  the piano  trade be
spared  such exhibitions  of the  degrading power  of the  day [wage]
system.”</em> [quoted by Brecher and Costello, <strong>Common Sense
for Hard Times</strong>, p. 26]</p>

<p>Clearly the  working class  did not consider  working for  a daily
wage, in  contrast to  working for themselves  and selling  their own
product, to be a step forward  for liberty or individual dignity. The
difference between selling the product  of one’s labour and selling
one’s labour  (i.e. oneself) was seen  and condemned (<em>“[w]hen
the producer ...  sold his product, he retained himself.  But when he
came to sell  his labour, he sold himself ...  the extension [of wage
labour]  to the  skilled  worker  was regarded  by  him  as a  symbol
of  a  deeper  change”</em> [Norman  Ware,  <strong>The  Industrial
Worker, 1840–1860</strong>, p. xiv]).  Indeed, one group of workers
argued that  they were  <em>“slaves in the  strictest sense  of the
word”</em> as they had <em>“to toil from the rising of the sun to
the going down of the same for  our masters — aye, masters, and for
our daily  bread”</em> [Quoted by Ware,  <strong>Op. Cit.</strong>,
p. 42] and another argued  that <em>“the factory system contains in
itself the elements of slavery, we think no sound reasoning can deny,
and everyday continues  to add power to  its incorporate sovereignty,
while  the  sovereignty  of  the  working  people  decreases  in  the
same  degree.”</em> [quoted  by Brecher  and Costello,  <strong>Op.
Cit.</strong>, p. 29]</p>

<p>Almost as  soon as  there were wage  workers, there  were strikes,
machine breaking, riots,  unions and many other  forms of resistance.
John Zerzan’s  argument that there was  a <em>“relentless assault
on  the worker’s  historical rights  to free  time, self-education,
craftsmanship,  and  play  was  at  the heart  of  the  rise  of  the
factory  system”</em> is  extremely  accurate [<strong>Elements  of
Refusal</strong>,  p.  105].  And  it was  an  assault  that  workers
resisted with all their might. In  response to being subjected to the
“law of value,” workers rebelled and tried to organise themselves
to  fight  the powers  that  be  and to  replace  the  system with  a
co-operative one. As the printer’s union argued, <em>“[we] regard
such an  organisation [a  union] not  only as  an agent  of immediate
relief, but also as an essential to the ultimate destruction of those
unnatural relations  at present  subsisting between the  interests of
the employing and the  employed classes...[W]hen labour determines to
sell itself no longer to speculators, but to become its own employer,
to own  and enjoy  itself and  the fruit  thereof, the  necessity for
scales of  prices will have  passed away  and labour will  be forever
rescued  from  the  control  of the  capitalist.”</em>  [quoted  by
Brecher and Costello, <strong>Op. Cit.</strong>, pp. 27–28]</p>

<p>Little wonder,  then, why wage labourers  considered capitalism as
a  form  of  <em>“slavery”</em>  and  why  the  term  <em>“wage
slavery”</em>  became  so popular  in  the  anarchist movement.  It
was  just  reflecting  the  feelings of  those  who  experienced  the
wages system  at first  hand and joined  the socialist  and anarchist
movements.  As labour  historian Norman  Ware notes,  the <em>“term
‘wage slave’  had a much better  standing in the forties  [of the
19<sup>th</sup> century] than it has  today. It was not then regarded
as an  empty shibboleth  of the soap-box  orator. This  would suggest
that it  has suffered  only the normal  degradation of  language, has
become a <strong>cliche</strong>, not that it is a grossly misleading
characterisation.”</em> [<strong>Op. Cit.</strong>, p. xvf]</p>

<p>These responses  of workers  to the experience  of wage  labour is
important to show  that capitalism is by no  means “natural.” The
fact is the first generation of workers tried to avoid wage labour is
at all possible as they hated  the restrictions of freedom it imposed
upon  them. They  were  perfectly  aware that  wage  labour was  wage
slavery —  that they were decidedly  <strong>unfree</strong> during
working  hours and  subjected  to  the will  of  another. While  many
working people now are accustomed  to wage labour (while often hating
their job)  the actual  process of resistance  to the  development of
capitalism indicates  well its inherently authoritarian  nature. Only
once other options  were closed off and capitalists given  an edge in
the “free”  market by state  action did people accept  and become
accustomed to wage labour.</p>

<p>Opposition to  wage labour  and factory fascism  was/is widespread
and  seems  to  occur  wherever it  is  encountered.  <em>“Research
has  shown”</em>,  summarises  William  Lazonick,  <em>“that  the
‘free-born Englishman’  of the eighteenth century  — even those
who,  by  force  of  circumstance,  had  to  submit  to  agricultural
wage  labour  —  tenaciously  resisted entry  into  the  capitalist
workshop.”</em> [<strong>Business Organisation and  the Myth of the
Market Economy</strong>, p. 37] British workers shared the dislike of
wage  labour  of  their  American cousins.  A  <em>“Member  of  the
Builders’ Union”</em> in  the 1830s argued that  the trade unions
<em>“will not only  strike for less work, and more  wages, but will
ultimately <strong>abolish  wages</strong>, become their  own masters
and  work for  each  other;  labour and  capital  will  no longer  be
separate but  will be  indissolubly joined together  in the  hands of
workmen  and  work-women.”</em>  [quoted  by  Geoffrey  Ostergaard,
<strong>The Tradition of Workers’ Control</strong>, p. 133] This is
unsurprising, for  as Ostergaard notes, <em>“the  workers then, who
had not been  swallowed up whole by the  industrial revolution, could
make  critical  comparisons  between  the  factory  system  and  what
preceded it.”</em>  [<strong>Op. Cit.</strong>, p. 134]  While wage
slavery may  seem “natural” today,  the first generation  of wage
labourers  saw the  transformation of  the social  relationships they
experienced in work, from a  situation in which they controlled their
own work (and so themselves)  to one in which <strong>others</strong>
controlled them, and they did not like it. However, while many modern
workers instinctively hate wage labour and having bosses, without the
awareness of  some other method  of working, many  put up with  it as
“inevitable.”  The first  generation  of wage  labourers had  the
awareness of something else (although,  of course, a flawed something
else) and this gave then a deep insight into the nature of capitalism
and produced  a deeply radical  response to it and  its authoritarian
structures.</p>

<p>Far from  being a “natural” development,  then, capitalism was
imposed on a society of free  and independent people by state action.
Those  workers  alive  at  the time  viewed  it  as  <em>“unnatural
relations”</em> and  organised to  overcome it. These  feelings and
hopes still exist,  and will continue to exist until  such time as we
organise and <em>“abolish the wage system”</em> (to quote the IWW
preamble) and the state that supports it.</p>

<h4 id="toc57">8.7 Why is the history of capitalism important?</h4>

<p>Simply because  it provides  us with  an understanding  of whether
that system is “natural” and whether it can be considered as just
and free.  If the system  was created  by violence, state  action and
other unjust means then the apparent “freedom” which we currently
face within  it is a fraud,  a fraud masking unnecessary  and harmful
relations  of  domination,  oppression  and  exploitation.  Moreover,
by  seeing how  capitalist  relationships were  viewed  by the  first
generation of  wage slaves reminds  us that just because  many people
have  adjusted to  this regime  and consider  it as  normal (or  even
natural) it is nothing of the kind.</p>

<p>Murray Rothbard  is well  aware of the  importance of  history. He
considered the  <em>“moral indignation”</em> of  socialism arises
from  the  argument  <em>“that  the  capitalists  have  stolen  the
rightful property of the workers,  and therefore that existing titles
to  accumulated  capital are  unjust.”</em>  He  argues that  given
<em>“this hypothesis, the remainder of the impetus for both Marxism
and anarchosyndicalism  follow quote  logically.”</em> [<strong>The
Ethics of Liberty</strong>, p. 52]</p>

<p>So  some right-libertarians  are aware  that the  current property
owners  have benefited  extensively  from violence  and state  action
in  the  past.  Murray  Rothbard argues  (in  <strong>The  Ethics  of
Liberty</strong>, p. 57) that if the  just owners cannot be found for
a property, then  the property simply becomes again  unowned and will
belong to  the first  person to  appropriate and  utilise it.  If the
current owners are  not the actual criminals then there  is no reason
at  all to  dispossess them  of their  property; if  the just  owners
cannot be found  then they may keep the property  as the first people
to use it (of course, those who  own capital and those who use it are
usually different people, but we will ignore this obvious point).</p>

<p>Thus, since  all original  owners and the  originally dispossessed
are  long  dead   nearly  all  current  title  owners   are  in  just
possession of their property except for recently stolen property. The
principle is  simple, dispossess  the criminals, restore  property to
the  dispossessed  if  they  can  be  found  otherwise  leave  titles
where  they   are  (as   Native  American   tribes  owned   the  land
<strong>collectively</strong> this  could have an  interesting effect
on such  a policy in  the USA. Obviously  tribes that were  wiped out
need  not apply,  but would  such right-libertarian  policy recognise
such collective,  non-capitalist ownership  claims? We doubt  it, but
we  could  be  wrong  —  the  Libertarian  Party  Manifesto  states
that  their “just”  property  rights will  be  restored. And  who
defines  “just”?  And  given  that unclaimed  federal  land  will
be  given to  Native  Americans,  its seems  pretty  likely that  the
<strong>original</strong> land will be left alone).</p>

<p>Of course, that  this instantly gives an advantage  to the wealthy
on the new “pure” market is not mentioned. The large corporations
that,  via state  protection  and support,  built  their empires  and
industrial base will still be in an excellent position to continue to
dominate the market. Wealthy land owners, benefiting from the effects
of  state taxation  and rents  caused by  the “land  monopoly” on
farmstead failures,  will keep their  property. The rich will  have a
great initial advantage and this may  be more than enough to maintain
them in  there place. After  all, exchanges between worker  and owner
tend to reinforce  existing inequalities, <strong>not</strong> reduce
them (and  as the owners can  move their capital elsewhere  or import
new, lower waged,  workers from across the world, its  likely to stay
that way).</p>

<p>So Rothbard’s “solution” to the  problem of past force seems
to be (essentially)  a justification of existing  property titles and
not a  serious attempt to  understand or correct past  initiations of
force that have shaped society into  a capitalist one and still shape
it today. The end result of his theory is to leave things pretty much
as  they are,  for  the past  criminals  are dead  and  so are  their
victims.</p>

<p>However,   what    Rothbard   fails   to   note    is   that   the
<strong>results</strong> of this state  action and coercion are still
with us.  He totally fails to  consider that the theft  of productive
wealth has  a greater impact  on society  than the theft  itself. The
theft of <strong>productive</strong> wealth shapes society in so many
ways  that <strong>all</strong>  suffer  from  it (including  current
generations). This  (the externalities generated by  theft) cannot be
easily undone by individualistic “solutions”.</p>

<p>Let us take an example somewhat  more useful that the one Rothbard
uses  (namely, a  stolen watch).  A watch  cannot really  be used  to
generate  wealth (although  if I  steal a  watch, sell  it and  buy a
winning lottery  ticket, does that  mean I  can keep the  prize after
returning the money  value of your watch to you?  Without the initial
theft, I would  not have won the prize but  obviously the prize money
far exceeds the amount stolen. Is the prize money mine?). Let us take
a tool of production rather than a watch.</p>

<p>Let assume  a ship  sinks and  50 people get  washed ashore  on an
island. One  woman has foresight  to take a  knife from the  ship and
falls unconscious  on the  beach. A  man comes  along and  steals her
knife. When the  woman awakes she cannot remember if  she had managed
to bring the knife ashore with her  or not. The man maintains that he
brought  it with  him and  no one  else saw  anything. The  survivors
decide  to  split  the  island  equally  between  them  and  work  it
separately, exchanging goods via barter.</p>

<p>However, the man with the knife  has the advantage and soon carves
himself a house and fields from the wilderness. Seeing that they need
the  knife and  the tools  created  by the  knife to  go beyond  mere
existing, some  of the other  survivors hire themselves to  the knife
owner. Soon  he is running a  surplus of goods, including  houses and
equipment which  he decides to  hire out  to others. This  surplus is
then used to tempt  more and more of the other  islanders to work for
him, exchanging their land in return  for the goods he provides. Soon
he owns the whole island and never has to work again. His hut is well
stocked  and extremely  luxurious.  His workers  face  the option  of
following his  orders or being  fired (i.e. expelled from  the island
and so  back into the  water and certain  death). Later, he  dies and
leaves his knife to his son.  The woman whose knife it originally was
had died long before, childless.</p>

<p>Note that the theft did not involve taking any land. All had equal
access to  it. It was the  initial theft of the  knife which provided
the man  with market power,  an edge which  allowed him to  offer the
others a choice between working by  themselves or working for him. By
working for him they did “benefit” in terms of increased material
wealth (and also made the thief better off) but the accumulate impact
of unequal  exchanges turned  them into the  effective slaves  of the
thief.</p>

<p>Now, would it <strong>really</strong> be  enough to turn the knife
over  to the  whoever happened  to  be using  it once  the theft  was
discovered (perhaps the  thief made a death-bed  confession). Even if
the woman who had originally taken it from the ship been alive, would
the return of the knife <strong>really</strong> make up for the years
of  work the  survivors had  put in  enriching the  the thief  or the
“voluntary exchanges” which had resulted  in the thief owning all
the island? The equipment people use, the houses they life in and the
food they eat  are all the product of many  hours of collective work.
Does  this mean  that the  transformation of  nature which  the knife
allowed remain in the hands of the descendants of the thief or become
the collective property of all? Would dividing it equally between all
be fair? Not everyone worked equally hard to produce it. So we have a
problem — the  result of the initial theft is  far greater than the
theft considered  in isolation due  to the productive nature  of what
was stolen.</p>

<p>In other words, what Rothbard  ignores in his attempt to undermine
anarchist  use  of  history  is  that when  the  property  stolen  is
of  a  productive nature,  the  accumulative  effect  of its  use  is
such  as  to  affect  all   of  society.  Productive  assets  produce
<strong>new</strong> property, <strong>new</strong>  values, create a
<strong>new</strong>  balance of  class forces,  <strong>new</strong>
income and  wealth inequalities  and so  on. This  is because  of the
<strong>dynamic</strong> nature  of production  and human  life. When
the  theft is  such that  it creates  accumulative effects  after the
initial  act, it  is hardly  enough to  say that  it does  not really
matter any more. If a nobleman  invests in a capitalist firm with the
tribute he  extracted from his  peasants, then (once the  firm starts
doing well)  sells the land  to the peasants  and uses that  money to
expand  his capitalist  holdings,  does that  <strong>really</strong>
make everything all right? Does not the crime transmit with the cash?
After all, the factory would not exist without the prior exploitation
of the peasants.</p>

<p>In the  case of actually  existing capitalism,  born as it  was of
extensive coercive  acts, the  resultant of these  acts have  come to
shape the  <strong>whole</strong> society. For example,  the theft of
common land  (plus the  enforcement of property  rights —  the land
monopoly — to  vast estates owned by the  aristocracy) ensured that
working people had no option to  sell their labour to the capitalists
(rural or  urban). The  terms of these  contracts reflected  the weak
position of  the workers and  so capitalists extracted  surplus value
from workers  and used  it to consolidate  their market  position and
economic power.  Similarly, the effect of  mercantilist policies (and
protectionism)  was  to enrich  the  capitalists  at the  expense  of
workers and allow them to build industrial empires.</p>

<p>The  accumulative  effect   of  these  acts  of   violation  of  a
“free” market was  to create a class society  wherein most people
“consent” to be  wage slaves and enrich the few.  While those who
suffered  the  impositions are  long  gone  and  the results  of  the
specific acts have multiplied and magnified well beyond their initial
form. And we are still living  with them. In other words, the initial
acts of coercion have been  transmitted and transformed by collective
activity (wage labour) into society-wide affects.</p>

<p>Rothbard  argues  in  the  situation  where  the  descendants  (or
others) of those  who initially tilled the soil  and their aggressors
(<em>“or those  who purchased  their claims”</em>)  still extract
<em>“tribute from  the modern tillers”</em>  that this is  a case
of  <em>“<strong>continuing</strong>  aggression against  the  true
owners”</em>.  This means  that  <em>“the land  titles should  be
transferred  to the  peasants, without  compensation to  the monopoly
landlords.”</em>  [<strong>Op. Cit.</strong>,  p. 65]  But what  he
fails to  note is  that the extracted  “tribute” could  have been
used to invest in industry and transform society. Why ignore what the
“tribute”  has been  used for?  Does stolen  property not  remain
stolen property after it has been  transferred to another? And if the
stolen property is used to create a society in which one class has to
sell their  liberty to another,  then surely any surplus  coming from
those exchanges  are also  stolen (as it  was generated  directly and
indirectly by the theft).</p>

<p>Yes, anarchist  agree with Rothbard  — peasants should  take the
land  they use  but which  is owned  by another.  But this  logic can
equally  be applied  to  capitalism. Workers  are  still living  with
the  effects  of past  initiations  of  force and  capitalists  still
extract  “tribute” from  workers  due to  the unequal  bargaining
powers  within  the   labour  market  that  this   has  created.  The
labour  market, after  all,  was created  by  state action  (directly
or  indirectly)  and  is  maintained  by  state  action  (to  protect
property  rights and  new initiations  of force  by working  people).
The  accumulative   effects  of  stealing  productive   resources  as
been  to  increase  the  economic  power of  one  class  compared  to
another.  As the  victims  of these  past abuses  are  long gone  and
attempts  to  find  their  descendants meaningless  (because  of  the
generalised  effects  the thefts  in  question),  anarchists feel  we
are justified  in demanding  the <strong><em>“expropriation  of the
expropriators”</em></strong>.</p>

<p>Due  to  Rothbard’s  failure   to  understand  the  dynamic  and
generalising  effects  that  result  from  the  theft  of  productive
resources (i.e. externalities that occur  from coercion of one person
against  a specific  set  of others)  and the  creation  of a  labour
market, his  attempt to refute  anarchist analysis of the  history of
“actually existing capitalism” also fails. Society is the product
of collective activity and should  belong to us all (although whether
and how we divide it up is another question).</p>

<h3    id="toc58">9   Is    Medieval    Iceland    an   example    of “anarcho”-capitalism working in practice?</h3>

<p>Ironically,   medieval  Iceland   is   a  good   example  of   why
“anarcho”-capitalism will <strong>not</strong> work, degenerating
into de facto rule  by the rich. It should be  pointed out first that
Iceland,  nearly 1,000  years  ago, was  not  a capitalistic  system.
In  fact, like  most  cultures  claimed by  “anarcho”-capitalists
as  examples   of  their  “utopia,”   it  was  a   communal,  not
individualistic, society, based on artisan production, with extensive
communal   institutions  as   well   as  individual   “ownership”
(i.e.   use)  and   a   form  of   social  self-administration,   the
<strong>thing</strong> — both local  and Iceland-wide — which can
be  considered  a  “primitive”  form of  the  anarchist  communal
assembly.</p>

<p>As   William  Ian   Miller   points  out   <em>“[p]eople  of   a
communitarian  nature... have  reason  to be  attracted [to  Medieval
Iceland]... the limited role of lordship, the active participation of
large  numbers of  free  people  ... in  decision  making within  and
without  the homestead.  The  economy barely  knew  the existence  of
markets.  Social relations  preceded  economic  relations. The  nexus
of  household,  kin, Thing,  even  enmity,  more  than the  nexus  of
cash,  bound people  to each  other. The  lack of  extensive economic
differentiation supported  a weakly  differentiated class  system ...
[and material]  deprivations were  more evenly distributed  than they
would be once  state institutions also had  to be maintained.”</em>
[<strong>Bloodtaking and  Peacemaking: Feud, Law and  Society in Saga
Iceland</strong>, p. 306]</p>

<p>At this time Iceland <em>“remained entirely rural. There were no
towns,  not  even  villages,  and  early  Iceland  participated  only
marginally  in the  active trade  of Viking  Age Scandinavia.”</em>
There was a <em>“diminished  level of stratification, which emerged
from  the first  phase of  social and  economic development,  lent an
appearance of egalitarianism — social stratification was restrained
and political hierarchy limited.”</em> [Jesse Byock, <strong>Viking
Age  Iceland</strong>, p.  2] That  such a  society could  be classed
as  “capitalist”  or even  considered  a  model for  an  advanced
industrial society is staggering.</p>

<p>Kropotkin  in  <strong>Mutual  Aid</strong> indicates  that  Norse
society,  from  which  the  settlers in  Iceland  came,  had  various
“mutual  aid”  institutions,  including communal  land  ownership
(based around  what he  called the  <em>“village community”</em>)
and  the <strong>thing</strong>  (see also  Kropotkin’s <strong>The
State: Its Historic Role</strong> for  a discussion of the “village
community”).  It is  reasonable to  think that  the first  settlers
in  Iceland  would  have  brought such  institutions  with  them  and
Iceland did indeed  have its equivalent of the  commune or “village
community,” the <strong>Hreppar</strong>,  which developed early in
the country’s history.  Like the early local assemblies,  it is not
much discussed  in the Sagas  but is mentioned  in the law  book, the
Grágás, and  was composed of  a minimum of  twenty farms and  had a
five  member commission.  The Hreppar  was self-governing  and, among
other things,  was responsible for  seeing that orphans and  the poor
within the  area were fed  and housed. The  Hreppar also served  as a
property insurance agency and assisted in case of fire and losses due
to diseased livestock.</p>

<p>In  addition,  as in  most  pre-capitalist  societies, there  were
“commons”,  common land  available  for use  by  all. During  the
summer,  <em>“common lands  and  pastures in  the highlands,  often
called  <strong>almenning</strong>,  were   used  by  the  region’s
farmers for  grazing.”</em> This increased the  independence of the
population  from the  wealthy  as these  <em>“public lands  offered
opportunities for enterprising individuals to increase their store of
provisions and  to find  saleable merchandise.”</em>  [Jesse Byock,
<strong>Op. Cit.</strong>, p. 47 and p. 48]</p>

<p>Thus Icelandic  society had  a network  of solidarity,  based upon
communal life:</p>

<blockquote class="citation">
The  status  of  farmers  as   free  agents  was  reinforced  by  the
presence  of communal  units  called <strong>hreppar</strong>  (sing.
<strong>hreppr</strong>)  ...  these  [were]  geographically  defined
associations  of   landowners...  the   <strong>hreppr</strong>  were
self-governing ...[and]  guided by  a five-member  steering committee
...  As early  as the  900s,  the whole  country seems  to have  been
divided  into  <strong>hreppar</strong> ...  <strong>Hreppar</strong>
provided a blanket of local security, allowing the landowning farmers
a measure of independence to  participate in the choices of political
life ...</em>
</blockquote>

<blockquote class="citation">
Through  copoperation among  their members,  <strong>hreppar</strong>
organised  and controlled  summer grazing  lands, organised  communal
labour, and provided an immediate  local forum for settling disputes.
Crucially,  they  provided fire  and  livestock  insurance for  local
farmers...  [They also]  saw  to  the feeding  and  housing of  local
orphans, and administered  poor relief to people  who were recognised
as  inhabitants of  their  area.  People who  could  not provide  for
themselves  were  assigned  to  member farms,  which  took  turns  in
providing for them.
</blockquote>

<blockquote class="citation_author">Byock</blockquote>
<blockquote class="citation_source">Op.cit.<i>, pp. 137–8</i></blockquote>

<p>In  practice this  meant  that <em>“each  commune  was a  mutual
insurance company,  or a miniature  welfare state. And  membership in
the  commune was  not voluntary.  Each farmer  had to  belong to  the
commune  in which  his  farm was  located and  to  contribute to  its
needs.”</em> [Gissurarson quoted by  Birgit T. Runolfsson Solvason,
<strong>Ordered  Anarchy,  State   and  Rent-Seeking:  The  Icelandic
Commonwealth,  930–1262</strong>]  The Icelandic  Commonwealth  did
not  allow  farmers <strong>not</strong>  to  join  its communes  and
<em>“[o]nce  attached  to   the  local  <strong>hreppr</strong>,  a
farm’s  affliation  could  not be  changed.”</em>  However,  they
did  play   a  key  role   in  keeping   the  society  free   as  the
<strong>hreppr</strong>  <em>“was   essentially  non-political  and
addressed subsistence and economic security needs. Its presence freed
farmers from depending on an overclass to provide comparable services
or  corresponding  security   measures.”</em>  [Byock,  <strong>Op.
Cit.</strong>, p. 138]</p>

<p>Therefore,  the Icelandic  Commonwealth can  hardly be  claimed in
any  significant way  as  an example  of “anarcho”-capitalism  in
practice. This can also be seen  from the early economy, where prices
were subject to popular  judgement at the <strong>skuldaping</strong>
(<em>“payment-thing”</em>)   <strong>not</strong>    supply   and
demand.  [Kirsten Hastrup,  <strong>Culture and  History in  Medieval
Iceland</strong>,  p. 125]  Indeed, with  its communal  price setting
system  in local  assemblies,  the early  Icelandic commonwealth  was
more  similar to  Guild  Socialism (which  was  based upon  guild’s
negotiating   “just  prices”   for  goods   and  services)   than
capitalism.  Therefore  Miller  correctly  argues that  it  would  be
wrong  to  impose capitalist  ideas  and  assumptions onto  Icelandic
society:</p>

<blockquote class="citation">
“Inevitably the attempt was made to add early Iceland to the
number of regions  that socialised people in  nuclear families within
simple  households... what  the sources  tell us  about the  shape of
Icelandic  householding must  compel a  different conclusion.”</em>
[<strong>Op. Cit.</strong>, p. 112]
</blockquote>

<p>In  other  words,  Kropotkin’s   analysis  of  communal  society
is   far   closer  to   the   reality   of  Medieval   Iceland   than
“anarcho”-capitalist  attempts to  turn it  into a  some kind  of
capitalist utopia.</p>

<p>However, the communal nature  of Icelandic society also co-existed
(as in most such  cultures) with hierarchical institutions, including
some  with   capitalistic  elements,  namely  private   property  and
“private  states” around  the  local <strong>godar.</strong>  The
godar  were  local  chiefs  who  also  took  the  role  of  religious
leaders. As  the <strong>Encyclopaedia  Britannica</strong> explains,
<em>“a kind of  local government was evolved [in  Iceland] by which
the  people of  a  district  who had  most  dealings together  formed
groups  under the  leadership of  the most  important or  influential
man  in the  district”</em> (the  godi). The  godi <em>“acted  as
judge  and  mediator”</em>  and  <em>“took  a  lead  in  communal
activities”</em>  such   as  building  places  of   worship.  These
<em>“local assemblies...  are heard of before  the establishment of
the  althing”</em>  (the  national  thing).  This  althing  led  to
co-operation between the local assemblies.</p>

<p>Thus Icelandic  society had different  elements, one based  on the
local  chiefs  and  communal  organisations. Society  was  marked  by
inequalities as  <em>“[a]mong the landed there  were differences in
wealth and prominence. Distinct  cleavages existed between landowners
and  landless  people  and   between  free  men  and  slaves.”</em>
This  meant it  was  <em>“marked by  aspects  of statelessness  and
egalitarianism as well  as elements of social  hierarchy ... Although
Iceland  was not  a  democratic  system, proto-democratic  tendencies
existed.”</em> [Byock, <strong>Op. Cit.</strong>,  p. 64 and p. 65]
The Icelandic social  system was designed to reduce the  power of the
wealthy by enhancing communal institutions:</p>

<blockquote class="citation">
“The  society ...  was based  on a  system of  decentralised
self-government  ... The  Viking Age  settlers began  by establishing
local  things, or  assemblies, which  had  been the  major forum  for
meetings  of freemen  and  aristocrats in  the  old Scandinavian  and
Germanic  social order...  They [the  Icelanders] excluded  overlords
with coercive power and expended the  mandate of the assembly to fill
the full  spectrum of the interests  of the landed free  farmers. The
changes transformed a Scandinavian decision-making body that mediated
between  freemen  and  overlords  into  an  Icelandic  self-contained
governmental  system  without overlords.  At  the  core of  Icelandic
government was  the Althing, a national  assembly of freemen.”</em>
[Byock, <strong>Op. Cit.</strong>, p. 75]
</blockquote>

<p>Therefore  we  see  communal  self-management  in  a  basic  form,
<strong>plus</strong>  co-operation  between   communities  as  well.
These communistic,  mutual-aid features exist in  many non-capitalist
cultures  and  are  often   essential  for  ensuring  the  people’s
continued freedom  within those cultures  ( section B.2.5 on  why the
wealthy undermine  these popular <em>“folk-motes”</em>  in favour
of centralisation).  Usually, the existence of  private property (and
so  inequality) soon  led to  the  destruction of  communal forms  of
self-management  (with  participation  by  all male  members  of  the
community  as in  Iceland), which  are replaced  by the  rule of  the
rich.</p>

<p>While such developments are  a commonplace in most “primitive”
cultures, the  Icelandic case has  an unusual feature  which explains
the interest  it provokes  in “anarcho”-capitalist  circles. This
feature was that individuals could  seek protection from any godi. As
the  <strong>Encyclopaedia  Britannica</strong> puts  it,  <em>“the
extent  of  the godord  [chieftancy]  was  not fixed  by  territorial
boundaries. Those who were dissatisfied with their chief could attach
themselves to another  godi... As a result rivalry  arose between the
godar [chiefs]; as may be seen from the Icelandic Sagas.”</em> This
was  because,  while  there  were <em>“a  central  legislature  and
uniform,  country-wide  judicial  and legal  systems,”</em>  people
would seek the  protection of any godi, providing  payment in return.
[Byock, <strong>Op. Cit.</strong>, p. 2] These godi, in effect, would
be  subject  to  “market  forces,”  as  dissatisfied  individuals
could  affiliate  themselves to  other  godi.  This system,  however,
had  an  obvious  (and  fatal)  flaw.  As  the  <strong>Encyclopaedia
Britannica</strong> points out:</p>

<blockquote class="citation">
“The position of the godi could  be bought and sold, as well
as inherited; consequently, with the  passing of time, the godord for
large areas  of the country became  concentrated in the hands  of one
man or a few men. This was  the principal weakness of the old form of
government: it  led to a struggle  of power and was  the chief reason
for the ending of the commonwealth and for the country’s submission
to the King of Norway.”
</blockquote>

<p>It was the  existence of these hierarchical  elements in Icelandic
society that explain its fall from anarchistic to statist society. As
Kropotkin argued <em>“from chieftainship sprang on the one hand the
State  and on  the  other <strong>private</strong>  property.”</em>
[<strong>Act  for Yourselves</strong>,  p. 85]  Kropotkin’s insight
that chieftainship  is a  transitional system  has been  confirmed by
anthropologists studying “primitive” societies. They have come to
the conclusion that societies made  up of chieftainships or chiefdoms
are  not states:  <em>“Chiefdoms  are neither  stateless nor  state
societies  in the  fullest  sense of  either term:  they  are on  the
borderline between the two. Having  emerged out of stateless systems,
they give the impression of being  on their way to centralised states
and  exhibit characteristics  of  both.”</em> [Y.  Cohen quoted  by
Birgit  T.  Runolfsson  Solvason,  <strong>Op.  Cit.</strong>]  Since
the  Commonwealth  was  made  up  of  chiefdoms,  this  explains  the
contradictory nature  of the  society —  it was  in the  process of
transition, from anarchy  to statism, from a communal  economy to one
based on private property.</p>

<p>The <strong>political</strong> transition within Icelandic society
went hand in hand  with an <strong>economic</strong> transition (both
tendencies  being  mutually  reinforcing).  Initially,  when  Iceland
was  settled,  large-scale  farming   based  on  extended  households
with  kinsmen  was the  dominant  economic  mode. This  semi-communal
mode  of  production changed  as  the  land  was divided  up  (mostly
through   inheritance  claims)   between   the  10<sup>th</sup>   and
11<sup>th</sup>  centuries.  This  new  economic  system  based  upon
individual  <strong>possession</strong>  and artisan  production  was
then slowly displaced  by tenant farming, in which  the farmer worked
for  a  landlord,  starting  in  the  late  11<sup>th</sup>  century.
This  economic system  (based  on tenant  farming, i.e.  capitalistic
production) ensured that <em>“great  variants of property and power
emerged.”</em>  [Kirsten Hastrup,  <strong>Culture  and History  in
Medieval Iceland</strong>, pp. 172–173]</p>

<p>So  significant  changes  in  society  started  to  occur  in  the
eleventh century,  as <em>“slavery  all but ceased.  Tenant farming
...  took [its]  place.”</em> Iceland  was moving  from an  economy
based on <strong>possession</strong> to  one based on <strong>private
property</strong>  and so  <em>“the renting  of land  was a  widely
established  practice by  the late  eleventh century  ... the  status
of  the   <strong>godar</strong>  must   have  been   connected  with
landownership and  rents.”</em> This  lead to  increasing oligarchy
and so the  mid- to late-twelfth century  was <em>“characterised by
the appearance  of a  new elite,  the big  chieftains who  are called
storgodar ... [who] struggled from the 1220s to the 1260s to win what
had earlier  been unobtainable  for Icelandic  leaders, the  prize of
overlordship  or  centralised  executive  authority.”</em>  [Byock,
<strong>Op. Cit.</strong>, p. 269 and pp. 3–4]</p>

<p>During this evolution in  ownership patterns and the concentration
of wealth and power into the hands  of a few, we should note that the
godi’s  and wealthy  landowners’ attitude  to profit  making also
changed, with market values starting to replace those associated with
honour, kin, and so on.  Social relations became replaced by economic
relations and the  nexus of household, kin and Thing  was replaced by
the  nexus  of cash  and  profit.  The  rise of  capitalistic  social
relationships  in  production  and  values within  society  was  also
reflected in exchange,  with the local marketplace,  with its pricing
<em>“subject  to  popular judgement”</em>  being  <em>“subsumed
under central  markets.”</em> [Hastrup,  <strong>Op. Cit.</strong>,
p. 225]</p>

<p>With a form of wage  labour (tenant farming) being dominant within
society,  it  is not  surprising  that  great differences  in  wealth
started  to  appear.  Also,  as  protection did  not  come  free,  it
is  not  surprising that  a  godi  tended  to  become rich  also  (in
Kropotkin’s words, <em>“the individual accumulation of wealth and
power”</em>). Powerful godi would  be useful for wealthy landowners
when disputes  over land  and rent  appeared, and  wealthy landowners
would  be  useful  for  a godi  looking  for  income.  Concentrations
of  wealth, in  other  words, produce  concentrations  of social  and
political  power (and  vice  versa) —  <em>“power always  follows
wealth.”</em> [Kropotkin, <strong>Mutual Aid</strong>, p. 131]</p>

<p>The    transformation    of    <strong>possession</strong>    into
<strong>property</strong> and the resulting  rise of hired labour was
a  <strong>key</strong> element  in  the accumulation  of wealth  and
power, and  the corresponding decline  in liberty among  the farmers.
Moreover,  with hired  labour springs  dependency —  the worker  is
now  dependent on  good relations  with  their landlord  in order  to
have  access to  the  land they  need. With  such  reductions in  the
independence  of  part  of  Icelandic  society,  the  undermining  of
self-management in  the various Things  was also likely  as labourers
could not  vote freely  as they  could be  subject to  sanctions from
their  landlord for  voting  the “wrong”  way (<em>“The  courts
were  less  likely  to  base  judgements  on  the  evidence  than  to
adjust  decisions to  satisfy the  honour and  resources of  powerful
individuals.”</em>  [Byock, <strong>Op.  Cit.</strong>, p.  185])..
Thus  hierarchy within  the economy  would  spread into  the rest  of
society, and  in particular its social  institutions, reinforcing the
effects of the accumulation of wealth and power.</p>

<p>The  resulting classification  of Icelandic  society played  a key
role  in its  move  from relative  equality and  anarchy  to a  class
society and statism. As Millar points out:</p>

<blockquote class="citation">
“as  long as  the  social organisation  of  the economy  did
not  allow for  people to  maintain retinues,  the basic  egalitarian
assumptions of the honour system... were reflected reasonably well in
reality... the  mentality of hierarchy never  fully extricated itself
from  the  egalitarian  ethos  of  a  frontier  society  created  and
recreated by juridically equal farmers. Much of the egalitarian ethic
maintained  itself  even  though  it  accorded  less  and  less  with
economic realities... by  the end of the  commonwealth period certain
assumptions about class privilege  and expectations of deference were
already well  enough established to  have become part of  the lexicon
of  self-congratulation and  self-justification.”</em> [<strong>Op.
Cit.</strong>, pp. 33–4]
</blockquote>

<p>This process in turn accelerated  the destruction of communal life
and the emergence  of statism, focused around the  godord. In effect,
the godi and wealthy farmers  became rulers of the country. Political
changes  simply  reflected  economic changes  from  a  communalistic,
anarchistic society to a  statist, propertarian one. Ironically, this
process  was a  natural  aspect  of the  system  of competing  chiefs
recommended by “anarcho”-capitalists:</p>

<blockquote class="citation">
 “In the twelfth  and thirteenth centuries Icelandic society
experienced  changes  in  the  balance  of  power.  As  part  of  the
evolution to a more stratified social order, the number of chieftains
diminished  and the  power  of  the remaining  leaders  grew. By  the
thirteenth  century six  large families  had come  to monopolise  the
control and ownership of  many of the original chieftaincies.”</em>
[Byock, <strong>Op. Cit.</strong>, p. 341]
</blockquote>

<p>These  families were  called  <strong>storgodar</strong> and  they
<em>“gained  control  over   whole  regions.”</em>  This  process
was   not  imposed,   as   <em>“the  rise   in  social   complexity
was   evolutionary  rather   than  revolutionary   ...  they   simply
moved  up   the  ladder.”</em>  This  political   change  reflected
economic   processes,   for   <em>“[a]t   the   same   time   other
social  transformations  were  at   work.  In  conjunction  with  the
development  of   the  <strong>storgadar</strong>  elite,   the  most
successful  among the  <strong>baendr</strong>  [farmers] also  moved
up  a  rung  on  the   social  ladder,  being  ‘big  farmers’  or
<strong>Storbaendr</strong>”</em>  [<strong>Op.  Cit.</strong>,  p.
342]  Unsurprisingly,   it  was   the  rich  farmers   who  initiated
the  final  step  towards  normal   statism  and  by  the  1250s  the
<strong>storbaendr</strong> and  their followers  had grown  weary of
the <strong>storgodar</strong>  and their  quarrels. In the  end they
accepted  the  King  of  Norway’s  offer  to  become  part  if  his
kingdom.</p>

<p>The  obvious  conclusion  is  that  as long  as  Iceland  was  not
capitalistic, it was anarchic and  as it became more capitalistic, it
became more statist.</p>

<p>This process,  wherein the  concentration of  wealth leads  to the
destruction  of communal  life and  so the  anarchistic aspects  of a
given society, can be seen elsewhere,  for example, in the history of
the United States after the Revolution  or in the degeneration of the
free cities of Medieval Europe.  Peter Kropotkin, in his classic work
<strong>Mutual Aid</strong>,  documents this process in  some detail,
in  many  cultures  and  time periods.  However,  that  this  process
occurred in a  society which is used  by “anarcho”-capitalists as
an  example  of  their  system in  action  reinforces  the  anarchist
analysis of  the statist  nature of “anarcho”-capitalism  and the
deep flaws in its theory, as discussed in section 6.</p>

<p>As Miller argues, <em>“[i]t is not the have-nots, after all, who
invented the state. The first steps toward state formation in Iceland
were made by  churchmen... and by the big men  content with imitating
Norwegian royal style.  Early state formation, I  would guess, tended
to involve redistributions,  not from rich to poor, but  from poor to
rich,  from weak  to strong.”</em>  [<strong>Op. Cit.</strong>,  p.
306]</p>

<p>The “anarcho”-capitalist argument that  Iceland was an example
of their  ideology working in  practice is  derived from the  work of
David Friedman. Friedman  is less gun-ho than many  of his followers,
arguing in  <strong>The Machinery  of Freedom</strong>,  that Iceland
only  had some  features of  an “anarcho”-capitalist  society and
these  provide  some evidence  in  support  of  his ideology.  How  a
pre-capitalist  society  can  provide  any  evidence  to  support  an
ideology aimed at an advanced industrial and urban economy is hard to
say  as  the institutions  of  that  society cannot  be  artificially
separated from its  social base. Ironically, though,  it does present
some evidence  against “anarcho”-capitalism precisely  because of
the rise of capitalistic elements within it.</p>

<p>Friedman is  aware of how  the Icelandic Republic  degenerated and
its causes. He states in a  footnote in his 1979 essay <em>“Private
Creation and Enforcement  of Law: A Historical  Case”</em> that the
<em>“question  of why  the  system eventually  broke  down is  both
interesting and difficult. I believe that two of the proximate causes
were  increased concentration  of wealth,  and hence  power, and  the
introduction into  Iceland of  a foreign  ideology —  kingship. The
former meant that in  many areas all or most of  the godord were held
by one family and  the latter that by the end  of the Sturlung period
the chieftains were no longer  fighting over the traditional quarrels
of  who owed  what  to  whom, but  over  who  should eventually  rule
Iceland. The ultimate reasons for  those changes are beyond the scope
of this paper.”</em></p>

<p>However,  from  an  anarchist  point of  view,  the  “foreign”
ideology  of  kingship  would   be  the  <strong>product</strong>  of
changing  socio-economic  conditions  that   were  expressed  in  the
increasing concentration of wealth and  not its cause. After all, the
settlers of Iceland were well aware of the “ideology” of kingship
for  the  300 years  during  which  the  Republic existed.  As  Byock
notes, Iceland  <em>“inherited the tradition and  the vocabulary of
statehood from its  European origins ... On the  mainland, kings were
enlarging their authority at the expense of the traditional rights of
free  farmers. The  emigrants  to  Iceland were  well  aware of  this
process ... available evidence does suggest that the early Icelanders
knew  quite well  what they  did not  want. In  particular they  were
collectively opposed to the  centralising aspects of a state.”</em>
[<strong>Op. Cit.</strong>, p. 64–6] Unless some kind of collective
and cultural amnesia occurred, the notion of a “foreign ideology”
causing  the  degeneration is  hard  to  accept. Moreover,  only  the
concentration  of wealth  allowed would-be  Kings the  opportunity to
develop and act and the  creation of boss-worker social relationships
on the land made the poor  subject to, and familiar with, the concept
of  authority. Such  familiarity  would spread  into  all aspects  of
life  and,  combined  with  the existence  of  “prosperous”  (and
so  powerful)  godi to  enforce  the  appropriate servile  responses,
ensured the  end of the  relative equality that  fostered Iceland’s
anarchistic tendencies in the first place.</p>

<p>In addition, as private property is a monopoly of rulership over a
given area,  the conflict  between chieftains for  power was,  at its
most basic, a conflict of who would <strong>own</strong> Iceland, and
so rule  it. The attempt  to ignore  the facts that  private property
creates rulership (i.e.  a monopoly of government over  a given area)
and that monarchies are privately owned states does Friedman’s case
no good. In  other words, the system of private  property has a built
in tendency to produce both the ideology and fact of Kingship — the
power  structures implied  by Kingship  are reflected  in the  social
relations which are produced by private property.</p>

<p>Friedman is also aware that an <em>“objection [to his system] is
that the rich (or powerful)  could commit crimes with impunity, since
nobody would be  able to enforce judgement against  them. Where power
is sufficiently concentrated this might be  true; this was one of the
problems which led  to the eventual breakdown of  the Icelandic legal
system in the thirteenth century. But so long as power was reasonably
dispersed, as it seem to have  been for the first two centuries after
the system was established, this  was a less serious problem.”</em>
[<strong>Op. Cit.</strong>]</p>

<p>Which is  quite ironic. Firstly,  because the first  two centuries
of  Icelandic society  was marked  by <strong>non-capitalist</strong>
economic relations (communal pricing and family/individual possession
of  land).  Only  when capitalistic  social  relationships  developed
(hired  labour and  property replacing  possession and  market values
replacing  social  ones) in  the  12<sup>th</sup>  century did  power
become concentrated,  leading to the  breakdown of the system  in the
13<sup>th</sup> century. Secondly, because  Friedman is claiming that
“anarcho”-capitalism will  only work  if there is  an approximate
equality  within society!  But  this  state of  affairs  is one  most
“anarcho”-capitalists claim is impossible and undesirable!</p>

<p>They claim  there will  <strong>always</strong> be rich  and poor.
But inequality in  wealth will also become inequality  of power. When
“actually existing”  capitalism has  become more free  market the
rich have  got richer and  the poor poorer. Apparently,  according to
the “anarcho”-capitalists, in an even “purer” capitalism this
process  will  be  reversed!  It  is ironic  that  an  ideology  that
denounces  egalitarianism  as  a  revolt  against  nature  implicitly
requires an egalitarian society in order to work.</p>

<p>In   reality,   wealth   concentration   is   a   fact   of   life
in   <strong>any</strong>    system   based   upon    hierarchy   and
private   property.   Friedman   is   aware  of   the   reasons   why
“anarcho”-capitalism will become rule by  the rich but prefers to
believe  that  “pure”  capitalism  will  produce  an  egalitarian
society! In  the case  of the  commonwealth of  Iceland this  did not
happen — the rise in private  property was accompanied by a rise in
inequality  and this  lead  to  the breakdown  of  the Republic  into
statism.</p>

<p>In  short, Medieval  Iceland  nicely  illustrates David  Weick’s
comments (as quoted  in section 6.3) that  <em>“when private wealth
is uncontrolled, then a  police-judicial complex enjoying a clientele
of wealthy  corporations whose  motto is  self-interest is  hardly an
innocuous social force controllable by  the possibility of forming or
affiliating with  competing ‘companies.’”</em>  This is  to say
that “free market” justice soon results  in rule by the rich, and
being able to affiliate  with “competing” “defence companies”
is insufficient to stop or change that process.</p>

<p>This is simply because any  defence-judicial system does not exist
in a social vacuum. The concentration of wealth — a natural process
under  the  “free market”  (particularly  one  marked by  private
property  and wage  labour)  —  has an  impact  on the  surrounding
society.  Private  property,  i.e.  monopolisation of  the  means  of
production,  allows  the monopolists  to  become  a ruling  elite  by
exploiting, and so accumulating vastly more wealth than, the workers.
This elite then uses its wealth to control the coercive mechanisms of
society  (military,  police,  “private security  forces,”  etc.),
which it  employs to  protect its  monopoly and  thus its  ability to
accumulate ever  more wealth and  power. Thus, private  property, far
from increasing  the freedom of  the individual, has always  been the
necessary precondition  for the  rise of  the state  and rule  by the
rich.  Medieval Iceland  is  a  classic example  of  this process  at
work.</p>

<h3 id="toc59">10 Would laissez-faire capitalism be stable?</h3>

<p>Unsurprisingly,  right-libertarians  combine   their  support  for
“absolute  property  rights”  with a  whole-hearted  support  for
laissez-faire capitalism. In  such a system (which  they maintain, to
quote Ayn Rand, is  an <em>“unknown ideal”</em>) everything would
be private property  and there would be few (if  any) restrictions on
“voluntary  exchanges.”  “Anarcho”-capitalists are  the  most
extreme of defenders of pure capitalism, urging that the state itself
be privatised  and no voluntary  exchange made illegal  (for example,
children would  be considered  the property of  their parents  and it
would be  morally right to turn  them into child prostitutes  — the
child has the option of leaving home if they object).</p>

<p>As  there have  been no  example  of “pure”  capitalism it  is
difficult  to  say  whether  their  claims  about  are  true  (for  a
discussion  of a  close  approximation see  the  section 10.3).  This
section  of  the  FAQ  is  an attempt  to  discover  whether  such  a
system  would  be stable  or  whether  it  would  be subject  to  the
usual booms  and slumps.  Before starting we  should note  that there
is  some disagreement  within  the right-libertarian  camp itself  on
this subject  (although instead  of stability  they usually  refer to
“equilibrium” — which is an  economics term meaning that all of
a societies resources are fully utilised).</p>

<p>In general terms, most right-Libertarians’ reject the concept of
equilibrium as such and instead stress that the economy is inherently
a dynamic (this is a key aspect of the Austrian school of economics).
Such a  position is correct, of  course, as such noted  socialists as
Karl  Marx and  Michal Kalecki  and capitalist  economists as  Keynes
recognised long ago. There seems to be two main schools of thought on
the nature of  disequilibrium. One, inspired by  von Mises, maintains
that  the  actions  of  the entrepreneur/capitalist  results  in  the
market  co-ordinating  supply and  demand  and  another, inspired  by
Joseph Schumpeter,  who question whether markets  co-ordinate because
entrepreneurs  are constantly  innovating and  creating new  markets,
products and techniques.</p>

<p>Of course both actions happen  and we suspect that the differences
in  the two  approaches are  not  important. The  important thing  to
remember is that  “anarcho”-capitalists and right-libertarians in
general  reject the  notion of  equilibrium —  but when  discussing
their  utopia  they  do  not actually  indicate  this!  For  example,
most “anarcho”-capitalists  will maintain  that the  existence of
government  (and/or unions)  causes unemployment  by either  stopping
capitalists  investing in  new lines  of industry  or forcing  up the
price  of  labour  above  its market  clearing  level  (by,  perhaps,
restricting  immigration,  minimum   wages,  taxing  profits).  Thus,
we  are  assured,  the  worker  will  be  better  off  in  “pure”
capitalism because  of the  unprecedented demand  for labour  it will
create.  However, full  employment  of labour  is  an equilibrium  in
economic terms and  that, remember, is impossible due  to the dynamic
nature of  the system.  When pressed, they  will usually  admit there
will  be  periods of  unemployment  as  the  market adjusts  or  that
full  unemployment  actually  means  under a  certain  percentage  of
unemployment. Thus, if you (rightly) reject the notion of equilibrium
you also reject the idea of  full employment and so the labour market
becomes a buyers market and labour is at a massive disadvantage.</p>

<p>The right-libertarian  case is  based upon logical  deduction, and
the premises required  to show that laissez-faire will  be stable are
somewhat incredible. If banks do not  set the wrong interest rate, if
companies do not extend too much trade credit, if workers are willing
to accept (real wage related)  pay cuts, if workers altruistically do
not abuse their market power in a fully employed society, if interest
rates  provide the  correct information,  if capitalists  predict the
future relatively  well, if  banks and companies  do not  suffer from
isolation paradoxes, then, perhaps, laissez-faire will be stable.</p>

<p>So,  will  laissez-faire  capitalism  be stable?  Let  us  see  by
analysing  the assumptions  of right-libertarianism  — namely  that
there will be full employment and that a system of private banks will
stop the  business cycle. We will  start on the banking  system first
(in  section 10.1)  followed  by  the effects  of  the labour  market
on  economic stability  (in  section 10.2).  Then  we will  indicate,
using the  example of 19<sup>th</sup> century  America, that actually
existing (“impure”) laissez-faire was very unstable.</p>

<p>Explaining booms  and busts by  state action plays  an ideological
convenience  as  it exonerates  market  processes  as the  source  of
instability within  capitalism. We hope  to indicate in the  next two
sections why the  business cycle is inherent in the  system (see also
sections C.7, C.8 and C.9).</p>

<h4 id="toc60">10.1   Would  privatising  banking   make  capitalism stable?</h4>

<p>It is  claimed that the  existence of  the state (or,  for minimal
statists,  government policy)  is  the cause  of  the business  cycle
(recurring economic booms and slumps). This is because the government
either  sets interest  rates  too  low or  expands  the money  supply
(usually by  easing credit restrictions and  lending rates, sometimes
by just printing fiat  money). This artificially increases investment
as capitalists take advantage of the artificially low interest rates.
The real balance between savings and investment is broken, leading to
over-investment,  a  drop in  the  rate  of  profit  and so  a  slump
(which  is quite  socialist in  a way,  as many  socialists also  see
over-investment  as  the key  to  understanding  the business  cycle,
although they obviously  attribute the slump to  different causes —
namely  the nature  of  capitalist production,  not  that the  credit
system does not play its part — see section C.7).</p>

<p>In the words of Austrian  Economist W. Duncan Reekie, <em>“[t]he
business cycle is generated by monetary expansion and contraction ...
When new money is printed it appears  as if the supply of savings has
increased.  Interest  rates  fall  and businessmen  are  misled  into
borrowing additional founds to  finance extra investment activity ...
This  would be  of  no consequence  if  it had  been  the outcome  of
[genuine saving] ... -but the  change was government induced. The new
money reaches factor  owners in the form of wages,  rent and interest
...  the factor  owners  will  then spend  the  higher money  incomes
in  their  existing  consumption:investment proportions  ...  Capital
goods  industries  will  find  their  expansion  has  been  in  error
and  malinvestments  have been  inoccured.”</em>  [<strong>Markets,
Entrepreneurs and Liberty</strong>, pp. 68–9]</p>

<p>In other words, there  has been <em>“wasteful mis-investment due
to  government  interference  with the  market.”</em>  [<strong>Op.
Cit.</strong>, p. 69] In response to this (negative) influence in the
workings of the market, it  is suggested by right-libertarians that a
system of  private banks should be  used and that interest  rates are
set by  them, via market  forces. In this  way an interest  rate that
matches the  demand and supply  for savings  will be reached  and the
business  cycle will  be no  more.  By truly  privatising the  credit
market, it is hoped by the business cycle will finally stop.</p>

<p>Unsurprisingly, this  particular argument has its  weak points and
in this  section of  the FAQ  we will  try to  show exactly  why this
theory is wrong.</p>

<p>Let  us  start with  Reckie’s  starting  point. He  states  that
the  <em>“main  problem”</em>  of  the  slump  is  <em>“why  is
there suddenly  a ’<strong>cluster</strong>’ of  business errors?
Businessmen  and entrepreneurs  are  market  experts (otherwise  they
would  not   survive)  and   why  should   they  all   make  mistakes
simultaneously?”</em> [<strong>Op. Cit.</strong>, p. 68] It is this
<em>“cluster”</em>  of mistakes  that  the  Austrians’ take  as
evidence that the  business cycle comes from outside  the workings of
the  market (i.e.  is exogenous  in  nature). Reekie  argues that  an
<em>“error cluster only occurs when all entrepreneurs have received
the wrong signals  on potential profitability, and  all have received
the signals  simultaneously through government interference  with the
money supply.”</em> [<strong>Op. Cit.</strong>,  p. 74] But is this
<strong>really</strong> the case?</p>

<p>The simple fact  is that groups of (rational)  individuals can act
in the same way based on the  same information and this can lead to a
collective problem.  For example,  we do  not consider  it irrational
that everyone  in a building leaves  it when the fire  alarm goes off
and that the  flow of people can cause hold-ups  at exits. Neither do
we think  that its unusual that  traffic jams occur, after  all those
involved are all trying to get to work (i.e. they are reacting to the
same desire). Now, is it so strange to think that capitalists who all
see the  same opportunity for profit  in a specific market  decide to
invest in  it? Or  that the aggregate  outcome of  these individually
rational  decisions may  be  irrational  (i.e. cause  a  glut in  the
market)?</p>

<p>In  other   words,  a  “cluster”  of   business  failures  may
come  about because  a  group of  capitalists,  acting in  isolation,
over-invest in  a given  market. They react  to the  same information
(namely super profits in market X), arrange loans, invest and produce
commodities to  meet demand  in that  market. However,  the aggregate
result of these  individually rational actions is  that the aggregate
supply  far exceeds  demand,  causing  a slump  in  that market  and,
perhaps,  business  failures.  The  slump in  this  market  (and  the
potential failure of some firms) has  an impact on the companies that
supplied them,  the companies that  are dependent on  their employees
wages/demand, the  banks that supplied  the credit and so  forth. The
accumulative  impact of  this slump  (or  failures) on  the chain  of
financial commitments  of which they  are but  one link can  be large
and, perhaps, push an economy into general depression. Thus the claim
that it is something external to the system that causes depression is
flawed.</p>

<p>It could be claimed the interest rate is the problem, that it does
not accurately reflect the demand for  investment or relate it to the
supply of savings. But, as we argued in section C.8, it is not at all
clear that  the interest rate  provides the necessary  information to
capitalists.  They need  investment  information  for their  specific
industry, but  the interest rate is  cross-industry. Thus capitalists
in market X do  not know if the investment in  market X is increasing
and so this lack of information can easily cause “mal-investment”
as over-investment (and  so over-production) occurs. As  they have no
way of knowing what the investment decisions of their competitors are
or  now  these  decisions  will affect  an  already  unknown  future,
capitalists may over-invest in certain markets and the net effects of
this aggregate  mistake can expand  throughout the whole  economy and
cause a general slump. In other words, a cluster of business failures
can  be accounted  for  by  the workings  of  the  market itself  and
<strong>not</strong> the (existence of) government.</p>

<p>This  is <strong>one</strong>  possible reason  for an  internally
generated business cycle but that is not the only one. Another is the
role of class  struggle which we discuss in the  next section and yet
another  is  the  endogenous  nature  of  the  money  supply  itself.
This  account  of money  (proposed  strongly  by, among  others,  the
post-Keynesian school)  argues that  the money  supply is  a function
of  the  demand  for  credit,  which itself  is  a  function  of  the
level  of  economic activity.  In  other  words, the  banking  system
creates  as much  money as  people need  and any  attempt to  control
that  creation  will cause  economic  problems  and, perhaps,  crisis
(interestingly,  this analysis  has strong  parallels with  mutualist
and  individualist anarchist  theories  on the  causes of  capitalist
exploitation and the business cycle).  Money, in other words, emerges
from <strong>within</strong> the system  and so the right-libertarian
attempt to “blame the state” is simply wrong.</p>

<p>Thus what  is termed “credit  money” (created by banks)  is an
essential  part of  capitalism and  would exist  without a  system of
central  banks. This  is because  money  is created  from within  the
system, in response to the needs  of capitalists. In a word, money is
endogenous and credit money an essential part of capitalism.</p>

<p>Right-libertarians do not agree.  Reekie argues that <em>“[o]nce
fractional  reserve banking  is  introduced, however,  the supply  of
money  substitutes will  include  fiduciary media.  The ingenuity  of
bankers,  other  financial  intermediaries and  the  endorsement  and
<strong>guaranteeing of  their activities by governments  and central
banks</strong>  has  ensured  that  the quantity  of  fiat  money  is
immense.”</em> [<strong>Op. Cit.</strong>, p. 73]</p>

<p>Therefore,     what     “anarcho”-capitalists    and     other
right-libertarians seem  to be  actually complaining about  when they
argue that “state action” creates  the business cycle by creating
excess  money is  that the  state <strong>allows</strong>  bankers to
meet the demand for credit by  creating it. This makes sense, for the
first  fallacy  of  this  sort  of  claim  is  how  could  the  state
<strong>force</strong> bankers to expand credit by loaning more money
than they have  savings. And this seems to be  the normal case within
capitalism —  the central banks accommodate  bankers activity, they
do not force them  to do it. Alan Holmes, a  senior vice president at
the New York Federal Reserve, stated that:</p>

<blockquote class="citation">
“In the  real world, banks extend  credit, creating deposits
in the  process, and look for  the reserves later. The  question then
becomes one of  whether and how the Federal  Reserve will accommodate
the demand for  reserves. In the very short run,  the Federal Reserve
has little or  no choice about accommodating that  demand, over time,
its influence can obviously be felt.”</em> [quoted by Doug Henwood,
<strong>Wall Street</strong>, p. 220]
</blockquote>

<p>(Although    we   must    stress    that    central   banks    are
<strong>not</strong> passive and do have many tools for affecting the
supply of money.  For example, central banks  can operate “tight”
money policies which  can have significant impact on  an economy and,
via creating high enough interest rates, the demand for money.)</p>

<p>It could  be argued  that because central  banks exist,  the state
creates an “environment” which bankers take advantage off. By not
being  subject  to  “free  market” pressures,  bankers  could  be
tempted to make more loans than  they would otherwise in a “pure”
capitalist system  (i.e. create  credit money). The  question arises,
would “pure”  capitalism generate  sufficient market  controls to
stop banks loaning in excess of available savings (i.e. eliminate the
creation of credit money/fiduciary media).</p>

<p>It is to this question we now turn.</p>

<p>As  noted  above,   the  demand  for  credit   is  generated  from
<strong>within</strong>  the  system  and   the  comments  by  Holmes
reinforce this.  Capitalists seek credit  in order to make  money and
banks create it precisely because  they are also seeking profit. What
right-libertarians  actually object  to  is the  government (via  the
central bank) <strong>accommodating</strong> this creation of credit.
If  only  the  banks  could  be  forced  to  maintain  a  savings  to
loans  ration  of  one,  then  the business  cycle  would  stop.  But
is  this  likely? Could  market  forces  ensure that  bankers  pursue
such  a  policy? We  think  not  —  simply  because the  banks  are
profit making institutions. As post-Keynesianist Hyman Minsky argues,
<em>“[b]ecause bankers  live in  the same expectational  climate as
businessmen, profit-seeking  bankers will find ways  of accommodating
their  customers... Banks  and bankers  are not  passive managers  of
money  to  lend or  to  invest;  they  are  in business  to  maximise
profits...”</em>  [quoted by  L.  Randall  Wray, <strong>Money  and
Credit in Capitalist Economies</strong>, p. 85]</p>

<p>This is recognised  by Reekie, in passing at least  (he notes that
<em>“fiduciary media could still exist  if bankers offered them and
clients  accepted them”</em>  [<strong>Op. Cit.</strong>,  p. 73]).
Bankers  will  tend  to  try  and  accommodate  their  customers  and
earn  as  much  money  as  possible.  Thus  Charles  P.  Kindleberger
comments that monetary expansion  <em>“is systematic and endogenous
rather  than random  and  exogenous”</em> seem  to  fit far  better
the  reality of  capitalism that  the Austrian  and right-libertarian
viewpoint [<strong>Manias,  Panics, and Crashes</strong>, p.  59] and
post-Keynesian L.  Randall Wray  argues that <em>“the  money supply
...  is  more obviously  endogenous  in  the monetary  systems  which
predate  the  development  of a  central  bank.”</em>  [<strong>Op.
Cit.</strong>, p. 150]</p>

<p>In other words, the money  supply cannot be directly controlled by
the central bank since it is determined by private decisions to enter
into  debt  commitments to  finance  spending.  Given that  money  is
generated from <strong>within</strong> the  system, can market forces
ensure the  non-expansion of credit  (i.e. that the demand  for loans
equals  the supply  of savings)?  To  begin to  answer this  question
we  must  note  that   investment  is  <em>“essentially  determined
by  expected  profitability.”</em>   [Philip  Arestis,  <strong>The
Post-Keynesian  Approach to  Economics</strong>, p.  103] This  means
that the actions  of the banks cannot be taken  in isolation from the
rest of the economy. Money, credit and banks are an essential part of
the capitalist system  and they cannot be  artificially isolated from
the expectations, pressures and influences of that system.</p>

<p>Let us assume that the banks desire to maintain a loans to savings
ratio  of one  and try  to adjust  their interest  rates accordingly.
Firstly, changes in  the rate of interest <em>“produce  only a very
small, if  any, movement in business  investment”</em> according to
empirical evidence [<strong>Op. Cit.</strong>,  pp. 82–83] and that
<em>“the demand for  credit is extremely inelastic  with respect to
interest rates.”</em> [L.  Randall Wray, <strong>Op. Cit.</strong>,
p. 245] Thus, to  keep the supply of savings in  line with the demand
for loans,  interest rates  would have  to increase  greatly (indeed,
trying to control the money  supply by controlling the monetary bases
in  this way  will only  lead to  very big  fluctuations in  interest
rates). And  increasing interest  rates has  a couple  of paradoxical
effects.</p>

<p>According  to  economists Joseph  Stiglitz  and  Andrew Weiss  (in
<em>“Credit Rationing in  Markets with Imperfect Knowledge”</em>,
<strong>American  Economic Review</strong>,  no.  71, pp.  393–410)
interest  rates  are subject  to  what  is called  the  <em>“lemons
problem”</em> (asymmetrical information  between buyer and seller).
Stiglitz and  Weiss applied  the “lemons  problem” to  the credit
market and  argued (and  unknowingly repeated Adam  Smith) that  at a
given interest rate, lenders will earn lower return by lending to bad
borrowers (because of defaults) than to  good ones. If lenders try to
increase interest rates  to compensate for this risk,  they may chase
away good  borrowers, who are unwilling  to pay a higher  rate, while
perversely  not chasing  away incompetent,  criminal, or  malignantly
optimistic borrowers. This  means that an increase  in interest rates
may actually increase the possibilities  of crisis, as more loans may
end up in the hands of defaulters.</p>

<p>This gives banks  a strong incentive to keep  interest rates lower
than they otherwise could  be. Moreover, <em>“increases in interest
rates make it  more difficult for economic agents to  meet their debt
repayments”</em>  [Philip Arestis,  <strong>Op. Cit.</strong>,  pp.
237–8] which means when interest rates <strong>are</strong> raised,
defaults will increase and place  pressures on the banking system. At
high  enough short-term  interest rates,  firms find  it hard  to pay
their interest bills, which cause/increase  cash flow problems and so
<em>“[s]harp increases in  short term interest rates  ...leads to a
fall in the present value  of gross profits after taxes (quasi-rents)
that  capital assets  are  expected to  earn.”</em> [Hyman  Minsky,
<strong>Post-Keynesian Economic Theory</strong>, p. 45]</p>

<p>In  addition,  <em>“production  of   most  investment  goods  is
undertaken  on order  and requires  time  for completion.  A rise  in
interest rates  is not likely to  cause firms to abandon  projects in
the process of production ... This  does not mean ... that investment
is completely  unresponsive to  interest rates.  A large  increase in
interest  rates causes  a ‘present  value reversal’,  forcing the
marginal efficiency  of capital to  fall below the interest  rate. If
the  long  term interest  rate  is  also  pushed above  the  marginal
efficiency of  capital, the project may  be abandoned.”</em> [Wray,
<strong>Op. Cit.</strong>,  pp. 172–3]  In other  words, investment
takes  <strong>time</strong> and  there is  a lag  between investment
decisions and actual  fixed capital investment. So  if interest rates
vary  during this  lag period,  initially profitable  investments may
become white elephants.</p>

<p>As Michal Kalecki argued, the rate  of interest must be lower than
the  rate  of  profit  otherwise investment  becomes  pointless.  The
incentive for a  firm to own and operate capital  is dependent on the
prospective rate  of profit on that  capital relative to the  rate of
interest at  which the firm  can borrow  at. The higher  the interest
rate, the less promising investment becomes.</p>

<p>If investment is unresponsive to  all but very high interest rates
(as we  indicated above),  then a privatised  banking system  will be
under intense  pressure to keep rates  low enough to maintain  a boom
(by, perhaps, creating credit above the amount available as savings).
And  if it  does this,  over-investment  and crisis  is the  eventual
outcome. If  it does not  do this  and increases interest  rates then
consumption and  investment will  dry up as  interest rates  rise and
the  defaulters (honest  and dishonest)  increase and  a crisis  will
eventually occur.</p>

<p>This  is because  increasing interest  rates may  increase savings
<strong>but</strong> it also reduce consumption (<em>“high interest
rates also deter both consumers  and companies from spending, so that
the domestic economy is weakened and unemployment rises”</em> [Paul
Ormerod,  <strong>The  Death  of Economics</strong>,  p.  70]).  This
means  that  firms can  face  a  drop  off  in demand,  causing  them
problems and (perhaps)  leading to a lack of  profits, debt repayment
problems  and failure.  An increase  in interest  rates also  reduces
demand for  investment goods,  which also  can cause  firms problems,
increase unemployment  and so  on. So an  increase in  interest rates
(particularly a  sharp rise) could reduce  consumption and investment
(i.e. reduce  aggregate demand) and  have a ripple  effect throughout
the economy which could cause a slump to occur.</p>

<p>In  other words,  interest  rates  and the  supply  and demand  of
savings/loans they are  meant to reflect may not  necessarily move an
economy towards  equilibrium (if such  a concept is  useful). Indeed,
the  workings of  a “pure”  banking system  without credit  money
may  increase unemployment  as demand  falls in  both investment  and
consumption in response to high interest rates and a general shortage
of money due to lack of (credit) money resulting from the “tight”
money regime implied by such a  regime (i.e. the business cycle would
still exist). This was the  case of the failed Monetarist experiments
on the early 1980s when central banks in America and Britain tried to
pursue a “tight”  money policy. The “tight”  money policy did
not, in  fact, control the money  supply. All it did  do was increase
interest rates  and lead  to a  serious financial  crisis and  a deep
recession (as  Wray notes, <em>“the  central bank uses  tight money
polices to  raise interest rates”</em>  [<strong>Op. Cit.</strong>,
p. 262]).  This recession, we must  note, also broke the  backbone of
working class resistance and the unions  in both countries due to the
high  levels  of  unemployment  it generated.  As  intended,  we  are
sure.</p>

<p>Such an outcome  would not surprise anarchists, as this  was a key
feature of  the Individualist  and Mutualist  Anarchists’ arguments
against the  “money monopoly” associated with  specie money. They
argued  that the  “money  monopoly” created  a “tight”  money
regime which reduced  the demand for labour by  restricting money and
credit and  so allowed  the exploitation  of labour  (i.e. encouraged
wage labour) and  stopped the development of  non-capitalist forms of
production.  Thus Lysander  Spooner’s  comments  that workers  need
<em>“<strong>money capital</strong>  to enable them to  buy the raw
materials  upon which  to  bestow their  labour,  the implements  and
machinery with which to labour ... Unless they get this capital, they
must all either  work at a disadvantage,  or not work at  all. A very
large portion  of them, to  save themselves from starvation,  have no
alternative but to sell their labour to others ...”</em> [<strong>A
Letter to Grover Cleveland</strong>, p. 39] It is interesting to note
that  workers  <strong>did</strong>  do  well during  the  1950s  and
1960s  under a  “liberal” money  regime than  they did  under the
“tighter” regimes of the 1980s and 1990s.</p>

<p>We  should  also  note  that  an  extended  period  of  boom  will
encourage banks  to make loans  more freely. According  to Minsky’s
<em>“financial  instability  model”</em> crisis  (see  <em>“The
Financial  Instability  Hypothesis”</em> in  <strong>Post-Keynesian
Economic Theory</strong> for example)  is essentially caused by risky
financial  practices during  periods  of  financial tranquillity.  In
other words,  <em>“stability is destabilising.”</em> In  a period
of boom, banks are happy and the increased profits from companies are
flowing into their vaults. Over time,  bankers note that they can use
a reserve  system to increase  their income  and, due to  the general
upward swing  of the economy,  consider it safe  to do so  (and given
that they are in competition with other banks, they may provide loans
simply because they  are afraid of losing customers  to more flexible
competitors). This  increases the  instability within the  system (as
firms  increase their  debts due  to  the flexibility  of the  banks)
and  produces  the  possibility  of  crisis  if  interest  rates  are
increased (because the ability of  business to fulfil their financial
commitments embedded in debts deteriorates).</p>

<p>Even if we assume that  interest rates <strong>do</strong> work as
predicted  in theory,  it  is false  to maintain  that  there is  one
interest rate. This is not  the case. <em>“Concentration of capital
leads to unequal access to  investment funds, which obstructs further
the possibility of smooth transitions in industrial activity. Because
of their past record of  profitability, large enterprises have higher
credit ratings and  easier access to credit facilities,  and they are
able  to put  up larger  collateral for  a loan.”</em>  [Michael A.
Bernstein, <strong>The Great Depression</strong>, p. 106] As we noted
in section  C.5.1, the larger the  firm, the lower the  interest rate
they have to pay. Thus banks  routinely lower their interest rates to
their  best clients  even though  the  future is  uncertain and  past
performance cannot and does not indicate future returns. Therefore it
seems a  bit strange to  maintain that  the interest rate  will bring
savings  and loans  into  line  if there  are  different rates  being
offered.</p>

<p>And,  of  course,  private  banks  cannot  affect  the  underlying
fundamentals that  drive the  economy — like  productivity, working
class power and  political stability — any more  than central banks
(although central  banks can  influence the  speed and  gentleness of
adjustment to a crisis).</p>

<p>Indeed,  given a  period of  full employment  a system  of private
banks may actually speed up the coming of a slump. As we argue in the
next section, full  employment results in a profits  squeeze as firms
face a  tight labour market  (which drives up costs)  and, therefore,
increased workers’  power at the  point of production and  in their
power of  exit. In  a central  bank system,  capitalists can  pass on
these  increasing costs  to consumers  and so  maintain their  profit
margins for  longer. This option  is restricted in a  private banking
system as banks  would be less inclined to devalue  their money. This
means  that firms  will face  a  profits squeeze  sooner rather  than
later, which  will cause a slump  as firms cannot make  ends meet. As
Reekie notes,  inflation <em>“can temporarily reduce  employment by
postponing the time when misdirected  labour will be laid off”</em>
but as  Austrian’s (like  Monetarists) think <em>“inflation  is a
monetary phenomenon”</em> he does not understand the real causes of
inflation  and what  they imply  for a  “pure” capitalist  system
[<strong>Op. Cit.</strong>, p. 67, p. 74]. As Paul Ormerod points out
<em>“the  claim  that inflation  is  always  and everywhere  purely
caused by increases  in the money supply, and that  there the rate of
inflation  bears  a  stable, predictable  relationship  to  increases
in  the  money  supply  is  ridiculous.”</em>  And  he  notes  that
<em>“[i]ncreases  in the  rate of  inflation tend  to be  linked to
falls  in unemployment,  and vice  versa”</em> which  indicates its
<strong>real</strong> causes — namely in the balance of class power
and in the class  struggle. [<strong>The Death of Economics</strong>,
p. 96, p. 131]</p>

<p>Moreover, if we do take the  Austrian theory of the business cycle
at face  value we are  drawn to conclusion  that in order  to finance
investment savings must be increased. But to maintain or increase the
stock  of loanable  savings, inequality  must be  increased. This  is
because,  unsurprisingly, rich  people  save a  larger proportion  of
their income than poor people and the proportion of profits saved are
higher than the proportion of wages. But increasing inequality (as we
argued in  section 3.1) makes  a mockery of  right-libertarian claims
that their system is based on freedom or justice.</p>

<p>This    means   that    the    preferred    banking   system    of
“anarcho”-capitalism   implies    increasing,   not   decreasing,
inequality within society.  Moreover, most firms (as  we indicated in
section C.5.1)  fund their investments  with their own  savings which
would make it hard for banks to  loan these savings out as they could
be withdrawn  at any time.  This could have serious  implications for
the economy, as banks refuse to fund new investment simply because of
the uncertainty they  face when accessing if  their available savings
can be  loaned to  others (after  all, they can  hardly loan  out the
savings of a customer who is likely  to demand them at any time). And
by refusing to  fund new investment, a boom could  falter and turn to
slump as firms do not find the necessary orders to keep going.</p>

<p>So,  would market  forces create  “sound banking”?  The answer
is  probably  not.  The  pressures  on banks  to  make  profits  come
into  conflict with  the  need  to maintain  their  savings to  loans
ration  (and  so   the  confidence  of  their   customers).  As  Wray
argues,  <em>“as banks  are profit  seeking firms,  they find  ways
to  increase  their liabilities  which  don’t  entail increases  in
reserve requirements”</em>  and <em>“[i]f banks share  the profit
expectations  of  prospective  borrowers,   they  can  create  credit
to  allow  [projects/investments]  to  proceed.”</em>  [<strong>Op.
Cit.</strong>, p. 295,  p. 283] This can be seen  from the historical
record.  As Kindleberger  notes,  <em>“the market  will create  new
forms of  money in periods  of boom  to get around  the limit”</em>
imposed on the money supply [<strong>Op. Cit.</strong>, p. 63]. Trade
credit is one  way, for example. Under the  Monetarist experiments of
1980s,  there was  <em>“deregulation and  central bank  constraints
raised  interest  rates   and  created  a  moral   hazard  —  banks
made  increasingly  risky loans  to  cover  rising costs  of  issuing
liabilities. Rising competition from  nonbanks and tight money policy
forced banks  to lower standards and  increase rates of growth  in an
attempt to ‘grow their way to profitability’”</em> [<strong>Op.
Cit.</strong>, p. 293]</p>

<p>Thus  credit  money  (“fiduciary  media”)  is  an  attempt  to
overcome the  scarcity of  money within capitalism,  particularly the
scarcity  of  specie money.  The  pressures  that banks  face  within
“actually  existing”  capitalism  would   still  be  faced  under
“pure”  capitalism. It  is likely  (as Reekie  acknowledges) that
credit money  would still be  created in  response to the  demands of
business people (although  not at the same level as  is currently the
case,  we imagine).  The  banks, seeking  profits  themselves and  in
competition for  customers, would  be caught between  maintaining the
value of their business (i.e. their  money) and the needs to maximise
profits.  As a  boom develops,  banks would  be tempted  to introduce
credit money to maintain it as  increasing the interest rate would be
difficult  and potentially  dangerous (for  reasons we  noted above).
Thus,  if credit  money is  not forth  coming (i.e.  the banks  stick
to  the Austrian  claims  that  loans must  equal  savings) then  the
rise  in interest  rates required  will generate  a slump.  If it  is
forthcoming, then the danger  of over-investment becomes increasingly
likely.  All  in  all,  the  business cycle  is  part  of  capitalism
and <strong>not</strong>  caused by  “external” factors  like the
existence of government.</p>

<p>As Reekie  notes, to Austrians  <em>“ignorance of the  future is
endemic”</em>  [<strong>Op. Cit.</strong>,  p. 117]  but you  would
be  forgiven  for  thinking  that  this  is  not  the  case  when  it
comes  to investment.  An  individual firm  cannot  know whether  its
investment project will  generate the stream of  returns necessary to
meet  the stream  of payment  commitments undertaken  to finance  the
project.  And neither  can the  banks who  fund those  projects. Even
<strong>if</strong> a bank does not get tempted into providing credit
money in  excess of  savings, it cannot  predict whether  other banks
will do the same or whether the projects it funds will be successful.
Firms, looking for credit, may turn to more flexible competitors (who
practice reserve banking to some  degree) and the inflexible bank may
see  its market  share and  profits decrease.  After all,  commercial
banks <em>“typically  establish relations with customers  to reduce
the uncertainty  involved in  making loans. Once  a bank  has entered
into  a  relationship  with  a customer,  it  has  strong  incentives
to  meet the  demands  of that  customer.”</em> [Wray,  <strong>Op.
Cit.</strong>, p. 85]</p>

<p>There  are example  of  fully privatised  banks.  For example,  in
the  United States  (<em>“which was  without a  central bank  after
1837”</em>)  <em>“the major  banks in  New  York were  in a  bind
between their roles  as profit seekers, which  made them contributors
to the instability  of credit, and as possessors  of country deposits
against whose  instability they had to  guard.”</em> [Kindleberger,
<strong>Op. Cit.</strong>, p. 85]</p>

<p>In Scotland, the banks were  unregulated between 1772 and 1845 but
<em>“the leading  commercial banks accumulated the  notes of lessor
ones, as the  Second Bank of the United  States did contemporaneously
in [the  USA], ready to convert  them to specie if  they thought they
were  getting out  of  line. They  served, that  is,  as an  informal
controller of the money supply. For the rest, as so often, historical
evidence runs against  strong theory, as demonstrated  by the country
banks in  England from 1745 to  1835, wildcat banking in  Michigan in
the 1830s, and the latest  experience with bank deregulation in Latin
America.”</em>  [<strong>Op. Cit.</strong>,  p. 82]  And we  should
note  there  were a  few  banking  “wars”  during the  period  of
deregulation in Scotland  which forced a few of the  smaller banks to
fail as  the bigger  ones refused  their money and  that there  was a
major bank failure in the Ayr Bank.</p>

<p>Kendleberger argues  that central  banking <em>“arose  to impose
control on  the instability of  credit”</em> and did not  cause the
instability which right-libertarians maintain it does. And as we note
in section 10.3, the USA suffered massive economic instability during
its period without central  banking. Thus, <strong>if</strong> credit
money  <strong>is</strong> the  cause of  the business  cycle, it  is
likely that a “pure” capitalism will still suffer from it just as
much  as  “actually  existing”  capitalism (either  due  to  high
interest rates or over-investment).</p>

<p>In  general, as  the failed  Monetarist experiments  of the  1980s
prove,  trying  to  control  the  money  supply  is  impossible.  The
demand  for  money   is  dependent  on  the  needs   of  the  economy
and  any  attempt  to  control  it   will  fail  (and  cause  a  deep
depression,   usually  via   high  interest   rates).  The   business
cycle,  therefore,   is  an  endogenous  phenomenon   caused  by  the
normal  functioning  of  the  capitalist  economic  system.  Austrian
and   right-libertarian   claims   that  <em>“slump   flows   boom,
but   for  a   totally   unnecessary   reason:  government   inspired
mal-investment”</em> [Reekie, <strong>Op. Cit.</strong>, p. 74] are
simply wrong. Over-investment <strong>does</strong>  occur, but it is
<strong>not</strong> <em>“inspired”</em> by the government. It is
<em>“inspired”</em> by the banks need  to make profits from loans
and  from  businesses  need  for investment  funds  which  the  banks
accommodate.  In  other  words,  by  the  nature  of  the  capitalist
system.</p>

<h4 id="toc61">10.2 How does the labour market effect capitalism?</h4>

<p>In many ways, the labour market is the one that affects capitalism
the most.  The right-libertarian assumption (like  that of mainstream
economics) is  that markets clear  and, therefore, the  labour market
will also clear. As this assumption has rarely been proven to be true
in actuality (i.e.  periods of full employment  within capitalism are
few and far  between), this leaves its supporters with  a problem —
reality contradicts the theory.</p>

<p>The theory predicts full employment but reality shows that this is
not  the case.  Since we  are  dealing with  logical deductions  from
assumptions,  obviously the  theory cannot  be wrong  and so  we must
identify  external factors  which cause  the business  cycle (and  so
unemployment). In this way attention is diverted away from the market
and its  workings — after  all, it  is assumed that  the capitalist
market works —  and onto something else.  This “something else”
has been quite  a few different things (most  ridiculously, sun spots
in  the  case  of  one  of the  founders  of  marginalist  economics,
William  Stanley Jevons).  However, these  days most  pro-free market
capitalist economists  and right-libertarians have now  decided it is
the state.</p>

<p>In this section  of the FAQ we will present  a case that maintains
that the  assumption that markets  clear is  false at least  for one,
unique, market — namely, the  market for labour. As the fundamental
assumption  underlying “free  market”  capitalism  is false,  the
logically consistent  superstructure built upon comes  crashing down.
Part  of  the  reason  why  capitalism is  unstable  is  due  to  the
commodification  of  labour  (i.e.  people)  and  the  problems  this
creates. The state  itself can have positive and  negative impacts on
the economy,  but removing  it or  its influence  will not  solve the
business cycle.</p>

<p>Why is this? Simply due to the nature of the labour market.</p>

<p>Anarchists  have  long  realised  that the  capitalist  market  is
based upon  inequalities and changes  in power. Proudhon  argued that
<em>“[t]he manufacturer says to the labourer, ‘You are as free to
go elsewhere with your services as I  am to receive them. I offer you
so much.’  The merchant says to  the customer, ‘Take it  or leave
it;  you are  master of  your money,  as  I am  of my  goods. I  want
so  much.’  Who  will  yield?  The  weaker.”</em>  He,  like  all
anarchists,  saw that  domination, oppression  and exploitation  flow
from inequalities of market/economic  power and that the <em>“power
of  invasion  lies  in superior  strength.”</em>  [<strong>What  is
Property?</strong>, p. 216, p. 215]</p>

<p>This  applies with  greatest  force to  the  labour market.  While
mainstream economics and right-libertarian variations of it refuse to
acknowledge that the capitalist market  is a based upon hierarchy and
power, anarchists (and  other socialists) do not  share this opinion.
And because  they do  not share  this understanding  with anarchists,
right-libertarians will never be able to understand capitalism or its
dynamics and development.  Thus, when it comes to  the labour market,
it is  essential to remember that  the balance of power  within it is
the key to understanding the business cycle. Thus the economy must be
understood as a system of power.</p>

<p>So how does the labour market effect capitalism? Let us consider a
growing economy, on that is coming out of a recession. Such a growing
economy stimulates  demand for employment and  as unemployment falls,
the costs of finding workers  increase and wage and condition demands
of existing workers  intensify. As the economy is  growing and labour
is scare, the threat associated  with the hardship of unemployment is
weakened. The  share of profits is  squeezed and in reaction  to this
companies  begin to  cut costs  (by reducing  inventories, postponing
investment plans  and laying off  workers). As a result,  the economy
moves  into  a downturn.  Unemployment  rises  and wage  demands  are
moderated. Eventually, this enables the share of profits first of all
to stabilise, and then rise. Such an <em>“interplay between profits
and unemployment  as the  key determinant of  business cycles”</em>
is  <em>“observed in  the  empirical  data.”</em> [Paul  Ormerod,
<strong>The Death of Economics</strong>, p. 188]</p>

<p>Thus,  as an  economy approaches  full employment  the balance  of
power on the labour market changes.  The sack is no longer that great
a threat as people see that they can get a job elsewhere easily. Thus
wages and  working conditions  increase as companies  try to  get new
(and keep)  existing employees and  output is harder to  maintain. In
the words of  economist William Lazonick, labour  <em>“that is able
to command a  higher price than previously because  of the appearance
of tighter  labour markets is,  by definition, labour that  is highly
mobile  via  the  market.  And  labour  that  is  highly  mobile  via
the  market  is  labour  whose  supply of  effort  is  difficult  for
managers to control  in the production process. Hence,  the advent of
tight  labour  markets  generally  results  in  more  rapidly  rising
average  costs  ...as well  as  upward  shifts  in the  average  cost
curve...”</em> [<strong>Business  Organisation and the Myth  of the
Market Economy</strong>, p. 106]</p>

<p>In    other   words,    under   conditions    of   full-employment
<em>“employers  are in  danger  of losing  the upper  hand.”</em>
[Juliet B.  Schor, <strong>The  Overworked American</strong>,  p. 75]
Schor argues that <em>“employers have a structural advantage in the
labour market, because there are  typically more candidates ready and
willing to  endure this work marathon  [of long hours] than  jobs for
them to  fill.”</em> [p. 71]  Thus the  labour market is  usually a
buyers market,  and so the  sellers have  to compromise. In  the end,
workers adapt to this inequality of power and instead of getting what
they want, they want what they get.</p>

<p>But under  full employment this  changes. As we argued  in section
B.4.4 and section C.7, in such a  situation it is the bosses who have
to  start compromising.  And they  do not  like it.  As Schor  notes,
America  <em>“has  never experienced  a  sustained  period of  full
employment. The  closest we have gotten  is the late 1960s,  when the
overall unemployment  rate was  under 4 percent  for four  years. But
that experience does more to prove  the point than any other example.
The  trauma caused  to  business by  those years  of  a tight  labour
market  was  considerable. Since  then,  there  has been  a  powerful
consensus  that  the nation  cannot  withstand  such  a low  rate  of
unemployment.”</em> [<strong>Op. Cit.</strong>, pp. 75–76]</p>

<p>So,  in  other  words,  full   employment  is  not  good  for  the
capitalist system due to the  power full employment provides workers.
Thus  unemployment  is  a  necessary  requirement  for  a  successful
capitalist economy  and not some  kind of aberration in  an otherwise
healthy system. Thus  “anarcho”-capitalist claims that “pure”
capitalism  will  soon  result   in  permanent  full  employment  are
false.  Any moves  towards full  employment  will result  in a  slump
as  capitalists  see their  profits  squeezed  from below  by  either
collective class  struggle or  by individual  mobility in  the labour
market.</p>

<p>This  was recognised  by  Individualist  Anarchists like  Benjamin
Tucker, who argued  that mutual banking would  <em>“give an unheard
of  impetus to  business,  and consequently  create an  unprecedented
demand for  labour, — a demand  which would always be  in excess of
the  supply,  directly  contrary  of the  present  condition  of  the
labour  market.”</em> [<strong>The  Anarchist Reader</strong>,  pp.
149–150]  In  other words,  full  employment  would end  capitalist
exploitation, drive non-labour  income to zero and  ensure the worker
the full  value of  her labour  — in  other words,  end capitalism.
Thus, for most (if not all)  anarchists the exploitation of labour is
only  possible when  unemployment  exists and  the  supply of  labour
exceeds the demand for it.  Any move towards unemployment will result
in a profits squeeze and either  the end of capitalism or an economic
slump.</p>

<p>Indeed, as we argued in the  last section, the extended periods of
(approximately)  full employment  until the  1960s had  the advantage
that any  profit squeeze could  (in the  short run anyway)  be passed
onto working class people in the  shape of inflation. As prices rise,
labour is made cheaper and  profits margins supported. This option is
restricted under a “pure” capitalism (for reasons we discussed in
the last  section) and so  “pure” capitalism will be  affected by
full employment faster than “impure” capitalism.</p>

<p>As  an  economy  approaches  full  employment,  <em>“hiring  new
workers  suddenly  becomes  much  more  difficult.  They  are  harder
to  find,  cost  more,  and  are  less  experiences.  Such  shortages
are  extremely   costly  for  a  firm.”</em>   [Schor,  <strong>Op.
Cit.</strong>,  p.  75] This  encourages  a  firm  to pass  on  these
rises  to   society  in  the   form  of  price  rises,   so  creating
inflation.  Workers,  in turn,  try  to  maintain their  standard  of
living.  <em>“Every  general increase  in  labour  costs in  recent
years,”</em> note  J. Brecher  and J. Costello  in the  late 1970s,
<em>“has followed,  rather than  preceded, an increase  in consumer
prices. Wage increases have been  the result of workers’ efforts to
catch up after  their incomes have already been  eroded by inflation.
Nor could  it easily  be otherwise.  All a businessman  has to  do to
raise a  price ...  [is to]  make an  announcement... Wage  rates ...
are  primarily  determined  by  contracts”</em> and  so  cannot  be
easily  adjusted in  the short  term. [<strong>Common  Sense for  Bad
Times</strong>, p, 120]</p>

<p>These full  employment pressures will still  exist with “pure”
capitalism (and due to the nature of the banking system will not have
the  safety value  of  inflation). This  means  that periodic  profit
squeezes  will occur,  due to  the nature  of a  tight labour  market
and  the increased  power of  workers  this generates.  This in  turn
means  that a  “pure” capitalism  will be  subject to  periods of
unemployment  (as we  argued  in section  C.9) and  so  still have  a
business cycle.  This is  usually acknowledged  by right-libertarians
in  passing, although  they  seem  to think  that  this  is purely  a
“short-term” problem (it seems a strange “short-term” problem
that continually occurs).</p>

<p>But such  an analysis  is denied  by right-libertarians.  For them
government action, combined  with the habit of many  labour unions to
obtain higher than  market wage rates for their  members, creates and
exacerbates mass unemployment. This flows from the deductive logic of
much capitalist economics. The basic assumption of capitalism is that
markets clear. So if unemployment exists  then it can only be because
the  price of  labour  (wages)  is too  high  (Austrian Economist  W.
Duncan Reekie argues that unemployment will <em>“disappear provided
real  wages  are   not  artificially  high”</em>  [<strong>Markets,
Entrepreneurs and Liberty</strong>, p. 72]).</p>

<p>Thus the  assumption provokes  the conclusion —  unemployment is
caused by an unclearing market as markets always clear. And the cause
for this is either the state or unions. But what if the labour market
<strong>cannot</strong>  clear without  seriously damaging  the power
and  profits of  capitalists?  What if  unemployment  is required  to
maximise profits  by weakening labours’ bargaining  position on the
market  and  so  maximising  the  capitalists  power?  In  that  case
unemployment  is caused  by  capitalism, not  by  forces external  to
it.</p>

<p>However,  let  us  assume  that the  right-libertarian  theory  is
correct. Let  us assume  that unemployment  is all  the fault  of the
selfish unions and that a job-seeker <em>“who does not want to wait
will always get a job  in the unhampered market economy.”</em> [von
Mises, <strong>Human Action</strong>, p. 595]</p>

<p>Would crushing the unions reduce  unemployment? Let us assume that
the unions have  been crushed and government has  been abolished (or,
at  the  very  least,  become  a  minimum  state).  The  aim  of  the
capitalist class  is to maximise  their profits  and to do  this they
invest in labour  saving machinery and otherwise  attempt to increase
productivity. But  increasing productivity  means that the  prices of
goods  fall and  falling prices  mean  increasing real  wages. It  is
high  real wages  that, according  to right-libertarians,  that cause
unemployment.  So as  a reward  for increasing  productivity, workers
will have to have their money wages cut in order to stop unemployment
occurring! For this reason some  employers might refrain from cutting
wages in order to avoid damage to morale — potentially an important
concern.</p>

<p>Moreover,  wage  contracts  involve  <strong>time</strong>  —  a
contract will usually agree a certain wage for a certain period. This
builds  in rigidity  into the  market,  wages cannot  be adjusted  as
quickly as other commodity prices. Of course, it could be argued that
reducing the  period of the contract  and/or allowing the wage  to be
adjusted  could overcome  this  problem. However,  if  we reduce  the
period of the  contract then workers are at a  suffer disadvantage as
they will not know  if they have a job tomorrow and  so they will not
be able to easily plan their  future (an evil situation for anyone to
be in).  Moreover, even without formal  contracts, wage renegotiation
can be  expensive. After all, it  takes time to bargain  (and time is
money under capitalism) and wage cutting  can involve the risk of the
loss of  mutual good  will between employer  and employee.  And would
<strong>you</strong> give  your boss  the power to  “adjust” your
wages  as he/she  thought  was necessary?  To do  so  would imply  an
altruistic trust in others not to abuse their power.</p>

<p>Thus a “pure” capitalism would be constantly seeing employment
increase  and decrease  as  productivity levels  change. There  exist
important reasons why the labour  market need not clear which revolve
around  the  avoidance/delaying  of  wage  cuts  by  the  actions  of
capitalists themselves.  Thus, given  a choice between  cutting wages
for all workers and laying off some workers without cutting the wages
of  the  remaining employees,  it  is  unsurprising that  capitalists
usually  go for  the  later.  After all,  the  sack  is an  important
disciplining  device  and  firing  workers  can  make  the  remaining
employees more inclined to work harder and be more obedient.</p>

<p>And,  of  course,   many  employers  are  not   inclined  to  hire
over-qualified  workers.  This is  because,  once  the economy  picks
up  again,  their  worker  has  a  tendency  to  move  elsewhere  and
so  it  can cost  them  time  and  money  finding a  replacement  and
training them.  This means  that involuntary unemployment  can easily
occur,  so reducing  tendencies  towards full  employment even  more.
In  addition, one  of  the assumptions  of  the standard  marginalist
economic  model is  one of  decreasing returns  to scale.  This means
that  as employment  increases, costs  rise and  so prices  also rise
(and  so  real wages  fall).  But  in  reality many  industries  have
<strong>increasing</strong>  returns to  scale, which  means that  as
production increases unit  costs fall, prices fall and  so real wages
rise.  Thus in  such an  economy unemployment  would increase  simply
because of the nature of the production process!</p>

<p>Moreover, as  we argued in-depth  in section  C.9, a cut  in money
wages is not a neutral act. A cut in money wages means a reduction in
demand for  certain industries,  which may have  to reduce  the wages
of  its employees  (or  fire  them) to  make  ends  meet. This  could
produce a accumulative  effect and actually <strong>increase</strong>
unemployment rather than reduce it.</p>

<p>In addition, there are no  “self-correcting” forces at work in
the labour  market which will  quickly bring employment back  to full
levels. This  is for  a few  reasons. Firstly,  the supply  of labour
cannot be reduced by cutting back production as in other markets. All
we  can do  is move  to  other areas  and  hope to  find work  there.
Secondly, the supply of labour can sometimes adjust to wage decreases
in the  wrong direction.  Low wages  might drive  workers to  offer a
greater  amount of  labour (i.e.  longer hours)  to make  up for  any
short  fall (or  to  keep  their job).  This  is  usually termed  the
<em>“efficiency  wage”</em>  effect.  Similarly,  another  family
member may seek  employment in order to maintain a  given standard of
living.  Falling  wages  may  cause the  number  of  workers  seeking
employment to <strong>increase</strong>, causing  a full further fall
in wages  and so  on (and  this is ignoring  the effects  of lowering
wages on demand discussed in section C.9).</p>

<p>The paradox of piece work is  an important example of this effect.
As Schor argues,  <em>“piece-rate workers were caught  in a viscous
downward spiral of poverty and  overwork... When rates were low, they
found themselves compelled to make up  in extra output what they were
losing  on each  piece. But  the  extra output  produced glutted  the
market  and  drove  rates  down further.”</em>  [Juliet  C.  Schor,
<strong>The Overworked American</strong>, p, 58]</p>

<p>Thus, in the face of reducing  wages, the labour market may see an
accumulative move  away from  (rather than towards)  full employment,
The right-libertarian argument is that unemployment is caused by real
wages being  too high which  in turn  flows from the  assumption that
markets  clear. If  there  is  unemployment, then  the  price of  the
commodity  labour  is  too  high  —  otherwise  supply  and  demand
would  meet  and the  market  clear.  But  if,  as we  argued  above,
unemployment  is  essential to  discipline  workers  then the  labour
market <strong>cannot</strong> clear except for short periods. If the
labour  market clears,  profits  are squeezed.  Thus  the claim  that
unemployment is caused by “too high”  real wages is false (and as
we argue in section C.9, cutting these wages will result in deepening
any slump and making recovery longer to come about).</p>

<p>In other words,  the assumption that the labour  market must clear
is  false, as  is any  assumption that  reducing wages  will tend  to
push  the  economy  quickly  back  to  full  employment.  The  nature
of  wage  labour  and  the “commodity”  being  sold  (i.e.  human
labour/time/liberty) ensure that it can  never be the same as others.
This has important implications for economic theory and the claims of
right-libertarians, implications that  they fail to see  due to their
vision of labour as a commodity like any other.</p>

<p>The question arises, of course, of whether, during periods of full
employment, workers  could not take  advantage of their  market power
and gain  increased workers’  control, create co-operatives  and so
reform away  capitalism. This was  the argument of the  Mutualist and
Individualist anarchists and it does  have its merits. However, it is
clear (see section  J.5.12) that bosses hate to  have their authority
reduced and so combat workers’ control whenever they can. The logic
is simple,  if workers  increase their  control within  the workplace
the  manager  and  bosses  may  soon  be  out  of  a  job  and  (more
importantly) they  may start  to control  the allocation  of profits.
Any  increase  in working  class  militancy  may provoke  capitalists
to  stop/reduce investment  and  credit and  so  create the  economic
environment  (i.e.  increasing  unemployment) necessary  to  undercut
working class power.</p>

<p>In other words, a period of full unemployment is not sufficient to
reform capitalism away. Full  employment (nevermind any struggle over
workers’ control)  will reduce profits  and if profits  are reduced
then firms find  it hard to repay debts, fund  investment and provide
profits for  shareholders. This  profits squeeze  would be  enough to
force capitalism into a slump  and any attempts at gaining workers’
self-management in periods of high  employment will help push it over
the  edge (after  all, workers’  control without  control over  the
allocation of any surplus is distinctly phoney). Moreover, even if we
ignore the  effects of full employment  may not last due  to problems
associated  with  over-investment  (see section  C.7.2),  credit  and
interest rate  problems (see section 10.1)  and realisation/aggregate
demand  disjoints. Full  employment adds  to the  problems associated
with  the capitalist  business cycle  and so,  if class  struggle and
workers power did  not exist or cost problem,  capitalism would still
not be stable.</p>

<p>If  equilibrium  is  a  myth,  then  so  is  full  employment.  It
seems  somewhat  ironic   that  “anarcho”-capitalists  and  other
right-libertarians  maintain that  there  will  be equilibrium  (full
employment)  in  the  one  market  within  capitalism  it  can  never
actually  exist in!  This  is usually  quietly  acknowledged by  most
right-libertarians, who mention in  passing that some “temporary”
unemployment  <strong>will</strong> exist  in  their  system —  but
“temporary” unemployment  is not full employment.  Of course, you
could maintain that all unemployment is “voluntary” and get round
the problem by denying it, but that will not get us very far.</p>

<p>So  it  is  all  fine   and  well  saying  that  “libertarian”
capitalism  would  be  based  upon  the  maxim  <em>“From  each  as
they  choose, to  each as  they are  chosen.”</em> [Robert  Nozick,
<strong>Anarchy,  State,  and Utopia</strong>,  p.  160]  But if  the
labour market is such that workers have little option about what they
“choose” to give and fear  that they will <strong>not</strong> be
chosen, then they are at a disadvantage when compared to their bosses
and  so  “consent”  to  being  treated as  a  resource  from  the
capitalist can  make a profit  from. And so  this will result  in any
“free” contract on  the labour market favouring one  party at the
expense of  the other —  as can  be seen from  “actually existing
capitalism”.</p>

<p>Thus any  “free exchange”  on the  labour market  will usually
<strong>not</strong> reflect the true  desires of working people (and
who will  make all the “adjusting”  and end up wanting  what they
get). Only when  the economy is approaching full  employment will the
labour market start to reflect the true desires of working people and
their wage start to approach its full product. And when this happens,
profits are squeezed and capitalism goes into slump and the resulting
unemployment  disciplines  the  working  class  and  restores  profit
margins. Thus full  employment will be the exception  rather than the
rule within capitalism (and that is a conclusion which the historical
record indicates).</p>

<p>In  other   words,  in  a  normally   working  capitalist  economy
any  labour  contracts  will  not  create  relationships  based  upon
freedom  due  to  the  inequalities  in  power  between  workers  and
capitalists. Instead,  any contracts  will be based  upon domination,
<strong>not</strong>  freedom. Which  prompts  the  question, how  is
libertarian capitalism <strong>libertarian</strong>  if it erodes the
liberty of a large class of people?</p>

<h4 id="toc62">10.3 Was laissez-faire capitalism stable?</h4>

<p>Firstly, we must state that a pure laissez-faire capitalist system
has not  existed. This  means that  any evidence  we present  in this
section  can be  dismissed by  right-libertarians for  precisely this
fact  — it  was  not “pure”  enough. Of  course,  if they  were
consistent, you would expect them  to shun all historical and current
examples of capitalism  or activity within capitalism,  but this they
do not. The logic is simple —  if X is good, then it is permissible
to use it. If X is bad, the system is not pure enough.</p>

<p>However, as right-libertarians  <strong>do</strong> use historical
examples  so  shall  we.  According to  Murray  Rothbard,  there  was
<em>“quasi-laissez-faire  industrialisation   [in]  the  nineteenth
century”</em> [<strong>The Ethics of  Liberty</strong>, p. 264] and
so we will use the example  of nineteenth century America — as this
is usually  taken as being the  closest to pure laissez-faire  — in
order to see if laissez-faire is stable or not.</p>

<p>Yes, we  are well aware  that 19<sup>th</sup> century USA  was far
from laissez-faire  — there was a  state, protectionism, government
economic activity  and so on —  but as this example  has been often
used by  right-Libertarians’ themselves (for example,  Ayn Rand) we
think  that  we  can  gain  a lot  from  looking  at  this  imperfect
approximation of “pure”  capitalism (and as we  argued in section
8,  it is  the  “quasi” aspects  of the  system  that counted  in
industrialisation, <strong>not</strong> the laissez-faire ones).</p>

<p>So,  was  19<sup>th</sup>  century  America stable?  No,  it  most
definitely was not.</p>

<p>Firstly, throughout  that century there were  a continual economic
booms  and slumps.  The  last third  of  the 19<sup>th</sup>  century
(often considered as a heyday of  private enterprise) was a period of
profound instability and anxiety. Between  1867 and 1900 there were 8
complete business cycles. Over these 396 months, the economy expanded
during 199 months  and contracted during 197. Hardly a  sign of great
stability (since the end  of world war II, only about  a fifth of the
time  has spent  in periods  of recession  or depression,  by way  of
comparison). Overall, the economy went  into a slump, panic or crisis
in 1807, 1817, 1828, 1834, 1837, 1854, 1857, 1873, 1882, and 1893 (in
addition, 1903 and 1907 were also crisis years).</p>

<p>Part  of  this instability  came  from  the eras  banking  system.
<em>“Lack  of  a  central banking  system,”</em>  writes  Richard
Du  Boff,  <em>“until   the  Federal  Reserve  act   of  1913  made
financial panics worse and  business cycle swings more severe”</em>
[<strong>Accumulation and Power</strong>, p.  177] It was in response
to this instability that the  Federal Reserve system was created; and
as Doug Henwood notes <em>“the  campaign for a more rational system
of money and credit was not a movement of Wall Street vs. industry or
regional  finance, but  a broad  movement  of elite  bankers and  the
managers of  the new corporations  as well as academics  and business
journalists. The emergence of the Fed was the culmination of attempts
to  define a  standard of  value  that began  in the  1890s with  the
emergence  of the  modern  professionally  managed corporation  owned
not  by  its  managers  but  dispersed  public  shareholders.”</em>
[<strong>Wall Street</strong>, p. 93] Indeed, the Bank of England was
often forced to act as lender of  last resort to the US, which had no
central bank.</p>

<p>In  the  decentralised  banking   system  of  the  19<sup>th</sup>
century, during panics  thousands of banks would  hoard resources, so
starving the system for liquidity precisely at the moment it was most
badly needed. The creation of trusts was one way in which capitalists
tried  to manage  the  system’s instabilities  (at  the expense  of
consumers) and  the corporation  was a response  to the  outlawing of
trusts. <em>“By  internalising lots  of the  competitive system’s
gaps —  by bring  more transactions  within the  same institutional
walls  —  corporations   greatly  stabilised  the  economy.”</em>
[Henwood, <strong>Op. Cit.</strong>, p. 94]</p>

<p>All  during the  hey-day of  laissez  faire we  also find  popular
protests against the money system  used, namely specie (in particular
gold), which was  considered as a hindrance to  economic activity and
expansion (as well  as being a tool for the  rich). The Individualist
Anarchists,  for  example,  considered   the  money  monopoly  (which
included  the  use  of  specie  as  money)  as  the  means  by  which
capitalists ensured that  <em>“the labourers ... [are]  kept in the
condition  of  wage  labourers,”</em>  and  reduced  <em>“to  the
conditions of servants;  and subject to all such  extortions as their
employers ... may  choose to practice upon  them”</em>, indeed they
became  the <em>“mere  tools and  machines  in the  hands of  their
employers”</em>.  With  the  end  of  this  monopoly,  <em>“[t]he
amount of  money, capable of  being furnished ... [would  assure that
all  would] be  under  no necessity  to  act as  a  servant, or  sell
his  or her  labour to  others.”</em> [Lysander  Spooner, <strong>A
Letter  to  Grover  Cleveland</strong>,  p.  47, p.  39,  p.  50,  p.
41]  In other  words,  a  specie based  system  (as  desired by  many
“anarcho”-capitalists) was  considered a  key way  of maintaining
wage labour and exploitation.</p>

<p>Interestingly, since the end of the  era of the Gold Standard (and
so commodity money)  popular debate, protest and  concern about money
has  disappeared. The  debate  and  protest was  in  response to  the
<strong>effects</strong> of  commodity money on the  economy — with
many  people correctly  viewing  the  seriously restrictive  monetary
regime  of the  time  responsible for  economic  problems and  crisis
as  well  as increasing  inequalities.  Instead  radicals across  the
political spectrum  urged a  more flexible regime,  one that  did not
cause wage  slavery and  crisis by  reducing the  amount of  money in
circulation when it could be used to expand production and reduce the
impact of slumps. Needless to say,  the Federal Reserve system in the
USA  was  far from  the  institution  these populists  wanted  (after
all,  it is  run  by and  for  the elite  interests  who desired  its
creation).</p>

<p>That  the laissez-faire  system was  so volatile  and panic-ridden
suggests   that   “anarcho”-capitalist  dreams   of   privatising
everything, including banking,  and everything will be  fine are very
optimistic at best (and, ironically, it was members of the capitalist
class who lead  the movement towards state-managed  capitalism in the
name of “sound money”).</p>

<h3 id="toc63">11 What is the myth of “Natural Law”?</h3>

<p>Natural Law,  and the related  concept of Natural Rights,  play an
important part in  Libertarian and “anarcho”-capitalist ideology.
Right-libertarians are  not alone  in claiming that  their particular
ideology  is based  on  the  “law of  nature”.  Hitler, for  one,
claimed  the same  thing  for  Nazi ideology.  So  do numerous  other
demagogues, religious fanatics,  and political philosophers. However,
each  likes  to  claim that  only  <strong>their</strong>  “natural
law”  is  the  “real”  one, all  the  others  being  subjective
impositions. We will ignore these assertions (they are not arguments)
and  concentrate on  explaining why  natural law,  in all  its forms,
is  a  myth.   In  addition,  we  will   indicate  its  authoritarian
implications.</p>

<p>Instead of such myths anarchists urge people to “work it out for
themselves” and realise that any ethical code is subjective and not
a law  of nature. If its  a good “code”, then  others will become
convinced of  it by your arguments  and their intellect. There  is no
need to claim its a function of “man’s nature”!</p>

<p>The following  books discuss the  subject of “Natural  Law” in
greater  depth and  are recommended  for a  fuller discussion  of the
issues raised in this section:</p>

<p>Robert  Anton   Wilson,  <strong>Natural  Law</strong>   and  L.A.
Rollins, <strong>The Myth of Natural Law</strong>.</p>

<p>We should note that these  books are written by people associated,
to some degree,  with right-libertarianism and, of  course, we should
point  out that  not all  right-libertarians subscribe  to “natural
law”  theories (David  Friedman, for  example, does  not). However,
such a position seems to be the minority in right-Libertarianism (Ayn
Rand, Robert Nozick and Murray  Rothbard, among others, did subscribe
to it).  We should  also point out  that the  Individualist Anarchist
Lysander Spooner  also subscribed to “natural  laws” (which shows
that, as we noted above, the concept is not limited to one particular
theory or ideology). We present a short critique of Spooner’s ideas
on this subject in section G.7.</p>

<p>Lastly, it could be maintained that it is a common “straw man”
to maintain that supporters of Natural  Law argue that their Laws are
like the laws  of physics (and so are capable  of stopping people’s
actions just as the law  of gravity automatically stops people flying
from  the Earth).  But that  is the  whole point  — using  the term
“Natural Law”  implies that  the moral rights  and laws  that its
supporters  argue for  are  to be  considered just  like  the law  of
gravity (although  they acknowledge, of course,  that unlike gravity,
<strong>their</strong>  <em>“natural  laws”</em>  <strong>can  be
violated in  nature</strong>). Far from  saying that the  rights they
support are  just that  (i.e. rights <strong>they</strong>  think are
good) they try  to associate them with universal  facts. For example,
Lysander Spooner (who, we must stress, used the concept of “Natural
law” to <strong>oppose</strong> the  transformation of America into
a capitalist society, unlike Rand, Nozick  and Rothbard who use it to
defend capitalism) stated that:</p>

<blockquote class="citation">
“the  true  definition  of  law  is, that  it  is  a  fixed,
immutable, natural principle; and not anything that man ever made, or
can make, unmake, or alter. Thus we  speak of the laws of matter, and
the laws  of mind;  of the  laws of gravitation,  the laws  of light,
heat, and  electricity...etc., etc... The  law of justice is  just as
supreme and universal in the moral  world, as these others are in the
mental or physical  world; and is as unalterable as  are these by any
human  power.  And  it  is  just  as false  and  absurd  to  talk  of
anybody’s having the  power to abolish the law of  justice, and set
up their own in its stead, as it would be to talk of their having the
power to abolish the law of gravitation, or any other natural laws of
the universe, and set up their own will in the place of them.”</em>
[<strong>A Letter to Grover Cleveland</strong>, p. 88]
</blockquote>

<p>Rothbard and other capitalist supporters of “Natural Law” make
the same sort of claims (as we will see). Now, why, if they are aware
of  the fact  that unlike  gravity  their “Natural  Laws” can  be
violated, do  they use  the term  at all?  Benjamin Tucker  said that
“Natural Law” was a <em>“religious”</em> concept — and this
provides a clue.  To say “Do not violate these  rights, otherwise I
will get cross” does not have <strong>quite</strong> the same power
as “Do not violate these rights,  they are facts of natural and you
are violating nature” (compare to  “Do not violate these laws, or
you will  go to hell”). So  to point out that  “Natural Law” is
<strong>not</strong> the same  as the law of gravity  (because it has
to be  enforced by  humans) is  not attacking  some kind  of “straw
man” — it is exposing the  fact that these “Natural Laws” are
just the personal  prejudices of those who hold them.  If they do not
want then to be exposed as such then they should call their laws what
they are  — personal ethical laws  — rather than compare  them to
the facts of nature.</p>

<h4 id="toc64">11.1 Why  the  term “Natural  Law”  in the  first
place?</h4>

<p>Murray Rothbard claims that <em>“Natural Law theory rests on the
insight...  that each  entity has  distinct and  specific properties,
a  distinct  ‘nature,’  which  can  be  investigated  by  man’s
reason”</em> [<strong>For  a New Liberty</strong>, p.  25] and that
<em>“man  has  rights  because  they  are  <strong>natural</strong>
rights. They are grounded in  the nature of man.”</em> [<strong>The
Ethics of Liberty</strong>, p. 155]</p>

<p>To  put it  bluntly, this  form of  “analysis” was  originated
by  Aristotle  and  has  not  been used  by  science  for  centuries.
Science investigates by proposing  theories and hypotheses to explain
empirical  observations, testing  and  refining  them by  experiment.
In  stark  contrast,  Rothbard  <strong>invents</strong>  definitions
(<em>“distinct”  “natures”</em>) and  then draws  conclusions
from them. Such a method was last  used by the medieval Church and is
devoid of  any scientific  method. It  is, of  course, a  fiction. It
attempts to deduce  the nature of a “natural”  society from <em>a
priori</em>  considerations  of  the  “innate”  nature  of  human
beings, which just means that  the assumptions necessary to reach the
desired conclusions have  been built into the  definition of “human
nature.”  In other  words, Rothbard  defines humans  as having  the
“distinct and  specific properties” that, given  his assumptions,
will allow his dogma (private state capitalism) to be inferred as the
“natural” society for humans.</p>

<p>Rothbard    claims    that    <em>“if    A,    B,    C,    etc.,
have   differing    attributes,   it    follows   that    they   have
different  <strong>natures.</strong>”</em>  [<strong>The Ethics  of
Liberty</strong>, p. 9]  Does this means that as  every individual is
unique (have different attributes), they have different natures? Skin
and hair  colour are  different attributes, does  this mean  that red
haired people have different natures  than blondes? That black people
have  different  natures  than  white (and  such  a  “theory”  of
“natural  law”  was  used  to justify  slavery  —  yes,  slaves
<strong>are</strong> human but they have “different natures” than
their masters and so slavery  is okay). Of course Rothbard aggregates
“attributes” to  species level,  but why  not higher?  Humans are
primates, does  that mean  we have  the same  natures are  monkeys or
gorillas? We  are also  mammals as  well, we share  many of  the same
attributes as whales and dogs. Do we have similar natures?</p>

<p>But this  is by the way.  To continue we find  that after defining
certain  “natures,” Rothbard  attempts  to derive  <em>“Natural
Rights  and Laws”</em>  from  them.  However, these  <em>“Natural
Laws”</em> are  quite strange, as  they can be violated  in nature!
Real natural  laws (like the law  of gravity) <strong>cannot</strong>
be violated and therefore do not  need to be enforced. The “Natural
Laws” the “Libertarian”  desires to foist upon us  are not like
this. They  need to be enforced  by humans and the  institutions they
create. Hence, Libertarian “Natural Laws”  are more akin to moral
prescriptions or juridical laws. However, this does not stop Rothbard
explicitly   <em>“plac[ing]”</em>   his  <em>“Natural   Laws”
“alongside  physical  or   ‘scientific’  natural  laws.”</em>
[<strong>The Ethics of Liberty</strong>, p. 42]</p>

<p>So why  do so  many Libertarians use  the term  “Natural Law?”
Simply, it gives  them the means by which to  elevate their opinions,
dogmas, and prejudices to a metaphysical level where nobody will dare
to criticise or  even think about them. The term  smacks of religion,
where “Natural  Law” has  replaced “God’s Law.”  The latter
fiction gave  the priest power  over believers. “Natural  Law” is
designed to  give the  Libertarian ideologist  power over  the people
that he or she wants to rule.</p>

<p>How  can  one be  against  a  “Natural  Law” or  a  “Natural
Right”? It  is impossible.  How can one  argue against  gravity? If
private  property, for  example, is  elevated  to such  a level,  who
would  dare  argue  against  it? Ayn  Rand  listed  having  landlords
and  employers   along  with   <em>“the  laws   of  nature.”</em>
They  are  <strong>not</strong> similar:  the  first  two are  social
relationships  which   have  to   be  imposed   by  the   state;  the
<em>“laws  of nature”</em>  (like  gravity,  needing food,  etc.)
are  <strong>facts</strong>   which  do  not  need   to  be  imposed.
Rothbard  claims   that  <em>“the  natural  fact   is  that  labour
service <strong>is</strong> indeed  a commodity.”</em> [<strong>Op.
Cit.</strong>, p. 40]  However, this is complete  nonsense — labour
service as  a commodity is a  <strong>social</strong> fact, dependent
on the  distribution of property  within society, its  social customs
and so  forth. It is only  “natural” in the sense  that it exists
within a  given society (the state  is also “natural” as  it also
exists within  nature at a given  time). But neither wage  slavery or
the state is “natural” in the  sense that gravity is natural or a
human having two arms is. Indeed,  workers at the dawn of capitalism,
faced with selling their labour services to another, considered it as
decidedly  “unnatural” and  used the  term “wage  slavery” to
describe it!</p>

<p>Thus,  where  and when  a  “fact”  appears is  essential.  For
example, Rothbard claims that <em>“[a]n  apple, let fall, will drop
to the ground;  this we all observe and acknowledge  to be <strong>in
the  nature</strong> of  the  apple.”</em>  [<strong>The Ethics  of
Liberty</strong>,  p.  9]  Actually,   we  do  not  “acknowledge”
anything of  the kind. We acknowledge  that the apple was  subject to
the force of gravity and that is  why it fell. The same apple, “let
fall” in a space ship would <strong>not</strong> drop to the floor.
Has the “nature”  of the apple changed? No, but  the situation it
is  in  has. Thus  any  attempt  to generate  abstract  “natures”
requires you to ignore reality in favour of ideals.</p>

<p>Because  of the  confusion its  usage creates,  we are  tempted to
think  that  the  use  of  “Natural  Law”  dogma  is  an  attempt
to  <strong>stop</strong> thinking,  to restrict  analysis, to  force
certain aspects of society off the  political agenda by giving them a
divine, everlasting quality.</p>

<p>Moreover, such  an “individualist”  account of the  origins of
rights will always  turn on a muddled  distinction between individual
rationality  and some  vague  notion of  rationality associated  with
membership of  the human  species. How  are we  to determine  what is
rational  for an  individual <strong>as  and individual</strong>  and
what  is  rational  for  that  same  individual  <strong>as  a  human
being</strong>?  It  is  hard  to  see   that  we  can  make  such  a
distinction  for  <em>“[i]f  I   violently  interfere  with  Murray
Rothbard’s  freedom,  this may  violate  the  ‘natural law’  of
Murray Rothbard’s  needs, but  it doesn’t violate  the ‘natural
law’   of   <strong>my</strong>   needs.”</em>   [L.A.   Rollins,
<strong>The  Myth of  Natural Rights</strong>,  p. 28]  Both parties,
after  all,  are human  and  if  such  interference is,  as  Rothbard
claims, <em>“antihuman”</em>  then why? <em>“If it  helps me, a
human,  to  advance  my  life,  then  how  can  it  be  unequivocally
‘antihuman’?”</em> [L.  A. Rollins,  <strong>Op. Cit.</strong>,
p. 27] Thus  “natural law” is contradictory as it  is well within
the bounds of human nature to violate it.</p>

<p>This  means that  in  order  to support  the  dogma of  “Natural
Law,” the  cultists <strong>must</strong> ignore reality.  Ayn Rand
claims  that <em>“the  source  of man’s  rights  is...the law  of
identity. A is A — and Man is Man.”</em> But Rand (like Rothbard)
<strong>defines</strong> <em>“Man”</em> as  an <em>“entity of a
specific  kind —  a rational  being”</em> [<strong>The  Virtue of
Selfishness</strong>,  pp.  94–95].  Therefore she  cannot  account
for  <strong>irrational</strong>  human  behaviours  (such  as  those
that  violate “Natural  Laws”), which  are also  products of  our
“nature.”  To assert  that such  behaviours are  not human  is to
assert that A  can be not-A, thus contradicting the  law of identity.
Her ideology cannot even meet its own test.</p>

<h4 id="toc65">11.2 But  “Natural Law”  provides protection  for individual rights from violation by  the State. Those who are against Natural Law desire total rule by the state.</h4>

<p>The second statement represents a common “Libertarian” tactic.
Instead of addressing the issues, they  accuse an opponent of being a
“totalitarian” (or the less sinister “statist”). In this way,
they hope  to distract attention  from, and so avoid  discussing, the
issue at  hand (while at the  same time smearing their  opponent). We
can therefore ignore the second statement.</p>

<p>Regarding the first,  “Natural Law” has <strong>never</strong>
stopped the rights  of individuals from being violated  by the state.
Such  “laws”  are as  much  use  as  a chocolate  fire-guard.  If
“Natural Rights” could  protect one from the power  of the state,
the Nazis  would not have been  able to murder six  million Jews. The
only thing that  stops the state from attacking  people’s rights is
individual (and social)  power — the ability and  desire to protect
oneself and what one considers to be right and fair. As the anarchist
Rudolf Rocker pointed out:</p>

<blockquote class="citation">
“Political [or individual] rights  do not exist because they
have been legally  set down on a  piece of paper, but  only when they
have become  the ingrown habit of  a people, and when  any attempt to
impair  them  will  be  meet  with  the  violent  resistance  of  the
populace...One  compels respect  from  others when  he  knows how  to
defend  his  dignity  as  a  human being...The  people  owe  all  the
political rights and  privileges which we enjoy today,  in greater or
lesser measure,  not to the  good will  of their governments,  but to
their own strength.”</em> [<strong>Anarcho-Syndicalism</strong>, p.
64]
</blockquote>

<p>Of course, if is there are no “Natural Rights,” then the state
has no  “right” to  murder you  or otherwise  take away  what are
commonly  regarded as  human rights.  One can  object to  state power
without believing in “Natural Law.”</p>

<h4 id="toc66">11.3 Why is “Natural Law” authoritarian?</h4>

<p>Rights, far from being fixed,  are the product of social evolution
and human  action, thought and  emotions. What is acceptable  now may
become unacceptable  in the  future. Slavery,  for example,  was long
considered “natural.”  In fact,  John Locke, the  “father” of
“Natural Rights,”  was heavily  involved in  the slave  trade. He
made a fortune  in violating what is today regarded  as a basic human
right: not to be enslaved. Many in Locke’s day claimed that slavery
was a “Natural Law.” Few would say so now.</p>

<p>Thomas  Jefferson  indicates  exactly  why  “Natural  Law”  is
authoritarian when  he wrote <em>“[s]ome men  look at constitutions
with  sanctimonious reverence,  and deem  them  like the  ark of  the
Covenant, too  sacred to be touched.  They ascribe to the  men of the
preceding age a wisdom more than  human, and suppose what they did to
be beyond amendment...laws and institutions must go hand in hand with
the progress  of the  human mind... as  that becomes  more developed,
more  enlightened, as  new  discoveries are  made, institutions  must
advance also, to keep pace with the times... We might as well require
a man to wear still the coat which fitted him when a boy as civilised
society  to  remain forever  under  the  regimen of  their  barbarous
ancestors.”</em></p>

<p>The  “Natural  Law”  cult  desires to  stop  the  evolutionary
process by which new rights are  recognised. Instead they wish to fix
social life into what <strong>they</strong>  think is good and right,
using a  form of argument  that tries  to raise their  ideology above
critique  or thought.  Such  a  wish is  opposed  to the  fundamental
feature of liberty: the ability to think for oneself. Michael Bakunin
writes <em>“the  liberty of  man consists solely  in this:  that he
obeys natural laws because he has <strong>himself</strong> recognised
them as such, and not because  they have been externally imposed upon
him  by any  extrinsic  will whatever,  divine  or human,  collective
or  individual.”</em>  [<strong>Bakunin on  Anarchism</strong>,  p.
227]</p>

<p>Thus  anarchism,  in  contrast  to  the  “natural  law”  cult,
recognises that “natural laws” (like  society) are the product of
individual evaluation of reality and  social life and are, therefore,
subject to change in the light  of new information and ideas (Society
<em>“progresses  slowly  through  the moving  power  of  individual
initiative”</em>  [Bakunin,  <strong>The  Political  Philosophy  of
Bakunin</strong>, p.  166] and  so, obviously,  do social  rights and
customs). Ethical or  moral “laws” (which is  what the “Natural
Law” cult is actually about) is not a product of “human nature”
or  abstract individuals.  Rather,  it  is a  <strong>social</strong>
fact, a  creation of  society and  human interaction.  In Bakunin’s
words, <em>“moral  law is not  an individual  but a social  fact, a
creation  of  society”</em>  and  any  <em>“natural  laws”</em>
are  <em>“inherent in  the  social body”</em>  (and  so, we  must
add,  not floating  abstractions existing  in “man’s  nature”).
[<strong>Ibid.</strong>, p. 125, p. 166]</p>

<p>The case for  liberty and a free society is  based on the argument
that,  since  every individual  is  unique,  everyone can  contribute
something that  no one else has  noticed or thought about.  It is the
free interaction of individuals which allows them, along with society
and its customs and rights, to evolve, change and develop. “Natural
Law,” like the  state, tries to arrest this  evolution. It replaces
creative inquiry  with dogma,  making people  subject to  yet another
god, destroying critical thought with a new rule book.</p>

<p>In  addition,   if  these  “Natural  Laws”   are  really  what
they  are  claimed   to  be,  they  are   necessarily  applicable  to
<strong>all</strong>  of humanity  (Rothbard explicitly  acknowledges
this  when  he  wrote  that  <em>“one  of  the  notable  attributes
of  natural  law”</em>  is  <em>“its applicability  to  all  men,
regardless   of  time   or  place”</em>   [<strong>The  Ethics   of
Liberty</strong>,  p. 42]).  In  other words,  every  other law  code
<strong>must</strong>  (by definition)  be  “against nature”  and
there  exists <strong>one</strong>  way  of  life (the  “natural”
one). The authoritarian implications of such arrogance is clear. That
the Dogma of  Natural Law was only invented a  few hundred years ago,
in one part of the planet, does not seem to bother its advocates. Nor
does the fact  that for the vast majority of  human existence, people
have lived in societies which violated almost <strong>all</strong> of
their so-called  “Natural Laws” To  take one example,  before the
late Neolithic, most societies were based on usufruct, or free access
to communally  held land  and other  resources [see  Murray Bookchin,
<strong>The  Ecology of  Freedom</strong>]. Thus  for millennia,  all
human beings  lived in  violation of  the supposed  “Natural Law”
of  private  property   —  perhaps  the  chief   “law”  in  the
“Libertarian” universe.</p>

<p>If  “Natural  Law”  did  exist, then  all  people  would  have
discovered these “true” laws years ago. To the contrary, however,
the  debate  is still  going  on,  with  (for example)  fascists  and
“Libertarians”  each  claiming  “the  laws  of  nature”  (and
socio-biology) as their own.</p>

<h4 id="toc67">11.4  Does   “Natural  Law”   actually  provides protection for individual liberty?</h4>

<p>But, it seems fair to ask, does “natural law” actually respect
individuals and their rights (i.e. liberty)? We think not. Why?</p>

<p>According to  Rothbard, <em>“the  natural law ethic  states that
for man,  goodness or badness  can be  determined by what  fulfils or
thwarts what is best  for man’s nature.”</em> [<strong>The Ethics
of Liberty</strong>,  p. 10] But,  of course, what may  be “good”
for  “man” may  be  decidedly <strong>bad</strong>  for men  (and
women).  If  we take  the  example  of the  sole  oasis  in a  desert
(see section  4.2) then,  according to  Rothbard, the  property owner
having  the  power  of  life  and death  over  others  is  “good”
while,  if  the  dispossessed  revolt and  refuse  to  recognise  his
“property”,  this  is  “bad”! In  other  words,  Rothbard’s
“natural law”  is good  for <strong>some</strong>  people (namely
property owners) while  it can be bad for others  (namely the working
class).  In  more general  terms,  this  means  that a  system  which
results in  extensive hierarchy (i.e.  <strong>archy</strong>, power)
is “good” (even  though it restricts liberty for  the many) while
attempts to <strong>remove</strong> power (such as revolution and the
democratisation of  property rights)  is “bad”.  Somewhat strange
logic, we feel.</p>

<p>However such  a position fails to  understand <strong>why</strong>
we consider coercion to be wrong/unethical. Coercion is wrong because
it subjects  an individual to the  will of another. It  is clear that
the victim  of coercion is  lacking the freedom that  the philosopher
Isaiah Berlin describes in the following terms:</p>

<blockquote class="citation">
“I wish  my life and decisions  to depend on myself,  not on
external forces  of whatever kind. I  wish to be an  instrument of my
own, not of other men’s, acts of  will. I wish to be a subject, not
an object; to  be moved by reasons, by conscious  purposes, which are
my own, not  by causes which affect  me, as it were,  from outside. I
wish  to be  somebody, not  nobody; a  doer —  deciding, not  being
decided for, self-directed  and not acted upon by  external nature or
by  other mean  as  if I  were  a thing,  or an  animal,  or a  slave
incapable of playing  a human role, that is, of  conceiving goals and
policies of  my own and realising  them.”</em> [<strong>Four Essays
on Liberty</strong>, p. 131]
</blockquote>

<p>Or, as  Alan Haworth points  out, <em>“we have to  view coercion
as  a  violation  of   what  Berlin  calls  <strong>positive</strong>
freedom.”</em> [<strong>Anti-Libertarianism</strong>, p. 48]</p>

<p>Thus, if a  system results in the violation  of (positive) liberty
by its very nature — namely, subject  a class of people to the will
of another class (the worker is subject to the will of their boss and
is turned into  an order-taker) — then it is  justified to end that
system. Yes,  it is “coercion”  is dispossess the  property owner
— but  “coercion” exists  only for  as long  as they  desire to
exercise power over  others. In other words, it is  not domination to
remove domination! And  remember it is the domination  that exists in
coercion which  fuels our hatred  of it, thus “coercion”  to free
ourselves from  domination is a necessary  evil in order to  stop far
greater evils  occurring (as, for  example, in the clear-cut  case of
the oasis monopoliser).</p>

<p>Perhaps it will  be argued that domination is only  bad when it is
involuntary, which  means that it  is only the involuntary  nature of
coercion that makes  it bad, not the domination it  involves. By this
argument wage slavery is not  domination as workers voluntarily agree
to  work  for  a  capitalist  (after  all,  no  one  puts  a  gun  to
their heads)  and any attempt  to overthrow capitalist  domination is
coercion and so wrong. However,  this argument ignores that fact that
<strong>circumstances</strong>  force workers  to sell  their liberty
and  so  violence on  behalf  of  property  owners is  not  (usually)
required  — market  forces  ensure that  physical  force is  purely
“defensive”  in nature.  And as  we argued  in section  2.2, even
Rothbard recognised that the economic power associated with one class
of  people being  dispossessed  and another  empowered  by this  fact
results  in  relations  of  domination  which  cannot  be  considered
“voluntary”  by  any stretch  of  the  imagination (although,  of
course, Rothbard  refuses to see  the economic power  associated with
capitalism —  when its capitalism, he  cannot see the wood  for the
trees — and we are ignoring the fact that capitalism was created by
extensive use of coercion and violence — see section 8).</p>

<p>Thus,  “Natural  law”  and  attempts  to  protect  individuals
rights/liberty and  see a  world in  which people  are free  to shape
their own  lives are  fatally flawed  if they  do not  recognise that
private property  is incompatible with  these goals. This  is because
the  existence  of  capitalist  property smuggles  in  power  and  so
domination (the restriction  of liberty, the conversion  of some into
order-givers and the many into  order-takers) and so Natural Law does
not fulfil its  promise that each person is free  to pursue their own
goals. The unqualified right of  property will lead to the domination
and degradation of large numbers  of people (as the oasis monopoliser
so graphically illustrates).</p>

<p>And we stress that anarchists  have no desire to harm individuals,
only to  change institutions.  If a  workplace is  taken over  by its
workers, the owners are not harmed  physically. If the oasis is taken
from the monopoliser, the ex-monopoliser  becomes like other users of
the  oasis (although  probably <strong>disliked</strong>  by others).
Thus anarchists desire to treat people  as fairly as possible and not
replace  one  form  of  coercion  and  domination  with  another  —
individuals  must <strong>never</strong>  be treated  as abstractions
(if they  have power over you,  destroy what creates the  relation of
domination, <strong>not</strong> the individual,  in other words! And
if this power can be removed  without resorting to force, so much the
better — a point which social and individualist anarchists disagree
on,  namely whether  capitalism can  be  reformed away  or not  comes
directly from this.  As the Individualists think it  can, they oppose
the use  of force.  Most social  anarchists think  it cannot,  and so
support revolution).</p>

<p>This argument may be considered as “utilitarian” (the greatest
good for the greatest number) and  so treats people not as “ends in
themselves” but as “means to an end”. Thus, it could be argued,
“natural law” is required to ensure that <strong>all</strong> (as
opposed to  some, or many, or  the majority of) individuals  are free
and have their rights protected.</p>

<p>However, it is clear that “natural law” can easily result in a
minority  having  their  freedom  and  rights  respected,  while  the
majority  are forced  by  circumstances (created  by the  rights/laws
produced by  applying “natural law”  we must note) to  sell their
liberty  and rights  in order  to survive.  If it  is wrong  to treat
anyone  as a  “means to  an  end”, then  it is  equally wrong  to
support a theory or economic system  that results in people having to
negate themselves  in order  to live.  A respect  for persons  — to
treat them  as ends  and never  as means —  is not  compatible with
private property.</p>

<p>The simple fact is that <strong>there are no easy answers</strong>
— we  need to weight  up our  options and act  on what we  think is
best. Yes, such subjectivism  lacks the “elegance” and simplicity
of  “natural law”  but  it  reflects real  life  and freedom  far
better. All in  all, we must always remember that  what is “good”
for man  need not be good  for people. “Natural law”  fails to do
this and stands condemned.</p>

<h4 id="toc68">11.5   But   Natural   Law   was   discovered,   not invented!</h4>

<p>This statement truly shows the religious nature of the Natural Law
cult. To  see why its notion  of “discovery” is confused,  let us
consider the Law of Gravity. Newton did not “discover” the law of
gravity,  he  invented  a  theory which  explained  certain  observed
phenomena in  the physical  world. Later Einstein  updated Newton’s
theories in  ways that allowed  for a better explanation  of physical
reality.  Thus,  unlike “Natural  Law,”  scientific  laws can  be
updated and  changed as our knowledge  changes and grows. As  we have
already noted, however, “Natural  Laws” cannot be updated because
they are derived from fixed  definitions (Rothbard is pretty clear on
this, he  states that it  is <em>“[v]ery true”</em>  that natural
law  is  <em>“universal, fixed  and  immutable”</em>  and so  are
<em>“‘absolute’ principles of justice”</em> and that they are
<em>“independent of  time and place”</em> [<strong>The  Ethics of
Liberty</strong>, p.  19]). However, what  he fails to  understand is
that what  the “Natural  Law” cultists are  “discovering” are
simply  the implications  of  their own  definitions,  which in  turn
simply reflect their own prejudices and preferences.</p>

<p>Since “Natural  Laws” are  thus “unchanging” and  are said
to  have  been  “discovered”  centuries  ago,  it’s  no  wonder
that  many  of  its  followers look  for  support  in  socio-biology,
claiming  that their  “laws” are  part of  the genetic  structure
of  humanity. But  socio-biology has  dubious scientific  credentials
for  many of  its  claims. Also,  it  has authoritarian  implications
<strong>exactly</strong>  like Natural  Law. Murray  Bookchin rightly
characterises  socio-biology as  <em>“suffocatingly  rigid; it  not
only impedes  action with the  autocracy of  a genetic tyrant  but it
closes the  door to any action  that is not biochemically  defined by
its  own  configuration.  When  freedom  is  nothing  more  than  the
recognition of necessity...we discover  the gene’s tyranny over the
greater  totality of  life...when  knowledge becomes  dogma (and  few
movements are more dogmatic than socio-biology) freedom is ultimately
denied.”</em> [<em>“Socio-biology  or Social  Ecology”</em>, in
<strong>Which way for  the Ecology Movement?</strong> pp.  49 — 75,
p. 60]</p>

<p>In conclusion  the doctrine  of Natural  Law, far  from supporting
individual  freedom, is  one  of its  greatest  enemies. By  locating
individual  rights   within  “Man’s  Nature,”  it   becomes  an
unchanging set of dogmas. Do we  really know enough about humanity to
say what are “Natural” and universal Laws, applicable forever? Is
it not a  rejection of critical thinking and  thus individual freedom
to do so?</p>

<h4 id="toc69">11.6 Why is the notion of “discovery” contradictory?</h4>

<p>Ayn Rand indicates  the illogical and contradictory  nature of the
concepts of  “discovering” “natural  law” and  the “natural
rights”  this  “discovery”  argument creates  when  she  stated
that her  theory was <em>“objective.”</em>  Her “Objectivist”
political  theory  <em>“holds that  good  is  neither an  attribute
of  ‘things  in  themselves’  nor man’s  emotional  state,  but
<strong>an  evaluation</strong> of  the facts  of reality  by man’s
consciousness  according  to  a  rational standard  of  value...  The
objective  theory  holds  that  <strong>the  good  is  an  aspect  of
reality  in  relation  to  man</strong>  —  and  that  it  must  be
discovered,  not invented,  by man.”</em>  [<strong>Capitalism: The
Unknown Ideal</strong>, p. 22]</p>

<p>However,   this   is  playing   with   words.   If  something   is
“discovered” then it has always been there and so is an intrinsic
part  of it.  If “good”  <strong>is</strong> “discovered”  by
“man” then  “good” exists independently  of people —  it is
waiting  to  be  “discovered.”  In  other  words,  “good”  is
an  attribute  of  <em>“man  as man,”</em>  of  <em>“things  in
themselves”</em>  (in addition,  such  a theory  also implies  that
there is just <strong>one</strong> possible interpretation of what is
“good” for all humanity). This can  be seen when Rand talks about
her system of “objective” values and rights.</p>

<p>When discussing the  difference between <em>“subjective,”</em>
<em>“intrinsic”</em>  and  <em>“objective”</em>  values  Rand
noted  that  <em>“intrinsic”</em>  and  <em>“subjective”</em>
theories <em>“make  it possible for a  man to believe what  is good
is  independent of  man’s  mind  and can  be  achieved by  physical
force.”</em>  [<strong>Op. Cit.</strong>,  p. 22]  In other  words,
intrinsic  and  subjective  values   justify  tyranny.  However,  her
<em>“objective”</em> values are placed squarely in <em>“Man’s
Nature”</em>  — she  states that  <em>“[i]ndividual rights  are
the  means of  subordinating society  to moral  law”</em> and  that
<em>“the  source  of  man’s  rights  is  man’s  nature.”</em>
[<strong>Op. Cit.</strong>, p. 320, p. 322]</p>

<p>She argues that the <em>“<strong>intrinsic</strong> theory holds
that the  good is  inherent in  certain things  or actions,  as such,
regardless  of  their context  and  consequences,  regardless of  any
benefit  or  injury  they  may  cause  to  the  actors  and  subjects
involved.”</em> [<strong>Op. Cit.</strong>, p. 21] According to the
<strong>Concise Oxford  Dictionary</strong>, <em>“intrinsic”</em>
is  defined   as  <em>“inherent,”</em>  <em>“essential,”</em>
<em>“belonging naturally”</em>  and defines <em>“nature”</em>
as <em>“a  thing’s, or person’s, innate  or essential qualities
or  character.”</em>  In  other   words,  if,  as  Rand  maintains,
man’s  rights <strong>are</strong>  the  product of  <em>“man’s
nature”</em> then  such rights are  <strong>intrinsic</strong>! And
if,  as  Rand maintains,  such  rights  are the  <em>“extension  of
morality into the social system”</em>  then morality itself is also
intrinsic.</p>

<p>Again, her ideology fails to meet  its own tests — and opens the
way for  tyranny. This can be  seen by her whole  hearted support for
wage slavery and her total lack of concern how it, and concentrations
of wealth and  power, affect the individuals subjected  to them. For,
after  all,  what  is  “good” is  “inherent”  in  capitalism,
regardless of the context, consequences,  benefits or injuries it may
cause to the actors and subjects involved.</p>

<p>The key to understanding  her contradictory and illogical ideology
lies in  her contradictory use  of the word “man.”  Sometimes she
uses it  to describe individuals but  usually it is used  to describe
the  human race  collectively  (<em>“man’s nature,”  “man’s
consciousness”</em>). But “Man” does  not have a consciousness,
only individuals  do. Man  is an abstraction,  it is  individuals who
live  and  think, not  “Man.”  Such  “Man worship”  —  like
Natural Law — has all the markings of a religion.</p>

<p>As Max Stirner  argues <em>“liberalism is a  religion because it
separates my essence from me and  sets it above me, because it exalts
‘Man’ to the same extent as  any other religion does to God... it
sets me  beneath Man.”</em> [<strong>The Ego  and Its Own</strong>,
p. 176] Indeed, he <em>“who is infatuated with <strong>Man</strong>
leaves persons out of account so far as that infatuation extends, and
floats in  an ideal, sacred interest.  <strong>Man</strong>, you see,
is  not  a  person,  but  an  ideal,  a  spook.”</em>  [<strong>Op.
Cit.</strong>, p.79]</p>

<p>Rand  argues that  we must  evaluate <em>“the  facts of  reality
by  man’s  consciousness  according   to  a  rational  standard  of
value”</em>  but  who  determines   that  value?  She  states  that
<em>“[v]alues  are   not  determined   by  fiat  nor   by  majority
vote”</em> [p. 24] but, however,  neither can they be determined by
“man” or  “man’s consciousness” because “man”  does not
exist.  Individuals exist  and  have consciousness  and because  they
are  unique  have different  values  (but  as  we argued  in  section
A.2.19, being  social creatures  these values are  generalised across
individuals into social, i.e. objective, values). So, the abstraction
“man” does not exist and because of this we see the healthy sight
of  different  individuals  convincing  others  of  their  ideas  and
theories by  discussion, presenting  facts and rational  debate. This
can be best seen in scientific debate.</p>

<p>The  aim of  the  scientific  method is  to  invent theories  that
explain facts,  the theories are  not part  of the facts  but created
by  the  individual’s  mind  in   order  to  explain  those  facts.
Such  scientific  “laws”  can  and  do change  in  light  of  new
information and new thought. In other words, the scientific method is
the  creation  of  subjective  theories that  explain  the  objective
facts. Rand’s  method is  the opposite  — she  assumes “man’s
nature,” “discovers” what is  “good” from those assumptions
and  draws  her  theories  by   deduction  from  that.  This  is  the
<strong>exact</strong> opposite  of the scientific method  and, as we
noted above, comes to us straight from the Roman Catholic church.</p>

<p>It  is  the  subjective  revolt by  individuals  against  what  is
considered “objective”  fact or “common sense”  which creates
progress  and  develops ethics  (what  is  considered “good”  and
“right”) and society. This,  in turn, becomes “accepted fact”
until the next  free thinker comes along and changes  how we view the
world by presenting  <strong>new</strong> evidence, re-evaluating old
ideas and facts or exposing  the evil effects associated with certain
ideas (and the  social relationships they reflect)  by argument, fact
and passion. Attempts to impose  <em>“an evaluation of the facts of
reality by  man’s consciousness”</em>  would be  a death  blow to
this process of  critical thought, development and  evaluation of the
facts of reality by individual’s consciousness. Human thought would
be subsumed by dogma.</p>

    </body>
</html>
