<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet href="../datas/text.xsl" type="text/xsl"?>

<!-- TRADUCTION FINIE | RELECTURE -->

<document>
  <meta>
    <toc>
      <notable />
      <section>a</section>
      <subsection>0</subsection>
    </toc>
    <type>faqtext</type>
    <translation>complete</translation>
    <pagetitle>FAQ Anarchiste – A - Introduction</pagetitle>
    <head>
      <line>Section A – Qu’est-ce que l’anarchisme<e/>?</line>
      <line>Introduction</line>
    </head>
  </meta>
  <text>
    <div n="0">

<p class="first">La civilisation moderne fait  face à trois types de
crises, potentiellement catastrophiques<e/>:</p>

<ol>
    <li> Une panne sociale, une augmentation à court terme du taux de
    pauvreté, le phénomène des sans-abris, le crime, la violence,
    l’aliénation, l’alcoolisme et autres dépendances aux drogues,
    l’isolation sociale, la torpeur politique, la déshumanisation, la
    détérioration des structures d’aide, etc.</li>

    <li> La destruction du fragile écosystème planétaire, dont toute
    forme de vie complexe dépend,</li>

    <li>La prolifération d’armes de destruction massive, en
    particulier les armes nucléaires.</li>
</ol>

<p>L’opinion courante, comprenant les <cit>experts</cit>, les
médias de masse, et les politiciens, considère souvent ces crises
comme séparables, chacune ayant sa propre cause et pouvant donc
être traité sur une base fragmentaire, de façon isolée des deux
autres. Cependant, il est évident que cette approche conformiste ne
fonctionne pas, puisque les problèmes deviennent de plus en plus
graves. Si l’on n’adopte pas une meilleure approche dans de brefs
délais, nous courrons clairement à la catastrophe, comme une guerre
terrible, un cataclysme écologique, ou une récession vers une
sauvagerie urbaine — ou tout en même temps.</p>

<p>L’anarchisme offre une vision unifiée et cohérente de ces
crises, en les faisant dériver d’une source commune. Cette
source est le principe d’autorité hiérarchique, qui est à
la base de toutes les sociétés dites civilisées, aussi bien
capitalistes que <cit>communistes</cit>. L’analyse des anarchistes
part du fait que toutes nos institutions majeures sont sous forme
hiérarchique, c’est-à-dire que le pouvoir est concentré au
sommet d’une structure pyramidale, comme c’est le cas pour les
entreprises, la bureaucratie, l’armée, les partis politiques, les
organisations religieuses, les universités, etc. Cette analyse se
poursuit en montrant que les relations d’autorité inhérentes à
de telles structures hiérarchiques ont un impact négatif sur
les individus, la société, et la culture. Dans la première
partie de cette FAQ (sections <ref sec="a">A</ref> à 
<ref sec="e">E</ref>), nous présenterons cette analyse de
cette hiérarchie autoritaire et de ses effets négatifs plus en
détail.</p>

<p>Il ne faut pas cependant penser que l’anarchisme n’est
qu’une critique <cit>négative</cit> ou <cit>destructrice</cit> de
la civilisation moderne<e/>; il est plus que cela, il est aussi une
proposition de société libre. <idx v="yes" s="name" id="goldman"> Emma
Goldman</idx> exprima ce que l’on pourrait appeler <cit>la question
anarchiste</cit> en ces termes<e/>: <cit>Le problème auquel nous
sommes confrontés aujourd’hui <el/> c’est [de savoir] comment
être soi-même tout en étant uni aux autres, de se sentir
profondément rattaché à tous les êtres humains, tout en gardant
ses propres caractéristiques <fn s="normal" id="01">1</fn></cit>. En
d’autres termes, comment pouvons-nous créer une société dans
laquelle le potentiel de chaque individu s’exprime, sans que
cela soit aux dépens des autres? Afin de parvenir à ceci, les
anarchistes conçoivent une société dans laquelle, à la place
d’une prise de décision <cit>descendante</cit> à travers la
structure hiérarchique d’un pouvoir centralisé, les affaires
humaines seraient, pour citer <idx v="yes" s="name" id="tucker">Benjamin
Tucker</idx>, <cit>régies par des individus ou par des associations
volontaires<fn id="02">2</fn></cit>. Bien que la description des
propositions anarchistes pour une meilleure organisation de la
société, <cit>du bas vers le haut</cit>, seront décrites plus
tard dans la FAQ (sections <ref sec="i">I</ref> et 
<ref sec="j">J</ref>), une partie du noyau constructif de
l’anarchisme sera abordée dans des sections précédentes. Le
cœur positif de l’anarchisme pourra être vu dans la critique
anarchiste de quelques solutions imparfaites données à la question
sociale, comme le Marxisme et l’<cit>anarcho</cit>-capitalisme
(respectivement section <ref sec="f">F</ref> et 
<ref sec="h">H</ref>).</p>

<p>Comme <idx v="yes" s="name" id="harper">Clifford Harper</idx> l’a
élégamment introduit, <cit>[comme] toutes les grandes idées,
l’anarchisme est plutôt simple quand vous le réduisez à sa plus
simple expression — les êtres humains montrent le meilleur
d’eux-mêmes quand ils vivent libérés de toute autorité,
décidant des choses par eux-mêmes plutôt qu’en se les faisant
imposer<a class="iref" href="#inote_03" name="iref_03">3</a></cit>.
Par leur désir de libérer un maximum l’individu et par la
liberté sociale, les anarchistes souhaitent démanteler toutes les
institutions qui oppresse le peuple<e/>:</p>

<blockquote class="citation">
Le désir d’une société libéré de tout politiciens et
d’institutions socialement coercitives est commun à tous les
anarchistes et conduit au développement d’une humanité libre.
</blockquote>
<blockquote class="citation_author"><idx s="name" id="rocker">Rudolph Rocker</idx></blockquote>
<blockquote class="citation_source">Anarcho-Syndicalism<i>, p.9</i></blockquote>

<p>Comme nous le verrons, de telles institutions sont
hiérarchisées, et leur nature répressive provient directement de
leur forme hiérarchique.</p>

<p>L’anarchisme est une théorie socio-économique et politique,
mais pas une idéologie. La différence est très importante.
Pour faire simple, <em>théorie</em> signifie que vous avez des
idées;<e/> <em>idéologie</em> signifie que ce sont les idées qui
vous possèdent. L’anarchisme est un réceptacle pour des idées,
mais elles sont flexibles, constamment en évolution, et ouverte à
la modification à la lumière de nouvelles données. De même que la
société, l’anarchisme change et se développe. Une idéologie,
par contraste, est un ensemble d’idées fixées que les gens
croient de façon dogmatique, souvent en ignorant la réalité ou en
la <cit>changeant</cit> de manière à ce qu’elle colle avec
l’idéologie, qui est (par définition) exacte. Toutes ces idées
fixes sont sources de tyrannie et de contradiction, conduisant à
vouloir faire tenir tout le monde dans le lit de 
Procruste<fn s="explain" id="01" />. Ceci est vrai quelle
que soit l’idéologie en question — Léninisme,
<cit>anarcho</cit>-capitalisme<el/> — toutes ayant le même
effet<e/>: la destruction de l’individualité au nom d’une
doctrine, qui sert généralement les intérêts d’une
élite dirigeante. Ou, comme <idx v="yes" s="name" id="bakounine">Michel
Bakounine</idx> l’a écrit<e/>: <cit>Jusqu’à maintenant toute
l’histoire humaine n’a été qu’une immolation perpétuelle et
sanglante de millions de pauvres être humains en l’honneur
de quelques abstractions sans pitié — Dieu, la nation, la
puissance d’un état, la fierté nationale, les droits historiques,
les droits juridiques, le bien-être 
public<fn s="normal" id="04">4</fn></cit>.</p>

<p>Les dogmes sont figés et pareil à la mort par leur rigidité,
souvent le travail de quelque <cit>prophète</cit> mort, religieux ou
laïques, et dont les disciples érigent les idées en idole,
immuable comme la pierre. Les anarchistes veulent que les vivants
enterrent les morts, pour qu’ils puissent vivre pleinement. Les vivants
devraient contrôler les morts, et non l’inverse. Les idéologies
sont les ennemies de la pensée critique et, par conséquent, de la
liberté, en fournissant un libre de règles et de <cit>réponses</cit> qui
nous soulagent du <cit>fardeau</cit> de penser par nous-mêmes.</p>

<p>Il n’est nullement dans nos intentions, en mettant à
disposition cette FAQ sur l’anarchisme, de vous donner les <cit>bonnes</cit>
réponses, ou un nouveau livre de règle. Nous expliquerons un peu ce
qu’était l’anarchisme par le passé, mais nous nous concentrerons
plus sur ses formes modernes, et sur pourquoi nous sommes anarchistes
aujourd’hui. Avec cette FAQ, nous voulons vous inviter à penser et
à analyser par vous-même. Si vous êtes à la recherche d’une
nouvelle idéologie, alors désolé, mais l’anarchisme n’est pas pour
vous.</p>

<p>Tandis que les anarchistes essayent d’être réalistes et
pragmatiques, nous ne sommes pas des personnes <cit>raisonnables</cit>. Les
personnes <cit>raisonnables</cit> acceptent sans critique ce que les 
<cit>experts</cit>
et les <cit>autorités</cit> définissent comme étant la vérité, et restent
pour toujours des esclaves! Les anarchistes savent, tout comme 
<idx id="bakounine" s="name">Bakounine</idx> que<e/>: 
<cit>[une] personne est forte seulement quand
elle se forge sa propre vérité, quand elle parle et qu’elle agît
avec une profonde conviction. Alors, quelle que soit la situation dans
laquelle elle se retrouve, elle sait toujours ce qu’elle doit dire et
faire. Elle peut tomber, mais jamais avoir honte d’elle ou de sa
cause<fn id="05">5</fn></cit>.</p>

<p>Ce que <idx id="bakounine" s="name">Bakounine</idx> décrit, c’est la puissance d’une pensée
indépendante, la puissance de la liberté. Nous vous encourageons à
ne pas être <cit>raisonnable</cit>, et à ne pas accepter
ce que les autres vous disent, mais à penser et à agir par
vous-même!</p>

<p>Une dernière chose<e/>: cela parait évident, mais ce
n’est pas la seule description possible de l’anarchisme. Beaucoup
d’anarchistes seront en désaccord avec ce que nous avons écrit ici,
mais c’est ce à quoi il faut s’attendre quand les gens pensent par
eux-mêmes. Tout ce que nous souhaitons faire, c’est d’indiquer les
idées de bases de l’anarchisme, et de vous donner notre analyse
de certains sujets, basée sur la façon dont nous comprenons
et appliquons ces idées. Nous sommes persuadés, cependant, que
tout anarchiste sera d’accord avec le cœur des idées que nous
présentons, même s’il peut y avoir un désaccord sur la façon dont
nous les appliquons ici ou là.</p>

    </div>
  </text>
  <footnotes>
    <system name="normal">
      <fn id="01">
	<idx s="name" id="goldman">Emma Goldman</idx>, 
	<em>Red Emma Speaks</em>, p.158-159.
      </fn>
      <fn id="02">
	<idx s="name" id="tucker">Benjamin Tucker</idx>, 
	<em>Anarchist Reader</em>, p.149.
      </fn>
      <fn id="03">
	<idx s="name" id="harper">Clifford Harper</idx>, 
	<em>Anarchy<e/>: A Graphic Guide</em>, p.VII.
      </fn>
      <fn id="04">
	<idx s="name" id="bakounine">Bakounine</idx>, 
	<em>Dieu et l’État</em> [<em>God and the State</em>], p.59 
	<book author="bakounine" id="godandstate" />.
      </fn>
      <fn id="05">
	Cité par <idx s="name" id="meltzer">Albert Meltzer</idx>, 
	<em>I couldn’t Paint Golden Angels, p.2</em>.
      </fn>
    </system>
    <system name="explain">
      <fn id="01">
	L’expression <cit>lit de Proc[r]uste</cit> désigne <cit>toute 
	tentative de réduire les hommes à un seul modèle, une seule 
	façon de penser ou d’agir</cit> 
	(<a href="https://fr.wikipedia.org/wiki/Procuste">Wikipédia</a>, 
	08/12/2017).<br />

	Le personnage de la mythologie grecque <cit>Procruste contraignait 
	les voyageurs de se jeter sur un lit<e/>; il leur coupait les membres 
	trop grands et qui dépassaient le lit, et étirait les pieds de 
	ceux qui étaient trop petits</cit> 
	(Diodore de Sicile, <em>Bibliothèque historique</em>, IV 59.5 
	<book href="http://remacle.org/bloodwolf/historiens/diodore/livre4b.htm" />).<br />

	Il est <cit>devenu le symbole du conformisme et de 
	l’uniformisation</cit> (Wikipédia, <em>ibid</em>).
      </fn>
    </system>
  </footnotes>
</document>
<!-- vim:set spelllang=fr: -->
